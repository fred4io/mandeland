// //https://www.npmjs.com/package/canvas 

// declare var Canvas: {
//     new(width: number, height: number): Canvas;
//     Image: { new(width?: number, height?: number): NodeImage; }
// }
// interface Canvas extends HTMLCanvasElement {
//     toDataURL(contentType?: string, onDone?:(err: any, dataUrl: string) => void): string;
//     toBuffer(encoding?: string, onDone?:(err: any, buf: Buffer) => void): Buffer;
//     createPNGStream(): NodeJS.ReadableStream;
//     stride: number;
// }
// interface NodeImage extends HTMLImageElement {
//     source: string | Buffer;
//     inspect(): string;
// }

// declare module 'canvas' {
    
//     export = Canvas;

// }

