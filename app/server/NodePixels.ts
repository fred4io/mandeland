// /// <reference path="./canvas-class.d.ts" />
// import Canvas = require('canvas')

// declare type Color = { r: number, g: number, b: number, a: number }

// export class NodePixels {

//     static withBufferPixels(canvas: Canvas, getColor: (x: number, y: number) => Color) {
//         const buf = canvas.toBuffer('raw')

//         const w = canvas.width, h = canvas.height
//         let i = 0
//         for (let y = 0; y < h; ++y) {
//             for (let x = 0; x < w; ++x) {
//                 const c = getColor(x, y) 
//                 buf[i] = c.r
//                 buf[i + 1] = c.g
//                 buf[i + 2] = c.b
//                 buf[i + 3] = c.a
//                 i += 4
//             }
//         }
//         console.log('buf', buf)
//         const ctx = canvas.getContext('2d')
//         NodePixels.drawBuffer(ctx, buf)
//     }

//     static createBuffer(width: number, height: number, encoding: string = 'raw'): Buffer {
//         return new Canvas(width, height).toBuffer(encoding)
//     }

//     static drawBuffer(ctx: CanvasRenderingContext2D, buf: Buffer) {
//         const w = ctx.canvas.width, h = ctx.canvas.height
//         const img = new Canvas.Image(w, h)
//         img.source = buf
//         console.log('inspect', img.inspect())
//         ctx.drawImage(img, 0, 0, w, h)
//     }

// }