
// // needed in package.json: 
// // "canvas-prebuilt": "^1.6.5-prerelease.1",
// // "express": "^4.16.2",

// import * as express from 'express'
// import { Express, Request, Response, NextFunction } from 'express'
// import * as moment from 'moment/moment'

// /// <reference path="./canvas-class.d.ts" />
// import Canvas = require('canvas')

// import { NodePixels } from './NodePixels'

// import { Color } from '../shared/Color'
// import { Pixels } from '../shared/Pixels'

// export class FractServer {

//     private exp: Express
//     constructor(private port: number = 3000) {
//         this.exp = express()
//     }
//     public start() {

//         const app = this.exp

//         const appDir = __dirname + '/..'
//         const clientBaseDir = appDir + '/client'

//         app.use(express.static(clientBaseDir + '/www'))

//         const options = { extensions: ['js', 'js.map'], dotfiles: 'ignore', index: false }
//         app.use('/app', express.static(clientBaseDir, options))
//         app.use('/shared', express.static(appDir +'/shared', options))
//         app.use('/test', express.static(appDir +'/test', options))
//         app.use('/js/app', express.static(appDir +'/shared', options))
//         //app.use('/js/app/node_modules', express.static(appDir+ '/../node_modules', options))

//         app.use((err: any, req: Request, res: Response, next: NextFunction) => {
//             console.error('WebApp error', err, req, res)
//             next(err)
//         })
//         app.use((req: Request, res: Response, next: NextFunction) => {
//             console.log(`WebApp ${req.method} ${req.path}`)
//             next()
//         })

//         app.get('/api/:action?', (req: Request, res: Response) => {

//             switch (req.params.action) {

//                 case 'time':
//                     this.actTime(res)
//                     break

//                 case 'pngUrl':
//                     this.actImgPngDataUri(res)
//                     break

//                 case 'png':
//                     this.actImgPng(res)
//                     break

//                 default:
//                     this.actNope(res)
//                     break
//             }
//         })

//         app.listen(this.port, () => {
//             console.log(`WebApp listening on port ${this.port}`)
//         })
//     }

//     private actNope(res: Response) {
//         res.send('?')
//     }
//     private actTime(res: Response) {
//         const now = moment().format('HH:mm:ss.SSS')
//         res.send(now)
//     }
//     private actImgPngDataUri(res: Response) {
//         const canvas = this.makeCanvas()
//         const dataUrl = canvas.toDataURL()
//         res.send(dataUrl)
//         console.log('WebApp: image sent', dataUrl)
//     }
//     private actImgPng(res: Response) {
//         const canvas = this.makeCanvas()
//         const stream = canvas.createPNGStream()
//         res.type("png")
//         stream.pipe(res)
//     }

//     private makeCanvas(w: number = 320, h: number = 240): Canvas {

//         //w = h = 10
//         const canvas = new Canvas(w, h)

//         Pixels.drawCircle(canvas.getContext('2d'))

//         // const c = new Color(255)
//         // NodePixels.withBufferPixels(canvas, (x, y) => {
//         //     //console.log(x, y, c)
//         //     return c
//         // })

//         return canvas
//     }

// }

