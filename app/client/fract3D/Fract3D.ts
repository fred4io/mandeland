import { FractBase, ILogger } from '../../shared/fract/FractBase'
import { Fract3DKeys } from './Fract3DKeys'
import { DomHelper } from '../../shared/DomHelper'
import { ThreeViewer } from '../../shared/three/ThreeViewer'
import { Scene, Mesh, VertexNormalsHelper, Object3D, } from 'three'
import { HeightMap } from '../../shared/HeightMap';
import { Easing } from '../../shared/Easing';
import { Fract2D } from '../fract2D/Fract2D';
import { RenderParams3D, IRenderParams3D } from './RenderParams3D';
import { MeshBuilder } from '../../shared/three/MeshBuilder';
import { ThreeUtil } from '../../shared/three/ThreeUtil';
import { HMapMeshData } from '../../shared/HMapMeshData';
import { MouseEvents } from '../../shared/MouseEvents';
import { FaceHelper } from '../../shared/three/FaceHelper';

export declare type TDataObj = HTMLCanvasElement | HeightMap | HMapMeshData

enum Tools { Doc, Switch, Info, FullScreen }

export class Fract3D extends FractBase {

    protected _allDivs = ['Doc']
    private threeViewport: HTMLDivElement
    private threeViewer: ThreeViewer
    private scene: Scene
    private object: Object3D
    private hmap: HeightMap
    private dataObj: TDataObj
    private params: RenderParams3D
    private mouseEvents: MouseEvents
    private faceHelper: FaceHelper
    private trackedFaceShown = false

    constructor(private fract2D: Fract2D) {
        super(fract2D)
        this.threeViewport =
            DomHelper.div(
                undefined,
                {
                    height: window.innerHeight + 'px',
                    width: '100%', opacity: 0
                })

        this.keys = new Fract3DKeys(true, (name, shift) => this.tool(name, shift), this.log, this.isDev)

        this.mouseEvents = new MouseEvents(
            this.threeViewport,
            (x, y) => this.onClick(x, y),
            (x, y, e) => this.onDblClick(x, y, e),
            (x, y, e) => { this.onMove(x, y, e) },
        )
    }

    protected _attach() {
        if (this.threeViewer) {
            this.threeViewer.attach()
            const c = this.threeViewport.querySelector('canvas')
            this.threeViewport.style.height = c.style.height = c.height + 'px'
        }
        else this.threeViewer = new ThreeViewer(
            this.threeViewport,
            scene => this.scene = scene,
            undefined,
            { statElement: DomHelper.div(undefined, DomHelper.topLeft()) },
            (n, v) => this.viewerParamChanged(n, v)
        )
        this.keys.attach()
        this.mouseEvents.attach()
    }
    protected _attached() {
        this.threeViewer.attached()
        this.updateFrom2D()
    }
    protected _detach() {
        this.mouseEvents.detach()
        this.keys.detach()
        this.threeViewer.detach()
    }
    protected _detached() {
        this.threeViewer.detached()
        this.threeViewport.style.height = '0'
    }
    protected _loggers(): ILogger[] {
        return [this.keys]
    }
    protected _moreElements(): HTMLElement[] {
        return [this.threeViewport as HTMLElement]
            .concat(this.threeViewer.doms)
    }

    protected _toolname(tool: number) { return Tools[tool] }

    protected _getInfo() {
        let s = ''
        if (this.dataObj instanceof HMapMeshData)
            s = 'vertices:' + this.dataObj.vertices.length / 3
                + ' triangles:' + this.dataObj.faces.reduce((p, c) => p + c.length / 3, 0)
                + ' '
        else if (this.dataObj instanceof HeightMap) {
            this.say('points', this.params.points)
            const w = this.dataObj.dataWidth, h = this.dataObj.data.length / w
            const lw = Math.round(w / this.params.lod), lh = Math.round(h / this.params.lod)
            if (this.params.points) s = 'points:' + lw * lh
            else s = 'vertices:' + (lw + 1) * (lh + 1) + ' triangles:' + lw * lh * 2
            s += ' '
        }
        return s + this.params.toString()
    }

    protected drawPath(x: number, y: number) {
        if (!this._pathData) { this._pathData = new Array<number>() }
        //const path = this.psr.computePathFromView(x, y, this.state.m, this.state.r, this._pathData)
    }
    private updateFrom2D() {
        if (!this.params) this.params = new RenderParams3D({ camera: { position: { y: 700 } } })
        this.say('updateFrom2D')
        const par = this.fract2D.getParams(), info = { min: 0 }
        this.hmap = this.fract2D.getHeightMap(false, info)
        const h = Math.min(par.m * (1 - info.min), 1000)

        //this.say('info', info)

        this.setup(this.hmap, {
            colors: par.color,
            invert: par.invert,
            easing: par.easing,
            maxHeight: h,
            camera: { target: { y: h / 2 } }
            //camera: { position: { x: -28, y: 106, z: -211 }, target: { x: -28, y: -13, z: -251 } }
            //camera: { position: { x: -51, y: 18, z: -228 }, target: { x: -51, y: -8, z: -223 } }
        }, true)
    }

    private tool(name: string, shift: boolean) {
        switch (name) {
            case 'stats': this.threeViewer.nextStat(); return
            case 'grid': case 'axis': case 'crosshair': case 'lights':
            case 'transparent':
            case 'fnh':
            case 'wireframe':
            case 'negateY':
            case 'colors':
            case 'invert':
            case 'mesh.optimize':
            case 'mesh.zones':
            case 'mesh.noiseZones':
            case 'mesh.seams':
            case 'points': this.toggleParam(name); return
            case 'material': this.nextParam(name, shift, undefined, 2); return
            case 'easing': this.nextParam(name, shift, Easing.nextEaser); return
            case 'lod': this.nextParam(name, !shift, this.nextLod); return
            case 'colorMode': this.nextColorMode(); return
            case 'easeHeight': this.nextParam(name, shift, Easing.nextEaser, Easing.maxFuncIndex); return
            case 'faceTracking':
                let tracking: boolean, tracked = false
                if (shift) {
                    tracked = this.toggleParam('trackedFace', null, true)
                    if (tracked) tracking = this.toggleParam('faceTracking', true, true)
                } else tracking = this.toggleParam(name, null, true);
                this.setupFaceTracking(tracking, tracked)
                this.updateInfo()
                return
        }
        if (super.basetool(name, shift)) return
        const v = this.tools.get(name)
        switch (name) {
            case Tools[Tools.FullScreen]:
                this.setTool(Tools.FullScreen, this.threeViewer.toggleFullScreen())
                return
            default:
                break
        }
    }

    private nextParam(name: string, reverse = false, getValue?: (v: number, reverse?: boolean) => number, max = Infinity) {
        if (!getValue) getValue = v => (v += (reverse ? -1 : 1)) > (max || Infinity) ? 0 : v < 0 ? (max || 0) : v
        const v = this.getParam<number>(name)
        this.setParam(name, getValue(v, reverse))
        this.setup(this.dataObj, this.params, name == 'lod')
    }

    private nextColorMode() {
        let vc = this.getParam<boolean>('vertexColors'),
            fc = this.getParam<boolean>('faceColors')

        if (!vc && !fc) vc = !(fc = false)
        else if (vc) vc = !(fc = true)
        else vc = fc = false

        this.setParam('vertexColors', vc)
        this.setParam('faceColors', fc)
        //this.setMatProp('vertexColors', this.params.getVertexColors())
        this.setup(this.dataObj, this.params)
    }

    private toggleParam(name: string, force: boolean = undefined, noSetup = false) {
        const v = force != undefined ? force : !this.getParam<boolean>(name)
        this.setParam(name, v)
        switch (name) {
            case 'grid': case 'axis': case 'crosshair': case 'lights':
                this.threeViewer.toggleHelper(name, v)
                return
            case 'transparent':
                this.setMatProp(name, v)
                return
            case 'wireframe':
                if (this.faceHelper) this.faceHelper.wireFrame = !v
                if (!this.getParam<boolean>('points')) {
                    this.setMatProp(name, v)
                    return
                }
                this.setParam('points', false)
                break
            case 'negateY':
                const { target: { y } } = this.threeViewer.getCamera()
                this.threeViewer.setCamera(undefined, { y: -y })
                break
        }
        if (!noSetup) this.setup(this.dataObj, this.params)
        return this.getParam<boolean>(name)
    }
    private getParam<T>(name: string) {
        if (name.indexOf('.') > -1) {
            let res: T, o = <any>this.params
            name.split('.').forEach((n, i, a) => {
                if (!o) return
                if (i == a.length - 1) res = o[n]
                else o = o[n]
            })
            return res
        }
        else return (<any>this.params)[name] as T
    }
    private setParam<T>(name: string, value: T) {
        if (name.indexOf('.') > -1) {
            let o = <any>this.params
            name.split('.').forEach((n, i, a) => {
                if (!o) return
                if (i == a.length - 1) o[n] = value
                else o = o[n]
            })
        }
        else (<any>this.params)[name] = value
        this.say(name + ':', value)
        return value
    }
    private setMatProp(name: string, value: any, traverse = true) {
        function setProp(o: Mesh) { if (o.material) (o.material as any)[name] = value }
        if (traverse) this.object.traverse(setProp)
        else setProp(this.object as Mesh)
        this.threeViewer.requestRender()
        this.updateInfo()
    }

    private nextLod(lod: number, reverse: boolean) {
        let lods = [1, 2, 4, 8, 16, 32]; if (reverse) lods = lods.reverse()
        return lods.find(l => reverse ? l < lod : l > lod) || lods[0]
    }

    private setup(dataObject: TDataObj, params?: IRenderParams3D, isMeshDirty = false) {
        if (!dataObject) return
        this.say('----------\nsetup')
        this.dbg(dataObject, params)

        this.dataObj = dataObject
        if (this.params) this.params.setFrom(params)
        else this.params = new RenderParams3D(params)
        params = this.params

        const start = performance.now()
        let obj3d: Object3D
        if (this.dataObj instanceof HTMLCanvasElement)
            obj3d = ThreeUtil.makeTerrainFromCanvas(this.dataObj)
        else if (params.points)
            obj3d = MeshBuilder.makePoints(this.dataObj = this.hmap, params, this.log, this.debug)
        else {
            const p = this.params, pm = p.mesh
            if (pm && pm.optimize) {
                let meshHeight = p.negateY ? -p.maxHeight : p.maxHeight
                const easeHeight = Easing.func(this.params.easeHeight)
                const heightFactor = (t: number) => meshHeight * easeHeight(t)
                const updatable = true, merge = true
                if (isMeshDirty || !(this.dataObj instanceof HMapMeshData)) {
                    this.dataObj = HMapMeshData.fromHeightMap(this.hmap,
                        heightFactor, p.lod, p.minZoneArea, this.log,
                        pm.zones, pm.noiseZones, pm.seams, updatable, merge)
                }
                else (this.dataObj as HMapMeshData)
                    .update(heightFactor, pm.zones, pm.noiseZones, pm.seams, merge, this.log)
            } else this.dataObj = this.hmap
            obj3d = MeshBuilder.makeMesh(this.dataObj, p, this.log, this.debug)
        }

        this.say('object built')
        this.dbg(this.dataObj, obj3d)

        if (obj3d) {
            if (this.object) {
                this.scene.remove(this.object)
                this.threeViewer.disposeObj(this.object)
            }
            if (params.fnh && !params.points)
                obj3d.add(new VertexNormalsHelper(obj3d, 5, 0XFFFF00))

            this.scene.add(this.object = obj3d)
        }

        if (params.grid != undefined) this.threeViewer.toggleHelper('grid', params.grid)
        if (params.background) this.threeViewer.setBackground(params.background)
        if (params.camera) this.threeViewer.setCamera(params.camera.position, params.camera.target)

        params.camera = undefined

        this.say((performance.now() - start).toFixed(3) + 'ms\n----------')

        this.resetFaceHelper()
        this.updateInfo()
    }


    private onClick(x: number, y: number) {

    }
    private onDblClick(x: number, y: number, e: MouseEvent) {
        const p = this.threeViewer.getPointedPoint(e, this.object)
        if (p) this.threeViewer.setCamera(undefined, p)
    }
    private onMove(x: number, y: number, e: MouseEvent) {
        if (this.params.faceTracking) this.trackFace(e)
    }

    private trackFace(e: MouseEvent) {
        if (!this.faceHelper) return
        if (this.dataObj instanceof HMapMeshData) {
            const hmd = this.dataObj as HMapMeshData
            const update = (obj: Object3D) => {
                const face = this.threeViewer.getPointedFace(e, obj)
                if (face) this.faceHelper.update(face)
                this.showHideTrackedFace(!!face)
                if (face) this.dbg(face)
                return !!face
            }
            if (hmd.merged) update(this.object)
            else hmd.names.some(n => update(this.object.getObjectByName(n)))
        }
    }

    private setupFaceTracking(tracking = false, tracked = false) {
        this.showHideTrackedFace(tracking && tracked)
        if (!tracking) this.disposeFaceHelper()
        else if (!this.faceHelper) this.resetFaceHelper()
    }

    private disposeFaceHelper() {
        if (!this.faceHelper) return
        this.faceHelper.dispose()
        this.faceHelper = null
    }

    private resetFaceHelper() {
        this.showHideTrackedFace(false)
        if (this.dataObj instanceof HMapMeshData) {
            const vertices = (this.dataObj as HMapMeshData).vertices
            if (this.faceHelper) this.faceHelper.reset(vertices)
            else this.faceHelper = new FaceHelper(vertices)
        }
    }

    private showHideTrackedFace(show: boolean) {
        const fh = this.faceHelper; if (!fh || !fh.hasObjects) return
        if (show) {
            if (!this.trackedFaceShown)
                this.scene.add(fh.mesh, fh.centroHelper)
            this.trackedFaceShown = true
        } else {
            if (this.trackedFaceShown)
                this.scene.remove(fh.mesh, fh.centroHelper)
            this.trackedFaceShown = false
        }
        this.threeViewer.requestRender()
    }

    private viewerParamChanged(name: string, value: any) {
        switch (name) {
            case 'background':
                this.params.background = value
                break
        }
        this.dbg('viewerParamChanged', name, value)
    }

}
