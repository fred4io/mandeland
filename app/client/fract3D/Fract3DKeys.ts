import { KeyCommands } from "../../shared/KeyCommands";

export class Fract3DKeys extends KeyCommands {
    constructor(
        inBrowser: boolean,
        tool: (name: string, shift?: boolean) => void,
        log = true, activateSpecials = false
    ) {
        super([
            { key: 'I', action: () => tool('Info'), doc: "info panel" },
            { key: 'R', action: () => tool('colorMode'), doc: "color mode" },
            { key: 'C', action: () => tool('colors'), doc: "colors/gray gradient" },
            { key: 'E', action: (shift) => tool('easing', shift), doc: "gradient easing type" },
            { key: 'V', action: () => tool('invert'), doc: "invert colors" },
            { key: 'T', action: () => tool('transparent'), doc: "transparency" },
            { key: 'W', action: () => tool('wireframe'), doc: "wireframe" },
            { key: 'M', action: () => tool('material'), doc: "material" },
            { key: 'N', action: () => tool('negateY'), doc: "invert height" },
            { key: 'Z', action: (shift) => tool('easeHeight', shift), doc: "ease height" },
            { key: 'L', action: (shift) => tool('lod', shift), doc: "level of detail" },
            { key: 'P', action: () => tool('points'), doc: "points/mesh" },
            { key: 'alt-N', action: () => tool('fnh'), doc: "face normals helper" },
            { key: 'alt-G', action: () => tool('grid'), doc: "grid" },
            { key: 'alt-A', action: () => tool('axis'), doc: "axis" },
            { key: 'alt-C', action: () => tool('crosshair'), doc: "crosshair" },
            { key: 'alt-L', action: () => tool('lights'), doc: "lights helpers" },
            { key: 'alt-S', action: () => tool('stats'), doc: "stats" },
            { key: 'K', action: (shift) => tool('faceTracking', shift), doc: "face tracking" },
            { key: 'F', action: () => tool('FullScreen'), doc: "fullscreen" },
            { key: 'Y', action: () => tool('Switch'), doc: "switch to 2D" },
            { key: 'H', action: () => tool('Doc'), doc: "commands list" },
            { key: 'ctrl-O', action: () => tool('mesh.optimize'), doc: "mesh-optimize" },
            { key: 'ctrl-Z', action: () => tool('mesh.zones'), doc: "mesh-zones" },
            { key: 'ctrl-N', action: () => tool('mesh.noiseZones'), doc: "mesh-noiseZones" },
            { key: 'ctrl-S', action: () => tool('mesh.seams'), doc: "mesh-seams" },
        ],
            inBrowser, undefined, undefined, log, activateSpecials)
    }
}