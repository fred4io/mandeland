import { Color, VertexColors, FaceColors, NoColors } from "three";
import { TVectorParam } from "../../shared/three/ThreeViewer";
import { Easing } from "../../shared/Easing";

export declare type ThreeColor = Color

export declare interface IRenderParams3D {

    mesh?: {
        optimize: boolean,
        zones: boolean,
        noiseZones: boolean,
        seams: boolean
    }

    colors?: boolean
    invert?: boolean
    easing?: number
    maxHeight?: number
    minZoneArea?: number

    lod?: number
    negateY?: boolean
    easeHeight?: number

    points?: boolean
    pointSize?: number
    pointSizeAttenuation?: boolean,

    wireframe?: boolean,
    depthTest?: boolean,
    opacity?: number,
    transparent?: boolean,

    vertexColors?: boolean
    faceColors?: boolean

    fnh?: boolean

    background?: ThreeColor
    grid?: boolean,
    camera?: { position?: TVectorParam, target?: TVectorParam }

    faceTracking?: boolean
    trackedFace?: boolean
}

class RenderParams3DMesh {
    optimize = true
    zones = true
    seams = true
    noiseZones = true

    toString(prefix = '') {
        return Object.keys(this).map(k => prefix + k + ':' + !!(<any>this)[k]).join(' ')
    }
}

export class RenderParams3D implements IRenderParams3D {

    mesh = new RenderParams3DMesh()

    colors = true
    invert = false
    easing = 0
    maxHeight = 300
    minZoneArea = 10

    lod = 4
    negateY = true
    easeHeight = 0

    points = false
    pointSize = 1
    pointSizeAttenuation = true

    wireframe = false
    depthTest = true
    opacity = 0.25
    transparent = false

    vertexColors = true
    faceColors = false

    fnh = false

    background = new Color(0, 0, 0)
    grid = false
    camera: { position?: TVectorParam, target?: TVectorParam }

    faceTracking = false
    trackedFace = false

    constructor(par: IRenderParams3D) {
        this.setFrom(par)
    }
    
    setFrom(par: IRenderParams3D) {
        if (!par) return this
        Object.keys(this).forEach(k => {
            const v = (<any>par)[k]
            if (v != undefined) (<any>this)[k] = v
        })
        if (par.mesh) this.mesh = par.mesh
        if (par.camera) this.camera = par.camera
        if (this.negateY && this.camera && this.camera.target) this.camera.target.y = -this.camera.target.y
        return this
    }

    getVertexColors() {
        return RenderParams3D.getVertexColors(this)
    }

    toString() {
        return Object.keys(this).map(k => this.keyToString(k)).filter(s => s).join(' ')
    }
    keyToString(name: string) {
        switch(name) {
            case 'camera': return null
            case 'mesh': return !this.points && this.mesh.toString('mesh-')
            default: return name +':' + this.valToString(name)
        }
    }
    valToString(name: string) {
        const v = (<any>this)[name]
        switch (name) {
            case 'background': return '#' + (v as Color).getHexString()
            case 'easing': case 'easeHeight': return Easing.funcName(v)
            case 'lod': return '1/' + v
        }
        return v
    }

    static getVertexColors(par: IRenderParams3D) {
        return par.vertexColors ? VertexColors : par.faceColors ? FaceColors : NoColors
    }

}
