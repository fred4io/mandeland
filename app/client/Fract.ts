import { Fract2D } from './fract2D/Fract2D'
import { Fract3D } from './fract3D/Fract3D'
(() => {
    console.log('v20190616.4 - starting')
    const f2: Fract2D = new Fract2D(null, () => new Fract3D(f2))
    console.log('started')
})()
