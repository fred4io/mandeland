import * as screenfull from 'screenfull'
import { Fract2DState } from './Fract2DState'
import { Fract2DKeys } from './Fract2DKeys'
import { setupHelpers, getCarAngleInfo, drawCarAngle } from './Fract2DHelpers'
import { FractBase, ILogger } from '../../shared/fract/FractBase'
import { FractRenderPar } from '../../shared/fract/FractRenderPar'
import { FractLoc, FractPar } from '../../shared/fract/FractPar'
import { FractProcesr, ComputedPoint } from '../../shared/fract/FractProcesr'
import { FractMiniMap } from '../../shared/fract/FractMiniMap'
import { Transform2D } from '../../shared/Transform2D'
import { Viewport2D } from '../../shared/Viewport2D'
import { MouseEvents } from '../../shared/MouseEvents'
import { CanvasSelection } from '../../shared/CanvasSelection'
import { HeightMap, IHeightMapProducer } from '../../shared/HeightMap'
import { HeightMapZones, HMapZone } from '../../shared/HeightMapZones'
import { Pixels } from '../../shared/Pixels'
import { Color } from '../../shared/Color'
import { Region2D } from '../../shared/Region2D'
import { DomHelper } from '../../shared/DomHelper'
import { fractionEntiere } from '../../shared/nombres'
import { IUserEvents, UserEvents } from '../../shared/UserEvents'
import { setupTestInCardioide } from '../../test/Cardioide-test'
import { testSf, testGetPartsTo, testGradient } from '../../test/Region2D-test'
import { testZones } from '../../test/zone-test';

declare type Size = { width: number, height: number }
declare type Rect = { x: number, y: number } & Size
declare type Test = {
    name: string,
    method: (ctx: CanvasRenderingContext2D, events: IUserEvents, ...args: any[]) => void,
    getArgs?: () => any[]
}
enum Tools { Doc, Switch, Info, FullScreen, Point, Map, Path, Fractions, Helpers, CarAngle, Edges }

export class Fract2D extends FractBase implements IHeightMapProducer {
    private vp: Viewport2D
    private lvp: Size
    private mouse2D: MouseEvents
    private state: Fract2DState
    private tfm: Transform2D
    private psr: FractProcesr
    private hmap: HeightMap
    private miniMap: FractMiniMap
    private ctx: CanvasRenderingContext2D
    private sel: CanvasSelection
    private imd: ImageData
    private cancelRequired: boolean
    private processedMs: number
    private _carAngle: number
    private _userEvents: UserEvents
    private _testNum: number
    private tests: Test[] = [
        { name: 'Zones', method: testZones, getArgs: () => [this] },
        { name: 'ScaleFactor', method: testSf },
        { name: 'InCardioide', method: setupTestInCardioide },
        { name: 'testGradient', method: testGradient },
        { name: 'GetPartsTo', method: testGetPartsTo },
    ]

    protected _allDivs = ['Doc', 'Info', 'InfoPoint', 'MiniMap']

    constructor(par?: FractLoc | FractPar | FractRenderPar, altAppInit?: () => FractBase) {
        super(altAppInit)
        this.state = new Fract2DState(par, () => this.update(), () => this.updateImage(), () => this.updateInfo(), this.log)
        const canvas = <HTMLCanvasElement>document.querySelector('canvas.fract')
        this.vp = new Viewport2D(canvas, () => this.onResized(), this.log)
        this.sel = new CanvasSelection(canvas,
            s => this.fromSel(s),
            false, (x, y, b) => this.getCursorColor(x, y, b),
            true, (x, y) => this.getCursorXYInfo(x, y), 'all'
        )
        this._userEvents = new UserEvents()
        this.keys = new Fract2DKeys(
            true,
            this.state,
            (name, shift) => this.tool(name, shift),
            () => this.cancel(),
            e => this._userEvents.doKey(e),
            this.log,
            this.isDev
        )
        this.mouse2D = new MouseEvents(
            canvas,
            (x, y) => this.onClick(x, y),
            (x, y) => this.onDblClick(x, y),
            (x, y, e) => { this.onMove(x, y); this._userEvents.doMove(e) },
        )
        setTimeout(() => {
            this.resize()
            if (this.showDocAtStart) this.showDoc(true, true)
        }, 333);
    }

    getParams() { return this.state.current.clone() }

    getHeightMap(flatten?: boolean, info?: { min: number }) {
        const hmap = this.hmap.clone()
        if (flatten) hmap.flatten(info)
        else if (info) info.min = hmap.getMin()
        return hmap
    }
    getHeightMapAsync(flatten?: boolean, info?: { min: number }) {
        return new Promise<HeightMap>((resolve, reject) => {
            const wait = () => {
                if (this.processing) setTimeout(wait, 500)
                else resolve(this.getHeightMap(flatten, info))
            }
            wait()
        })
    }
    getImage(): HTMLImageElement {
        if (!this.ctx) return undefined
        const img = document.createElement('img')
        img.src = this.ctx.canvas.toDataURL("image/jpg")
        return img
    }
    getContext(): CanvasRenderingContext2D {
        return this.ctx
    }


    protected _attach() {
        this.ctx.canvas.style.opacity = '0'
        this.ctx.canvas.style.height = this.ctx.canvas.height + 'px'
        this.sel.attach()
        this.mouse2D.attach()
        this.keys.attach()
        this.vp.attach()
    }
    protected _attached() { this.say('attached') }

    protected _detach() {
        this.sel.detach()
        this.mouse2D.detach()
        this.keys.detach()
        this.vp.detach()
    }
    protected _detached() { this.ctx.canvas.style.height = '0' }

    protected _moreElements() { return [this.ctx.canvas] }

    protected _loggers(): ILogger[] {
        return [this.miniMap, this.psr, this.state, this.sel, this.vp, this.keys]
    }

    protected _toolname(tool: number) { return Tools[tool] }

    protected _getInfo() {
        return this.state + '\n'
            + (this.processedMs ? (this.processedMs.toFixed(3) + 'ms\n') : '')
            + this.vp + '\n'
    }

    protected drawPath(x: number, y: number) {
        if (!this._pathData) { this._pathData = new Array<number>() }
        const par = this.state.current, path = this.psr.computePathFromView(x, y, par.m, par.r, this._pathData)
        Pixels.drawGradientPoly(this.ctx, path)
    }

    private resize() {
        this.lvp = this.vp.size
        this.ctx = this.vp.getContext()
        if (this.tfm) { this.tfm.updateView(this.vp.width, this.vp.height) }
        else { this.tfm = new Transform2D(this.vp.width, this.vp.height) }
        this.sel.reset(this.ctx, true)
        this.updateMiniMap(true)
        this.update()
    }
    private update() {
        if (this.processing) { return false }
        const par = this.state.current
        this.tfm.setTarget(par.a, par.b, par.z)
        return this.process()
    }
    private process() {
        if (this.processing) { this.say('busy'); return false }
        this.processing = true
        this.cancelRequired = false
        this.updateShowBusy()
        this.say('---------------\n' + this.state.toString())
        if (!this.psr) { this.psr = new FractProcesr(this.tfm, this.log) }
        const par = this.state.current,
            start = performance.now(),
            updateProgress = (p: number) => {
                if (this.state.progress || (performance.now() - start) > 666) {
                    this.updateProgress(p)
                }
            },
            processing = this.psr.computeHeightMap(
                Region2D.fromSize(this.vp.size), par,
                (hmap, canceled) => this.processed(start, hmap, canceled),
                this.hmap && this.hmap.clone(),
                this.state.parts && this.vp.isSame(this.lvp) && this.state.previousPar,
                this.state.worker, this.state.optimize, par.mask,
                p => { updateProgress(p); return this.cancelRequired },
                this.state.parts,
                this.state.gpu,
                this.state.gpuDynOut
            )
        if (!processing) { this.say('procesr busy'); this.updateShowBusy() }
        return processing
    }
    private cancel() {
        if (!this.processing) { return }
        this.say('canceling')
        this.cancelRequired = true
        this.psr.cancel()
    }
    private processed(start: number, hmap: HeightMap, canceled: boolean) {
        if (canceled) {
            this.say('canceled')
            this.restoreImage()
            this.state.restore()
        } else {
            this.state.onRecomputed()
            this.withTime('updateImage', () => this.updateImage(hmap, true))
            this.processedMs = performance.now() - start
            this.updateInfo()
            this.sayMs(start, 'done')
            this._zonesData = undefined
        }
        this.say('---------------')
        this.updateProgress(false)
        this.processing = false
        this.cancelRequired = false

        //if (this.debug) hmap.computeIndicesByZoneByLevel(4, true, true)

        this.updateShowBusy()
    }
    private updateImage(hmap?: HeightMap, noUpdateInfo = false) {
        if (hmap) this.hmap = hmap; else hmap = this.hmap; if (!hmap) return
        const par = this.state.current
        this.imd = Pixels.imageFromHeightMap(
            hmap.data, hmap.dataWidth,
            par.color, par.invert, par.easing)
        this.restoreImage(noUpdateInfo)
    }
    private restoreImage(elseSave: boolean = false, isMove = false, noUpdateInfo = false) {
        if (this.imd) { this.ctx.putImageData(this.imd, 0, 0) }
        else if (elseSave) { this.imd = this.ctx.getImageData(0, 0, this.vp.width, this.vp.height) }
        if (this.isTool(Tools.Helpers)) { this.drawHelpers() }
        if (!isMove) {
            this.sel.reset(null, true)
            this.updateMiniMap()
        }
        if (!noUpdateInfo) { this.updateInfo() }
    }

    private onResized() {
        this.cancel()
        this.resize()
    }
    private onClick(x: number, y: number) {
        this.say('onClick', x, y)
        DomHelper.showHideMouse(false, this.ctx.canvas)
        this.showAll(false)
    }
    private onDblClick(x: number, y: number) {
        this.zoomAt(x, y)
    }
    private onMove(x: number, y: number) {
        DomHelper.showHideMouse(true, this.ctx.canvas)
        this.showAll(true)
        const localRefresh = this.isTool(Tools.Helpers, Tools.CarAngle, Tools.Path, Tools.Edges)
        if (localRefresh && !this.sel.showCross) { this.restoreImage(localRefresh, true) }
        if (this.isTool(Tools.Edges)) { this.drawZones(x, y) } else { this._zonesData = this._zonesInfo = undefined }
        if (this.isTool(Tools.CarAngle)) { this.drawCarAngle(x, y) } else { this._carAngle = undefined }
        if (this.isTool(Tools.Path)) { this.drawPath(x, y) }
    }

    private tool(name: string, shift: boolean) {
        if (super.basetool(name, shift)) return
        switch (name) {
            case 'mask': this.state.toggle('mask', this.isMaskVisible()); break
            case 'test': this.nextTest(); break
            case 'save':
                if (shift) {
                    const data = window.prompt("Paste content of fract .json file")
                    if (data) { this.state.update(data) }
                }
                else { DomHelper.download(this.state.current, 'fract-' + Date.now() + '.json') }
                break
            default:
                const v = this.tools.get(name)
                switch (name) {
                    case Tools[Tools.Point]: this.sel.showCross = v; this.showInfoPoint(null); break
                    case Tools[Tools.FullScreen]:
                        const sf = (<any>screenfull).screenfull
                        if (sf.enabled) { if (!sf.isFullscreen) sf.request(); else sf.exit() }
                        return
                    default: this.updateImage(); break
                }
                break
        }
    }

    private nextTest() {
        this._userEvents.doDetach()
        this._testNum = this._testNum == undefined ? 0 : this._testNum == this.tests.length - 1 ? undefined : 1 + this._testNum
        if (this._testNum == undefined) {
            this.keys.bypass = false
            this.say('---------------\ntests off\n---------------')
            this.restoreImage()
        } else {
            this.keys.bypass = true
            const t = this.tests[this._testNum]
            this.say('---------------\ntest: ' + t.name)
            Pixels.clear(this.ctx)
            this.sel.reset(this.ctx)
            t.method.apply(null, [this.ctx, this._userEvents].concat(t.getArgs ? t.getArgs() : undefined))
        }
    }

    private fromSel(r: Rect) {
        this._userEvents.doSelect(r.x, r.y, r.width, r.height)
        if (!this.tfm || this._testNum) { return }
        this.say('zoomSel', r)
        const loc = this.tfm.locFromViewRect(r, this.state.currentZoom)
        this.state.update(loc)
    }

    private zoomAt(x: number, y: number) {
        this.say('zoomAt', x, y)
        const loc = this.tfm.locFromView(x, y, this.state.currentZoom, 2, true)
        this.state.update(loc)
    }

    private updateMiniMap(resized = false) {
        if (!this.miniMap) { this.miniMap = new FractMiniMap(this.log) }
        this.miniMap.updateMiniMap(this.isTool(Tools.Map), resized, this.vp.size, this.tfm, this.state.currentZoom)
    }

    private computePoint(x: number, y: number) {
        const par = this.state.current
        return this.psr.computePointFromView(x, y,
            par.m, par.r, par.mask, this.state.optimize)
    }
    private getCursorColor(x: number, y: number, forSelectBox: boolean) {
        if (forSelectBox || !this.psr) { return Color.MBlue }
        const cp = this.computePoint(x, y), optim = this.state.optimize
        this.showInfoPoint(cp)
        return cp.value == cp.max ?
            cp.optimized ?
                Color.LGreen
                : optim && cp.nooptim ?
                    Color.LBlue
                    : Color.MGreen
            : cp.optimized ?
                Color.MidYellow
                : optim && cp.nooptim ?
                    Color.Pink
                    : Color.MRed
    }

    private getCursorXYInfo(x: number, y: number) {
        if (!this.tfm) { return { x, y } }
        const fx = this.tfm.xToTarget(x, y),
            fy = this.tfm.yToTarget(x, y),
            df = this.isTool(Tools.Fractions),
            sx = df ? (fractionEntiere(fx) + ' ') : '',
            sy = df ? (fractionEntiere(fy) + ' ') : ''
        return {
            x: sx + fx + ' ' + x + '/' + this.vp.width,
            y: sy + fy + ' ' + y + '/' + this.vp.height
        }
    }
    private showInfoPoint(cp: ComputedPoint) {
        const show = !!cp
        DomHelper.crud('InfoPoint', show, show && DomHelper.topRight('all', 'info'),
            show && cp.value + '/' + cp.max
            + ' ' + cp.elapsedMs.toFixed(3) + 'ms'
            + (this.state.optimize && cp.nooptim ? ' nooptim' : '')
            + (cp.optimized ? ' optimized' : '')
            + (this._carAngle == undefined ? '' : (' ' + getCarAngleInfo(this._carAngle, this.isTool(Tools.Fractions))))
            + (this._zonesInfo ? (' ' + this._zonesInfo) : '')
        )
    }

    private drawHelpers() {
        setupHelpers(this.ctx, this.tfm, this.state.current.r, this.isTool(Tools.CarAngle))
            .forEach(h => h.draw())
    }

    private drawCarAngle(x: number, y: number) {
        this._carAngle = drawCarAngle(this.ctx, this.tfm, x, y)
    }

    private _zonesData: HeightMapZones
    private _zonesInfo: string
    private drawZones(x: number, y: number) {
        if (!this._zonesData) {
            this.updateShowBusy(true)
            this._zonesData = HeightMapZones.compute(this.hmap, 1, 100, this.log, this.debug)
            this.updateShowBusy(false)
        }
        const zd = this._zonesData,
            lzs = zd.getLevelZones(zd.getLevelIndex(x, y)),
            lcounts = { points: 0, contour: 0 }
        let zone: HMapZone = undefined
        Pixels.withImageData(this.ctx, imd => {
            Pixels.setAlpha(imd, 128)
            if (lzs.length == 1) zone = lzs[0]
            else zd.drawLevel(lzs, imd, Color.Gray, Color.Green, lcounts)
            zd.drawZone(zone = zd.getZone(x, y), imd, Color.LGray, Color.LGreen)
        })
        this._zonesInfo = ''
            + 'level-edges:' + lcounts.contour
            + ' level-points:' + lcounts.points
            + ' level-zones:' + (lzs && lzs.length || 0)
            + ' zone-edges:' + (zone && zone.contour.length || 0)
            + ' zone-points:' + (zone && zone.points.length || 0)
    }

    private isMaskVisible() {
        return true //todo compute(this.viewport.region, this.state.a, this.state.b, this.state.z)
    }
}

