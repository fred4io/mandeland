import { KeyCommands } from "../../shared/KeyCommands";
import { Fract2DState } from "./Fract2DState";

export class Fract2DKeys extends KeyCommands {
    constructor(
        inBrowser: boolean,
        state: Fract2DState,
        tool: (name: string, shift?: boolean) => void,
        cancel: () => void,
        bypassed: (e: KeyboardEvent) => void,
        log: boolean,
        activateSpecials: boolean
    ) {
        super([
            {
                key: KeyCommands._digits, action: (shift, ctrl, alt, meta, digit) =>
                    state.go(digit + 10 * (meta ? 4 : alt ? 3 : ctrl ? 2 : shift ? 1 : 0)),
                doc: "presets", default: "0-9", shift: "10-19", ctrl: "20-29" //, alt: "30-39", meta: "40-49"
            },
            { key: KeyCommands._right, action: () => state.inc('a', false) },
            { key: KeyCommands._left, action: () => state.inc('a', true) },
            { key: KeyCommands._up, action: () => state.inc('b', false) },
            { key: KeyCommands._down, action: () => state.inc('b', true) },
            { key: KeyCommands._arrows, doc: "step right/left/up/down" },
            { key: 'Z', action: (shift, ctrl) => state.times('z', shift, ctrl), doc: "zoom", default: "in 10%", shift: "out", ctrl: "200%" },
            { key: 'M', action: (shift, ctrl) => state.times('m', shift, ctrl, true), doc: "detail", default: "increase", shift: "decrease", ctrl: "double" },
            { key: 'R', action: (shift, ctrl) => state.times('r', shift, ctrl), doc: "radius", default: "increase", shift: "decrease", ctrl: "double" },
            { key: 'V', action: () => state.toggle('invert'), doc: "invert colors" },
            { key: 'C', action: () => state.toggle('color'), doc: "colors/gray gradient" },
            { key: 'E', action: (shift) => state.next('easing', shift), doc: "gradient easing type" },
            { key: KeyCommands._esc, action: () => cancel(), doc: "cancel progress" },
            { key: 'K', action: () => state.toggle('mask'), doc: "show optimization mask" },
            { key: 'B', action: () => state.back(), doc: "revert to last location/parameters" },
            { key: 'S', action: () => tool('Point'), doc: "show info about point under pointer" },
            { key: 'G', action: () => tool('Helpers'), doc: "show guides" },
            { key: 'D', action: () => tool('Path'), doc: "draw path from point under pointer" },
            { key: 'J', action: () => tool('Fractions'), doc: "coordinates as whole fractions" },
            { key: 'I', action: () => tool('Info'), doc: "info panel" },
            { key: 'A', action: () => tool('Map'), doc: "minimap" },
            { key: 'N', action: () => tool('CarAngle'), doc: "draw insectection cardioid/segment to pointer" },
            { key: 'Q', action: () => tool('Edges'), doc: "edges and zones" },
            { key: 'T', action: () => tool('test'), doc: "testing programs" },
            { key: 'X', action: (shift) => tool('save', shift), doc: "save to file", shift: "paste from file" },
            { key: 'F', action: () => tool('FullScreen'), doc: "fullscreen on/off" },
            { key: 'Y', action: () => tool('Switch'), doc: "switch to 3D" },
            { key: 'H', action: () => tool('Doc'), doc: "commands list" },
            { special: true, key: 'P', action: () => state.toggle('progress'), doc: "show progress bar" },
            { special: true, key: 'W', action: () => state.toggle('worker'), doc: "process in background" },
            { special: true, key: 'O', action: () => state.toggle('optimize'), doc: "optimize (circle and cardioid)" },
            { special: true, key: 'U', action: () => state.toggle('parts'), doc: "optimize (skip visible parts, subdivide)" },
            { special: true, key: KeyCommands._lt, action: () => state.toggle('gpu'), doc: "compute with gpu" },
            { special: true, key: 'L', action: () => tool('log'), doc: "message logging (in console)" },
        ],
            inBrowser, 'T', bypassed, log, activateSpecials)
    }
}
