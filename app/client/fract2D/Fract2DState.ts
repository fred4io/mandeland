import { FractPar, FractLoc, Loc, Par } from "../../shared/fract/FractPar"
import { FractRenderPar } from "../../shared/fract/FractRenderPar"
import { Easing } from "../../shared/Easing"

export class Fract2DState {

    public progress = false
    public optimize = true
    public worker = true
    public parts = true
    public gpu = false
    public gpuDynOut = true

    private locals = ['progress', 'optimize', 'worker', 'parts', 'gpu', 'log']

    private rpar: FractRenderPar
    private lastRPar: FractRenderPar
    private lastPar: FractPar
    private lastLoc: FractLoc
    private computeProfile: FractRenderPar
    private doAfterUpdate: () => void

    get current() { return this.rpar }
    get previous() { return this.lastRPar }
    get previousPar() { return this.lastPar }
    get previousLoc() { return this.lastLoc }

    get currentZoom() { return this.rpar.z }

    get stepInc() { return .1 }

    constructor(
        par: FractLoc | FractPar | FractRenderPar,
        private recompute = () => { return true },
        private updateImage = () => { },
        private updateInfo = () => { },
        public log = false
    ) {
        this.rpar = FractRenderPar.from(par)
        this.computeProfile = this.rpar.getComputeProfile()
        this.save()
    }

    toString() {
        return (
            this.rpar.toString()
            + ' ' + this.locals.map(n => (this.getValue<boolean>(n) ? '' : '!') + n).join(' ')
        ).replace(/\s+/g, '\n')
    }

    go(numOrPar: number | FractPar) {
        if (this.log) console.log('go ', numOrPar)
        this.doAfterUpdate = () => this.rpar.copy(this.computeProfile)
        const par: FractPar | FractRenderPar =
            numOrPar instanceof FractPar ? numOrPar
                : FractRenderPar.loc(numOrPar)
        this.update(par)
    }
    back() {
        if (this.log) { console.log('back') }
        this.restore()
        this.doRecompute()
    }

    inc(parName: string, dec: boolean, recompute = true) {
        return this.setParVal<number>(parName, v => v + (dec ? -this.stepInc : this.stepInc) / this.rpar.z,
            recompute)
    }
    times(parName: string, less: boolean, dbl: boolean, round = false, recompute = true) {
        return this.setParVal<number>(parName, v => {
            v = v * (less ? dbl ? .5 : .9 : dbl ? 2 : 1.1)
            return round ? Math.abs(v) < 1 ? v < 0 ? -1 : 1 : less ? Math.floor(v) : Math.ceil(v) : v
        }, recompute)
    }
    toggle(parName: string, recompute = false) {
        if (this.locals.some(n => n == parName))
            return this.toggleLocal(parName)
        else
            return this.setParVal<boolean>(parName, v => !v, recompute)
    }
    next(parName: string, dec = false, max?: number, recompute = false) {
        let getValue: (v: number) => number
        switch (parName) {
            case 'easing': getValue = v => Easing.nextEaser(v, dec);
                break;
            default: getValue = v => (v += (dec ? -1 : 1)) > (max || Infinity) ? 0 : v < 0 ? (max || 0) : v
                break;
        }
        return this.setParVal<number>(parName, getValue, recompute)
    }
    update(a?: number | Loc | Par | FractRenderPar | string, b?: number, z?: number, m?: number, r?: number) {
        if (typeof a == 'string') { a = FractRenderPar.from(a) }
        this.setPar(a, b, z, m, r)
        this.doRecompute()
    }
    save() {
        this.lastRPar = this.rpar.clone()
        if (!this.lastPar || !this.lastPar.equals(this.rpar)) { this.lastPar = this.rpar.clone() }
        if (!this.lastLoc || !this.lastLoc.equals(this.rpar)) { this.lastLoc = this.rpar.clone() }
    }
    restore() {
        this.rpar = this.lastRPar
        this.updateInfo()
    }

    onRecomputed() {
        if (this.doAfterUpdate) {
            this.doAfterUpdate()
            this.doAfterUpdate = undefined
        }
    }

    private setPar(aOrParOrLoc?: number | Loc | Par | FractRenderPar,
        b?: number, z?: number, m?: number, r?: number
    ) {
        this.save()
        if (typeof aOrParOrLoc == 'object') { this.rpar.copy(aOrParOrLoc) }
        else { this.rpar.set(aOrParOrLoc, b, z, m, r) }
        this.updateInfo()
    }

    private toggleLocal(name: string) {
        const v = this.setValue(name, v => !v)
        if (this.log) console.log(name, v)
        this.updateInfo()
    }

    private setParVal<T extends number | boolean>(parName: string, getValue: (v: T) => T, recompute = false) {
        this.save()
        const v = this.setValue(parName, getValue, this.rpar)
        if (this.log) console.log(parName, this.rpar.valToString(parName))
        if (FractRenderPar.isComputer(parName)) { (<any>this.computeProfile)[parName] = v }
        if (recompute) { this.doRecompute() }
        else if (FractRenderPar.isImager(parName)) { this.updateImage() }
        else { this.updateInfo() }
        return v
    }

    private setValue<T extends number | boolean>(name: string, getValue: (v: T) => T, obj: object = this): T {
        let ov = <T>(<any>obj)[name];
        if (ov == undefined) { console.log('par ' + name + ' undefined'); return; }
        const nv = getValue(ov);
        if (nv != undefined) { (<any>obj)[name] = nv; }
        return nv == undefined ? ov : nv;
    }

    private getValue<T extends number | boolean>(name: string, obj: object = this) {
        return (<any>obj)[name] as T
    }

    private doRecompute() {
        if (!this.recompute()) {
            if (this.log) console.log('busy')
            this.restore()
        }
    }
}