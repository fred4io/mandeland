import { Transform2D } from "../../shared/Transform2D"
import { Pixels } from "../../shared/Pixels"
import { Circle } from "../../shared/Circle"
import { Color, IColoR, IColor } from "../../shared/Color"
import { Cardioide } from "../../shared/Cardioide"
import { fractionEntiere } from "../../shared/nombres"

export function setupHelpers(c: CanvasRenderingContext2D, t: Transform2D, lim: number, doCardioRays: boolean) {

    const x = (v: number) => t.xToView(v, 0), y = (v: number) => t.yToView(v, 0),
        x0 = x(0), y0 = y(0), dx = (v: number) => x(v) - x0, dy = (v: number) => y(v) - y0

    return [
        {
            name: 'view center',
            draw: () => Pixels.drawCross(c, c.canvas.width / 2, c.canvas.height / 2, 20, Color.Gray)
        },
        {
            name: 'limit circle',
            draw: () => Circle.draw(c, x0, y0, dx(lim), Color.MGreen)
        },
        {
            name: 'world origin',
            draw: () => Pixels.drawCross(c, x0, y0, 10, Color.MGreen)
        },
        {
            name: 'points at y=0, x= -1, -3/4, 1/4',
            draw: () => [-1, -3 / 4, 1 / 4].forEach(tx => Pixels.drawCross(c, x(tx), y0, 10, Color.MRed))
        },
        {
            name: 'cardioid r=-1/2 at 1/4,0',
            draw: () => Cardioide.draw(c, x(.25), y0, dx(-.5), 100, Color.MRed)
        },
        {
            name: 'circle r=1/4 at -1,0',
            draw: () => Circle.draw(c, x(-1), y0, dx(.25), Color.MRed)
        },
        {
            name: 'circle box',
            draw: () => Pixels.drawBox(c, Circle.getBox(dx(.25), x(-1), y0))
        },
        {
            name: 'cardioid box',
            draw: () => Pixels.drawBox(c, Cardioide.getBox(x(.25), y0, dx(-.5)))
        },
        {
            name: 'circle in cardioid, with center',
            draw: () => { Circle.draw(c, x(-.25), y0, dx(.5), Color.MRed); Pixels.drawCross(c, x(-.25), y0, 10, Color.MRed) }
        },
        {
            name: 'vertical segment at x=1/8,+-3/4',
            draw: () => Pixels.drawLine(c, x(-1 / 8), y(3 / 4), x(-1 / 8), y(-3 / 4), Color.MRed)
        },
        {
            name: 'cardioid rays',
            draw: () => {

                if (!doCardioRays) { return }

                const cardioRays = (n0: number, inc: number, repeat: number, cFrom: IColor, cTo: IColor|IColoR) => {
                    const cl = new Color(), max = repeat * inc
                    for (let i = n0, j = 0; j < repeat; j++ , i += inc) {
                        Cardioide.drawNth(c, x(.25), y0, dx(-.5), i, repeat, cl.setLerp(cFrom, cTo, i / max))
                    }
                }
                const repeat = 20, cIn = new Color(), cOut = Color.White
                const phase = 0, spans = 4, inc = 2
                for (let j of [-1, 1]) {
                    for (let a = phase, i = 0; i < spans; i++ , a += (inc / spans)) {
                        cIn.gradient7(i / spans, 1, spans)
                        cardioRays(a, j * inc, repeat, cIn, cOut)
                    }
                }
            }
        }
    ]
}

export function drawCarAngle(ctx: CanvasRenderingContext2D, trf: Transform2D, x: number, y: number) {
    const y0 = trf.yToView(0, 0),
        x14 = trf.xToView(.25, 0)

    Pixels.drawLine(ctx, x14, y0, x, y, Color.MBlue)

    const px = trf.xToTarget(x, y), py = trf.yToTarget(x, y),
        car = new Cardioide(-.5, .25, 0),
        a = car.getAngleRayTo(px, py),
        q = car.getPointAtAngle(a)
    trf.pointToView(q)
    Pixels.drawCross(ctx, q.x, q.y, 5, Color.MBlue)
    return a
}
export function getCarAngleInfo(carAngle: number, doFractions: boolean) {
    const pi = Math.PI, a = carAngle - pi, rad = 180 / pi,
        ap = a / pi, ap3 = parseFloat(ap.toFixed(3))
    return (a * rad).toFixed(0) //+ '°' 
        + ',' + ap3
        + (doFractions ? ',' + fractionEntiere(ap3) : '')
}
