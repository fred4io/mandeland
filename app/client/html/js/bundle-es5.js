System.register("shared/fract/FractPar", [], function (exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var FractLoc, FractPar;
    return {
        setters: [],
        execute: function () {
            FractLoc = class FractLoc {
                constructor(a = 0, b = 0, z = 1) {
                    this.a = a;
                    this.b = b;
                    this.z = z;
                }
                static fromLoc(p) { return new FractLoc(p.a, p.b, p.z); }
                copy(l) { return this.set(l.a, l.b, l.z); }
                clone() { return new FractLoc().copy(this); }
                toString() { return `a=${this.a.toFixed(20)} b=${this.b.toFixed(20)} z=${this.z.toFixed(1)}`; }
                set(a, b, z) {
                    if (a != undefined)
                        this.a = a;
                    if (b != undefined)
                        this.b = b;
                    if (z != undefined)
                        this.z = z;
                    return this;
                }
                equals(l) {
                    return l && l.a == this.a && l.b == this.b && l.z == this.z;
                }
            };
            exports_1("FractLoc", FractLoc);
            FractPar = class FractPar extends FractLoc {
                constructor(a = 0, b = 0, z = 1, m = 50, r = 2) {
                    super(a, b, z);
                    this.m = m;
                    this.r = r;
                }
                static from(l, p) {
                    const o = new FractPar();
                    if (l) {
                        o.copy(l);
                    }
                    if (p) {
                        o.copyPar(p);
                    }
                    return o;
                }
                copyPar(o) {
                    super.copy(o);
                    if (o.m != undefined) {
                        this.m = o.m;
                    }
                    if (o.r != undefined) {
                        this.r = o.r;
                    }
                    return this;
                }
                copy(o) {
                    this.copyPar(o);
                    return this;
                }
                clone() { return new FractPar().copy(this); }
                toString() { return super.toString() + ` m=${this.m} r=${this.r}`; }
                set(a, b, z, m, r) {
                    super.set(a, b, z);
                    if (m != undefined) {
                        this.m = m;
                    }
                    if (r != undefined) {
                        this.r = r;
                    }
                    return this;
                }
                setLoc(a, b, z) { return super.set(a, b, z); }
                isSameMR(o) { return this.m == o.m && this.r == o.r; }
                equals(o) { return super.equals(o) && this.isSameMR(o); }
                isSameABZ(o) { return super.equals(o); }
            };
            exports_1("FractPar", FractPar);
        }
    };
});
System.register("shared/Easing", [], function (exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    var EaseDir, EaseType, Easer, Easing;
    return {
        setters: [],
        execute: function () {
            (function (EaseDir) {
                EaseDir[EaseDir["In"] = 0] = "In";
                EaseDir[EaseDir["Out"] = 1] = "Out";
                EaseDir[EaseDir["InOut"] = 2] = "InOut";
            })(EaseDir || (EaseDir = {}));
            exports_2("EaseDir", EaseDir);
            (function (EaseType) {
                EaseType[EaseType["Quad"] = 0] = "Quad";
                EaseType[EaseType["Cubic"] = 1] = "Cubic";
                EaseType[EaseType["Quart"] = 2] = "Quart";
                EaseType[EaseType["Quint"] = 3] = "Quint";
            })(EaseType || (EaseType = {}));
            exports_2("EaseType", EaseType);
            (function (Easer) {
                Easer[Easer["Linear"] = 0] = "Linear";
                Easer[Easer["InQuad"] = 1] = "InQuad";
                Easer[Easer["InCubic"] = 2] = "InCubic";
                Easer[Easer["InQuart"] = 3] = "InQuart";
                Easer[Easer["InQuint"] = 4] = "InQuint";
                Easer[Easer["OutQuad"] = 5] = "OutQuad";
                Easer[Easer["OutCubic"] = 6] = "OutCubic";
                Easer[Easer["OutQuart"] = 7] = "OutQuart";
                Easer[Easer["OutQuint"] = 8] = "OutQuint";
                Easer[Easer["InOutQuad"] = 9] = "InOutQuad";
                Easer[Easer["InOutCubic"] = 10] = "InOutCubic";
                Easer[Easer["InOutQuart"] = 11] = "InOutQuart";
                Easer[Easer["InOutQuint"] = 12] = "InOutQuint";
            })(Easer || (Easer = {}));
            exports_2("Easer", Easer);
            Easing = class Easing {
                static ease(t, ed, et) {
                    return Easing.func(ed, et)(t);
                }
                static func(ed, et) {
                    return Easing[Easing.funcName(ed, et)];
                }
                static funcName(ed, et) {
                    if (et == undefined) {
                        const n = Math.max(0, Math.min(12, ed || 0));
                        ed = n ? ~~((n - 1) / 4) : undefined;
                        et = n ? ((n - 1) % 4) : undefined;
                    }
                    return (et == undefined || ed == undefined) ? 'linear' : ('ease' + EaseDir[ed] + EaseType[et]);
                }
                static _test() {
                    for (let i = 0; i < 13; i++) {
                        console.log(Easing.funcName(i), Easing.ease(.25, i));
                    }
                }
                static nextEaser(ed, reverse = false) {
                    return (ed += (reverse ? -1 : 1)) > Easing.maxFuncIndex ? 0 : ed < 0 ? Easing.maxFuncIndex : ed;
                }
                /*
                * From https://gist.github.com/gre/1650294
                * Easing Functions - inspired from http://gizma.com/easing/
                * only considering the t value for the range [0, 1] => [0, 1]
                */
                // no easing, no acceleration
                static linear(t) { return t; }
                // accelerating from zero velocity
                static easeInQuad(t) { return t * t; }
                // decelerating to zero velocity
                static easeOutQuad(t) { return t * (2 - t); }
                // acceleration until halfway, then deceleration
                static easeInOutQuad(t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t; }
                // accelerating from zero velocity 
                static easeInCubic(t) { return t * t * t; }
                // decelerating to zero velocity 
                static easeOutCubic(t) { return (--t) * t * t + 1; }
                // acceleration until halfway, then deceleration 
                static easeInOutCubic(t) { return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1; }
                // accelerating from zero velocity 
                static easeInQuart(t) { return t * t * t * t; }
                // decelerating to zero velocity 
                static easeOutQuart(t) { return 1 - (--t) * t * t * t; }
                // acceleration until halfway, then deceleration
                static easeInOutQuart(t) { return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t; }
                // accelerating from zero velocity
                static easeInQuint(t) { return t * t * t * t * t; }
                // decelerating to zero velocity
                static easeOutQuint(t) { return 1 + (--t) * t * t * t * t; }
                // acceleration until halfway, then deceleration 
                static easeInOutQuint(t) { return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t; }
            };
            Easing.maxFuncIndex = 12;
            exports_2("Easing", Easing);
        }
    };
});
System.register("shared/fract/FractRenderPar", ["shared/Easing", "shared/fract/FractPar"], function (exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    var Easing_1, FractPar_1, FractRenderPar;
    return {
        setters: [
            function (Easing_1_1) {
                Easing_1 = Easing_1_1;
            },
            function (FractPar_1_1) {
                FractPar_1 = FractPar_1_1;
            }
        ],
        execute: function () {
            FractRenderPar = class FractRenderPar extends FractPar_1.FractPar {
                constructor(invert = false, color = true, easing = 0, mask = false, a, b, z, m, r) {
                    super(a, b, z, m, r);
                    this.invert = invert;
                    this.color = color;
                    this.easing = easing;
                    this.mask = mask;
                }
                static allRProps() { return this._bools.concat(this._nums); }
                static allProps(filter) {
                    const props = this.allRProps().concat('a', 'b', 'z', 'm', 'r');
                    return filter ? props.filter(filter) : props;
                }
                static isComputer(name) { return this._computers.some(c => c == name); }
                static isImager(name) { return this._imagers.some(c => c == name); }
                static copyProps(tgt, src, names) {
                    if (!src || !tgt) {
                        return tgt;
                    }
                    names.forEach(pn => { const v = src[pn]; if (v != undefined) {
                        tgt[pn] = v;
                    } });
                    return tgt;
                }
                static loc(n) {
                    let rp;
                    const colorInv = { invert: true, color: true }, grayInv = { invert: true, color: false };
                    switch (n) {
                        //zmax? 24947790800071
                        //Region2D.zeroCentered(4, 2.25),
                        //new Region2D(-2.1, .6, -1.2, 1.2, true),
                        //l = 1.2, x = -.5, rw = new Region2D(x - l, x + l, -l, l, true),
                        case 1: return FractRenderPar.from(new FractPar_1.FractPar(-.75, 0, 1.75, 50, 2.1915), { invert: false, color: false });
                        case 2: return FractRenderPar.from(new FractPar_1.FractPar(-0.19616829451540196, 0.4127473077886301, 3.993000000000001, 50), grayInv);
                        case 3: return FractRenderPar.from(new FractPar_1.FractPar(0.336558573388203, 0.047351543209876584, 47174.924165824064, 666));
                        case 4: return FractRenderPar.from(new FractPar_1.FractPar(-1.2388170062348742, 0.09860571193872883, 3274.8273685494632, 312), grayInv);
                        case 5: return FractRenderPar.from(new FractPar_1.FractPar(-1.235306354684753, 0.09734096086361498, 8711.36344343207, 624), colorInv);
                        case 6: return FractRenderPar.from(new FractPar_1.FractPar(-0.23006461896505995, 0.755071578437906, 25055.581752642876, 6400), colorInv);
                        case 7: return FractRenderPar.from(new FractPar_1.FractPar(-1.0076379502907715, 0.32123299594109134, 1145001726661.055, 2520), colorInv);
                        case 8: return FractRenderPar.from(new FractPar_1.FractPar(-1.6749999284744286, 0, 33554432, 400));
                        case 9: return FractRenderPar.from(new FractPar_1.FractPar(0.33679259259259253, -0.05131851851851849, 254.9858223062382, 226));
                        case 10: return FractRenderPar.from(new FractPar_1.FractPar(-0.7449300342878324, 0.11786674080252069, 527.3664710198092, 1352));
                        case 11: return FractRenderPar.from(new FractPar_1.FractPar(0.13543610074626849515, 0.63736007462686583569, 2144.0, 2294));
                        case 12: return FractRenderPar.from(new FractPar_1.FractPar(-0.23006461896505994713, 0.75507157843790595475, 25055.6, 1600), colorInv);
                        case 13: return FractRenderPar.from(new FractPar_1.FractPar(-0.19654267896927768433, 0.65101409324616743568, 451.7, 271), { easing: 2 /*InCubic*/ });
                        case 14: return FractRenderPar.from({ a: -1.4153616375100906, b: 0, z: 18.98003571428572, m: 200, invert: true, color: true, easing: 0 });
                        case 15: return FractRenderPar.from({ a: 0.13530936722694523, b: 0.637802768154106, z: 319335.46175115206, m: 2294, invert: false, color: true, easing: 5 });
                        case 16: return FractRenderPar.from({ a: -1.2329096703980087, b: 0.09688811800373118, z: 15832.615384615385, m: 535, invert: false, color: true });
                        case 17: return FractRenderPar.from({ a: -0.1876314942699442, b: -0.6660138362911809, z: 567.0315789473684, m: 114, invert: false, color: true, easing: 5 });
                        case 18: return FractRenderPar.from({ a: -0.18814477216649397, b: -0.6665049598853583, z: 3279.8085573646345, m: 228, invert: false, color: true, easing: 5 });
                        case 19: return FractRenderPar.from({ a: -0.188018476643721, b: -0.6663422069213536, z: 4824627678263.674, m: 912, invert: false, color: true, easing: 5 });
                        case 20: return FractRenderPar.from({ a: 0.3046954988934953, b: 0.030101333015795297, z: 101525.31108654167, m: 1332, invert: true, color: true });
                        case 21: return FractRenderPar.from({ a: 0.3047108697039768, b: 0.030109264665048836, z: 453877.86132806865, m: 2664, invert: true, color: true });
                        case 22: return FractRenderPar.from({ a: 0.3047064632321694, b: 0.03011190854813334, z: 453877.86132806865, m: 5328, invert: true, color: true, easing: 5 });
                        default: return FractRenderPar.from(new FractPar_1.FractPar());
                    }
                }
                static from(p, rp) {
                    if (typeof p == 'string') {
                        p = JSON.parse(p);
                    }
                    const o = new FractRenderPar();
                    if (p) {
                        o.copy(p);
                    }
                    if (rp) {
                        o.copyRPar(rp);
                    }
                    return o;
                }
                getComputeProfile() {
                    const r = this.clone();
                    FractRenderPar.allProps(p => !FractRenderPar.isComputer(p)).forEach(p => r[p] = undefined);
                    return r;
                }
                copyRPar(o) {
                    return FractRenderPar.copyProps(this, o, FractRenderPar.allRProps());
                }
                copy(o) {
                    FractRenderPar.copyProps(this, o, FractRenderPar.allProps());
                    return this;
                }
                clone() {
                    const o = new FractRenderPar().copy(this);
                    return o;
                }
                toString() {
                    return super.toString() + '\n'
                        + FractRenderPar._bools.reduce((p, c) => p + ' ' + (this[c] ? '' : '!') + c, '')
                        + FractRenderPar._nums.reduce((p, c) => p + ' ' + c + ':' + this.valToString(c), '');
                }
                setPar(a, b, z, m, r) {
                    return super.set(a, b, z, m, r);
                }
                isSamePar(o) { return super.equals(o); }
                valToString(name) {
                    const v = this[name];
                    if (name == 'easing') {
                        const fn = Easing_1.Easing.funcName(v);
                        return fn.startsWith('ease') ? fn.substr(4) : fn;
                    }
                    return v;
                }
            };
            FractRenderPar._bools = ['invert', 'color', 'optimize', 'mask'];
            FractRenderPar._nums = ['easing'];
            FractRenderPar._imagers = ['invert', 'color', 'easing'];
            FractRenderPar._computers = ['optimize', 'mask'];
            exports_3("FractRenderPar", FractRenderPar);
        }
    };
});
System.register("client/fract2D/Fract2DState", ["shared/fract/FractPar", "shared/fract/FractRenderPar", "shared/Easing"], function (exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    var FractPar_2, FractRenderPar_1, Easing_2, Fract2DState;
    return {
        setters: [
            function (FractPar_2_1) {
                FractPar_2 = FractPar_2_1;
            },
            function (FractRenderPar_1_1) {
                FractRenderPar_1 = FractRenderPar_1_1;
            },
            function (Easing_2_1) {
                Easing_2 = Easing_2_1;
            }
        ],
        execute: function () {
            Fract2DState = class Fract2DState {
                constructor(par, recompute = () => { return true; }, updateImage = () => { }, updateInfo = () => { }, log = false) {
                    this.recompute = recompute;
                    this.updateImage = updateImage;
                    this.updateInfo = updateInfo;
                    this.log = log;
                    this.progress = false;
                    this.optimize = true;
                    this.worker = true;
                    this.parts = true;
                    this.locals = ['progress', 'optimize', 'worker', 'parts', 'log'];
                    this.rpar = FractRenderPar_1.FractRenderPar.from(par);
                    this.computeProfile = this.rpar.getComputeProfile();
                    this.save();
                }
                get current() { return this.rpar; }
                get previous() { return this.lastRPar; }
                get previousPar() { return this.lastPar; }
                get previousLoc() { return this.lastLoc; }
                get currentZoom() { return this.rpar.z; }
                get stepInc() { return .1; }
                toString() {
                    return (this.rpar.toString()
                        + ' ' + this.locals.map(n => (this.getValue(n) ? '' : '!') + n).join(' ')).replace(/\s+/g, '\n');
                }
                go(numOrPar) {
                    if (this.log)
                        console.log('go ', numOrPar);
                    this.doAfterUpdate = () => this.rpar.copy(this.computeProfile);
                    const par = numOrPar instanceof FractPar_2.FractPar ? numOrPar
                        : FractRenderPar_1.FractRenderPar.loc(numOrPar);
                    this.update(par);
                }
                back() {
                    if (this.log) {
                        console.log('back');
                    }
                    this.restore();
                    this.doRecompute();
                }
                inc(parName, dec, recompute = true) {
                    return this.setParVal(parName, v => v + (dec ? -this.stepInc : this.stepInc) / this.rpar.z, recompute);
                }
                times(parName, less, dbl, round = false, recompute = true) {
                    return this.setParVal(parName, v => {
                        v = v * (less ? dbl ? .5 : .9 : dbl ? 2 : 1.1);
                        return round ? Math.abs(v) < 1 ? v < 0 ? -1 : 1 : less ? Math.floor(v) : Math.ceil(v) : v;
                    }, recompute);
                }
                toggle(parName, recompute = false) {
                    if (this.locals.some(n => n == parName))
                        return this.toggleLocal(parName);
                    else
                        return this.setParVal(parName, v => !v, recompute);
                }
                next(parName, dec = false, max, recompute = false) {
                    let getValue;
                    switch (parName) {
                        case 'easing':
                            getValue = v => Easing_2.Easing.nextEaser(v, dec);
                            break;
                        default:
                            getValue = v => (v += (dec ? -1 : 1)) > (max || Infinity) ? 0 : v < 0 ? (max || 0) : v;
                            break;
                    }
                    return this.setParVal(parName, getValue, recompute);
                }
                update(a, b, z, m, r) {
                    if (typeof a == 'string') {
                        a = FractRenderPar_1.FractRenderPar.from(a);
                    }
                    this.setPar(a, b, z, m, r);
                    this.doRecompute();
                }
                save() {
                    this.lastRPar = this.rpar.clone();
                    if (!this.lastPar || !this.lastPar.equals(this.rpar)) {
                        this.lastPar = this.rpar.clone();
                    }
                    if (!this.lastLoc || !this.lastLoc.equals(this.rpar)) {
                        this.lastLoc = this.rpar.clone();
                    }
                }
                restore() {
                    this.rpar = this.lastRPar;
                    this.updateInfo();
                }
                onRecomputed() {
                    if (this.doAfterUpdate) {
                        this.doAfterUpdate();
                        this.doAfterUpdate = undefined;
                    }
                }
                setPar(aOrParOrLoc, b, z, m, r) {
                    this.save();
                    if (typeof aOrParOrLoc == 'object') {
                        this.rpar.copy(aOrParOrLoc);
                    }
                    else {
                        this.rpar.set(aOrParOrLoc, b, z, m, r);
                    }
                    this.updateInfo();
                }
                toggleLocal(name) {
                    const v = this.setValue(name, v => !v);
                    if (this.log)
                        console.log(name, v);
                    this.updateInfo();
                }
                setParVal(parName, getValue, recompute = false) {
                    this.save();
                    const v = this.setValue(parName, getValue, this.rpar);
                    if (this.log)
                        console.log(parName, this.rpar.valToString(parName));
                    if (FractRenderPar_1.FractRenderPar.isComputer(parName)) {
                        this.computeProfile[parName] = v;
                    }
                    if (recompute) {
                        this.doRecompute();
                    }
                    else if (FractRenderPar_1.FractRenderPar.isImager(parName)) {
                        this.updateImage();
                    }
                    else {
                        this.updateInfo();
                    }
                    return v;
                }
                setValue(name, getValue, obj = this) {
                    let ov = obj[name];
                    if (ov == undefined) {
                        console.log('par ' + name + ' undefined');
                        return;
                    }
                    const nv = getValue(ov);
                    if (nv != undefined) {
                        obj[name] = nv;
                    }
                    return nv == undefined ? ov : nv;
                }
                getValue(name, obj = this) {
                    return obj[name];
                }
                doRecompute() {
                    if (!this.recompute()) {
                        if (this.log)
                            console.log('busy');
                        this.restore();
                    }
                }
            };
            exports_4("Fract2DState", Fract2DState);
        }
    };
});
System.register("shared/KeyCommands", [], function (exports_5, context_5) {
    "use strict";
    var __moduleName = context_5 && context_5.id;
    var KeyCommands;
    return {
        setters: [],
        execute: function () {
            KeyCommands = class KeyCommands {
                constructor(commands, inBrowser = false, bypassKey, bypassed = (e) => { }, log = true, activateSpecials = false) {
                    this.commands = commands;
                    this.inBrowser = inBrowser;
                    this.bypassKey = bypassKey;
                    this.bypassed = bypassed;
                    this.log = log;
                    this.activateSpecials = activateSpecials;
                    this.bypass = false;
                    const self = this;
                    this.kd = (e) => self.onKeyDown(e);
                    this.attach();
                }
                static modedKeyName(c, kc, e) {
                    this._ks.length = 0;
                    const b = this._mods.reduce((p, m) => {
                        if (e[m.s + 'Key'])
                            this._ks.push(m.s);
                        return p && c != m.v;
                    }, true);
                    if (b)
                        this._ks.push('' + kc);
                    return this._ks.join('-');
                }
                attach() {
                    if (this.isAttached) {
                        return;
                    }
                    window.addEventListener('keydown', this.kd);
                    this.isAttached = true;
                }
                detach() {
                    if (!this.isAttached) {
                        return;
                    }
                    window.removeEventListener('keydown', this.kd);
                    this.isAttached = false;
                }
                getDoc(includeSpecials = false) {
                    const docs = this.commands.filter(c => !!c.doc && (includeSpecials || !c.special));
                    const makeDoc = (c) => ({
                        key: this.keyName(c.key),
                        section: c.section,
                        special: c.special,
                        doc: c.doc
                            + (c.default ? (' ' + c.default) : '')
                            + (c.shift ? (', shift: ' + c.shift) : '')
                            + (c.ctrl ? (', ctrl: ' + c.ctrl) : '')
                            + (c.alt ? (', alt: ' + c.alt) : '')
                            + (c.meta ? (', meta: ' + c.meta) : '')
                    });
                    return docs.map(makeDoc);
                }
                keyName(k) {
                    if (typeof k == 'string') {
                        return k;
                    }
                    switch (k) {
                        case KeyCommands._arrows: return 'arrows';
                        case KeyCommands._digits: return 'digits';
                        case KeyCommands._tab: return 'tab';
                        case KeyCommands._shift: return 'shift';
                        case KeyCommands._alt: return 'alt';
                        case KeyCommands._ctrl: return 'ctrl';
                        case KeyCommands._esc: return 'esc';
                        case KeyCommands._left: return 'left arrow';
                        case KeyCommands._up: return 'up arrow';
                        case KeyCommands._right: return 'right arrow';
                        case KeyCommands._down: return 'down arrow';
                        case KeyCommands._meta: return 'meta/windows';
                    }
                }
                _onUserEvent(e) { if (this.bypassed)
                    this.bypassed(e); }
                onKeyDown(e) {
                    const c = e.charCode || e.keyCode, k = String.fromCharCode(c);
                    const kc = /[A-Z|0-9]/.test(k) ? k : c;
                    if (this.bypass) {
                        this._onUserEvent(e);
                        if (k != this.bypassKey)
                            return;
                    }
                    const shift = e.shiftKey, ctrl = e.ctrlKey, alt = e.altKey, meta = e.metaKey;
                    if (this.isModifier(kc)
                        || this.isOs(kc, shift, ctrl, alt, meta)
                        || this.inBrowser && this.isBrowser(kc, shift, ctrl, alt, meta)) {
                        return;
                    }
                    const action = (filter, isDigit = false) => {
                        const cmd = this.commands.find(c => filter(c));
                        if (!cmd)
                            return false;
                        if (cmd.special && !this.activateSpecials)
                            return true;
                        if (isDigit)
                            cmd.action(shift, ctrl, alt, meta, parseInt(kc, 10));
                        else
                            cmd.action(shift, ctrl, alt, meta);
                        return true;
                    };
                    const mkn = KeyCommands.modedKeyName(c, kc, e);
                    if (!action(c => c.key == mkn)
                        && (!this.isDigit(k) || !action(c => c.key == KeyCommands._digits, true))
                        && !action(c => c.key == kc)
                        && this.log)
                        console.log('key: ' + k, c);
                }
                isModifier(k) {
                    return k == KeyCommands._shift
                        || k == KeyCommands._ctrl
                        || k == KeyCommands._alt
                        || k == KeyCommands._meta;
                }
                isDigit(k) {
                    return typeof k == 'string' && KeyCommands._digitChars.some(d => d == k);
                }
                isOs(k, shift, ctrl, alt, meta) {
                    return ctrl && k == KeyCommands._tab;
                }
                isBrowser(k, shift, ctrl, alt, meta) {
                    return meta && k == 'R'
                        || alt && meta && k == 'I';
                }
            };
            KeyCommands._arrows = -2;
            KeyCommands._digits = -1;
            KeyCommands._tab = 9;
            KeyCommands._shift = 16;
            KeyCommands._alt = 17;
            KeyCommands._ctrl = 18;
            KeyCommands._esc = 27;
            KeyCommands._left = 37;
            KeyCommands._up = 38;
            KeyCommands._right = 39;
            KeyCommands._down = 40;
            KeyCommands._meta = 91;
            KeyCommands._digitChars = Array.from('0123456789');
            KeyCommands._mods = [
                { s: 'shift', v: KeyCommands._shift },
                { s: 'alt', v: KeyCommands._alt },
                { s: 'ctrl', v: KeyCommands._ctrl },
                { s: 'meta', v: KeyCommands._meta }
            ];
            KeyCommands._ks = new Array();
            exports_5("KeyCommands", KeyCommands);
        }
    };
});
System.register("client/fract2D/Fract2DKeys", ["shared/KeyCommands"], function (exports_6, context_6) {
    "use strict";
    var __moduleName = context_6 && context_6.id;
    var KeyCommands_1, Fract2DKeys;
    return {
        setters: [
            function (KeyCommands_1_1) {
                KeyCommands_1 = KeyCommands_1_1;
            }
        ],
        execute: function () {
            Fract2DKeys = class Fract2DKeys extends KeyCommands_1.KeyCommands {
                constructor(inBrowser, state, tool, cancel, bypassed, log, activateSpecials) {
                    super([
                        {
                            key: KeyCommands_1.KeyCommands._digits, action: (shift, ctrl, alt, meta, digit) => state.go(digit + 10 * (meta ? 4 : alt ? 3 : ctrl ? 2 : shift ? 1 : 0)),
                            doc: "presets", default: "0-9", shift: "10-19", ctrl: "20-29" //, alt: "30-39", meta: "40-49"
                        },
                        { key: KeyCommands_1.KeyCommands._right, action: () => state.inc('a', false) },
                        { key: KeyCommands_1.KeyCommands._left, action: () => state.inc('a', true) },
                        { key: KeyCommands_1.KeyCommands._up, action: () => state.inc('b', false) },
                        { key: KeyCommands_1.KeyCommands._down, action: () => state.inc('b', true) },
                        { key: KeyCommands_1.KeyCommands._arrows, doc: "step right/left/up/down" },
                        { key: 'Z', action: (shift, ctrl) => state.times('z', shift, ctrl), doc: "zoom", default: "in 10%", shift: "out", ctrl: "200%" },
                        { key: 'M', action: (shift, ctrl) => state.times('m', shift, ctrl, true), doc: "detail", default: "increase", shift: "decrease", ctrl: "double" },
                        { key: 'R', action: (shift, ctrl) => state.times('r', shift, ctrl), doc: "radius", default: "increase", shift: "decrease", ctrl: "double" },
                        { key: 'V', action: () => state.toggle('invert'), doc: "invert colors" },
                        { key: 'C', action: () => state.toggle('color'), doc: "colors/gray gradient" },
                        { key: 'E', action: (shift) => state.next('easing', shift), doc: "gradient easing type" },
                        { key: KeyCommands_1.KeyCommands._esc, action: () => cancel(), doc: "cancel progress" },
                        { key: 'K', action: () => state.toggle('mask'), doc: "show optimization mask" },
                        { key: 'B', action: () => state.back(), doc: "revert to last location/parameters" },
                        { key: 'S', action: () => tool('Point'), doc: "show info about point under pointer" },
                        { key: 'G', action: () => tool('Helpers'), doc: "show guides" },
                        { key: 'D', action: () => tool('Path'), doc: "draw path from point under pointer" },
                        { key: 'J', action: () => tool('Fractions'), doc: "coordinates as whole fractions" },
                        { key: 'I', action: () => tool('Info'), doc: "info panel" },
                        { key: 'A', action: () => tool('Map'), doc: "minimap" },
                        { key: 'N', action: () => tool('CarAngle'), doc: "draw insectection cardioid/segment to pointer" },
                        { key: 'Q', action: () => tool('Edges'), doc: "edges and zones" },
                        { key: 'T', action: () => tool('test'), doc: "testing programs" },
                        { key: 'X', action: (shift) => tool('save', shift), doc: "save to file", shift: "paste from file" },
                        { key: 'F', action: () => tool('FullScreen'), doc: "fullscreen on/off" },
                        { key: 'Y', action: () => tool('Switch'), doc: "switch to 3D" },
                        { key: 'H', action: () => tool('Doc'), doc: "commands list" },
                        { special: true, key: 'P', action: () => state.toggle('progress'), doc: "show progress bar" },
                        { special: true, key: 'W', action: () => state.toggle('worker'), doc: "process in background" },
                        { special: true, key: 'O', action: () => state.toggle('optimize'), doc: "optimize (circle and cardioid)" },
                        { special: true, key: 'U', action: () => state.toggle('parts'), doc: "optimize (skip visible image parts)" },
                        { special: true, key: 'L', action: () => tool('log'), doc: "message logging (in console)" },
                    ], inBrowser, 'T', bypassed, log, activateSpecials);
                }
            };
            exports_6("Fract2DKeys", Fract2DKeys);
        }
    };
});
System.register("shared/Matrix2D", [], function (exports_7, context_7) {
    "use strict";
    var __moduleName = context_7 && context_7.id;
    var Matrix2D;
    return {
        setters: [],
        execute: function () {
            /** 3x2, columns first */
            Matrix2D = class Matrix2D {
                static multiply(m1, m2, result) {
                    return (result || new Matrix2D(false)).set(m1.a * m2.a + m1.c * m2.b, m1.b * m2.a + m1.d * m2.b, m1.a * m2.c + m1.c * m2.d, m1.b * m2.c + m1.d * m2.d, m1.a * m2.e + m1.c * m2.f + m1.e, m1.b * m2.e + m1.d * m2.f + m1.f);
                }
                static inverse(m, result) {
                    const ad_bc = m.a * m.d - m.b * m.c;
                    return (result || new Matrix2D(false)).set(m.d / ad_bc, m.b / -ad_bc, m.c / -ad_bc, m.a / ad_bc, (m.d * m.e - m.c * m.f) / -ad_bc, (m.b * m.e - m.a * m.f) / ad_bc);
                }
                static translation(tx, ty = 0) {
                    return new Matrix2D(false).setAsTranslate(tx, ty);
                }
                static scale(sx, sy) {
                    return new Matrix2D(false).setAsScale(sx, sy);
                }
                static rotation(angle) {
                    return new Matrix2D(false).setAsRotate(angle);
                }
                /** a, b, c => i.m(c).m(b).m(a) */
                static transformLR(...matrices) {
                    switch (matrices.length) {
                        case 0: return new Matrix2D();
                        case 1: return new Matrix2D(matrices[0]);
                        default: return matrices.reduceRight((p, c) => c ? p.multiplyBy(c) : p, new Matrix2D());
                    }
                }
                /** a, b, c => i.m(a).m(b).m(c) */
                static transformRL(...matrices) {
                    switch (matrices.length) {
                        case 0: return new Matrix2D();
                        case 1: return new Matrix2D(matrices[0]);
                        default: return matrices.reduce((p, c) => c ? p.multiplyBy(c) : p, new Matrix2D());
                    }
                }
                static rotationAround(cx, cy, angle) {
                    return Matrix2D.transformLR(Matrix2D.translation(cx, cy), Matrix2D.rotation(angle), Matrix2D.translation(-cx, -cy));
                }
                static flipX() {
                    return Matrix2D.scale(-1, 1);
                }
                static flipY() {
                    return Matrix2D.scale(1, -1);
                }
                static flipXY() {
                    return Matrix2D.scale(-1, -1);
                }
                constructor(m) {
                    if (m == undefined) {
                        this.setIdentity();
                    }
                    else if (m !== false) {
                        this.copy(m);
                    }
                }
                set(a, b, c, d, e, f) {
                    this.a = a;
                    this.c = c;
                    this.e = e;
                    this.b = b;
                    this.d = d;
                    this.f = f;
                    return this;
                }
                setIdentity() {
                    this.a = 1;
                    this.c = 0;
                    this.e = 0;
                    this.b = 0;
                    this.d = 1;
                    this.f = 0;
                    return this;
                }
                copy(m) {
                    this.a = m.a;
                    this.c = m.c;
                    this.e = m.e;
                    this.b = m.b;
                    this.d = m.d;
                    this.f = m.f;
                    return this;
                }
                setAsTranslate(tx, ty = 0) {
                    this.a = 1;
                    this.c = 0;
                    this.e = tx;
                    this.b = 0;
                    this.d = 1;
                    this.f = ty;
                    return this;
                }
                setAsScale(sx, sy) {
                    if (sy == undefined) {
                        sy = sx;
                    }
                    this.a = sx;
                    this.c = 0;
                    this.e = 0;
                    this.b = 0;
                    this.d = sy;
                    this.f = 0;
                    return this;
                }
                setAsRotate(angle) {
                    const cos = Math.cos(angle);
                    const sin = Math.sin(angle);
                    this.a = cos;
                    this.c = -sin;
                    this.e = 0;
                    this.b = sin;
                    this.d = cos;
                    this.f = 0;
                    return this;
                }
                multiplyBy(m) {
                    return Matrix2D.multiply(this, m, this);
                }
                inverse() {
                    return Matrix2D.inverse(this, this);
                }
                clone() {
                    return new Matrix2D(this);
                }
                getInverse() {
                    return this.clone().inverse();
                }
                getMultiplyBy(m) {
                    return this.clone().multiplyBy(m);
                }
                transformX(x, y) {
                    return this.a * x + this.c * y + this.e;
                }
                transformY(x, y) {
                    return this.b * x + this.d * y + this.f;
                }
                transformPoint(p) {
                    const x = p.x, y = p.y;
                    p.x = this.a * x + this.c * y + this.e;
                    p.y = this.b * x + this.d * y + this.f;
                    return p;
                }
                transformRegion(r) {
                    const { xmin, ymin, xmax, ymax } = r;
                    r.xmin = this.a * xmin + this.c * ymin + this.e;
                    r.ymin = this.b * xmin + this.d * ymin + this.f;
                    r.xmax = this.a * xmax + this.c * ymax + this.e;
                    r.ymax = this.b * xmax + this.d * ymax + this.f;
                    return r;
                }
                transformRect(r) {
                    let x = r.x, y = r.y;
                    r.x = this.a * x + this.c * y + this.e;
                    r.y = this.b * x + this.d * y + this.f;
                    x += r.width;
                    y += r.height;
                    const a = this.a * x + this.c * y + this.e;
                    const b = this.b * x + this.d * y + this.f;
                    r.width = a - r.x;
                    r.height = b - r.y;
                    return r;
                }
                transformXYs(xys, result) {
                    result = result || xys;
                    const l = xys && xys.length;
                    for (let i = 0; i < l; i += 2) {
                        let x = xys[i], y = xys[i + 1];
                        result[i] = this.a * x + this.c * y + this.e;
                        result[i + 1] = this.b * x + this.d * y + this.f;
                    }
                    return result;
                }
            };
            exports_7("Matrix2D", Matrix2D);
        }
    };
});
System.register("shared/Region2D", [], function (exports_8, context_8) {
    "use strict";
    var __moduleName = context_8 && context_8.id;
    function rgba(ss) {
        return typeof ss == 'string' ? ss
            : ss.a == undefined ? `rgb(${ss.r},${ss.g},${ss.b})`
                : `rgba(${ss.r},${ss.g},${ss.b},${(ss.a / 255).toFixed(2)})`;
    }
    var Region2D;
    return {
        setters: [],
        execute: function () {
            Region2D = class Region2D {
                constructor(xmin, xmax, ymin, ymax, topIsYMax) {
                    this.xmin = xmin;
                    this.xmax = xmax;
                    this.ymin = ymin;
                    this.ymax = ymax;
                    this.topIsYMax = topIsYMax;
                    //console.log(xmin, xmax, ymin, ymax, topIsYMax)
                }
                static scaleFactorToFit(out, sw, sh, dw, dh) {
                    return sh * (out ? sw / dw : dw / sw) > dh ? (dh / sh) : (dw / sw);
                }
                static draw(r, ctx, strokeStyle, fill = false) {
                    ctx.beginPath();
                    if (fill) {
                        if (strokeStyle) {
                            ctx.fillStyle = rgba(strokeStyle);
                        }
                        ctx.fillRect(r.left - .5, r.top - .5, r.width, r.height);
                    }
                    else {
                        if (strokeStyle) {
                            ctx.strokeStyle = rgba(strokeStyle);
                        }
                        ctx.strokeRect(r.left - .5, r.top - .5, r.width, r.height);
                    }
                }
                static zeroCentered(width = 1, height) {
                    if (height == undefined) {
                        height = width;
                    }
                    return new Region2D(-width / 2, width / 2, -height / 2, height / 2, true);
                }
                static fromScreen(widthOrSize, height) {
                    const size = typeof widthOrSize == 'object' ? widthOrSize : { width: widthOrSize, height: height };
                    return Region2D.fromSize(size, false);
                }
                static fromSize(r, topIsYMax = false) {
                    return Region2D.fromXYWH(0, 0, r.width, r.height, topIsYMax);
                }
                static fromCenterAndSize(x, y, w, h, topIsYMax = false) {
                    return Region2D.fromXYWH(x - w / 2, y - w / 2, w, h, topIsYMax);
                }
                static fromRect(r, topIsYMax = false) {
                    return Region2D.fromXYWH(r.x, r.y, r.width, r.height, topIsYMax);
                }
                static fromXYWH(x, y, w, h, topIsYMax = false) {
                    return new Region2D(x, x + w, y, y + h, topIsYMax);
                }
                static fromRMinMax(r) {
                    return new Region2D(r.xmin, r.xmax, r.ymin, r.ymax, r.topIsYMax);
                }
                static fromPoints(p1, p2, topIsYMax) {
                    return new Region2D(p1.x < p2.x ? p1.x : p2.x, p1.x > p2.x ? p1.x : p2.x, p1.y < p2.y ? p1.y : p2.y, p1.y > p2.y ? p1.y : p2.y, topIsYMax);
                }
                toString() { return `x:${this.xmin},${this.xmax} y:${this.ymin},${this.ymax}`; }
                set x(value) { this.xmin = value; }
                set y(value) { if (this.topIsYMax) {
                    this.ymax = value;
                }
                else {
                    this.ymin = value;
                } }
                set width(value) { this.xmax = this.xmin + Math.abs(value); }
                set height(value) { this.ymax = this.ymin + Math.abs(value); }
                get x() { return this.xmin; }
                get y() { return this.topIsYMax ? this.ymax : this.ymin; }
                get width() { return Math.abs(this.xmax - this.xmin); }
                get height() { return Math.abs(this.ymax - this.ymin); }
                get top() { return this.topIsYMax ? this.ymax : this.ymin; }
                get left() { return this.xmin; }
                get bottom() { return this.topIsYMax ? this.ymin : this.ymax; }
                get right() { return this.xmax; }
                get cx() { return (this.xmax + this.xmin) / 2; }
                get cy() { return (this.ymax + this.ymin) / 2; }
                get ratio() { return this.width / this.height; }
                get isPortrait() { return this.ratio < 1; }
                get isSquare() { return this.ratio == 1; }
                get isLandscape() { return this.ratio > 1; }
                get area() { return this.width * this.height; }
                get maxLength() { return Math.max(this.width, this.height); }
                get minLength() { return Math.min(this.width, this.height); }
                containsX(x) { return this.xmin <= x && x <= this.xmax; }
                containsY(y) { return this.ymin <= y && y <= this.ymax; }
                containsXY(x, y) {
                    return this.xmin <= x && x <= this.xmax
                        && this.ymin <= y && y <= this.ymax;
                }
                containsPoint(p) { return this.containsXY(p.x, p.y); }
                scaleFactorToFit(r, out) {
                    return Region2D.scaleFactorToFit(out, this.width, this.height, r.width, r.height);
                }
                equals(r, strict = false) {
                    if (this.xmin == r.xmin && this.xmax == r.xmax
                        && this.ymin == r.ymin && this.ymax == r.ymax) {
                        return true;
                    }
                    if (!strict) {
                        return this.xmax == r.xmin && this.xmin == r.xmax
                            && this.ymax == r.ymin && this.ymin == r.ymax;
                    }
                }
                is(xmin, xmax, ymin, ymax, strict = true) {
                    return this.xmin == xmin && this.xmax == xmax && (strict || this.xmax == xmin && this.xmin == xmax)
                        && this.ymin == ymin && this.ymax == ymax && (strict || this.ymax == ymin && this.ymin == ymax);
                }
                isSameSize(r) { return this.width == r.width && this.height == r.height; }
                isBiggerThan(r) { return this.width > r.width || this.height > r.height; }
                intersects(r) {
                    return this.xmax < r.xmin || r.xmax < this.xmin
                        || r.ymax < this.ymin || this.ymax < r.ymin ? false : true;
                }
                surrounds(xmin, xmax, ymin, ymax, strict = false) {
                    return strict ?
                        this.xmin < xmin && xmax < this.xmax
                            && this.ymin < ymin && ymax < this.ymax
                        : this.xmin <= xmin && xmax <= this.xmax
                            && this.ymin <= ymin && ymax <= this.ymax;
                }
                surroundsR(r, strict = false) {
                    return this.surrounds(r.xmin, r.xmax, r.ymin, r.ymax, strict);
                }
                getRatioAsXY() { return { x: this.width, y: this.height }; }
                center() { return { x: this.cx, y: this.cy }; }
                topLeft() { return { x: this.top, y: this.left }; }
                topRight() { return { x: this.top, y: this.right }; }
                bottomLeft() { return { x: this.bottom, y: this.left }; }
                bottomRight() { return { x: this.bottom, y: this.right }; }
                clone() { return new Region2D(this.xmin, this.xmax, this.ymin, this.ymax, this.topIsYMax); }
                getIntersection(r) { return this.clone().intersect(r); }
                getUnion(r) { return this.clone().union(r); }
                getPartsTo(dst, preferColumns = false) {
                    const parts = new Array();
                    if (!this.intersects(dst)) {
                        return parts;
                    }
                    const inter = this.getIntersection(dst);
                    if (inter.width == 0 || inter.height == 0) {
                        return parts;
                    }
                    function sortDistinct(ns) {
                        return ns.reduce((p, c) => {
                            if (!p.some(o => o == c)) {
                                p.push(c);
                            }
                            return p;
                        }, new Array()).sort((a, b) => a - b);
                    }
                    const xs = sortDistinct([this.xmin, this.xmax, dst.xmin, dst.xmax]), ys = sortDistinct([this.ymin, this.ymax, dst.ymin, dst.ymax]);
                    //consecutive x-pairs
                    for (let i = 1; i < xs.length; i++) {
                        const xmin = xs[i - 1], xmax = xs[i];
                        //consecutive y-pairs
                        for (let j = 1; j < ys.length; j++) {
                            const ymin = ys[j - 1], ymax = ys[j];
                            //each small rect
                            //exclude if not in destination or is intersection
                            if (!dst.surrounds(xmin, xmax, ymin, ymax) || inter.is(xmin, xmax, ymin, ymax, true)) {
                                continue;
                            }
                            //find neighbour
                            const t = parts.find(t => preferColumns ?
                                t.xmin == xmin && t.xmax == xmax && (t.ymin == ymax || t.ymax == ymin)
                                : t.ymin == ymin && t.ymax == ymax && (t.xmin == xmax || t.xmax == xmin));
                            if (t) {
                                if (preferColumns) {
                                    t.ymin = Math.min(t.ymin, ymin), t.ymax = Math.max(t.ymax, ymax);
                                }
                                else {
                                    t.xmin = Math.min(t.xmin, xmin), t.xmax = Math.max(t.xmax, xmax);
                                }
                            }
                            else {
                                parts.push(new Region2D(xmin, xmax, ymin, ymax, this.topIsYMax));
                            }
                        }
                    }
                    //intersection first
                    parts.unshift(inter);
                    return parts;
                }
                copy(r) {
                    this.xmin = r.xmin;
                    this.xmax = r.xmax;
                    this.ymin = r.ymin;
                    this.ymax = r.ymax;
                    this.topIsYMax = r.topIsYMax;
                    return this;
                }
                setMin(x, y) { this.xmin = x, this.ymin = y; return this; }
                setMax(x, y) { this.xmax = x, this.ymax = y; return this; }
                setX(min, max) { this.xmin = min, this.xmax = max; return this; }
                setY(min, max) { this.ymin = min, this.ymax = max; return this; }
                floorX() { this.xmin = Math.floor(this.xmin); this.xmax = Math.floor(this.xmax); return this; }
                floorY() { this.ymin = Math.floor(this.ymin); this.ymax = Math.floor(this.ymax); return this; }
                ceilX() { this.xmin = Math.ceil(this.xmin); this.xmax = Math.ceil(this.xmax); return this; }
                ceilY() { this.ymin = Math.ceil(this.ymin); this.ymax = Math.ceil(this.ymax); return this; }
                roundX() { this.xmin = Math.round(this.xmin); this.xmax = Math.round(this.xmax); return this; }
                roundY() { this.ymin = Math.round(this.ymin); this.ymax = Math.round(this.ymax); return this; }
                truncX() { this.xmin = Math.trunc(this.xmin); this.xmax = Math.trunc(this.xmax); return this; }
                truncY() { this.ymin = Math.trunc(this.ymin); this.ymax = Math.trunc(this.ymax); return this; }
                floor() { return this.floorX().floorY(); }
                ceil() { return this.ceilX().ceilY(); }
                round() { return this.roundX().roundY(); }
                trunc() { return this.truncX().truncY(); }
                floorMinCeilMax() {
                    this.xmin = Math.floor(this.xmin);
                    this.ymin = Math.floor(this.ymin);
                    this.xmax = Math.ceil(this.xmax);
                    this.ymax = Math.ceil(this.ymax);
                    return this;
                }
                flipX() {
                    const t = this.xmin;
                    this.xmin = this.xmax;
                    this.xmax = t;
                    return this;
                }
                flipY() {
                    const t = this.ymin;
                    this.ymin = this.ymax;
                    this.ymax = t;
                    return this;
                }
                flip(flipX = false, flipY = false) {
                    if (flipY) {
                        this.flipY();
                    }
                    if (flipX) {
                        this.flipX();
                    }
                    return this;
                }
                conform() {
                    return this.flip(this.xmin > this.xmax, this.ymin > this.ymax);
                }
                translate(x, y) {
                    this.xmin += x;
                    this.xmax += x;
                    this.ymin += y;
                    this.ymax += y;
                    return this;
                }
                scale(sf, sy) {
                    if (sy == undefined) {
                        sy = sf;
                    }
                    this.xmin *= sf;
                    this.xmax *= sf;
                    this.ymin *= sy;
                    this.ymax *= sy;
                    return this;
                }
                centerOn(x, y) {
                    return this.translate(x == undefined ? 0 : x - this.cx, y == undefined ? 0 : y - this.cy);
                }
                topLeftOn(x, y) {
                    return this.translate(x == undefined ? 0 : x - this.left, y == undefined ? 0 : y - this.top);
                }
                maxTo(widthOrSize, height) {
                    const size = typeof widthOrSize == 'object' ? widthOrSize : { width: widthOrSize, height: height };
                    const sf = this.scaleFactorToFit(size, false);
                    this.width *= sf;
                    this.height *= sf;
                    return this;
                }
                maxToCentered(r) {
                    return this.maxTo(r)
                        .translate(r.x + r.width / 2 - this.cx, r.y + r.height / 2 - this.cy);
                }
                clamp(maxWidth, maxHeight) {
                    if (maxWidth != false && this.width < maxWidth) {
                        this.width = maxWidth;
                    }
                    if (maxHeight == undefined)
                        maxHeight = maxWidth;
                    if (maxHeight != false && this.height < maxHeight) {
                        this.height = maxHeight;
                    }
                    return this;
                }
                scaleOnCenter(sf, sy) {
                    const x = this.cx, y = this.cy;
                    return this.scale(sf, sy).centerOn(x, y);
                }
                scaleOnTopLeft(sf, sy) {
                    const x = this.left, y = this.top;
                    return this.scale(sf, sy).topLeftOn(x, y);
                }
                fitTo(r, out, topLeft = false) {
                    const sf = this.scaleFactorToFit(r, out);
                    return topLeft ? this.scale(sf).topLeftOn(r.left, r.top)
                        : this.scale(sf).centerOn(r.cx, r.cy);
                }
                intersect(r) {
                    this.xmin = Math.max(this.xmin, r.xmin);
                    this.xmax = Math.min(this.xmax, r.xmax);
                    this.ymin = Math.max(this.ymin, r.ymin);
                    this.ymax = Math.min(this.ymax, r.ymax);
                    return this;
                }
                union(r) {
                    this.xmin = Math.min(this.xmin, r.xmin);
                    this.xmax = Math.min(this.ymin, r.ymin);
                    this.ymin = Math.max(this.xmax, r.ymax);
                    this.ymax = Math.max(this.ymax, r.ymax);
                    return this;
                }
                draw(ctx, strokeStyle, fill = false) {
                    Region2D.draw(this, ctx, strokeStyle, fill);
                }
            };
            exports_8("Region2D", Region2D);
        }
    };
});
System.register("shared/Transform2D", ["shared/Matrix2D", "shared/Region2D"], function (exports_9, context_9) {
    "use strict";
    var __moduleName = context_9 && context_9.id;
    var Matrix2D_1, Region2D_1, Transform2D;
    return {
        setters: [
            function (Matrix2D_1_1) {
                Matrix2D_1 = Matrix2D_1_1;
            },
            function (Region2D_1_1) {
                Region2D_1 = Region2D_1_1;
            }
        ],
        execute: function () {
            Transform2D = class Transform2D {
                constructor(vw, vh, ww = 4, wh = 4, worldIsZeroCentered = true, worldTopIsYmax = true) {
                    this.vw = vw;
                    this.vh = vh;
                    this.ww = ww;
                    this.wh = wh;
                    this.worldIsZeroCentered = worldIsZeroCentered;
                    this.worldTopIsYmax = worldTopIsYmax;
                    this.mwt = Transform2D.makeWorldToTarget(0, 0, 1);
                    this.updateView(vw, vh);
                }
                static makeTarget(wx, wy, zoom, vw, vh, result) {
                    const mvw = this.makeViewToWorld(vw, vh), mwt = this.makeWorldToTarget(wx, wy, zoom);
                    result = result || Region2D_1.Region2D.fromScreen(vw, vh);
                    return this.transformToTarget(result, mvw, mwt);
                }
                static makeViewToWorld(vw, vh, ww = 4, wh = 4, worldIsZeroCentered = true, worldTopIsYmax = true) {
                    return Matrix2D_1.Matrix2D.transformLR(Matrix2D_1.Matrix2D.translation(-vw / 2, -vh / 2), worldTopIsYmax ? Matrix2D_1.Matrix2D.flipY() : null, Matrix2D_1.Matrix2D.scale(Region2D_1.Region2D.scaleFactorToFit(true, vw, vh, ww, wh)), worldIsZeroCentered ? null : Matrix2D_1.Matrix2D.translation(-ww / 2, -wh / 2));
                }
                static makeWorldToTarget(wx, wy, zoom) {
                    return Matrix2D_1.Matrix2D.transformLR(Matrix2D_1.Matrix2D.scale(1 / zoom), Matrix2D_1.Matrix2D.translation(wx, wy));
                }
                static makeViewToTarget(mvw, mwt) {
                    return Matrix2D_1.Matrix2D.transformLR(mvw, mwt);
                }
                static transformToTarget(vr, mvw, mwt, flipY = true) {
                    const tr = this.makeViewToTarget(mvw, mwt).transformRegion(vr);
                    if (flipY) {
                        tr.flipY();
                    }
                    return tr;
                }
                updateView(vw, vh) {
                    this.vw = vw, this.vh = vh;
                    this.mvw = Transform2D.makeViewToWorld(vw, vh, this.ww, this.wh, this.worldIsZeroCentered, this.worldTopIsYmax);
                    this.mvt = Transform2D.makeViewToTarget(this.mvw, this.mwt);
                    this.mtv = this.mvt.getInverse();
                    return this;
                }
                setTarget(wx, wy, zoom) {
                    const mwt = Transform2D.makeWorldToTarget(wx, wy, zoom || 1);
                    return this.setTargetTransform(mwt);
                }
                setTargetTransform(mwt) {
                    this.mwt = mwt;
                    this.mvt = Transform2D.makeViewToTarget(this.mvw, this.mwt);
                    this.mtv = this.mvt.getInverse();
                    return this;
                }
                toTarget(vr, flipY) {
                    return this.mvt.transformRegion(vr).flip(false, flipY);
                }
                toOtherTarget(vr, wx, wy, zoom, flipY) {
                    const mvt = Transform2D.makeViewToTarget(this.mvw, Transform2D.makeWorldToTarget(wx, wy, zoom));
                    return mvt.transformRegion(vr).flip(false, flipY);
                }
                xToTarget(x, y) {
                    return this.mvt.transformX(x, y);
                }
                yToTarget(x, y) {
                    return this.mvt.transformY(x, y);
                }
                pointToTarget(p) {
                    return this.mvt.transformPoint(p);
                }
                regionToTarget(r) {
                    return this.mvt.transformRegion(r);
                }
                rectToTarget(r) {
                    return this.mvt.transformRect(r);
                }
                xysToTarget(xys, result) {
                    return this.mvt.transformXYs(xys, result);
                }
                toView(rt, flipY) {
                    return this.mtv.transformRegion(rt).flip(false, flipY);
                }
                xToView(x, y) {
                    return this.mtv.transformX(x, y);
                }
                yToView(x, y) {
                    return this.mtv.transformY(x, y);
                }
                pointToView(p) {
                    return this.mtv.transformPoint(p);
                }
                regionToView(r) {
                    return this.mtv.transformRegion(r);
                }
                rectToView(r) {
                    return this.mtv.transformRect(r);
                }
                xysToView(xys, result) {
                    return this.mtv.transformXYs(xys, result);
                }
                locFromViewRect(r, currentZoom) {
                    const rv = Region2D_1.Region2D.fromRect(r), zoomFactor = Region2D_1.Region2D.scaleFactorToFit(false, rv.width, rv.height, this.vw, this.vh);
                    return this.locFromView(rv.cx, rv.cy, currentZoom, zoomFactor);
                }
                locFromView(x, y, currentZoom, zoomFactor = 2, inplace = false) {
                    if (inplace) {
                        const v = Region2D_1.Region2D.fromScreen(this.vw, this.vh), c = this.pointToTarget(v.center()), p = this.pointToTarget({ x, y }), tx = (c.x - p.x) * currentZoom, ty = (c.y - p.y) * currentZoom, newZoom = currentZoom * zoomFactor, mwt = Matrix2D_1.Matrix2D.transformLR(Matrix2D_1.Matrix2D.translation(tx, ty), Matrix2D_1.Matrix2D.scale(1 / newZoom), Matrix2D_1.Matrix2D.translation(p.x, p.y)), mvt = Transform2D.makeViewToTarget(this.mvw, mwt), q = mvt.transformPoint(v.center());
                        return { a: q.x, b: q.y, z: newZoom };
                    }
                    else {
                        return {
                            a: this.xToTarget(x, y),
                            b: this.yToTarget(x, y),
                            z: currentZoom * zoomFactor
                        };
                    }
                }
            };
            exports_9("Transform2D", Transform2D);
        }
    };
});
System.register("shared/Color", ["shared/Easing"], function (exports_10, context_10) {
    "use strict";
    var __moduleName = context_10 && context_10.id;
    var Easing_3, Color, ColoR;
    return {
        setters: [
            function (Easing_3_1) {
                Easing_3 = Easing_3_1;
            }
        ],
        execute: function () {
            Color = class Color {
                constructor(r = 0, g = 0, b = 0, a = 255) {
                    this.r = r;
                    this.g = g;
                    this.b = b;
                    this.a = a;
                }
                static text(r, g, b, a) {
                    return a == undefined ? `rgb(${r},${g},${b})` : `rgba(${r},${g},${b},${(a / 255).toFixed(2)}`;
                }
                static get(key) {
                    let cr = this._instances.get(key);
                    if (cr != undefined) {
                        return cr;
                    }
                    cr = new ColoR(this[key]());
                    this._instances.set(key, cr);
                    return cr;
                }
                static get Black() { return this.get('black'); }
                static get Red() { return this.get('red'); }
                static get LRed() { return this.get('pink'); }
                static get MRed() { return this.get('mred'); }
                static get DRed() { return this.get('dred'); }
                static get Yellow() { return this.get('yellow'); }
                static get MYellow() { return this.get('myellow'); }
                static get Green() { return this.get('green'); }
                static get MGreen() { return this.get('mgreen'); }
                static get LGreen() { return this.get('lgreen'); }
                static get DGreen() { return this.get('dgreen'); }
                static get Cyan() { return this.get('cyan'); }
                static get MCyan() { return this.get('mcyan'); }
                static get Blue() { return this.get('blue'); }
                static get MBlue() { return this.get('mblue'); }
                static get LBlue() { return this.get('lblue'); }
                static get DBlue() { return this.get('dblue'); }
                static get Magenta() { return this.get('magenta'); }
                static get MMagenta() { return this.get('mmagenta'); }
                static get White() { return this.get('white'); }
                static get DGray() { return this.get('dgray'); }
                static get Gray() { return this.get('gray'); }
                static get LGray() { return this.get('lgray'); }
                static get Pink() { return this.get('pink'); }
                static get MidYellow() { return this.get('midyellow'); }
                static get Orange() { return this.get('orange'); }
                static Bool(b) {
                    return b == undefined ? this.Blue : b ? this.Green : this.Red;
                }
                static black() { return new Color(0, 0, 0); }
                static red() { return new Color(255, 0, 0); }
                static mred() { return new Color(128, 0, 0); }
                static dred() { return new Color(64, 0, 0); }
                static yellow() { return new Color(255, 255, 0); }
                static myellow() { return new Color(128, 128, 0); }
                static green() { return new Color(0, 255, 0); }
                static mgreen() { return new Color(0, 128, 0); }
                static dgreen() { return new Color(0, 64, 0); }
                static cyan() { return new Color(0, 255, 255); }
                static mcyan() { return new Color(0, 128, 128); }
                static blue() { return new Color(0, 0, 255); }
                static mblue() { return new Color(0, 0, 128); }
                static dblue() { return new Color(0, 0, 64); }
                static magenta() { return new Color(255, 0, 255); }
                static mmagenta() { return new Color(128, 0, 128); }
                static white() { return new Color(255, 255, 255); }
                static dgray() { return new Color(64, 64, 64); }
                static gray() { return new Color(128, 128, 128); }
                static lgray() { return new Color(192, 192, 192); }
                static pink() { return new Color(128, 80, 80); }
                static lgreen() { return new Color(128, 255, 128); }
                static lblue() { return new Color(173, 216, 230); }
                static midyellow() { return new Color(160, 160, 0); }
                static orange() { return new Color(255, 165, 0); }
                static bool(b) {
                    return b == undefined ? this.blue() : b ? this.green() : this.red();
                }
                static lerp(a, b, t) {
                    return new Color(Math.round(a.r + (b.r - a.r) * t), Math.round(a.g + (b.g - a.g) * t), Math.round(a.b + (b.b - a.b) * t), Math.round(a.a + (b.a - a.a) * t));
                }
                static getColorizer(color, colors, invert = false, easing) {
                    const ease = Easing_3.Easing.func(easing), colorize = colors ?
                        (t) => color.gradient7(ease(t))
                        : (t) => color.gray01(ease(t));
                    return invert ? (t) => colorize(t).invert() : colorize;
                }
                static getNormalizedColorizer(color, colors, invert, easing) {
                    const colorize = this.getColorizer(new Color(), colors, invert, easing);
                    return color.length > 3 ?
                        (t) => colorize(t).normalizedRGBA(color)
                        : (t) => colorize(t).normalizedRGB(color);
                }
                toString(withAlpha = true) {
                    return withAlpha ? Color.text(this.r, this.g, this.b, this.a) : Color.text(this.r, this.g, this.b);
                }
                average(withAlpha = false) {
                    return (this.r + this.g + this.b + (withAlpha ? this.a : 0)) / (withAlpha ? 4 : 3);
                }
                is(c, checkAlpha = false) {
                    return c
                        && c.r == this.r
                        && c.g == this.g
                        && c.b == this.b
                        && (!checkAlpha || c.a == this.a);
                }
                copy(c) {
                    return this.set(c.r, c.g, c.b, c.a);
                }
                set(r, g, b, a) {
                    if (r != undefined) {
                        this.r = r;
                    }
                    if (g != undefined) {
                        this.g = g;
                    }
                    if (b != undefined) {
                        this.b = b;
                    }
                    if (a != undefined) {
                        this.a = a;
                    }
                    return this;
                }
                setFromImageData(imd, x, y) {
                    const d = imd.data, i = y * imd.width + x;
                    return this.set(d[i], d[i + 1], d[i + 2], d[i + 3]);
                }
                setFromUint8ClampedArray(array, start = 0) {
                    return this.set(array[start], array[start + 1], array[start + 2], array[start + 3]);
                }
                gray(n, alpha) {
                    return this.set(n, n, n, alpha);
                }
                gray01(t, alpha) {
                    return this.gray(Math.round(t * 255), alpha);
                }
                gradient7(t, start = 0, end = 7, alpha) {
                    const a = t / (1 / Math.min(7, end - start));
                    const x = Math.floor(a);
                    const y = Math.floor(255 * (a - x));
                    let r, g, b;
                    switch (x + start) {
                        case 0:
                            r = y;
                            g = 0;
                            b = 0;
                            break; //noir - rouge
                        case 1:
                            r = 255;
                            g = y;
                            b = 0;
                            break; //rouge - jaune
                        case 2:
                            r = 255 - y;
                            g = 255;
                            b = 0;
                            break; //jaune - vert
                        case 3:
                            r = 0;
                            g = 255;
                            b = y;
                            break; //vert - cyan
                        case 4:
                            r = 0;
                            g = 255 - y;
                            b = 255;
                            break; //cyan - bleu
                        case 5:
                            r = y;
                            g = 0;
                            b = 255;
                            break; //bleu - magenta
                        case 6:
                            r = 255;
                            g = y;
                            b = 255;
                            break; //magenta - blanc
                        default:
                            r = 255;
                            g = 255;
                            b = 255;
                            break; //blanc
                    }
                    return this.set(r, g, b, alpha);
                }
                setLerp(a, b, t) {
                    this.r = Math.round(a.r + (b.r - a.r) * t);
                    this.g = Math.round(a.g + (b.g - a.g) * t);
                    this.b = Math.round(a.b + (b.b - a.b) * t);
                    this.a = Math.round(a.a + (b.a - a.a) * t);
                    return this;
                }
                randomize() {
                    this.r = Math.round(Math.random() * 255);
                    this.g = Math.round(Math.random() * 255);
                    this.b = Math.round(Math.random() * 255);
                    return this;
                }
                invert() {
                    this.r = 255 - this.r;
                    this.g = 255 - this.g;
                    this.b = 255 - this.b;
                    return this;
                }
                setAlpha01(t) {
                    if (t != undefined) {
                        this.a = Math.round(255 * t);
                    }
                    return this;
                }
                normalizedRGB(result) {
                    result[0] = this.r / 255;
                    result[1] = this.g / 255;
                    result[2] = this.b / 255;
                    return result;
                }
                normalizedRGBA(result) {
                    result[0] = this.r / 255;
                    result[1] = this.g / 255;
                    result[2] = this.b / 255;
                    result[3] = this.a / 255;
                    return result;
                }
            };
            Color._instances = new Map();
            exports_10("Color", Color);
            ColoR = class ColoR extends Color {
                constructor(c) {
                    super(c.r, c.g, c.b, c.a);
                    this._initDone = true;
                }
                get r() { return super.r; }
                get g() { return super.g; }
                get b() { return super.b; }
                get a() { return super.a; }
                set r(v) { if (this._initDone)
                    throw ColoR.roe;
                else
                    super.r = v; }
                set g(v) { if (this._initDone)
                    throw ColoR.roe;
                else
                    super.g = v; }
                set b(v) { if (this._initDone)
                    throw ColoR.roe;
                else
                    super.b = v; }
                set a(v) { if (this._initDone)
                    throw ColoR.roe;
                else
                    super.a = v; }
            };
            ColoR.roe = 'this color is readonly';
            exports_10("ColoR", ColoR);
        }
    };
});
System.register("shared/Pixels", ["shared/Color"], function (exports_11, context_11) {
    "use strict";
    var __moduleName = context_11 && context_11.id;
    function rgba(c) { return typeof c == 'string' ? c : Color_1.Color.text(c.r, c.g, c.b, c.a); }
    var Color_1, Pixels;
    return {
        setters: [
            function (Color_1_1) {
                Color_1 = Color_1_1;
            }
        ],
        execute: function () {
            Pixels = class Pixels {
                static clear(ctx, c = Color_1.Color.Black) {
                    ctx.beginPath();
                    ctx.fillStyle = c.toString();
                    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                }
                static clearAlpha(ctx, c = 0) {
                    if (!Pixels.noLog)
                        console.log('Pixels clearAlpha', c);
                    Pixels.withImageData(ctx, imd => Pixels.setAlpha(imd, c));
                }
                static withImageData(ctx, withData) {
                    const w = ctx.canvas.width, h = ctx.canvas.height;
                    //console.log('Pixels withImageData', w, h)
                    const imd = ctx.getImageData(0, 0, w, h);
                    //console.log('withImageData before', imd.data)
                    withData(imd);
                    //console.log('withImageData after', imd.data)
                    ctx.putImageData(imd, 0, 0);
                }
                static setAlpha(imd, alpha = 0) {
                    const data = imd.data, l = data.length;
                    for (let i = 0; i < l; i += 4) {
                        data[i + 3] = alpha;
                    }
                }
                static invert(ctx) {
                    if (!Pixels.noLog)
                        console.log('Pixels invert');
                    Pixels.withImageData(ctx, imd => {
                        const data = imd.data, l = data.length;
                        for (var i = 0; i < l; i += 4) {
                            data[i] = 255 - data[i]; // red
                            data[i + 1] = 255 - data[i + 1]; // green
                            data[i + 2] = 255 - data[i + 2]; // blue
                        }
                    });
                }
                static getColoriser(c, colors, invert, easing) {
                    return typeof colors == 'boolean' ?
                        Color_1.Color.getColorizer(c, colors, invert, easing)
                        : invert ?
                            (v) => { colors(v, c); c.invert(); }
                            : (v) => colors(v, c);
                }
                static drawHeightMap(ctx, data, width, colors, invert, easing, alpha01, threshold = 0, alphaIfUnderThreshold) {
                    const height = data.length / width, c = new Color_1.Color().setAlpha01(alpha01), colorize = Pixels.getColoriser(c, colors, invert, easing), alpha = Math.floor(255 * alphaIfUnderThreshold);
                    Pixels.withImageData(ctx, imd => {
                        const img = imd.data;
                        for (let y = 0, yw, yw4; y < height; y++) {
                            yw = y * width, yw4 = yw * 4;
                            for (let x = 0; x < width; x++) {
                                const v = data[yw + x];
                                if (threshold && v < threshold) {
                                    if (alphaIfUnderThreshold != undefined)
                                        img[yw4 + x * 4 + 3] = alpha;
                                    continue;
                                }
                                colorize(v);
                                const i = yw4 + x * 4;
                                img[i] = c.r;
                                img[i + 1] = c.g;
                                img[i + 2] = c.b;
                                img[i + 3] = c.a;
                            }
                        }
                    });
                }
                static imageFromHeightMap(data, width, colors, invert, easing, alpha01) {
                    width = Math.round(width);
                    const height = data.length / width, imd = new ImageData(width, height), d = imd.data, c = new Color_1.Color().setAlpha01(alpha01), colorize = Pixels.getColoriser(c, colors, invert, easing);
                    for (let y = 0, yw, yw4; y < height; y++) {
                        yw = y * width, yw4 = yw * 4;
                        for (let x = 0; x < width; x++) {
                            colorize(data[yw + x]);
                            const i = yw4 + x * 4;
                            d[i] = c.r;
                            d[i + 1] = c.g;
                            d[i + 2] = c.b;
                            d[i + 3] = c.a;
                        }
                    }
                    return imd;
                }
                static drawCircle(ctx, r, x, y, strokeStyle) {
                    if (!Pixels.noLog)
                        console.log('Pixels drawCircle', r);
                    ctx.beginPath();
                    if (strokeStyle) {
                        ctx.strokeStyle = rgba(strokeStyle);
                    }
                    const c = ctx.canvas;
                    if (x == undefined) {
                        x = c.width / 2;
                    }
                    if (y == undefined) {
                        y = c.height / 2;
                    }
                    if (r == undefined) {
                        r = Math.min(c.width, c.height) / 2;
                    }
                    ctx.arc(x - .5, y - .5, r, 0, 2 * Math.PI);
                    ctx.stroke();
                }
                static drawLine(ctx, x1, y1, x2, y2, strokeStyle) {
                    if (!Pixels.noLog)
                        console.log('Pixels drawLine', x1, y1, x2, y2);
                    ctx.beginPath();
                    if (strokeStyle) {
                        ctx.strokeStyle = rgba(strokeStyle);
                    }
                    ctx.moveTo(x1 - .5, y1 - .5);
                    ctx.lineTo(x2 - .5, y2 - .5);
                    ctx.stroke();
                }
                static drawBox(ctx, r, strokeStyle, fill = false) {
                    if (!r)
                        return;
                    Pixels.drawRect(ctx, r.xmin, r.ymin, r.xmax, r.ymax, strokeStyle, fill);
                }
                static drawRect(ctx, x1, y1, x2, y2, strokeStyle, fill = false) {
                    if (!Pixels.noLog)
                        console.log('Pixels drawRect', x1, y1, x2, y2);
                    ctx.beginPath();
                    if (strokeStyle)
                        if (fill)
                            ctx.fillStyle = rgba(strokeStyle);
                        else
                            ctx.strokeStyle = rgba(strokeStyle);
                    if (fill)
                        ctx.fillRect(x1 - .5, y1 - .5, x2 - x1, y2 - y1);
                    else
                        ctx.strokeRect(x1 - .5, y1 - .5, x2 - x1, y2 - y1);
                }
                static randomCross(ctx, r = 5, strokeStyle = 'rgba(255,255,255,255)') {
                    const x = Math.random() * ctx.canvas.width, y = Math.random() * ctx.canvas.height;
                    this.drawCross(ctx, x, y, r, strokeStyle);
                    return { x, y };
                }
                static drawCross(ctx, cx, cy, r, strokeStyle) {
                    if (!Pixels.noLog)
                        console.log('Pixels drawCross', cx, cy, r);
                    ctx.beginPath();
                    if (strokeStyle) {
                        ctx.strokeStyle = rgba(strokeStyle);
                    }
                    ctx.moveTo(cx - .5, cy - r - .5);
                    ctx.lineTo(cx - .5, cy + r - .5);
                    ctx.moveTo(cx - r - .5, cy - .5);
                    ctx.lineTo(cx + r - .5, cy - .5);
                    ctx.stroke();
                }
                static drawPoly(ctx, xys, strokeStyle, fill = false, close = fill) {
                    if (!Pixels.noLog)
                        console.log('Pixels drawPoly', xys);
                    ctx.beginPath();
                    if (strokeStyle)
                        if (fill)
                            ctx.fillStyle = rgba(strokeStyle);
                        else
                            ctx.strokeStyle = rgba(strokeStyle);
                    const l = xys.length, correct = true;
                    if (correct) {
                        ctx.moveTo(xys[0] - .5, xys[1] - .5);
                        for (let i = 2; i < l; i += 2)
                            ctx.lineTo(xys[i] - .5, xys[i + 1] - .5);
                    }
                    else {
                        ctx.moveTo(xys[0], xys[1]);
                        for (let i = 2; i < l; i += 2)
                            ctx.lineTo(xys[i], xys[i + 1]);
                    }
                    if (close)
                        ctx.closePath();
                    if (fill)
                        ctx.fill();
                    else
                        ctx.stroke();
                }
                static drawGradientPoly(ctx, xys) {
                    const c = new Color_1.Color();
                    const l = xys.length;
                    for (let i = 2; i < l; i += 2) {
                        ctx.beginPath();
                        ctx.strokeStyle = c.gradient7(i / l).toString();
                        ctx.moveTo(xys[i - 2] - .5, xys[i - 1] - .5);
                        ctx.lineTo(xys[i] - .5, xys[i + 1] - .5);
                        ctx.stroke();
                    }
                }
                static drawCrossBox(ctx, xmin, xmax, ymin, ymax, minSize = 0, strokeStyle) {
                    xmin = Math.round(xmin), xmax = Math.round(xmax), ymin = Math.round(ymin), ymax = Math.round(ymax);
                    //console.log('drawCrossBox', xmin, xmax, ymin, ymax)
                    ctx.beginPath();
                    if (strokeStyle) {
                        ctx.strokeStyle = rgba(strokeStyle);
                    }
                    const w = xmax - xmin, h = ymax - ymin;
                    ctx.strokeRect(xmin - .5, ymin - .5, w, h);
                    if (!(minSize > 0 && (w < minSize || h < minSize))) {
                        return;
                    }
                    let cx = xmin + w / 2, cy = ymin + h / 2;
                    if (w % 2 != 0)
                        cx -= .5;
                    if (h % 2 != 0)
                        cy -= .5;
                    ctx.moveTo(cx - .5, 0);
                    ctx.lineTo(cx - .5, ymin - .5);
                    ctx.moveTo(cx - .5, ctx.canvas.height);
                    ctx.lineTo(cx - .5, ymax - .5);
                    ctx.moveTo(0, cy - .5);
                    ctx.lineTo(xmin - .5, cy - .5);
                    ctx.moveTo(xmax, cy - .5);
                    ctx.lineTo(ctx.canvas.width, cy - .5);
                    ctx.stroke();
                }
            };
            Pixels.noLog = true;
            exports_11("Pixels", Pixels);
        }
    };
});
System.register("shared/Circle", [], function (exports_12, context_12) {
    "use strict";
    var __moduleName = context_12 && context_12.id;
    function rgb(c) { return typeof c == 'string' ? c : `rgb(${c.r},${c.g},${c.b})`; }
    var Circle;
    return {
        setters: [],
        execute: function () {
            Circle = class Circle {
                constructor(r = 1, x = 0, y = 0) {
                    this.r = r;
                    this.x = x;
                    this.y = y;
                }
                static IsIn(px, py, cx, cy, r) {
                    const x = px - cx, y = py - cy;
                    return x * x + y * y <= r * r;
                }
                static draw(ctx, x, y, r, strokeStyle) {
                    ctx.beginPath();
                    if (strokeStyle) {
                        ctx.strokeStyle = rgb(strokeStyle);
                    }
                    ctx.arc(x - .5, y - .5, r, 0, 2 * Math.PI);
                    ctx.stroke();
                }
                static random(xmax, ymax, rmax) {
                    const x = Math.random() * xmax;
                    const y = Math.random() * ymax;
                    const r = Math.random() * Math.abs(rmax || (ymax / 2));
                    return new Circle(r, x, y);
                }
                static randomIn(r) {
                    return Circle.random(r.width, r.height, Math.min(r.width, r.height));
                }
                static fromCxyr(c) {
                    return new Circle(c.r, c.x, c.y);
                }
                static getBox(r, x, y) {
                    const x1 = x - r, x2 = x + r, y1 = y - r, y2 = y + r;
                    return {
                        xmin: x1 < x2 ? x1 : x2,
                        xmax: x1 > x2 ? x1 : x2,
                        ymin: y1 < y2 ? y1 : y2,
                        ymax: y1 > y2 ? y1 : y2
                    };
                }
                draw(ctx, strokeStyle) {
                    Circle.draw(ctx, this.x, this.y, this.r, strokeStyle);
                }
                contains(px, py) {
                    const dx = px - this.x, dy = py - this.y, r = this.r;
                    return dx * dx + dy * dy <= r * r;
                }
                containsPoint(p) {
                    return this.contains(p.x, p.y);
                }
                getBox() {
                    return Circle.getBox(this.r, this.x, this.y);
                }
            };
            exports_12("Circle", Circle);
        }
    };
});
System.register("shared/Cardioide", [], function (exports_13, context_13) {
    "use strict";
    var __moduleName = context_13 && context_13.id;
    function rgb(c) { return typeof c == 'string' ? c : `rgb(${c.r},${c.g},${c.b})`; }
    var Cardioide;
    return {
        setters: [],
        execute: function () {
            Cardioide = class Cardioide {
                constructor(r = 1, x = 0, y = 0) {
                    this.r = r;
                    this.x = x;
                    this.y = y;
                }
                static getXYs(cx, cy, r, thetaStart, thetaEnd, steps) {
                    const l = steps * 2, xys = new Array(l || 0);
                    if (!steps) {
                        return xys;
                    }
                    const thetaStep = (thetaEnd - thetaStart) / steps;
                    for (let i = 0, theta = thetaStart; i <= l; theta += thetaStep, i += 2) {
                        const t = Math.tan(theta / 2), t2 = t * t, d = 1 + t2, d2 = d * d;
                        const x = 2 * r * ((1 - t2) / d2), y = (4 * r * t) / d2;
                        xys[i] = cx + x;
                        xys[i + 1] = cy + y;
                    }
                    return xys;
                }
                static getBox(x, y, r) {
                    const ar = Math.abs(r), hh = r * Cardioide._hp6;
                    return r > 0 ? { xmin: x - ar / 4, ymin: y - hh, xmax: x + ar * 2, ymax: y + hh }
                        : { xmin: x - ar * 2, ymin: y + hh, xmax: x + ar / 4, ymax: y - hh };
                }
                static getPointAtAngle(x, y, r, angle) {
                    const t = Math.tan(angle / 2), t2 = t * t, d = 1 + t2, d2 = d * d;
                    const dx = 2 * r * ((1 - t2) / d2), dy = (4 * r * t) / d2;
                    return { x: x + dx, y: y + dy };
                }
                static getAngleRayTo(x, y, r, px, py) {
                    const vx = px - x, vy = py - y;
                    let a = Math.acos(vx / Math.sqrt(vx * vx + vy * vy));
                    return (py < y ? -a : a) + (r < 0 ? Math.PI : 0);
                }
                static getPointIntersectRayTo(x, y, r, px, py) {
                    return this.getPointAtAngle(x, y, r, this.getAngleRayTo(x, y, r, px, py));
                }
                static draw(ctx, x, y, r, segments = 100, strokeStyle) {
                    const twoPI = 2 * Math.PI;
                    const xys = this.getXYs(x, y, r, 0, twoPI, segments);
                    const l = xys.length;
                    ctx.beginPath();
                    if (strokeStyle) {
                        ctx.strokeStyle = rgb(strokeStyle);
                    }
                    ctx.moveTo(xys[0] - .5, xys[1] - .5);
                    for (let i = 2; i < l; i += 2) {
                        ctx.lineTo(xys[i] - .5, xys[i + 1] - .5);
                    }
                    ctx.stroke();
                }
                static drawNth(ctx, cx, cy, r, nth, n, strokeStyle) {
                    const t = Math.tan(Math.PI / nth / 2), t2 = t * t, d = 1 + t2, d2 = d * d;
                    const x = 2 * r * ((1 - t2) / d2), y = (4 * r * t) / d2;
                    ctx.beginPath();
                    if (strokeStyle) {
                        ctx.strokeStyle = rgb(strokeStyle);
                    }
                    ctx.moveTo(cx - .5, cy - .5);
                    ctx.lineTo(cx + x - .5, cy + y - .5);
                    ctx.stroke();
                }
                static isIn(px, py, cx, cy, r) {
                    const vx = px - cx, vy = py - cy, lcp = Math.sqrt(vx * vx + vy * vy);
                    let a = Math.acos(vx / lcp);
                    if (r < 0) {
                        a += Math.PI;
                    }
                    if (py < cy) {
                        a = -a;
                    }
                    const t = Math.tan(a / 2), t2 = t * t, d = 1 + t2, d2 = d * d, dr = 2 * r;
                    const ux = dr * ((1 - t2) / d2), uy = (2 * dr * t) / d2;
                    return lcp < Math.sqrt(ux * ux + uy * uy);
                }
                static random(xmax, ymax, rmax) {
                    const x = Math.random() * xmax;
                    const y = Math.random() * ymax;
                    const ar = Math.random() * (rmax == undefined ? (ymax / 2) : rmax);
                    const r = Math.random() > .5 ? ar : -ar;
                    return new Cardioide(r, x, y);
                }
                static randomIn(size) {
                    const cx = size.width / 2, cy = size.height / 2;
                    const x = cx - (Math.random() * cx / 2);
                    const y = cy - (Math.random() * cy / 2);
                    const ar = Math.random() * Math.min(size.width, size.height) / 4;
                    const r = Math.random() > .5 ? ar : -ar;
                    return new Cardioide(r, x, y);
                }
                draw(ctx, segments = 100, strokeStyle) {
                    Cardioide.draw(ctx, this.x, this.y, this.r, segments, strokeStyle);
                }
                getCircleIn() {
                    return { x: this.x + this.r, y: this.y, r: Math.abs(this.r) };
                }
                getBox() {
                    return Cardioide.getBox(this.x, this.y, this.r);
                }
                getHeight() {
                    return 2 * Math.abs(this.r) * Cardioide._hp6;
                }
                getWidth() {
                    return 2.25 * Math.abs(this.r);
                }
                getPointAtAngle(angle) {
                    return Cardioide.getPointAtAngle(this.x, this.y, this.r, angle);
                }
                getAngleRayTo(px, py) {
                    return Cardioide.getAngleRayTo(this.x, this.y, this.r, px, py);
                }
                getIntersectRayTo(p) {
                    return Cardioide.getPointIntersectRayTo(this.x, this.y, this.r, p.x, p.y);
                }
                containsPoint(p) {
                    return this.contains(p.x, p.y);
                }
                contains(px, py) {
                    const r = this.r, ar = r < 0 ? -r : r;
                    const x = this.x;
                    if (px < (r > 0 ? (x - ar / 4) : (x - ar * 2)) || (r > 0 ? (x + ar * 2) : (x + ar / 4)) < px) {
                        return false;
                    }
                    const y = this.y, hh = ar * Cardioide._hp6;
                    if (py < (y - hh) || (y + hh) < py) {
                        return false;
                    }
                    const vx = px - x, vy = py - y, vy2 = vy * vy;
                    const dx = vx - r;
                    if (dx * dx + vy2 <= r * r) {
                        return true;
                    }
                    const lcp = Math.sqrt(vx * vx + vy2);
                    let a = Math.acos(vx / lcp);
                    if (r < 0) {
                        a += Math.PI;
                    }
                    if (py < y) {
                        a = -a;
                    }
                    const t = Math.tan(a / 2), t2 = t * t, d = 1 + t2, d2 = d * d, ux = 2 * r * ((1 - t2) / d2), uy = (4 * r * t) / d2;
                    return lcp < Math.sqrt(ux * ux + uy * uy);
                }
            };
            Cardioide._hp6 = (() => { const t = Math.tan(Math.PI / 6), d = 1 + t * t; return 4 * t / (d * d); })();
            exports_13("Cardioide", Cardioide);
        }
    };
});
//nombres
System.register("shared/nombres", [], function (exports_14, context_14) {
    "use strict";
    var __moduleName = context_14 && context_14.id;
    function fractionEntiere(n) {
        const an = Math.abs(n);
        if (an == Math.trunc(an)) {
            return '' + n;
        }
        const sign = n / an, eps = Math.pow(10, -9);
        for (let i = 1, x, y, ex, ey; i < 10000000; i++) {
            x = i * an, ex = Math.trunc(x);
            if (x - ex < eps) {
                return (sign * ex) + '/' + i;
            }
            y = i / an, ey = Math.trunc(y);
            if (y - ey < eps) {
                return (sign * i) + '/' + ey;
            }
        }
        return '' + n;
    }
    exports_14("fractionEntiere", fractionEntiere);
    function pgcd(a, b) {
        a = Math.trunc(a), b = Math.trunc(b);
        let t;
        while (b != 0) {
            t = a;
            a = b;
            b = t % b;
        }
        if (a == 0) {
            throw 'pgcd zero';
        }
        return a;
    }
    exports_14("pgcd", pgcd);
    function simplifieFraction(x, y) {
        const p = pgcd(x, y);
        return (x / p) + '/' + (y / p);
    }
    exports_14("simplifieFraction", simplifieFraction);
    function simplifieFractionObj(o, toString = false) {
        if (toString) {
            return simplifieFraction(o.x, o.y);
        }
        const p = pgcd(o.x, o.y);
        return { x: o.x / p, y: o.y / p };
    }
    exports_14("simplifieFractionObj", simplifieFractionObj);
    return {
        setters: [],
        execute: function () {//nombres
        }
    };
});
System.register("client/fract2D/Fract2DHelpers", ["shared/Pixels", "shared/Circle", "shared/Color", "shared/Cardioide", "shared/nombres"], function (exports_15, context_15) {
    "use strict";
    var __moduleName = context_15 && context_15.id;
    function setupHelpers(c, t, lim, doCardioRays) {
        const x = (v) => t.xToView(v, 0), y = (v) => t.yToView(v, 0), x0 = x(0), y0 = y(0), dx = (v) => x(v) - x0, dy = (v) => y(v) - y0;
        return [
            {
                name: 'view center',
                draw: () => Pixels_1.Pixels.drawCross(c, c.canvas.width / 2, c.canvas.height / 2, 20, Color_2.Color.Gray)
            },
            {
                name: 'limit circle',
                draw: () => Circle_1.Circle.draw(c, x0, y0, dx(lim), Color_2.Color.MGreen)
            },
            {
                name: 'world origin',
                draw: () => Pixels_1.Pixels.drawCross(c, x0, y0, 10, Color_2.Color.MGreen)
            },
            {
                name: 'points at y=0, x= -1, -3/4, 1/4',
                draw: () => [-1, -3 / 4, 1 / 4].forEach(tx => Pixels_1.Pixels.drawCross(c, x(tx), y0, 10, Color_2.Color.MRed))
            },
            {
                name: 'cardioid r=-1/2 at 1/4,0',
                draw: () => Cardioide_1.Cardioide.draw(c, x(.25), y0, dx(-.5), 100, Color_2.Color.MRed)
            },
            {
                name: 'circle r=1/4 at -1,0',
                draw: () => Circle_1.Circle.draw(c, x(-1), y0, dx(.25), Color_2.Color.MRed)
            },
            {
                name: 'circle box',
                draw: () => Pixels_1.Pixels.drawBox(c, Circle_1.Circle.getBox(dx(.25), x(-1), y0))
            },
            {
                name: 'cardioid box',
                draw: () => Pixels_1.Pixels.drawBox(c, Cardioide_1.Cardioide.getBox(x(.25), y0, dx(-.5)))
            },
            {
                name: 'circle in cardioid, with center',
                draw: () => { Circle_1.Circle.draw(c, x(-.25), y0, dx(.5), Color_2.Color.MRed); Pixels_1.Pixels.drawCross(c, x(-.25), y0, 10, Color_2.Color.MRed); }
            },
            {
                name: 'vertical segment at x=1/8,+-3/4',
                draw: () => Pixels_1.Pixels.drawLine(c, x(-1 / 8), y(3 / 4), x(-1 / 8), y(-3 / 4), Color_2.Color.MRed)
            },
            {
                name: 'cardioid rays',
                draw: () => {
                    if (!doCardioRays) {
                        return;
                    }
                    const cardioRays = (n0, inc, repeat, cFrom, cTo) => {
                        const cl = new Color_2.Color(), max = repeat * inc;
                        for (let i = n0, j = 0; j < repeat; j++, i += inc) {
                            Cardioide_1.Cardioide.drawNth(c, x(.25), y0, dx(-.5), i, repeat, cl.setLerp(cFrom, cTo, i / max));
                        }
                    };
                    const repeat = 20, cIn = new Color_2.Color(), cOut = Color_2.Color.White;
                    const phase = 0, spans = 4, inc = 2;
                    for (let j of [-1, 1]) {
                        for (let a = phase, i = 0; i < spans; i++, a += (inc / spans)) {
                            cIn.gradient7(i / spans, 1, spans);
                            cardioRays(a, j * inc, repeat, cIn, cOut);
                        }
                    }
                }
            }
        ];
    }
    exports_15("setupHelpers", setupHelpers);
    function drawCarAngle(ctx, trf, x, y) {
        const y0 = trf.yToView(0, 0), x14 = trf.xToView(.25, 0);
        Pixels_1.Pixels.drawLine(ctx, x14, y0, x, y, Color_2.Color.MBlue);
        const px = trf.xToTarget(x, y), py = trf.yToTarget(x, y), car = new Cardioide_1.Cardioide(-.5, .25, 0), a = car.getAngleRayTo(px, py), q = car.getPointAtAngle(a);
        trf.pointToView(q);
        Pixels_1.Pixels.drawCross(ctx, q.x, q.y, 5, Color_2.Color.MBlue);
        return a;
    }
    exports_15("drawCarAngle", drawCarAngle);
    function getCarAngleInfo(carAngle, doFractions) {
        const pi = Math.PI, a = carAngle - pi, rad = 180 / pi, ap = a / pi, ap3 = parseFloat(ap.toFixed(3));
        return (a * rad).toFixed(0) //+ '°' 
            + ',' + ap3
            + (doFractions ? ',' + nombres_1.fractionEntiere(ap3) : '');
    }
    exports_15("getCarAngleInfo", getCarAngleInfo);
    var Pixels_1, Circle_1, Color_2, Cardioide_1, nombres_1;
    return {
        setters: [
            function (Pixels_1_1) {
                Pixels_1 = Pixels_1_1;
            },
            function (Circle_1_1) {
                Circle_1 = Circle_1_1;
            },
            function (Color_2_1) {
                Color_2 = Color_2_1;
            },
            function (Cardioide_1_1) {
                Cardioide_1 = Cardioide_1_1;
            },
            function (nombres_1_1) {
                nombres_1 = nombres_1_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("shared/DomHelper", [], function (exports_16, context_16) {
    "use strict";
    var __moduleName = context_16 && context_16.id;
    var Fader, DomHelper;
    return {
        setters: [],
        execute: function () {
            Fader = class Fader {
                constructor(show, seconds = .5, opacity = show ? 1 : 0) {
                    this.show = show;
                    this.seconds = seconds;
                    this.opacity = opacity;
                }
            };
            exports_16("Fader", Fader);
            DomHelper = class DomHelper {
                static _fader(_in, seconds = .5, toOpacity01 = _in ? 1 : 0) {
                    return this.transition(`opacity ${seconds}s ease-${_in ? 'in' : 'out'}`, { opacity: toOpacity01 });
                }
                static _fade(el, fader) {
                    this.setStyle(el, this._fader(fader.show, fader.seconds, fader.opacity));
                    return new Promise((resolve, reject) => setTimeout(() => resolve(), 1000 * fader.seconds));
                }
                static _unfade(el) {
                    this._prefixes.forEach(p => el.style.removeProperty(p + 'transition'));
                }
                static absDiv(o) { return this.merge(this._abs, this._inb, this._noevents, o); }
                static getEl(el, elseBody = false) {
                    return ((typeof el == 'object') ? el : this._divs.get(el)) || (elseBody ? document.body : undefined);
                }
                static get(name, elseBody = false) { return this.getEl(name, elseBody); }
                static merge(...objs) { return objs.reduce(Object.assign, {}); }
                static mergeIn(o, ...stylesOrClasses) {
                    stylesOrClasses.filter(sc => !!sc).forEach(sc => {
                        if (typeof sc == 'string') {
                            let a = o.classNames;
                            if (a == undefined) {
                                o.classNames = a = new Array();
                            }
                            if (a) {
                                a.push(sc);
                            }
                        }
                        else {
                            Object.assign(o, sc);
                        }
                    });
                    return o;
                }
                static windowSize() { return { width: window.innerWidth, height: window.innerHeight }; }
                static top(...style) { return this.mergeIn(this.absDiv(this._top), ...style); }
                static bottom(...style) { return this.mergeIn(this.absDiv(this._btm), ...style); }
                static left(...style) { return this.mergeIn(this.absDiv(this._lft), ...style); }
                static right(...style) { return this.mergeIn(this.absDiv(this._rgt), ...style); }
                static topLeft(...style) { return this.mergeIn(this.absDiv(this._tl), ...style); }
                static topRight(...style) { return this.mergeIn(this.absDiv(this._tr), ...style); }
                static bottomLeft(...style) { return this.mergeIn(this.absDiv(this._bl), ...style); }
                static bottomRight(...style) { return this.mergeIn(this.absDiv(this._br), ...style); }
                static fader(_in, seconds = .5, opacity) { return new Fader(_in, seconds, opacity); }
                static setStyle(el, style, className) {
                    if (!el)
                        return;
                    el = this.getEl(el);
                    if (className != undefined) {
                        el.className = className;
                    }
                    if (!style) {
                        return el;
                    }
                    if (typeof style == 'string') {
                        el.classList.add(style);
                        return el;
                    }
                    for (let pn in style) {
                        const v = style[pn];
                        if (pn == 'className' && typeof v == 'string') {
                            el.classList.add(v);
                        }
                        else if (pn == 'classNames' && Array.isArray(v)) {
                            v.forEach(cn => el.classList.add(cn));
                        }
                        else if (pn && v === null) {
                            el.style.removeProperty(pn);
                        }
                        else if (v != undefined) {
                            el.style.setProperty(pn, v.toString());
                        }
                    }
                    return el;
                }
                static transition(value, style = {}) {
                    return this._prefixes.reduce((p, c) => { p[c + 'transition'] = value; return p; }, style);
                }
                static append(el, replace, ...content) {
                    el = this.getEl(el);
                    if (!el)
                        return el;
                    if (replace)
                        el.innerHTML = '';
                    content.forEach(c => el.appendChild(c));
                    return el;
                }
                static setContent(el, content, nospaces = false) {
                    el = this.getEl(el);
                    if (el && content)
                        el.innerHTML = content.replace(nospaces ? /\s+|\n/g : /\s|\n/g, '<br>');
                    return el;
                }
                static remove(...els) {
                    els.forEach(el => { const e = this.getEl(el); if (e)
                        e.remove(); });
                }
                static removeProperties(el, ...names) {
                    el = this.getEl(el);
                    if (el)
                        names.filter(pn => !!pn).forEach(pn => el.style.removeProperty(pn));
                    return el;
                }
                static reparent(el, parent) {
                    el = this.getEl(el), parent = this.getEl(parent);
                    if (el && parent) {
                        el.remove();
                        parent.appendChild(el);
                    }
                    return el;
                }
                static div(parent, style, ...content) {
                    return this.create('div', parent, style, ...content);
                }
                static span(text, style, parent) {
                    return this.create('span', parent, style, document.createTextNode(text));
                }
                static create(elementName, parent, style, ...content) {
                    const el = document.createElement(elementName);
                    this.setStyle(el, style);
                    parent = parent !== null && this.getEl(parent, true);
                    if (parent)
                        parent.appendChild(el);
                    content.filter(e => !!e).forEach(c => el.appendChild(c));
                    return el;
                }
                static crud(key, show, style, content) {
                    const divs = this._divs, fader = typeof show == 'object' ? show : undefined;
                    let div = divs.get(key), isNew = false;
                    if (!show || fader && !fader.show) {
                        if (!div)
                            return;
                        divs.delete(key);
                        if (fader) {
                            this._fade(div, fader).then(() => div.remove());
                        }
                        else
                            div.remove();
                        return;
                    }
                    else if (!div) {
                        divs.set(key, div = this.div(null, style));
                        if (fader)
                            div.style.opacity = '0';
                        document.body.appendChild(div);
                        isNew = true;
                    }
                    div.style.visibility = 'visible';
                    this._unfade(div);
                    if (fader) {
                        if (fader && !isNew)
                            div.style.opacity = fader.show ? '0' : '1';
                        setTimeout(() => this._fade(div, fader), 0);
                    }
                    if (typeof content == 'function')
                        content(div, isNew);
                    else
                        this.setContent(div, content);
                    return div;
                }
                static showHide(show, ...keys) {
                    keys.map(k => this._divs.get(k)).filter(el => !!el).forEach(el => el.style.visibility = show ? 'visible' : 'hidden');
                }
                static fade(_in, seconds, ...els) {
                    els.map(k => this.getEl(k)).filter(el => !!el).forEach(el => this.setStyle(el, this._fader(_in, seconds)));
                    return new Promise((resolve, reject) => setTimeout(() => resolve(), 1000 * (seconds || .5)));
                }
                static showHideMouse(show, el) {
                    el = this.getEl(el);
                    if (el) {
                        el.style.cursor = show ? 'default' : 'none';
                    }
                    return el;
                }
                static setSize(el, w, h) {
                    this.setStyle(el, { 'width': w + 'px', 'height': h + 'px' });
                    return el;
                }
                static setupCanvas(c, w, h, style) {
                    this.setStyle(c, style);
                    if (w == undefined || w <= 0)
                        w = window.innerWidth + (w || 0);
                    if (h == undefined || h <= 0)
                        h = window.innerHeight + (h || 0);
                    this.setSize(c, c.width = w, c.height = h);
                    return c;
                }
                static download(obj, fileName) {
                    fileName = fileName || 'file.json';
                    var a = document.createElement('a');
                    a.setAttribute('style', 'display:none');
                    a.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(obj)));
                    a.setAttribute('download', fileName);
                    document.body.appendChild(a);
                    try {
                        a.click();
                    }
                    finally {
                        document.body.removeChild(a);
                    }
                }
            };
            DomHelper._divs = new Map();
            DomHelper._abs = { position: 'absolute' };
            DomHelper._inb = { display: 'inline-block' };
            DomHelper._noevents = { 'pointer-events': 'none' };
            DomHelper._tl = { top: 0, left: 0 };
            DomHelper._bl = { bottom: 0, left: 0 };
            DomHelper._tr = { top: 0, right: 0 };
            DomHelper._br = { bottom: 0, right: 0 };
            DomHelper._top = { top: 0, left: 0, right: 0 };
            DomHelper._btm = { bottom: 0, left: 0, right: 0 };
            DomHelper._lft = { top: 0, bottom: 0, left: 0 };
            DomHelper._rgt = { top: 0, bottom: 0, right: 0 };
            DomHelper._prefixes = ['-webkit-', '-moz-', '-ms-', '-o-', ''];
            exports_16("DomHelper", DomHelper);
        }
    };
});
System.register("shared/fract/FractBase", ["shared/DomHelper"], function (exports_17, context_17) {
    "use strict";
    var __moduleName = context_17 && context_17.id;
    var DomHelper_1, Tools, FractBase;
    return {
        setters: [
            function (DomHelper_1_1) {
                DomHelper_1 = DomHelper_1_1;
            }
        ],
        execute: function () {
            (function (Tools) {
                Tools[Tools["Doc"] = 0] = "Doc";
                Tools[Tools["Switch"] = 1] = "Switch";
                Tools[Tools["Info"] = 2] = "Info";
            })(Tools || (Tools = {}));
            FractBase = class FractBase {
                constructor(altAppInit) {
                    this.altAppInit = altAppInit;
                    this.showDocAtStart = !this.isDev;
                    this.log = this.isDev;
                    this.debug = true;
                    this._allDivs = ['Doc', 'Info'];
                    this.tools = new Map();
                }
                get isDocShown() { return this.isTool(Tools.Doc); }
                get isDev() { return document.location.hash.toLowerCase() == '#dev'; }
                attach() {
                    this._attach();
                    this.allShown = false;
                    this.showAll(true, ...this._moreElements());
                    this.updateInfo(true);
                    this._attached();
                    return this;
                }
                detach() {
                    this._detach();
                    this.setTool(Tools.Doc, false);
                    this.allShown = true;
                    return this.showAll(false, ...this._moreElements())
                        .then(() => { DomHelper_1.DomHelper.remove('Info'); this._detached(); });
                }
                showDoc(force, fade = false) {
                    const show = force || this.isDocShown;
                    if (force)
                        this.setTool(Tools.Doc, true);
                    const docs = show && this.keys.getDoc(this.isDev);
                    this.doShowDoc(show && docs, fade || undefined);
                }
                _attach() { }
                _attached() { }
                _detach() { }
                _detached() { }
                isTool(...name) {
                    return name.reduce((p, c) => p || this.tools.get(typeof c == 'string' ? c : this._toolname(c)), false);
                }
                setTool(tool, v) {
                    this.tools.set(this._toolname(tool), v);
                    return v;
                }
                switch() {
                    if (!this.altAppInit)
                        return;
                    const showDoc = this.isDocShown || !this.altApp && this.showDocAtStart;
                    this.detach().then(() => {
                        if (!this.altApp)
                            this.altApp = (typeof this.altAppInit == 'function') ? this.altAppInit() : this.altAppInit;
                        this.altApp.attach();
                        if (showDoc)
                            this.altApp.showDoc(true, true);
                    });
                }
                basetool(name, shift) {
                    switch (name) {
                        case 'log':
                            this.log = !this.log;
                            this._loggers().forEach(l => l.log = this.log);
                            console.log('logging: ' + (this.log ? 'on' : 'off'));
                            this.updateInfo();
                            return true;
                        case Tools[Tools.Switch]:
                            this.switch();
                            return true;
                        default:
                            let v;
                            this.tools.set(name, v = !this.tools.get(name));
                            if (this.log)
                                console.log(name, v);
                            switch (name) {
                                case Tools[Tools.Doc]:
                                    this.showDoc();
                                    return true;
                                case Tools[Tools.Info]:
                                    this.updateInfo();
                                    return true;
                                default: break;
                            }
                    }
                }
                drawPath(x, y) {
                    if (!this._pathData) {
                        this._pathData = new Array();
                    }
                    //const path = this.psr.computePathFromView(x, y, this.state.m, this.state.r, this._pathData)
                }
                updateProgress(p) {
                    const show = p !== false; //&& this.state.progress
                    this._divProgress = DomHelper_1.DomHelper.crud('Progress', show, !this._divProgress
                        && DomHelper_1.DomHelper.topLeft('progressBar'), (el, isNew) => {
                        // if (isNew) {
                        //     const p = DomHelper.create('progress', el, 'progress') as HTMLProgressElement
                        //     p.max = 1, p.value = 0;
                        // }
                        //(el.firstElementChild as HTMLProgressElement).value = p || 0
                        //setTimeout(() =>
                        DomHelper_1.DomHelper.setStyle(el, 
                        //    DomHelper.transition('width 0.5s ease',
                        { width: Math.ceil(100 * (p || 0)) + '%' });
                        //)
                        //    , 0)
                    });
                }
                updateShowBusy(force) {
                    if (force != undefined)
                        this.processing = force;
                    //if (this._divInfo) setTimeout(() => this._divInfo.classList.toggle('busy', this.processing), 0)
                    if (this._divInfo)
                        this._divInfo.classList.toggle('busy', this.processing);
                }
                updateInfo(fadeIn = false) {
                    const show = this.isTool(Tools.Info);
                    this._divInfo = DomHelper_1.DomHelper.crud('Info', show ? fadeIn ? DomHelper_1.DomHelper.fader(true) : true : false, show && DomHelper_1.DomHelper.bottomLeft('all', 'info', 'green'), show && this._getInfo());
                }
                showAll(show, ...more) {
                    if (this.allShown == show) {
                        return DomHelper_1.DomHelper.fade(show, .5, ...more);
                    }
                    else {
                        this.allShown = show;
                        return DomHelper_1.DomHelper.fade(show, .5, ...more.concat(this._allDivs));
                    }
                }
                doShowDoc(docs, forceFade) {
                    const div = DomHelper_1.DomHelper.crud('Doc', forceFade != undefined ? DomHelper_1.DomHelper.fader(forceFade) : !!docs, !!docs && DomHelper_1.DomHelper.bottomRight('all', 'info', { 'width': '250px', 'z-index': 10 }), div => {
                        const sections = docs.reduce((p, c) => {
                            const sn = c.special ? 'specials' : c.section || '', s = p.find(s => s.name == sn);
                            if (s)
                                s.docs.push(c);
                            else
                                p.push({ name: sn, docs: [c] });
                            return p;
                        }, new Array());
                        console.log(sections);
                        DomHelper_1.DomHelper.append(div, true, ...sections.map(s => DomHelper_1.DomHelper.div(null, { display: 'block' }, DomHelper_1.DomHelper.span(s.name, { 'font-weight': 'bold' }, null), ...s.docs.map(c => DomHelper_1.DomHelper.div(null, { display: 'block' }, DomHelper_1.DomHelper.span(c.key, { 'font-weight': 'bold' }, null), DomHelper_1.DomHelper.span(c.doc, { 'margin-left': '5px' }, null))))));
                    });
                }
            };
            exports_17("FractBase", FractBase);
        }
    };
});
System.register("shared/NumberArrays", [], function (exports_18, context_18) {
    "use strict";
    var __moduleName = context_18 && context_18.id;
    var NumberArrayInfo, NumberArrays;
    return {
        setters: [],
        execute: function () {
            NumberArrayInfo = class NumberArrayInfo {
                constructor(min, max, nans) {
                    this.min = min;
                    this.max = max;
                    this.nans = nans;
                }
            };
            exports_18("NumberArrayInfo", NumberArrayInfo);
            NumberArrays = class NumberArrays {
                /** returns result containg values in range [0.0, 1.0] */
                static normalize(data, result = new Float32Array(data.length), min, max, checkNaN, info) {
                    if (min == undefined || max == undefined) {
                        const mm = NumberArrays.getMinMax(data, info);
                        min = mm.min, max == mm.max;
                    }
                    if (checkNaN)
                        NumberArrays.checkNaN(data, checkNaN.throwOnNaN, checkNaN.replaceNanBy, checkNaN.warnOnNaN, info);
                    const h = (max - min) || 1;
                    for (let i = 0, l = data.length; i < l; i++)
                        result[i] = (data[i] - min) / h;
                    return result;
                }
                static getMinMax(data, info = new NumberArrayInfo()) {
                    let min = Infinity, max = -Infinity;
                    for (let i = 0, l = data.length; i < l; i++) {
                        const v = data[i];
                        if (v < min)
                            min = v;
                        if (v > max)
                            max = v;
                    }
                    info.min = min;
                    info.max = max;
                    return info;
                }
                static checkNaN(data, throwOnNaN = false, replaceNanBy = 0, warnOnNaN = true, info = new NumberArrayInfo()) {
                    let nans = 0;
                    for (let i = 0, l = data.length; i < l; i++) {
                        let v = data[i];
                        if (isNaN(v)) {
                            nans++;
                            if (throwOnNaN)
                                throw 'NaN at ' + i;
                            if (warnOnNaN)
                                console.warn(v, i);
                            data[i] = replaceNanBy;
                        }
                    }
                    info.nans = nans;
                    return info;
                }
                static getDistinctValues(data) {
                    return Array.from(new Set(data).values());
                }
                static countDistinctValues(data) {
                    return NumberArrays.getDistinctValues(data).length;
                }
                static makeEven(data, dataWidth, divisor = 2, getNewArray) {
                    let w = dataWidth, w0 = w;
                    let h = data.length / w;
                    if (w % divisor == 0 && h % divisor == 0)
                        return data;
                    //could really do better ^^
                    while (w && w % divisor)
                        w--;
                    while (h && h % divisor)
                        h--;
                    const result = getNewArray(w, h);
                    for (let j = 0; j < h; j++) {
                        const jwr = j * w, jwd = j * w0;
                        for (let i = 0; i < w; i++)
                            result[jwr + i] = data[jwd + i];
                    }
                    return result;
                }
            };
            exports_18("NumberArrays", NumberArrays);
        }
    };
});
System.register("shared/HeightMap", ["shared/Region2D", "shared/NumberArrays"], function (exports_19, context_19) {
    "use strict";
    var __moduleName = context_19 && context_19.id;
    var Region2D_2, NumberArrays_1, HeightMap;
    return {
        setters: [
            function (Region2D_2_1) {
                Region2D_2 = Region2D_2_1;
            },
            function (NumberArrays_1_1) {
                NumberArrays_1 = NumberArrays_1_1;
            }
        ],
        execute: function () {
            HeightMap = class HeightMap {
                constructor(region, data, dataWidth) {
                    this.region = region;
                    this.data = data;
                    this.dataWidth = dataWidth;
                }
                static fromData(regionOrWidthOrSize, inputData, min, max, dataWidth = typeof regionOrWidthOrSize == 'number' ? regionOrWidthOrSize : regionOrWidthOrSize.width, dataHolder, log = false, checkNaN, info = new NumberArrays_1.NumberArrayInfo()) {
                    const hmap = new HeightMap(regionOrWidthOrSize instanceof Region2D_2.Region2D ? regionOrWidthOrSize : Region2D_2.Region2D.fromScreen(regionOrWidthOrSize), NumberArrays_1.NumberArrays.normalize(inputData, dataHolder, min, max, checkNaN, info), dataWidth);
                    if (log)
                        console.log('fromData', {
                            length: inputData.length, width: dataWidth, height: (inputData.length / dataWidth),
                            min: info.min, max: info.max, nans: info.nans
                        });
                    return hmap;
                }
                makeEven(divisor, log = false) {
                    if (log)
                        console.log('makeEven', divisor, this.dataWidth, this.data.length, this.data.length / this.dataWidth);
                    let w = this.dataWidth, h = this.data.length / this.dataWidth;
                    this.data = NumberArrays_1.NumberArrays.makeEven(this.data, this.dataWidth, divisor, (nw, nh) => {
                        w = nw;
                        h = nh;
                        return new Float32Array(nw * nh);
                    });
                    if (this.region.width == this.dataWidth) {
                        this.region.width = w;
                        this.region.height = h;
                    }
                    this.dataWidth = w;
                    if (log)
                        console.log('makeEven', this.dataWidth, this.data.length, this.data.length / this.dataWidth);
                    return this;
                }
                clone() {
                    return new HeightMap(this.region.clone(), this.data.slice(), this.dataWidth);
                }
                copy(hmap, byRef) {
                    this.region = byRef ? hmap.region : hmap.region.clone();
                    this.data = byRef ? hmap.data : hmap.data.slice();
                    this.dataWidth = hmap.dataWidth;
                    return this;
                }
                getMin() { return NumberArrays_1.NumberArrays.getMinMax(this.data).min; }
                flatten(info, ckeckNaN) {
                    NumberArrays_1.NumberArrays.normalize(this.data, this.data, undefined, undefined, ckeckNaN, info);
                    return this;
                }
                combine(newParts, commonPart, log = false) {
                    if (log)
                        console.log('combine', newParts.length, !!commonPart);
                    const isDezoom = commonPart && newParts && newParts.length == 4;
                    if (commonPart)
                        if (isDezoom)
                            this.dezoom(commonPart, log);
                        else
                            this.move(commonPart, log);
                    newParts.forEach(part => this.copyPart(part.region, part.data, log));
                    return this;
                }
                copyPart(sr, sd, log = false) {
                    const dd = this.data, dr = this.region, dw = dr.width;
                    let y0 = sr.ymin, x0 = sr.xmin, h = sr.height, w = sr.width;
                    const xodd = sr.xmin % 2 !== 0;
                    if (log)
                        console.log('copy part', x0, y0, w, h);
                    for (let y = 0, d, s; y < h; y++) {
                        d = dw * (y0 + y) + x0;
                        s = w * y;
                        for (let x = 0; x < w; x++) {
                            dd[d + x] = sd[s + x];
                        }
                    }
                }
                move(dr, log = false) {
                    const d = this.data, sr = this.region, txp = dr.xmin - sr.xmin, txn = dr.xmax - sr.xmax, typ = dr.ymin - sr.ymin, tyn = dr.ymax - sr.ymax;
                    if (log)
                        console.log('move part', txp || txn, typ || tyn);
                    const s = sr.width, h = dr.height, w = dr.width, t = 1, dx = txp ? -1 : 1, dy = typ ? -1 : 1, x0 = txp ? (sr.xmax - txp - t) : sr.xmin, y0 = typ ? (sr.ymax - typ - t) : sr.ymin;
                    for (let j = 0, y = y0, syw, dyw; j < h; j++, y += dy) {
                        syw = s * (y - tyn);
                        dyw = s * (y + typ);
                        for (let i = 0, x = x0; i < w; i++, x += dx) {
                            d[dyw + x + txp] = d[syw + x - txn];
                        }
                    }
                }
                dezoom(dr, log = false) {
                    const dd = this.data, sd = dd.slice(), sr = this.region, sw = sr.width, sh = sr.height, dw = dr.width, dh = dr.height, dx0 = dr.xmin, dy0 = dr.ymin, ax = sw / dw, ay = sh / dh;
                    if (log)
                        console.log('dezoom', (100 / ax).toFixed(1) + '%', (100 / ay).toFixed(1) + '%');
                    for (let y = 0, sy, dywx0, syw; y < dh; y++) {
                        sy = Math.round(y * ay);
                        syw = sy * sw;
                        dywx0 = (dy0 + y) * sw + dx0;
                        for (let x = 0, sx; x < dw; x++)
                            dd[dywx0 + x] = sd[syw + Math.round(x * ax)];
                    }
                }
            };
            exports_19("HeightMap", HeightMap);
        }
    };
});
System.register("shared/WorkerLoader", [], function (exports_20, context_20) {
    "use strict";
    var __moduleName = context_20 && context_20.id;
    function compute(data, onResult) { }
    var WorkerLoader;
    return {
        setters: [],
        execute: function () {
            WorkerLoader = class WorkerLoader {
                static setup(onWorkerReady, computeFunction, dependencies, computeClassName, log = false, debug = false, verbose = false) {
                    if (log)
                        console.log('building');
                    let comp = computeFunction.toString().trimLeft(), cn = computeClassName;
                    if (cn)
                        comp = comp.replace(new RegExp('\\w+\\.' + cn, 'g'), cn);
                    if (comp.indexOf('function ') != 0)
                        comp = 'function ' + comp;
                    comp = `function compute(data, onProgress){\n(${comp})(data, onProgress)\n}`;
                    const func = `(function ${this.webWorker.toString().trimLeft()})(${log && debug},${log && debug && verbose})`;
                    const blobParts = dependencies.concat(comp, func).map(o => o + '\n');
                    const blobUrl = URL.createObjectURL(new Blob(blobParts, { type: 'text/javascript' }));
                    if (log)
                        console.log('loading');
                    const worker = new Worker(blobUrl);
                    worker.onmessage = e => {
                        if (log)
                            console.log('worker: ' + e.data);
                        if (e.data == 'ready') {
                            onWorkerReady(worker);
                        }
                    };
                }
                static webWorker(log = false, verbose = false) {
                    var canceled = false, busy = false;
                    function unbusy() { setTimeout(() => busy = false, 0); }
                    onmessage = e => {
                        switch (e.data) {
                            case 'cancel':
                                if (log) {
                                    console.log('canceling');
                                }
                                ;
                                canceled = true;
                                break;
                            case 'silent':
                                log = false;
                                break;
                            case 'debug':
                                log = true, verbose = false;
                                break;
                            case 'verbose':
                                log = verbose = true;
                                break;
                            default:
                                if (typeof e.data != 'object') {
                                    console.warn('unknown request: ' + e.data);
                                    break;
                                }
                                if (busy) {
                                    if (log) {
                                        console.log('(busy)');
                                    }
                                    break;
                                }
                                canceled = false;
                                if (log) {
                                    console.log('starting');
                                }
                                compute(e.data, (wr) => {
                                    if (canceled) {
                                        unbusy();
                                        return true;
                                    }
                                    if (log && verbose) {
                                        console.log('computing', wr);
                                    }
                                    postMessage(wr, undefined, [wr.data]);
                                    if (wr.progress >= 1 && wr.data) {
                                        unbusy();
                                    }
                                    return false;
                                });
                                if (log) {
                                    console.log('started');
                                }
                                break;
                        }
                    };
                    if (log)
                        console.log('loaded');
                    postMessage('ready', undefined);
                }
            };
            exports_20("WorkerLoader", WorkerLoader);
        }
    };
});
System.register("shared/WorkProcesr", ["shared/WorkerLoader"], function (exports_21, context_21) {
    "use strict";
    var __moduleName = context_21 && context_21.id;
    var WorkerLoader_1, WorkProcesr;
    return {
        setters: [
            function (WorkerLoader_1_1) {
                WorkerLoader_1 = WorkerLoader_1_1;
            }
        ],
        execute: function () {
            WorkProcesr = class WorkProcesr {
                constructor(computeFunction, dependencies, computeClassName, log = false, verbose = false) {
                    this.computeFunction = computeFunction;
                    this.dependencies = dependencies;
                    this.computeClassName = computeClassName;
                    this.log = log;
                    this.verbose = verbose;
                }
                get busy() { return this._busy; }
                dispose() { if (this.worker)
                    this.worker.terminate(); }
                cancel() {
                    if (!this._busy)
                        return;
                    this.cancelRequired = true;
                    if (this.log)
                        console.log('canceling compute');
                    if (this.worker) {
                        this.worker.postMessage('cancel');
                    }
                }
                start(workInput, useWorker, onComplete, onProgressCancel, id) {
                    if (this._busy) {
                        if (this.log)
                            console.log('busy');
                        return false;
                    }
                    this.cancelRequired = false, this._busy = true;
                    function progress(wr) {
                        let canceled = self.cancelRequired;
                        if (!canceled && onProgressCancel && wr && wr.progress) {
                            canceled = onProgressCancel(wr.progress);
                            if (canceled) {
                                self.cancel();
                            }
                        }
                        if (canceled || !wr || wr.data) {
                            const elapsed = performance.now() - start;
                            setTimeout(() => complete(elapsed, wr, canceled), 0);
                        }
                        return canceled;
                    }
                    function complete(elapsed, wr, canceled) {
                        self._busy = false;
                        if (canceled) {
                            if (self.log)
                                console.log('canceled');
                            onComplete(null, true);
                        }
                        else {
                            if (self.log)
                                console.log('computed', elapsed.toFixed(0) + 'ms');
                            onComplete(wr, false);
                        }
                    }
                    const self = this;
                    let start;
                    if (useWorker) {
                        const doWork = (worker) => {
                            (this.worker = worker).onmessage = e => progress(e.data);
                            if (this.log)
                                if (id) {
                                    console.log('work', id);
                                }
                                else {
                                    console.log('work');
                                }
                            start = performance.now();
                            worker.postMessage(workInput);
                        };
                        if (this.worker) {
                            doWork(this.worker);
                        }
                        else {
                            WorkerLoader_1.WorkerLoader.setup(doWork, this.computeFunction, this.dependencies, this.computeClassName, this.log, this.verbose);
                        }
                    }
                    else {
                        if (this.worker) {
                            this.worker.terminate();
                            this.worker = undefined;
                            if (this.log) {
                                console.log('worker terminated');
                            }
                        }
                        if (id && this.log) {
                            console.log('compute', id);
                        }
                        start = performance.now();
                        this.computeFunction(workInput, progress);
                    }
                    return true;
                }
            };
            exports_21("WorkProcesr", WorkProcesr);
        }
    };
});
System.register("shared/fract/FractWorker", [], function (exports_22, context_22) {
    "use strict";
    var __moduleName = context_22 && context_22.id;
    var WorkerData, WorkerResult, FractWorker;
    return {
        setters: [],
        execute: function () {
            WorkerData = class WorkerData {
                constructor(amin, amax, bmin, bmax, width, height, max, radius, optimize = true, progressSteps = 10, mask = false) {
                    this.amin = amin;
                    this.amax = amax;
                    this.bmin = bmin;
                    this.bmax = bmax;
                    this.width = width;
                    this.height = height;
                    this.max = max;
                    this.radius = radius;
                    this.optimize = optimize;
                    this.progressSteps = progressSteps;
                    this.mask = mask;
                }
                static fromRegion(r, width, height, max = 50, radius = 2, optimize = true, progressSteps = 10, mask = false) {
                    return new WorkerData(r.xmin, r.xmax, r.ymin, r.ymax, width, height, max, radius, optimize, progressSteps, mask);
                }
                static forPoint(a = 0, b = 0, max = 50, radius = 2, optimize = true, mask = false) {
                    return new WorkerData(a, a, b, b, 1, 1, max, radius, optimize, 0, mask);
                }
                setForPoint(a, b, max, radius = 2, optimize = true, mask = false) {
                    this.amin = a, this.amax = a;
                    this.bmin = b, this.bmax = b;
                    this.width = 1, this.height = 1;
                    this.max = max;
                    this.radius = radius;
                    this.optimize = optimize;
                    this.progressSteps = 0;
                    this.mask = mask;
                    return this;
                }
            };
            exports_22("WorkerData", WorkerData);
            WorkerResult = class WorkerResult {
                constructor(progress, data, min, max, optimized, optims, elapsedMs) {
                    this.progress = progress;
                    this.data = data;
                    this.min = min;
                    this.max = max;
                    this.optimized = optimized;
                    this.optims = optims;
                    this.elapsedMs = elapsedMs;
                }
            };
            exports_22("WorkerResult", WorkerResult);
            FractWorker = class FractWorker {
                static computePoint(a, b, m, r = 2, optimize = true, mask = false) {
                    return this.generate(this._wd.setForPoint(a, b, m, r, optimize, mask)).next().value;
                }
                static compute(wd, onProgress, dataHolder) {
                    let lwr;
                    const progress = (r) => {
                        lwr = r;
                        if (r.data) {
                            if (onProgress) {
                                onProgress(r);
                            }
                            return;
                        }
                        else if (r.progress == 0 || r.progress == 1)
                            return false;
                        else
                            return !!onProgress && onProgress(r) === true;
                    };
                    if (onProgress && wd.progressSteps) {
                        this.computeWithEvents(wd, progress, dataHolder);
                    }
                    else {
                        this.computeWithoutEvents(wd, progress, dataHolder);
                    }
                    return lwr;
                }
                static computeWithoutEvents(wd, onProgress, dataHolder) {
                    let canceled;
                    for (let wr of this.generate(wd, dataHolder)) {
                        canceled = !!onProgress && onProgress(wr) === true;
                        if (canceled) {
                            break;
                        }
                    }
                }
                static computeWithEvents(wd, onProgress, dataHolder) {
                    const iwr = this.generate(wd, dataHolder);
                    const next = () => {
                        const ir = iwr.next();
                        if (ir.done) {
                            return;
                        }
                        const canceled = !!onProgress && onProgress(ir.value) === true;
                        if (!canceled) {
                            setTimeout(() => next(), 0);
                        }
                    };
                    next();
                }
                static *generate(wd, dataHolder) {
                    const start = performance.now();
                    const w = Math.trunc(wd.width), h = Math.trunc(wd.height), data = dataHolder || new Uint16Array(w * h), bmin = wd.bmin, bmax = wd.bmax, db = (bmax - bmin) / h, amin = wd.amin, amax = wd.amax, da = (amax - amin) / w, max = Math.round(Math.abs(wd.max)), nmax = max - 1, mask = wd.mask, omax = mask ? max + 1 : max, r2 = wd.radius * wd.radius, PI = Math.PI, ps = wd.progressSteps > 0 && Math.trunc(h / wd.progressSteps);
                    let optimize = !!wd.optimize, n, t, za, zb, za2, zb2;
                    let optims = 0, testInC1, testInC2, testInCa;
                    let c1x, c1y, c1r2, c1x1, c1x2, c1y1, c1y2;
                    let c2x, c2y, c2r2, c2x1, c2x2, c2y1, c2y2;
                    let cax1, cay1, cax2, cay2, b2;
                    let min = Infinity, dx, dy, c1dy2, c2dy2;
                    if (optimize) {
                        let y = 0, r = .25, x = -1;
                        c1x = x, c1y = y, c1r2 = r * r;
                        c1x1 = x - r, c1x2 = x + r, c1y1 = y - r, c1y2 = y + r;
                        r = .5, x = -.25;
                        c2x = x, c2y = y, c2r2 = r * r;
                        c2x1 = x - r, c2x2 = x + r, c2y1 = y - r, c2y2 = y + r;
                        cax1 = -.75, cax2 = .375;
                        const t = Math.tan(Math.PI / 6), d = 1 + t * t;
                        cay1 = -2 * t / (d * d), cay2 = -cay1;
                        if (bmax < cay1 || cay2 < bmin
                            || amax < c1x1 || cax2 < amin
                            || amax < cax1 && (bmax < c1y1 || c1y2 < bmin)) {
                            optimize = false;
                        }
                    }
                    for (let y = 0, b = bmax, yw; y < h; y++, b -= db) {
                        yw = y * w;
                        if (optimize) {
                            if (cay1 <= b && b <= cay2) {
                                testInCa = true;
                                b2 = b * b;
                                if (c2y1 <= b && b <= c2y2) {
                                    testInC2 = true;
                                    dy = b - c2y;
                                    c2dy2 = dy * dy;
                                    if (c1y1 <= b && b <= c1y2) {
                                        testInC1 = true;
                                        dy = b - c1y;
                                        c1dy2 = dy * dy;
                                    }
                                    else {
                                        testInC1 = false;
                                    }
                                }
                                else {
                                    testInC1 = testInC2 = false;
                                }
                            }
                            else {
                                testInCa = false;
                            }
                        }
                        for (let x = 0, a = amin; x < w; x++, a += da) {
                            if (optimize) {
                                if (testInC1 && c1x1 <= a && a <= c1x2) {
                                    dx = a - c1x;
                                    if (dx * dx + c1dy2 <= c1r2) {
                                        data[yw + x] = omax;
                                        optims++;
                                        continue;
                                    }
                                }
                                if (testInCa && cax1 <= a && a <= cax2) {
                                    if (testInC2 && c2x1 <= a && a <= c2x2) {
                                        dx = a - c2x;
                                        if (dx * dx + c2dy2 <= c2r2) {
                                            data[yw + x] = omax;
                                            optims++;
                                            continue;
                                        }
                                    }
                                    const vx = a - .25, lcp = Math.sqrt(vx * vx + b2);
                                    let phi = Math.acos(vx / lcp);
                                    phi = (b < 0 ? -phi : phi) + PI;
                                    const t = Math.tan(phi / 2), t2 = t * t, d = 1 + t2, d2 = d * d;
                                    const ux = ((1 - t2) / d2), uy = (2 * t) / d2;
                                    if (lcp < Math.sqrt(ux * ux + uy * uy)) {
                                        data[yw + x] = omax;
                                        optims++;
                                        continue;
                                    }
                                }
                            }
                            n = 0, za = a, zb = b, za2 = za * za, zb2 = zb * zb;
                            while (za2 + zb2 <= r2 && n++ < nmax) {
                                t = za;
                                za = za2 - zb2 + a;
                                zb = 2 * t * zb + b;
                                za2 = za * za, zb2 = zb * zb;
                            }
                            data[yw + x] = n;
                            if (n < min)
                                min = n;
                        }
                        if (ps && y % ps == 0) {
                            yield new WorkerResult(y / h);
                        }
                    }
                    yield new WorkerResult(1, data, min, omax, optimize, optims, performance.now() - start);
                }
                static computePath(a, b, max, r = 2, result) {
                    max = Math.ceil(max);
                    const xys = result || new Array(2 * max), r2 = r * r;
                    let i = 0, n = 0, za = a, zb = b, za2 = za * za, zb2 = zb * zb, t;
                    while (za2 + zb2 <= r2 && n++ < max) {
                        t = za;
                        za = za2 - zb2 + a;
                        zb = 2 * t * zb + b;
                        za2 = za * za, zb2 = zb * zb;
                        xys[i] = za;
                        xys[i + 1] = zb;
                        i += 2;
                    }
                    xys.length = i > max ? i - 2 : i;
                    return xys;
                }
            };
            FractWorker._wd = WorkerData.forPoint();
            exports_22("FractWorker", FractWorker);
        }
    };
});
System.register("shared/fract/FractProcesr", ["shared/HeightMap", "shared/WorkProcesr", "shared/fract/FractWorker"], function (exports_23, context_23) {
    "use strict";
    var __moduleName = context_23 && context_23.id;
    var HeightMap_1, WorkProcesr_1, FractWorker_1, ComputeData, ComputedPoint, FractProcesr;
    return {
        setters: [
            function (HeightMap_1_1) {
                HeightMap_1 = HeightMap_1_1;
            },
            function (WorkProcesr_1_1) {
                WorkProcesr_1 = WorkProcesr_1_1;
            },
            function (FractWorker_1_1) {
                FractWorker_1 = FractWorker_1_1;
            }
        ],
        execute: function () {
            ComputeData = class ComputeData {
                constructor(target, width, height, max = 50, optimize = true, radius = 2, mask = false) {
                    this.target = target;
                    this.width = width;
                    this.height = height;
                    this.max = max;
                    this.optimize = optimize;
                    this.radius = radius;
                    this.mask = mask;
                }
            };
            ComputedPoint = class ComputedPoint {
                constructor(value, max, nooptim, optimized, elapsedMs) {
                    this.value = value;
                    this.max = max;
                    this.nooptim = nooptim;
                    this.optimized = optimized;
                    this.elapsedMs = elapsedMs;
                }
            };
            exports_23("ComputedPoint", ComputedPoint);
            FractProcesr = class FractProcesr {
                constructor(tfm, log = false) {
                    this.tfm = tfm;
                    this._log = log;
                }
                get log() { return this._log; }
                set log(value) {
                    this._log = value;
                    if (this.workProcesr)
                        this.workProcesr.log = value;
                }
                cancelProcess() {
                    this.processCancelRequired = true;
                    if (this.workProcesr)
                        this.workProcesr.cancel();
                    if (this.log)
                        console.log('process canceling');
                }
                process(vp, par, onComplete, prevHmap, prevPar, useWorker = false, optimize = true, mask = false, onProgressCancel) {
                    if (this.processing) {
                        if (this.log)
                            console.log('busy!');
                        return false;
                    }
                    this.processing = true;
                    this.processCancelRequired = false;
                    if (this.log)
                        console.log('process');
                    const start = performance.now();
                    const progress = (p) => {
                        let canceled = this.processCancelRequired;
                        if (!canceled && onProgressCancel && p) {
                            canceled = onProgressCancel(p);
                            if (canceled) {
                                this.processCancelRequired = true;
                            }
                        }
                        return canceled;
                    };
                    const processed = (hmap, canceled) => {
                        const processedMs = performance.now() - start;
                        if (this.log) {
                            if (canceled || this.processCancelRequired) {
                                console.log('process canceled');
                            }
                            else {
                                console.log('processed', processedMs.toFixed(3) + 'ms');
                            }
                        }
                        onComplete(hmap, canceled);
                        this.processCancelRequired = this.processing = false;
                    };
                    let processing = false;
                    const target = this.tfm.toTarget(vp.clone(), true), data = new ComputeData(target, vp.width, vp.height, par.m, optimize, par.r, mask);
                    if (prevPar && prevHmap && prevHmap.data && par.isSameMR(prevPar) && !par.isSameABZ(prevPar)) {
                        const prevTarget = this.tfm.toOtherTarget(vp.clone(), prevPar.a, prevPar.b, prevPar.z, true);
                        if (!prevTarget.surroundsR(target))
                            processing = this.computeHeightMaps(prevTarget, data, (newParts, commonPart, canceled) => {
                                if (!canceled)
                                    this.combine(prevHmap, newParts, commonPart);
                                processed(prevHmap, canceled);
                            }, progress, useWorker);
                        if (processing && this.log)
                            console.log('image parts');
                    }
                    if (!processing) {
                        processing = this.computeHeightMap(data, (hmap, canceled) => {
                            if (!canceled)
                                this.tfm.regionToView(hmap.region).flipY().round();
                            processed(hmap, canceled);
                        }, progress, useWorker);
                        if (processing && this.log)
                            console.log('whole image');
                    }
                    this.processing = processing;
                    if (!processing) {
                        if (this.log)
                            console.log('compute busy');
                    }
                    return processing;
                }
                combine(prevHmap, newParts, commonPart) {
                    const isDezoom = commonPart && newParts.length == 4;
                    if (this.log)
                        console.log('combine', newParts.length, !!commonPart, isDezoom);
                    //console.log('combine', prevHmap, newParts, commonPart)
                    newParts.forEach(part => {
                        this.tfm.regionToView(part.region).flipY().round();
                        if (part.region.width != part.dataWidth)
                            console.warn('part size diff', part);
                    });
                    this.tfm.regionToView(commonPart).flipY().round();
                    prevHmap.combine(newParts, commonPart, this.log);
                }
                computeHeightMaps(fromTarget, cdr, onComplete, onProgressCancel, useWorker) {
                    const self = this, newParts = new Array(), target = cdr.target, targets = fromTarget.getPartsTo(target), kw = cdr.width / target.width, kh = cdr.height / target.height, cd = new ComputeData(null, 0, 0, cdr.max, cdr.optimize, cdr.radius, cdr.mask);
                    if (this.log)
                        console.log('computeHeightMaps', targets.length);
                    let commonPart, ta = target.area, i = 0, progress = 0;
                    if (targets.length > 1) {
                        commonPart = targets.shift();
                        ta -= commonPart.area;
                    }
                    function processNext() {
                        const targetPart = targets[i], k = targetPart.area / ta, id = 'part ' + (i + 1) + '/' + targets.length;
                        cd.width = Math.round(targetPart.width * kw);
                        cd.height = Math.round(targetPart.height * kh);
                        cd.target = targetPart;
                        function progressed(p) {
                            return onProgressCancel && onProgressCancel(progress + k * p) || self.processCancelRequired;
                        }
                        function completed(wr, canceled) {
                            if (canceled || !wr) {
                                if (self.log)
                                    console.log('canceled at compute');
                                onComplete(null, null, true);
                            }
                            else {
                                newParts.push(HeightMap_1.HeightMap.fromData(targetPart, wr.data, 0 /*wr.min*/, wr.max, cd.width));
                                if (i == targets.length - 1)
                                    onComplete(newParts, commonPart, false);
                                else {
                                    i++;
                                    progress += k;
                                    const started = processNext();
                                    if (!started) {
                                        if (self.log)
                                            console.log('next compute not started');
                                    }
                                }
                            }
                        }
                        return self.computeData(cd, useWorker, completed, progressed, id);
                    }
                    return processNext();
                }
                computeHeightMap(data, onComplete, onProgressCancel, useWorker, result) {
                    const self = this;
                    let canceled = false;
                    function progressed(p) {
                        canceled = onProgressCancel && onProgressCancel(p);
                        return canceled || self.processCancelRequired;
                    }
                    function completed(wr, canceled) {
                        if (canceled || !wr) {
                            if (self.log)
                                console.log('canceled at compute');
                            onComplete(null, true);
                        }
                        else {
                            let hmap = HeightMap_1.HeightMap.fromData(data.target, wr.data, 0 /*wr.min*/, wr.max, data.width, result && result.data, self.log);
                            if (result) {
                                result.copy(hmap, true);
                                hmap = result;
                            }
                            onComplete(hmap, false);
                        }
                    }
                    return this.computeData(data, useWorker, completed, progressed);
                }
                computeData(cd, useWorker, onComplete, onProgressCancel, id) {
                    if (!this.workProcesr)
                        this.workProcesr = new WorkProcesr_1.WorkProcesr(function (wd, onProgress) { FractWorker_1.FractWorker.compute(wd, onProgress); }, [FractWorker_1.WorkerData, FractWorker_1.WorkerResult, FractWorker_1.FractWorker], 'FractWorker', this.log);
                    if (this.workProcesr.busy) {
                        if (this.log)
                            console.log('busy');
                        return false;
                    }
                    const self = this, ps = onProgressCancel ? cd.max > 10000 ? 100 : cd.max > 5000 ? 50 : cd.max > 2000 ? 20 : 10 : 0, wd = FractWorker_1.WorkerData.fromRegion(cd.target, cd.width, cd.height, cd.max, cd.radius, cd.optimize, ps, cd.mask);
                    function completed(wr, canceled) {
                        if (wr && self.log)
                            console.log('optim ' + (wr.optimized ? ((100 * wr.optims / wr.data.length).toFixed(0) + '%') : false));
                        onComplete(wr, canceled);
                    }
                    return this.workProcesr.start(wd, useWorker, completed, onProgressCancel, id);
                }
                computePointFromView(x, y, max, radius = 2, mask = false, optimize = true) {
                    const a = this.tfm.xToTarget(x, y), b = this.tfm.yToTarget(x, y), wr = FractWorker_1.FractWorker.computePoint(a, b, max, radius, optimize, mask);
                    return new ComputedPoint(wr.data[0], wr.max, !wr.optimized, wr.optims > 0, wr.elapsedMs);
                }
                computePathFromView(x, y, max = 50, radius = 2, dataHolder) {
                    const a = this.tfm.xToTarget(x, y), b = this.tfm.yToTarget(x, y), xys = FractWorker_1.FractWorker.computePath(a, b, max, radius, dataHolder);
                    return this.tfm.xysToView(xys);
                }
            };
            exports_23("FractProcesr", FractProcesr);
        }
    };
});
System.register("shared/fract/FractMiniMap", ["shared/fract/FractWorker", "shared/HeightMap", "shared/DomHelper", "shared/Region2D", "shared/Matrix2D", "shared/Pixels", "shared/Color"], function (exports_24, context_24) {
    "use strict";
    var __moduleName = context_24 && context_24.id;
    var FractWorker_2, HeightMap_2, DomHelper_2, Region2D_3, Matrix2D_2, Pixels_2, Color_3, FractMiniMap;
    return {
        setters: [
            function (FractWorker_2_1) {
                FractWorker_2 = FractWorker_2_1;
            },
            function (HeightMap_2_1) {
                HeightMap_2 = HeightMap_2_1;
            },
            function (DomHelper_2_1) {
                DomHelper_2 = DomHelper_2_1;
            },
            function (Region2D_3_1) {
                Region2D_3 = Region2D_3_1;
            },
            function (Matrix2D_2_1) {
                Matrix2D_2 = Matrix2D_2_1;
            },
            function (Pixels_2_1) {
                Pixels_2 = Pixels_2_1;
            },
            function (Color_3_1) {
                Color_3 = Color_3_1;
            }
        ],
        execute: function () {
            FractMiniMap = class FractMiniMap {
                constructor(log = false) {
                    this.log = log;
                }
                updateMiniMap(doShowMap, resized, vp, tfm, zoom) {
                    if (!doShowMap || resized) {
                        this._minimapData = this._mmwv = undefined;
                    }
                    DomHelper_2.DomHelper.crud('MiniMap', doShowMap, DomHelper_2.DomHelper.topLeft('all'), (div, isNew) => {
                        const start = performance.now(), c = isNew ? div.appendChild(document.createElement('canvas')) : div.querySelector('canvas');
                        let mmd = this._minimapData;
                        if (!mmd) {
                            mmd = this._minimapData = this.makeMiniMapData(vp);
                        }
                        if (isNew || resized) {
                            DomHelper_2.DomHelper.setupCanvas(c, mmd.width, mmd.height);
                        }
                        const ctx = c.getContext('2d');
                        ctx.putImageData(this._minimapData, 0, 0);
                        if (zoom > 1) {
                            const rv = this._mmwv.transformRegion(tfm.regionToTarget(Region2D_3.Region2D.fromSize(vp)));
                            Pixels_2.Pixels.drawCrossBox(ctx, rv.xmin, rv.xmax, rv.ymin, rv.ymax, 15, Color_3.Color.Red);
                        }
                        if (this.log)
                            console.log('miniMap', (performance.now() - start).toFixed(3) + 'ms');
                    });
                }
                makeMiniMapData(vp, maxWidth = 200, maxHeight = 150) {
                    const start = performance.now(), vm = Region2D_3.Region2D.fromSize(vp).maxTo(maxWidth, maxHeight).round(), l = 1.2, x = -.5, rw = new Region2D_3.Region2D(x - l, x + l, -l, l, true), ro = vm.clone().fitTo(rw, true), wd = FractWorker_2.WorkerData.fromRegion(ro, vm.width, vm.height), wr = FractWorker_2.FractWorker.compute(wd), hmap = HeightMap_2.HeightMap.fromData(vm, wr.data, wr.min, wr.max), imd = Pixels_2.Pixels.imageFromHeightMap(hmap.data, vm.width, false, true);
                    if (this.log)
                        console.log('minimap', imd.width + 'x' + imd.height, (performance.now() - start).toFixed(3) + 'ms');
                    this._mmwv = this.makeMiniMapMatrix(vm, ro);
                    return imd;
                }
                makeMiniMapMatrix(rv, rt) {
                    return Matrix2D_2.Matrix2D.transformLR(Matrix2D_2.Matrix2D.translation(-rv.cx, -rv.cy), Matrix2D_2.Matrix2D.flipY(), Matrix2D_2.Matrix2D.scale(rv.scaleFactorToFit(rt, true)), Matrix2D_2.Matrix2D.translation(rt.cx, rt.cy)).getInverse();
                }
            };
            exports_24("FractMiniMap", FractMiniMap);
        }
    };
});
System.register("shared/Viewport2D", ["shared/DomHelper"], function (exports_25, context_25) {
    "use strict";
    var __moduleName = context_25 && context_25.id;
    var DomHelper_3, Viewport2D;
    return {
        setters: [
            function (DomHelper_3_1) {
                DomHelper_3 = DomHelper_3_1;
            }
        ],
        execute: function () {
            Viewport2D = class Viewport2D {
                constructor(canvas, onResized, log = true) {
                    this.canvas = canvas;
                    this.onResized = onResized;
                    this.log = log;
                    const self = this;
                    this.rs = _ => self._onResize();
                    this.attach(true);
                    this.resize(true);
                }
                get width() { return this.canvas.width; }
                get height() { return this.canvas.height; }
                get size() { return { width: this.width, height: this.height }; }
                getContext() { return this.canvas.getContext('2d'); }
                isSame(size) { return size && this.width == size.width && this.height == size.height; }
                attach(noResize = false) {
                    if (this.attached) {
                        return;
                    }
                    window.addEventListener('resize', this.rs);
                    this.attached = true;
                    //if (this.log) console.log('resize attached')
                    if (!noResize && !this.isSame(DomHelper_3.DomHelper.windowSize()))
                        this.resize();
                }
                detach() {
                    if (!this.attached) {
                        return;
                    }
                    window.removeEventListener('resize', this.rs);
                    //if (this.log) console.log('resize detached')
                    this.attached = false;
                }
                _onResize() {
                    clearTimeout(this.resizing);
                    this.resizing = setTimeout(() => this.resize(), 100);
                }
                resize(nodispatch = false) {
                    DomHelper_3.DomHelper.setupCanvas(this.canvas);
                    if (this.log)
                        console.log('resized', this.width + 'x' + this.height);
                    if (this.onResized && !nodispatch) {
                        this.onResized();
                    }
                }
            };
            exports_25("Viewport2D", Viewport2D);
        }
    };
});
System.register("shared/MouseEvents", [], function (exports_26, context_26) {
    "use strict";
    var __moduleName = context_26 && context_26.id;
    var MouseEvents;
    return {
        setters: [],
        execute: function () {
            MouseEvents = class MouseEvents {
                constructor(element, onClick, onDblClick, onMove) {
                    this.element = element;
                    this.onClick = onClick;
                    this.onDblClick = onDblClick;
                    this.onMove = onMove;
                    const self = this;
                    this.cl = (e) => { self._onClick(e); };
                    this.dc = (e) => { self._onDblClick(e); };
                    this.mm = (e) => { self._onMouseMove(e); };
                    this.md = (e) => { self.mdown = performance.now(); };
                    this.cm = (e) => { e.preventDefault(); };
                    this.attach();
                }
                attach() {
                    if (this.isAttached || !this.element) {
                        return;
                    }
                    this.element.addEventListener('click', this.cl);
                    this.element.addEventListener('dblclick', this.dc);
                    this.element.addEventListener('mousemove', this.mm);
                    this.element.addEventListener('mousedown', this.md);
                    this.element.addEventListener('contextmenu', this.cm);
                    this.isAttached = true;
                }
                detach() {
                    if (!this.isAttached || !this.element) {
                        return;
                    }
                    this.element.removeEventListener('click', this.cl);
                    this.element.removeEventListener('dblclick', this.dc);
                    this.element.removeEventListener('mousemove', this.mm);
                    this.element.removeEventListener('mousedown', this.md);
                    this.element.removeEventListener('contextmenu', this.cm);
                    this.isAttached = false;
                }
                _onClick(e) {
                    clearTimeout(this.clicking);
                    if (this.mdown && performance.now() - this.mdown > 700) {
                        return;
                    }
                    this.clicking = setTimeout(() => {
                        if (this.onClick)
                            this.onClick(e.layerX, e.layerY, e);
                    }, 200);
                }
                _onDblClick(e) {
                    clearTimeout(this.clicking);
                    if (this.onDblClick)
                        this.onDblClick(e.layerX, e.layerY, e);
                }
                _onMouseMove(e) {
                    if (this.onMove) {
                        this.onMove(e.layerX, e.layerY, e);
                    }
                }
            };
            exports_26("MouseEvents", MouseEvents);
        }
    };
});
System.register("shared/CanvasSelection", [], function (exports_27, context_27) {
    "use strict";
    var __moduleName = context_27 && context_27.id;
    function rgb(c) { return typeof c == 'string' ? c : `rgb(${c.r},${c.g},${c.b})`; }
    var CanvasSelection;
    return {
        setters: [],
        execute: function () {
            CanvasSelection = class CanvasSelection {
                constructor(canvas, onSelected, _showCross = false, strokeStyle = 'rgb(255,0,0)', _showXYInfo = false, getInfoXY = (x, y) => { return { x, y }; }, _infoXYClassName) {
                    this.canvas = canvas;
                    this.onSelected = onSelected;
                    this._showCross = _showCross;
                    this.strokeStyle = strokeStyle;
                    this._showXYInfo = _showXYInfo;
                    this.getInfoXY = getInfoXY;
                    this._infoXYClassName = _infoXYClassName;
                    this.log = false;
                    this.onUpdated = () => { };
                    const self = this;
                    this.md = (e) => { self.start(e.layerX, e.layerY); };
                    this.mm = (e) => { self.update(e.layerX, e.layerY); };
                    this.mu = (e) => { self.end(e.layerX, e.layerY); };
                    this.kd = (e) => { if (e.keyCode == 27)
                        self.cancelSelection(); };
                    this.getStrokeStyle = typeof strokeStyle == 'function' ? strokeStyle : () => strokeStyle;
                    this.attach();
                }
                get showCross() { return this._showCross; }
                set showCross(show) {
                    this.showXYInfo = this._showCross = show;
                    if (!show) {
                        this.cancelSelection();
                    }
                }
                get showXYInfo() { return this._showXYInfo; }
                set showXYInfo(show) { this._showXYInfo = show; if (!show) {
                    this.removeInfoDivs();
                } }
                attach() {
                    if (this.isAttached) {
                        return;
                    }
                    const canvas = this.canvas;
                    canvas.addEventListener('mousedown', this.md);
                    canvas.addEventListener('mousemove', this.mm);
                    canvas.addEventListener('mouseup', this.mu);
                    document.addEventListener('keydown', this.kd);
                    this.isAttached = true;
                }
                detach() {
                    if (!this.isAttached) {
                        return;
                    }
                    const canvas = this.canvas;
                    canvas.removeEventListener('mousedown', this.md);
                    canvas.removeEventListener('mousemove', this.mm);
                    canvas.removeEventListener('mouseup', this.mu);
                    document.removeEventListener('keydown', this.kd);
                    this.removeInfoDivs();
                    this.clear();
                    this.isAttached = false;
                }
                cancel() {
                    const wasStarted = this.isStarted;
                    this.reset();
                    if (wasStarted) {
                        if (this.log)
                            console.log('selection canceled');
                    }
                }
                reset(ctx, noUpdate = false) {
                    if (ctx) {
                        this.ctx = ctx;
                    }
                    this.imd = null;
                    this.isStarted = false;
                    this.x = this.y = undefined;
                    if (this._showCross && !noUpdate) {
                        this.update(this.cx, this.cy);
                    }
                    else if (noUpdate) {
                        this.removeInfoDivs();
                    }
                }
                start(x, y) {
                    this.tmdown = performance.now();
                    if (this.isStarted) {
                        return;
                    }
                    this.isStarted = true;
                    if (this.log)
                        console.log('selection start');
                    this.x = x;
                    this.y = y;
                    this.clear();
                }
                update(x, y) {
                    if (this._showCross && !this.isStarted) {
                        this.updateCross(x, y);
                    }
                    else if (this.isStarted && this.imd && x != this.x && y != this.y) {
                        this.updateBox(x, y);
                    }
                }
                end(x, y) {
                    this.tmup = performance.now();
                    if (!this.isStarted) {
                        return;
                    }
                    if (this.log)
                        console.log('selection end');
                    if (x != this.x || y != this.y) {
                        this.putImage();
                        this.imd = null;
                        const x1 = Math.min(this.x, x), y1 = Math.min(this.y, y);
                        const x2 = Math.max(this.x, x), y2 = Math.max(this.y, y);
                        this.onSelected({ x: x1, y: y1, width: x2 - x1, height: y2 - y1 });
                    }
                    else {
                        this.removeInfoDivs();
                    }
                    this.isStarted = false;
                }
                putImage() { if (this.imd) {
                    this.getCtx().putImageData(this.imd, 0, 0);
                    return true;
                } }
                getImage() { this.imd = this.getCtx().getImageData(0, 0, this.canvas.width, this.canvas.height); }
                clear() { this.putImage() || this.getImage(); }
                cancelSelection() {
                    this.putImage();
                    this.cancel();
                }
                updated() {
                    if (this.onUpdated) {
                        this.onUpdated();
                    }
                }
                getCtx() {
                    if (!this.ctx)
                        this.ctx = this.canvas.getContext('2d');
                    return this.ctx;
                }
                updateCross(x, y) {
                    if (x == undefined || y == undefined) {
                        return;
                    }
                    this.cx = x, this.cy = y;
                    const ctx = this.getCtx();
                    this.clear();
                    ctx.beginPath();
                    ctx.strokeStyle = rgb(this.getStrokeStyle(x, y, false));
                    ctx.moveTo(x - .5, -.5);
                    ctx.lineTo(x - .5, ctx.canvas.height);
                    ctx.moveTo(-.5, y - .5);
                    ctx.lineTo(ctx.canvas.width, y - .5);
                    ctx.stroke();
                    if (this._showXYInfo) {
                        this.showPosition(this.cx, this.cy);
                    }
                    this.updated();
                }
                updateBox(x, y) {
                    const ctx = this.getCtx();
                    this.putImage();
                    ctx.beginPath();
                    ctx.strokeStyle = rgb(this.getStrokeStyle(x, y, true));
                    ctx.strokeRect(this.x - .5, this.y - .5, x - this.x, y - this.y);
                    this.cx = x, this.cy = y;
                    this.updated();
                }
                showPosition(x, y) {
                    this.createInfoDivs();
                    const elX = this.divInfoX, elY = this.divInfoY;
                    if (!elX || !elY) {
                        return;
                    }
                    const info = this.getInfoXY(x, y), w = this.ctx.canvas.width, h = this.ctx.canvas.height, ww = window.innerWidth, wh = window.innerHeight, isLeft = x < w / 2, isTop = y < h / 2, sx = elX.style, sy = elY.style;
                    sx.setProperty('left', isLeft ? ((x + 1) + 'px') : 'auto');
                    sx.setProperty('right', isLeft ? 'auto' : ((ww - x + 2) + 'px'));
                    sx.setProperty('top', isTop ? 'auto' : '0');
                    sx.setProperty('bottom', isTop ? ((wh - h) + 'px') : 'auto');
                    elX.innerText = '' + info.x;
                    sy.setProperty('top', isTop ? ((y + 1) + 'px') : 'auto');
                    sy.setProperty('bottom', isTop ? 'auto' : ((wh - y + 3) + 'px'));
                    sy.setProperty('left', isLeft ? 'auto' : '0');
                    sy.setProperty('right', isLeft ? ((ww - w) + 'px') : 'auto');
                    elY.innerText = '' + info.y;
                }
                createInfoDiv() {
                    const el = document.createElement('div');
                    if (this._infoXYClassName) {
                        el.className = this._infoXYClassName;
                    }
                    const es = el.style;
                    es.setProperty('position', 'absolute');
                    es.setProperty('top', '0');
                    es.setProperty('left', '0');
                    es.setProperty('background-color', 'lightgray');
                    es.setProperty('padding', '0 2px');
                    document.body.appendChild(el);
                    return el;
                }
                createInfoDivs() {
                    if (!this.divInfoX) {
                        this.divInfoX = this.createInfoDiv();
                    }
                    if (!this.divInfoY) {
                        this.divInfoY = this.createInfoDiv();
                    }
                }
                removeInfoDivs() {
                    if (this.divInfoX) {
                        document.body.removeChild(this.divInfoX);
                    }
                    this.divInfoX = undefined;
                    if (this.divInfoY) {
                        document.body.removeChild(this.divInfoY);
                    }
                    this.divInfoY = undefined;
                }
            };
            exports_27("CanvasSelection", CanvasSelection);
        }
    };
});
System.register("shared/Zoning", [], function (exports_28, context_28) {
    "use strict";
    var __moduleName = context_28 && context_28.id;
    var ContourMethod, Zoning;
    return {
        setters: [],
        execute: function () {
            (function (ContourMethod) {
                ContourMethod[ContourMethod["NoContour"] = 0] = "NoContour";
                ContourMethod[ContourMethod["Moore"] = 1] = "Moore";
                ContourMethod[ContourMethod["Square"] = 2] = "Square";
            })(ContourMethod || (ContourMethod = {}));
            exports_28("ContourMethod", ContourMethod);
            Zoning = class Zoning {
                /** flood fill, DFS */
                static getZone(startPointIndex, pointLabel, zoneLabel, pointsIndex, zonesIndex, defaultZoneLabel, paddedWidth, zone, edges, pointTypeIndex, PT_INTERIOR, PT_EDGE) {
                    edges.length = 0;
                    const nrs = [1, paddedWidth, -1, -paddedWidth]; // relative von neumann neighborhood; E, S, W, N
                    const stack = new Array();
                    stack.push(startPointIndex);
                    while (stack.length) {
                        const pi = stack.pop();
                        if (zonesIndex[pi] != defaultZoneLabel)
                            continue; //already visited
                        //mark point as visited
                        zonesIndex[pi] = zoneLabel;
                        zone.push(pi);
                        //visit each of its 4 neighbours
                        let nn = 0;
                        for (let inr = 0; inr < 4; inr++) {
                            const ni = pi + nrs[inr];
                            if (pointsIndex[ni] == pointLabel) {
                                nn++;
                                if (zonesIndex[ni] == defaultZoneLabel)
                                    stack.push(ni); //add it to visit
                            }
                        }
                        if (nn < 4) {
                            edges.push(pi);
                            if (pointTypeIndex)
                                pointTypeIndex[pi] = PT_EDGE;
                        }
                        else if (pointTypeIndex)
                            pointTypeIndex[pi] = PT_INTERIOR;
                    }
                }
                static getContour(firstEdgeIdx, zoneIdx, zoneIndex, paddedWidth, contourMethod = ContourMethod.Moore, removeColinears = false) {
                    let contour = contourMethod == ContourMethod.Moore ?
                        Zoning.mooreContour(firstEdgeIdx, zoneIdx, zoneIndex, paddedWidth)
                        : Zoning.squareContour(firstEdgeIdx, zoneIdx, zoneIndex, paddedWidth);
                    if (contour.length > 1) {
                        if (contour[contour.length - 1] == contour[0]) {
                            //duplicate first/last point
                            contour.splice(contour.length - 1);
                        }
                        //first point is second
                        contour.unshift(...contour.splice(contour.length - 1)); //moves last to first
                        if (removeColinears)
                            contour = Zoning.removeColinears(contour, paddedWidth);
                    }
                    return contour;
                }
                static squareContour(firstEdgePointIndex, zoneIdx, zoneIndex, paddedWidth) {
                    ///http://www.imageprocessingplace.com/downloads_V3/root_downloads/tutorials/contour_tracing_Abeer_George_Ghuneim/square.html
                    const w = paddedWidth, nrs = [1, w, -1, -w]; // relative von neumann neighborhood; E, S, W, N
                    let p, d;
                    for (d = 0; d < 4; d++) {
                        p = firstEdgePointIndex + nrs[d];
                        if (zoneIndex[p] == zoneIdx)
                            break;
                    }
                    const p0 = p, d0 = d, boundary = new Array();
                    boundary.push(p0);
                    do {
                        if (zoneIndex[p] == zoneIdx) {
                            boundary.push(p);
                            d = d == 0 ? 3 : d - 1;
                        }
                        else
                            d = (d + 1) % 4;
                        p += nrs[d];
                    } while (p != p0 || d != d0);
                    return boundary;
                }
                static mooreContour(firstEdgePointIndex, zoneIdx, zoneIndex, paddedWidth) {
                    /// https://fr.mathworks.com/matlabcentral/fileexchange/42144-moore-neighbor-boundary-trace
                    const initial_entry = firstEdgePointIndex;
                    /// Designate a directional offset array for search positions
                    const w = paddedWidth;
                    const neighborhood = [-1, -w - 1, -w, -w + 1, 1, w + 1, w, w - 1];
                    const exit_direction = [7, 7, 1, 1, 3, 3, 5, 5];
                    /// Find the first point in the boundary, Moore-Neighbor of entry point
                    let c, n;
                    for (n = 0; n < 8; n++) {
                        c = initial_entry + neighborhood[n];
                        if (zoneIndex[c] == zoneIdx)
                            break;
                    }
                    const initial_position = c;
                    /// Set next direction based on found pixel ( i.e. 3 -> 1)
                    const initial_direction = exit_direction[n];
                    /// Start the boundary set with this pixel
                    const boundary = new Array();
                    boundary.push(initial_position);
                    /// Initialize variables for boundary search
                    let position = initial_position;
                    let direction = initial_direction;
                    /// Return a list of the ordered boundary pixels
                    while (true) {
                        /// Find the next neighbor with a clockwise search
                        for (let i = 0; i < 8; i++) {
                            n = (i + direction) % 8;
                            c = position + neighborhood[n];
                            if (zoneIndex[c] == zoneIdx) {
                                position = c;
                                break;
                            }
                        }
                        /// Neighbor found, save its information
                        direction = exit_direction[n];
                        boundary.push(position);
                        /// Entered the initial pixel the same way twice, the end
                        if (position == initial_position && direction == initial_direction)
                            break;
                    }
                    return boundary;
                }
                static removeColinears(indices, width, isClosed = true, result = new Array()) {
                    if (!indices)
                        return result;
                    if (indices.length < 3) {
                        result.push(...indices);
                        return result;
                    }
                    let i1 = indices[0], x1 = i1 % width, y1 = ~~(i1 / width), i2 = indices[1], x2 = i2 % width, y2 = ~~(i2 / width), i3, x3, y3;
                    result.push(i1);
                    for (let i = 2, l = indices.length; i < l; i++) {
                        i3 = indices[i], x3 = i3 % width, y3 = ~~(i3 / width);
                        if ((y1 - y2) * (x1 - x3) != (y1 - y3) * (x1 - x2)) {
                            result.push(i2);
                            i1 = i2, x1 = x2, y1 = y2;
                        }
                        i2 = i3, x2 = x3, y2 = y3;
                    }
                    if (isClosed) {
                        i3 = indices[0], x3 = i3 % width, y3 = ~~(i3 / width);
                        if ((y1 - y2) * (x1 - x3) != (y1 - y3) * (x1 - x2))
                            result.push(i2);
                    }
                    return result;
                }
            };
            exports_28("Zoning", Zoning);
        }
    };
});
System.register("shared/Triangulation", ["poly2tri", "shared/Zoning"], function (exports_29, context_29) {
    "use strict";
    var __moduleName = context_29 && context_29.id;
    var poly2tri_1, Zoning_1, Triangulation, TriResult;
    return {
        setters: [
            function (poly2tri_1_1) {
                poly2tri_1 = poly2tri_1_1;
            },
            function (Zoning_1_1) {
                Zoning_1 = Zoning_1_1;
            }
        ],
        execute: function () {
            Triangulation = class Triangulation {
                static triangulate(contour, width, lod, steinerPointsIndices, log = false, maxRemoveBadPoints = 100) {
                    if (!contour)
                        return;
                    const indices = Zoning_1.Zoning.removeColinears(contour, width);
                    if (!indices)
                        return;
                    function getPoints(indices) {
                        return indices.map(i => ({
                            x: (i % width) * lod,
                            y: ~~(i / width) * lod,
                            i
                        }));
                    }
                    const steinerPoints = steinerPointsIndices
                        && steinerPointsIndices.length
                        && getPoints(steinerPointsIndices);
                    let sc, bps = new Array();
                    while (--maxRemoveBadPoints) {
                        try {
                            sc = new poly2tri_1.SweepContext(getPoints(indices));
                            if (steinerPoints)
                                sc.addPoints(steinerPoints);
                            sc.triangulate();
                            break;
                        }
                        catch (e) {
                            if (log)
                                console.warn(e);
                            const pe = e, pps = pe && pe.points, ids = pps && pps.length && pps.map(p => p.i);
                            if (ids) {
                                //if triplet remove mid point
                                const pi = ids[ids.length === 3 ? 1 : 0];
                                const ii = indices.indexOf(pi);
                                indices.splice(ii, 1);
                                bps.push(pi);
                            }
                        }
                    }
                    return new TriResult(sc.getTriangles(), bps && bps.length ? bps : undefined, steinerPointsIndices);
                }
            };
            exports_29("Triangulation", Triangulation);
            TriResult = class TriResult {
                constructor(tris, badPoints, steinerPoints) {
                    this.tris = tris;
                    this.badPoints = badPoints;
                    this.steinerPoints = steinerPoints;
                }
                get nbTriangles() { return this.tris && this.tris.length || 0; }
                /** [ia, ib, ic,  ia, ib, ic ] */
                getTriIndices(result = new Array()) {
                    const tris = this.tris;
                    for (let i = 0, l = tris.length; i < l; i++) {
                        const t = tris[i];
                        result.push(//2, 1, 0 to get normal vectors upwards
                        t.getPoint(2).i, t.getPoint(1).i, t.getPoint(0).i);
                    }
                    return result;
                }
                /** [[[x,y], [x,y], [x,y]], [[x,y], [x,y], [x,y]], ...] */
                getTriangles(result = new Array()) {
                    const tris = this.tris;
                    for (let i = 0, l = tris.length; i < l; i++) {
                        const t = tris[i].getPoints(), a = t[0], b = t[1], c = t[2];
                        result.push([[a.x, a.y], [b.x, b.y], [c.x, c.y]]);
                    }
                    return result;
                }
                /** [[xa,ya,xb,yb,xc,yc], [xa,ya,xb,yb,xc,yc], ...] */
                getTrianglesXys(result = new Array()) {
                    const triangles = this.tris;
                    for (let i = 0, l = triangles && triangles.length || 0; i < l; i++) {
                        const t = triangles[i].getPoints(), a = t[0], b = t[1], c = t[2];
                        result.push([a.x, a.y, b.x, b.y, c.x, c.y]);
                    }
                    return result;
                }
            };
            exports_29("TriResult", TriResult);
        }
    };
});
System.register("shared/Neighborhood", [], function (exports_30, context_30) {
    "use strict";
    var __moduleName = context_30 && context_30.id;
    var Neighborhood;
    return {
        setters: [],
        execute: function () {
            Neighborhood = class Neighborhood {
                static prepare(imageWidth) {
                    const nrs = Neighborhood.get8Wcw(imageWidth);
                    function getDir(fi, ti) {
                        return nrs.indexOf(ti - fi);
                    }
                    function getIndex(ri, dir) {
                        return ri - nrs[dir];
                    }
                    function getIndexRight(fi, ti) {
                        return ti + nrs[Neighborhood._dirsRightFrom[nrs.indexOf(ti - fi)]];
                    }
                    function getIndexLeft(fi, ti) {
                        return ti + nrs[Neighborhood._dirsLeftFrom[nrs.indexOf(ti - fi)]];
                    }
                    return {
                        getDir, getIndex, getIndexRight, getIndexLeft
                    };
                }
                /** Von Neumann neighborhood: 4 relative indices, from right, clockwise (like E, S, W, N), at the given distance or 1 */
                static get4Ecw(imgWidth, distance = 1) {
                    return [distance, imgWidth * distance, -distance, -imgWidth * distance];
                }
                /** Moore neighborhood: 8 relative indices, from top-left, clockwise, at the given distance or 1 */
                static get8Wcw(imgWidth, distance = 1) {
                    const l = distance, w = imgWidth * l;
                    return [-w - l, -w, -w + l, l, w + l, w, w - l, -l];
                }
                /** neighbors relative indices within a radial range */
                static getRange(range, imgWidth, lod = 1) {
                    if (range < 1)
                        return;
                    const neighborhood = new Array(), nr = -range, r2 = range * range;
                    for (let y = nr; y <= range; y++) {
                        let y2 = y * y, ywl = y * imgWidth * lod;
                        for (let x = nr; x <= range; x++)
                            if (x != 0 && y != 0 && y2 + x * x <= r2)
                                neighborhood.push(ywl + x * lod);
                    }
                    return neighborhood;
                }
                /**
                 * neighbors relative indices within a radial range, with caching
                 * @param range radius of a circle
                 * @param imgWidth image width
                 * @param lod level of detail (ie distance between grid points)
                 * @param log message logging to console
                 */
                static getCachedRange(range, imgWidth, lod = 1, log = false) {
                    if (range < 1)
                        return;
                    let map = Neighborhood._nrRangeCache;
                    if (!map)
                        Neighborhood._nrRangeCache = map = new Map();
                    const key = range + '/' + imgWidth + '/' + lod;
                    let neighborhood = map.get(key);
                    if (neighborhood)
                        return neighborhood;
                    neighborhood = Neighborhood.getRange(range, imgWidth, lod);
                    map.set(key, neighborhood);
                    if (log)
                        console.log('getCachedRange', key, neighborhood);
                    return neighborhood;
                }
                /**
                 * returns the direction id, from the given input pixel index
                 * to the given neighbor pixel index.
                 * 0=NW, 1=N incrementing clockwise up to 7=W
                 * @param fi from pixel index
                 * @param ti to neighbour pixel index
                 * @param w image width
                 */
                static getDir(fi, ti, w) {
                    switch (ti - fi) {
                        case -w - 1: return 0; //NW
                        case -w: return 1; //N
                        case -w + 1: return 2; //NE
                        case 1: return 3; //E
                        case w + 1: return 4; //SE
                        case w: return 5; //S
                        case w - 1: return 6; //SW
                        case -1: return 7; //W
                    }
                }
                /**
                 * returns the index of the neighbor pixel in the given direction from the given reference pixel
                 * @param ri: reference pixel index
                 * @param dir: direction; 0=NW, incrementing clockwise up to 7=W
                 * @param w: image width
                */
                static getIndex(ri, dir, w) {
                    switch (dir) {
                        case 0: return ri - w - 1;
                        case 1: return ri - w;
                        case 2: return ri - w + 1;
                        case 3: return ri + 1;
                        case 4: return ri + w + 1;
                        case 5: return ri + w;
                        case 6: return ri + w - 1;
                        case 7: return ri - 1;
                    }
                }
                /** W|NW => N; N|NE => E; E|SE => S; S|SW => W */
                static getDirLeftFrom(dir) {
                    return Neighborhood._dirsLeftFrom[dir];
                }
                /** NW|N => W; NE|E => N; SE,S => E; SW,W => S */
                static getDirRightFrom(dir) {
                    return Neighborhood._dirsRightFrom[dir];
                }
                /** NW => N; N => NE, ... */
                static getDirLeftForwardFrom(dir) {
                    return (dir + 1) % 8;
                }
                /** NW => W; W => SW, ... */
                static getDirRightForwardFrom(dir) {
                    return dir == 0 ? 7 : dir - 1;
                }
                /** NW => SE; W => E, ... */
                static getDirOpposite(dir) {
                    return (dir + 4) % 8;
                }
                static getMidDir(dirIn, dirOut) {
                    return (dirIn + ~~((dirOut - dirIn) / 2)) % 8;
                }
                static getRandomDir() {
                    return ~~(Math.random() * 8);
                }
                /** returns true is the turn is to the left  */
                static isTurnLeft(dirIn, dirOut) {
                    switch (dirIn) {
                        case 0: return dirOut > 0 && dirOut < 4;
                        case 1: return dirOut > 1 && dirOut < 5;
                        case 2: return dirOut > 2 && dirOut < 6;
                        case 3: return dirOut > 3 && dirOut < 7;
                        case 4: return dirOut > 4 && dirOut <= 7;
                        case 5: return dirOut > 5 || dirOut < 1;
                        case 6: return dirOut > 6 || dirOut < 2;
                        case 7: return dirOut < 3;
                    }
                }
                /** returns true is the turn is to the right */
                static isTurnRight(dirIn, dirOut) {
                    switch (dirIn) {
                        case 0: return dirOut > 4;
                        case 1: return dirOut < 1 || dirOut > 5;
                        case 2: return dirOut < 2 || dirOut > 6;
                        case 3: return dirOut < 3;
                        case 4: return dirOut > 0 && dirOut < 4;
                        case 5: return dirOut > 1 && dirOut < 5;
                        case 6: return dirOut > 2 && dirOut < 6;
                        case 7: return dirOut > 3 && dirOut < 7;
                    }
                }
                /** returns true if the given directions are opposites  */
                static isNoTurn(dirIn, dirOut) {
                    return dirOut == (dirIn < 4 ? (dirIn + 4) : (dirIn - 4));
                }
                static isDiagonal(dir) {
                    return dir % 2 == 0;
                }
                static isStraight(dir) {
                    return dir % 2 != 0;
                }
                /**
                 * returns the index of the pixel on the right of the reference given pixel
                 * when coming from the given neighbor pixel
                 * @param ri reference pixel index
                 * @param pi previous pixel index
                 * @param w image width
                 */
                static getIndexRightFrom(ri, pi, w) {
                    const dir = Neighborhood.getDir(pi, ri, w);
                    const right = Neighborhood.getDirRightFrom(dir);
                    return Neighborhood.getIndex(ri, right, w);
                }
                /**
                 * returns the index of the pixel on the left of the reference given pixel
                 * when coming from the given neighbor pixel
                 * @param ri center pixel index
                 * @param pi previous pixel index
                 * @param w image width
                 */
                static getIndexLeftFrom(ri, pi, w) {
                    const dir = Neighborhood.getDir(pi, ri, w);
                    const left = Neighborhood.getDirLeftFrom(dir);
                    return Neighborhood.getIndex(ri, left, w);
                }
                /**
                 * returns the index of the pixel on the right and forward of the reference given pixel
                 * when coming from the given neighbor pixel
                 * @param ri reference pixel index
                 * @param pi previous pixel index
                 * @param w image width
                 */
                static getIndexRightForwardFrom(ri, pi, w) {
                    const dir = Neighborhood.getDir(pi, ri, w);
                    const right = Neighborhood.getDirRightForwardFrom(dir);
                    return Neighborhood.getIndex(ri, right, w);
                }
                /**
                 * returns the index of the pixel on the left and forward of the reference given pixel
                 * when coming from the given neighbor pixel
                 * @param ri reference pixel index
                 * @param pi previous pixel index
                 * @param w image width
                 */
                static getIndexLeftForwardFrom(ri, pi, w) {
                    const dir = Neighborhood.getDir(pi, ri, w);
                    const left = Neighborhood.getDirLeftForwardFrom(dir);
                    return Neighborhood.getIndex(ri, left, w);
                }
                /**
                 * returns the index of the neighbour pixel of the reference given pixel,
                 * at the opposite direction from the given previous pixel
                 * @param ri reference pixel index
                 * @param pi previous pixel index
                 * @param w image width
                 */
                static getIndexOppositeFrom(ri, pi, w) {
                    const dir = Neighborhood.getDir(pi, ri, w);
                    const opp = (dir + 4) % 8;
                    return Neighborhood.getIndex(ri, opp, w);
                }
                /**
                 * return the angle id:
                 * 4 = flat; 0 = u-turn; >0 = CW, <0 = CCW;
                 * 0=0°, 1=45°; 2=90°; 3=135°; 4=180°; 5=225°; 6=270; 7=315°
                 * @param pi previous pixel index (entering)
                 * @param ri reference pixel index
                 * @param ni next pixel index (exiting)
                 * @param w image width
                 */
                static getAngleId(pi, ri, ni, w) {
                    const dirIn = Neighborhood.getDir(pi, ri, w);
                    const dirOut = Neighborhood.getDir(ri, ni, w);
                    return dirOut - dirIn;
                }
                /**
                 * 4 = flat; 0 = u-turn; >0 = CW, <0 = CCW;
                 * 1 = 45°; 2 = 90°; 3=135°; 4=180°; 5=225°; 6=270; 7=315°
                */
                static angleOfDirs(dirIn, dirOut) {
                    return dirOut - dirIn;
                }
            };
            Neighborhood._dirsRightFrom = [7, 7, 1, 1, 3, 3, 5, 5];
            Neighborhood._dirsLeftFrom = [1, 3, 3, 5, 5, 7, 7, 1];
            exports_30("Neighborhood", Neighborhood);
        }
    };
});
System.register("shared/HeightMapZones", ["shared/Triangulation", "shared/Zoning", "shared/Neighborhood"], function (exports_31, context_31) {
    "use strict";
    var __moduleName = context_31 && context_31.id;
    var Triangulation_1, Zoning_2, Neighborhood_1, HMZPointType, HMapZone, HeightMapZones;
    return {
        setters: [
            function (Triangulation_1_1) {
                Triangulation_1 = Triangulation_1_1;
            },
            function (Zoning_2_1) {
                Zoning_2 = Zoning_2_1;
            },
            function (Neighborhood_1_1) {
                Neighborhood_1 = Neighborhood_1_1;
            }
        ],
        execute: function () {
            (function (HMZPointType) {
                HMZPointType[HMZPointType["Nothing"] = 0] = "Nothing";
                HMZPointType[HMZPointType["Interior"] = 1] = "Interior";
                HMZPointType[HMZPointType["Edge"] = 2] = "Edge";
                HMZPointType[HMZPointType["Contour"] = 3] = "Contour";
                HMZPointType[HMZPointType["Noise"] = 4] = "Noise";
            })(HMZPointType || (HMZPointType = {}));
            exports_31("HMZPointType", HMZPointType);
            HMapZone = class HMapZone {
                constructor(zoneIndex, levelIndex, levelZoneIndex) {
                    this.zoneIndex = zoneIndex;
                    this.levelIndex = levelIndex;
                    this.levelZoneIndex = levelZoneIndex;
                    this.points = new Array();
                }
            };
            exports_31("HMapZone", HMapZone);
            HeightMapZones = class HeightMapZones {
                constructor(lod = 1, contourMinArea = 1, log = false, debug = false, data, dataWidth, width, height, zones, levelZoneIndex, pointLevelIndex, pointZoneIndex, pointTypeIndex, noiseZones, pointNoiseZoneIndex, maxLevelIndex) {
                    this.lod = lod;
                    this.contourMinArea = contourMinArea;
                    this.log = log;
                    this.debug = debug;
                    this.data = data;
                    this.dataWidth = dataWidth;
                    this.width = width;
                    this.height = height;
                    this.zones = zones;
                    this.levelZoneIndex = levelZoneIndex;
                    this.pointLevelIndex = pointLevelIndex;
                    this.pointZoneIndex = pointZoneIndex;
                    this.pointTypeIndex = pointTypeIndex;
                    this.noiseZones = noiseZones;
                    this.pointNoiseZoneIndex = pointNoiseZoneIndex;
                    this.maxLevelIndex = maxLevelIndex;
                }
                static compute(hmap, lod = 1, contourMinArea = 1, log = false, debug = false, contourMethod = Zoning_2.ContourMethod.Moore, removeColinears = false) {
                    if (lod > 1)
                        hmap.makeEven(lod, log && debug);
                    const result = HeightMapZones.zonesByLevel(hmap.data, hmap.dataWidth, lod, contourMethod, contourMinArea, removeColinears, log, debug);
                    if (log && debug)
                        console.log('HeightMapZones', !result ? result : result.levelZoneIndex.length + ' levels', result.zones.length + ' zones');
                    return result;
                }
                get levelsCount() { return this.levelZoneIndex.length; }
                get zonesCount() { return this.zones.length; }
                getZonesByLevel() {
                    return this.levelZoneIndex.map(zis => zis.map(zi => this.zones[zi]));
                }
                getPointTypeCounts() {
                    return this.pointTypeIndex.reduce((p, c) => { p[c]++; return p; }, new Array(4).fill(0));
                }
                getPoint(index) {
                    return { x: index % this.width, y: ~~(index / this.width) };
                }
                getLevelIndex(x, y) {
                    return this.pointLevelIndex[this.width * y + x];
                }
                getLevelZones(levelIndex) {
                    const zis = this.levelZoneIndex[levelIndex];
                    return zis && zis.map(zi => this.zones[zi]);
                }
                drawLevel(level, imd, pointsColor, contourColor, info, fillLod = false, mixColor = false, byZone = false) {
                    if (pointsColor) {
                        if (byZone) {
                            if (info)
                                info.points = 0;
                            level.forEach(z => {
                                this.draw(z.points, imd, pointsColor, fillLod, mixColor);
                                if (info)
                                    info.points += z.contour.length;
                            });
                        }
                        else {
                            const indices = Array.prototype.concat(...level.map(z => z.points));
                            this.draw(indices, imd, pointsColor, fillLod, mixColor);
                            if (info)
                                info.points = indices.length;
                        }
                    }
                    if (contourColor) {
                        if (byZone) {
                            if (info)
                                info.contour = 0;
                            level.forEach(z => {
                                this.draw(z.contour, imd, contourColor, fillLod, mixColor);
                                if (info)
                                    info.contour += z.contour.length;
                            });
                        }
                        else {
                            const indices = Array.prototype.concat(...level.map(z => z.contour));
                            this.draw(indices, imd, contourColor, fillLod, mixColor);
                            if (info)
                                info.contour = indices.length;
                        }
                    }
                }
                getZone(x, y) {
                    return this.zones[this.pointZoneIndex[this.width * y + x]];
                }
                drawZone(zone, imd, pointsColor, contourColor, fillLod = false, mixColor = false) {
                    if (pointsColor)
                        this.draw(zone.points, imd, pointsColor, fillLod, mixColor);
                    if (contourColor)
                        this.draw(zone.contour, imd, contourColor, fillLod, mixColor);
                }
                drawTris(tris, imd, color, mixColor = false) {
                    this.draw(tris, imd, color, true, mixColor);
                }
                triangulate(zone, steinersGridDistance, steinersEdgeDistance, steinersClipInterior = false, maxRemovedBadPoints = 100, logWarnings = true) {
                    if (!zone || !zone.contour || !zone.contour.length)
                        return;
                    const steiners = steinersGridDistance &&
                        this.getSteinerPoints(zone, steinersGridDistance, steinersEdgeDistance, steinersClipInterior);
                    return Triangulation_1.Triangulation.triangulate(zone.contour, this.width, this.lod, steiners, this.debug && logWarnings, maxRemovedBadPoints);
                }
                computeBoundingBox(zone) {
                    if (!zone.boundingBox)
                        zone.boundingBox = HeightMapZones.getBoundingBox(zone.contour, this.width);
                    return zone.boundingBox;
                }
                /** return points in the polygon described by the given zone contour */
                getSteinerPoints(zone, gridMinDistance, edgeMinDistance, clipInterior = true) {
                    gridMinDistance = Math.round(gridMinDistance);
                    const indices = new Array(), bb = this.computeBoundingBox(zone), neighborhood = edgeMinDistance &&
                        Neighborhood_1.Neighborhood.getCachedRange(edgeMinDistance, this.width, 1, this.debug), zoneIdx = zone.zoneIndex, imgWidth = this.width, zoneIndex = this.pointZoneIndex, pointType = this.pointTypeIndex, hgd = Math.round(gridMinDistance / 2), y0 = bb.ymin + hgd, yl = bb.ymax, x0 = bb.xmin + hgd, xl = bb.xmax, PT_INTERIOR = HMZPointType.Interior, PT_CONTOUR = HMZPointType.Contour;
                    for (let y = y0; y < yl; y += gridMinDistance) {
                        for (let x = x0; x < xl; x += gridMinDistance) {
                            const pi = y * imgWidth + x;
                            if (pointType[pi] == PT_INTERIOR
                                && zoneIndex[pi] == zoneIdx
                                && (!neighborhood || !neighborhood.some(ri => {
                                    const ni = pi + ri;
                                    return pointType[ni] == PT_CONTOUR
                                        && zoneIndex[ni] == zoneIdx;
                                }))) {
                                indices.push(pi);
                            }
                        }
                    }
                    if (!clipInterior)
                        return indices;
                    const nrs = Neighborhood_1.Neighborhood.get4Ecw(imgWidth, gridMinDistance);
                    return indices.reduce((sis, si) => {
                        if (nrs.reduce((sum, ri) => indices.some(i => i == si + ri)
                            ? (sum + 1) : sum, 0) < 4) {
                            sis.push(si);
                        }
                        return sis;
                    }, new Array());
                }
                draw(indices, imd, color, fillLod, mixColor = false) {
                    if (!indices || indices.length == 0)
                        return;
                    const lod = this.lod, lf = fillLod ? lod : 1, lw = this.width, w = imd.width, il = Math.max(1, indices.length - 1);
                    let c, isFunc = typeof color == 'function';
                    if (!isFunc)
                        c = color;
                    let an, ao, mix;
                    indices.forEach((pi, i) => {
                        if (isFunc)
                            c = color(i / il);
                        mix = mixColor && c.a != undefined;
                        if (mix) {
                            an = c.a / 255, ao = 1 - an;
                        }
                        for (let ly = 0; ly < lf; ly++) {
                            for (let lx = 0; lx < lf; lx++) {
                                const p = 4 * (lod == 1 ? pi : (~~(pi / lw) * lod + ly) * w + (pi % lw) * lod + lx);
                                if (mix) {
                                    imd.data[p] = ~~(imd.data[p] * ao + c.r * an);
                                    imd.data[p + 1] = ~~(imd.data[p + 1] * ao + c.g * an);
                                    imd.data[p + 2] = ~~(imd.data[p + 2] * ao + c.b * an);
                                    imd.data[p + 3] = 255;
                                }
                                else {
                                    imd.data[p] = c.r;
                                    imd.data[p + 1] = c.g;
                                    imd.data[p + 2] = c.b;
                                    if (c.a != undefined)
                                        imd.data[p + 3] = c.a;
                                }
                            }
                        }
                    });
                }
                static getBoundingBox(indices, width, unpad = false) {
                    let xmin = Infinity, xmax = -Infinity, ymin = xmin, ymax = xmax;
                    indices.forEach(i => {
                        const x = i % width, y = ~~(i / width);
                        if (x < xmin)
                            xmin = x;
                        if (x > xmax)
                            xmax = x;
                        if (y < ymin)
                            ymin = y;
                        if (y > ymax)
                            ymax = y;
                    });
                    if (unpad) {
                        xmin--, xmax--, ymin--, ymax--;
                    }
                    return { xmin, xmax, ymin, ymax };
                }
                static zonesByLevel(data, dataWidth, lod = 1, contourMethod, contourMinArea = 1, removeContourColinears = false, log = false, debug = false) {
                    if (!data)
                        return null;
                    let levelIndex, zoneIndex, typeIndex, noiseZoneIndex, maxLevelIndex;
                    const levelZoneIndex = new Array(), zones = new Array(), noiseZones = new Array(), w = Math.round(dataWidth / lod), h = Math.round((data.length / dataWidth) / lod), wp2 = w + 2, hp2 = h + 2, whp2 = wp2 * hp2, wp1 = w + 1, hp1 = h + 1, NOTHING = -1 >>> 0; // Uint32 max value
                    function init() {
                        typeIndex = new Uint32Array(whp2);
                        levelIndex = new Uint32Array(whp2).fill(NOTHING);
                        zoneIndex = new Uint32Array(whp2).fill(NOTHING);
                        noiseZoneIndex = new Uint32Array(whp2).fill(NOTHING);
                        const levelsMap = new Map(); //value -> index
                        const lw = lod * dataWidth;
                        let ywp2, yn1lw, v, li;
                        for (let y = 1; y < hp1; y++) {
                            ywp2 = y * wp2;
                            yn1lw = (y - 1) * lw;
                            for (let x = 1; x < wp1; x++) {
                                v = data[yn1lw + (x - 1) * lod];
                                li = levelsMap.get(v);
                                if (li == undefined) {
                                    li = levelZoneIndex.length;
                                    levelZoneIndex.push(new Array());
                                    levelsMap.set(v, li);
                                }
                                levelIndex[ywp2 + x] = li;
                            }
                        }
                        maxLevelIndex = levelsMap.get(1);
                    }
                    function parcour() {
                        const PT_CONTOUR = HMZPointType.Contour, noContour = new Array(), edges = new Array(), noise = new Array();
                        for (let y = 1; y < hp1; y++) {
                            const ywp2 = y * wp2;
                            for (let x = 1; x < wp1; x++) {
                                const i = ywp2 + x;
                                if (zoneIndex[i] == NOTHING) {
                                    const li = levelIndex[i], lzis = levelZoneIndex[li], zi = zones.length, lzi = lzis.length, zone = new HMapZone(zi, li, lzi);
                                    lzis.push(zi);
                                    zones.push(zone);
                                    edges.length = 0;
                                    Zoning_2.Zoning.getZone(i, li, zi, levelIndex, zoneIndex, NOTHING, wp2, zone.points, edges, typeIndex, HMZPointType.Interior, HMZPointType.Edge);
                                    zone.nbEdges = edges.length;
                                    if (contourMethod == Zoning_2.ContourMethod.NoContour) {
                                        zone.contour = noContour;
                                    }
                                    else if (zone.points.length < contourMinArea) {
                                        zone.contour = noContour;
                                        noise.push(...zone.points);
                                    }
                                    else {
                                        zone.contour = Zoning_2.Zoning.getContour(edges[0], zi, zoneIndex, wp2, contourMethod, removeContourColinears);
                                        zone.contour.forEach(i => typeIndex[i] = PT_CONTOUR);
                                    }
                                }
                            }
                        }
                        const NOISE = HMZPointType.Noise;
                        noise.forEach(i => typeIndex[i] = NOISE);
                        noise.forEach(i => {
                            if (noiseZoneIndex[i] == NOTHING) {
                                const nzi = noiseZones.length;
                                const zone = new HMapZone(nzi);
                                noiseZones.push(zone);
                                Zoning_2.Zoning.getZone(i, NOISE, nzi, typeIndex, noiseZoneIndex, NOTHING, wp2, zone.points, edges);
                                zone.contour = Zoning_2.Zoning.getContour(edges[0], nzi, noiseZoneIndex, wp2);
                                //zone.contour.forEach(i => typeIndex[i] = PT_CONTOUR)
                            }
                        });
                        if (debug) {
                            const c = noiseZones.reduce((p, c) => p + c.points.length, 0);
                            if (c != noise.length)
                                console.warn('noise', noiseZones.length, c, noiseZones.map(z => z.points.length));
                        }
                    }
                    function unpad() {
                        function unpadImage(image) {
                            const result = new Uint32Array(h * w);
                            let yw, ywp;
                            for (let y = 0; y < h; y++) {
                                yw = y * w;
                                ywp = (y + 1) * wp2 + 1;
                                for (let x = 0; x < w; x++)
                                    result[yw + x] = image[ywp + x];
                            }
                            return result;
                        }
                        function unpadIndices(indices) {
                            if (!indices)
                                return;
                            for (let ii = 0, l = indices.length; ii < l; ii++) {
                                const pi = indices[ii];
                                indices[ii] = (~~(pi / wp2) - 1) * w + (pi % wp2) - 1;
                            }
                        }
                        levelIndex = unpadImage(levelIndex);
                        zoneIndex = unpadImage(zoneIndex);
                        typeIndex = unpadImage(typeIndex);
                        zones.forEach(zone => {
                            unpadIndices(zone.points);
                            if (zone.contour)
                                unpadIndices(zone.contour);
                        });
                        noiseZones.forEach(zone => {
                            unpadIndices(zone.points);
                            if (zone.contour)
                                unpadIndices(zone.contour);
                        });
                    }
                    function trace(id, work) {
                        if (log)
                            console.time(id);
                        work();
                        if (log)
                            console.timeEnd(id);
                    }
                    trace('zonesByLevel', () => {
                        trace('init', init);
                        trace('parcour', parcour);
                        trace('unpad', unpad);
                    });
                    return new HeightMapZones(lod, contourMethod, log, debug, data, dataWidth, w, h, zones, levelZoneIndex, levelIndex, zoneIndex, typeIndex, noiseZones, noiseZoneIndex, maxLevelIndex);
                }
            };
            exports_31("HeightMapZones", HeightMapZones);
        }
    };
});
System.register("shared/UserEvents", [], function (exports_32, context_32) {
    "use strict";
    var __moduleName = context_32 && context_32.id;
    var UserEvents;
    return {
        setters: [],
        execute: function () {
            UserEvents = class UserEvents {
                doMove(e) {
                    if (!this.onMove) {
                        return;
                    }
                    try {
                        this.onMove(e.layerX, e.layerY);
                    }
                    catch (e) {
                        console.warn('onMove', e);
                    }
                }
                doKey(e) {
                    if (!this.onKey) {
                        return;
                    }
                    const c = e.charCode || e.keyCode, k = String.fromCharCode(c), kc = /[A-Z|0-9]/.test(k) ? k : c, ks = new Array();
                    if (UserEvents._mods.reduce((p, m) => {
                        if (e[m.s + 'Key']) {
                            ks.push(m.s);
                        }
                        return p && c != m.v;
                    }, true)) {
                        ks.push('' + kc);
                    }
                    try {
                        this.onKey(ks.join('-'));
                    }
                    catch (e) {
                        console.warn('onKey', e);
                    }
                }
                doSelect(x, y, w, h) {
                    if (!this.onSelect) {
                        return;
                    }
                    try {
                        this.onSelect(x, y, w, h);
                    }
                    catch (e) {
                        console.warn('onSelect', e);
                    }
                }
                doDetach() {
                    this.onMove = this.onKey = this.onSelect = undefined;
                    if (!this.onDetach) {
                        return;
                    }
                    try {
                        this.onDetach();
                    }
                    catch (e) {
                        console.warn('doDetach', e);
                    }
                }
            };
            UserEvents._mods = [
                { s: 'shift', v: 16 },
                { s: 'alt', v: 17 },
                { s: 'ctrl', v: 18 },
                { s: 'meta', v: 91 }
            ];
            exports_32("UserEvents", UserEvents);
        }
    };
});
System.register("test/lib/Vector2D", [], function (exports_33, context_33) {
    "use strict";
    var __moduleName = context_33 && context_33.id;
    var Vector2D;
    return {
        setters: [],
        execute: function () {
            Vector2D = class Vector2D {
                constructor(x = 0, y = 0) {
                    this.x = x;
                    this.y = y;
                }
                static fromPoint(p) {
                    return new Vector2D(p.x, p.y);
                }
                static random(xmax, ymax) {
                    return new Vector2D(Math.random() * xmax, Math.random() * ymax);
                }
                static randomIn(r) {
                    return Vector2D.random(r.width, r.height);
                }
                clone() {
                    return new Vector2D(this.x, this.y);
                }
                length() {
                    return Math.sqrt(this.x * this.x + this.y * this.y);
                }
                dot(v) {
                    return this.x * v.x + this.y * v.y;
                }
                angleWith(v) {
                    return Math.acos(this.dot(v) / (this.length() * v.length()));
                }
            };
            exports_33("Vector2D", Vector2D);
        }
    };
});
System.register("test/Cardioide-test", ["test/lib/Vector2D", "shared/Circle", "shared/Color", "shared/Pixels", "shared/Cardioide", "shared/Region2D"], function (exports_34, context_34) {
    "use strict";
    var __moduleName = context_34 && context_34.id;
    function setupTestInCardioide(ctx, events) {
        let car, cir, box;
        function refresh() {
            car = Cardioide_2.Cardioide.randomIn(ctx.canvas);
            cir = Circle_2.Circle.fromCxyr(car.getCircleIn());
            box = Region2D_4.Region2D.fromRMinMax(car.getBox());
            clear();
        }
        function clear() {
            Pixels_3.Pixels.clear(ctx);
            car.draw(ctx, 100, Color_4.Color.Gray);
            cir.draw(ctx, Color_4.Color.MGreen);
            box.draw(ctx, Color_4.Color.MRed);
        }
        refresh();
        events.onKey = _ => refresh();
        events.onMove = (x, y) => {
            clear();
            const p = { x, y };
            let color = (car.containsPoint(p) ?
                cir.containsPoint(p) ?
                    Color_4.Color.MGreen
                    : Color_4.Color.LBlue
                : box.containsPoint(p) ?
                    Color_4.Color.MidYellow
                    : Color_4.Color.MRed);
            Pixels_3.Pixels.drawCross(ctx, p.x, p.y, 10, color);
            Pixels_3.Pixels.drawLine(ctx, car.x, car.y, p.x, p.y, Color_4.Color.White);
            let angle = new Vector2D_1.Vector2D(1, 0).angleWith(new Vector2D_1.Vector2D(p.x - car.x, p.y - car.y));
            if (car.r < 0) {
                angle += Math.PI;
            }
            if (p.y < car.y) {
                angle = -angle;
            }
            const q = car.getPointAtAngle(angle);
            Pixels_3.Pixels.drawCross(ctx, q.x, q.y, 5, Color_4.Color.White);
        };
    }
    exports_34("setupTestInCardioide", setupTestInCardioide);
    var Vector2D_1, Circle_2, Color_4, Pixels_3, Cardioide_2, Region2D_4;
    return {
        setters: [
            function (Vector2D_1_1) {
                Vector2D_1 = Vector2D_1_1;
            },
            function (Circle_2_1) {
                Circle_2 = Circle_2_1;
            },
            function (Color_4_1) {
                Color_4 = Color_4_1;
            },
            function (Pixels_3_1) {
                Pixels_3 = Pixels_3_1;
            },
            function (Cardioide_2_1) {
                Cardioide_2 = Cardioide_2_1;
            },
            function (Region2D_4_1) {
                Region2D_4 = Region2D_4_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("test/Region2D-test", ["shared/Region2D", "shared/Color", "shared/Pixels", "shared/Easing", "shared/DomHelper"], function (exports_35, context_35) {
    "use strict";
    var __moduleName = context_35 && context_35.id;
    function testSfMany(ctx) {
        const n = 1000000;
        for (let i = 0; i < n; ++i) {
            testSf(ctx);
        }
        console.log('testSfMany Done ' + n);
    }
    exports_35("testSfMany", testSfMany);
    function testSf(ctx, events) {
        let fitOut = true;
        function draw() {
            Pixels_4.Pixels.clear(ctx);
            const rnd = (n) => Math.random() * (n || 1);
            const w = ctx ? ctx.canvas.width : 1000, h = ctx ? ctx.canvas.height : 1000;
            const rs = Region2D_5.Region2D.fromXYWH(rnd(w / 2), rnd(w / 2), rnd(w / 2), rnd(w / 2)), rd = Region2D_5.Region2D.fromXYWH(rnd(h / 2), rnd(h / 2), rnd(h / 2), rnd(h / 2)), rr = rs.clone();
            rr.fitTo(rd, fitOut);
            const p = 7, b = rs.ratio.toPrecision(p) != rr.ratio.toPrecision(p);
            if (ctx) {
                Region2D_5.Region2D.draw(rs, ctx, Color_5.Color.White);
                Region2D_5.Region2D.draw(rd, ctx, Color_5.Color.Blue);
                Region2D_5.Region2D.draw(rr, ctx, b ? Color_5.Color.Red : Color_5.Color.Green);
                console.log('testSf', rs.ratio, rr.ratio);
            }
            else if (b) {
                console.log('testSf', rs.ratio.toPrecision(p), rr.ratio.toPrecision(p));
            }
        }
        events.onKey = k => { if (k == 'shift')
            fitOut = !fitOut; draw(); };
        draw();
    }
    exports_35("testSf", testSf);
    function testGetPartsTo(ctx, events) {
        const c = new Color_5.Color().setAlpha01(.5), red = Color_5.Color.red().setAlpha01(.5), green = Color_5.Color.green().setAlpha01(.5), bgColor = ctx.canvas.style.backgroundColor;
        ctx.canvas.style.backgroundColor = 'lightgray';
        let rs, rd;
        function reset() {
            const rnd = (n) => Math.random() * (n || 1), w = ctx ? ctx.canvas.width : 1000, h = ctx ? ctx.canvas.height : 1000, ld = 4, ls = 3;
            rs = Region2D_5.Region2D.fromXYWH(rnd(w / 2), rnd(h / 2), rnd(w / ls), rnd(h / ls)).round(),
                rd = Region2D_5.Region2D.fromCenterAndSize(w / 2, h / 2, w / ld, h / ld).round();
        }
        reset();
        let preferColumns = false;
        const draw = (x, y) => {
            if (x && y) {
                const r = rs, tw = r.width, th = r.height;
                r.xmin = x;
                r.ymin = y;
                r.xmax = x + tw;
                r.ymax = y + th;
            }
            Pixels_4.Pixels.clear(ctx, Color_5.Color.Gray);
            const rrs = rs.getPartsTo(rd, preferColumns);
            console.log(rrs.length);
            rrs.forEach((r, i) => r.draw(ctx, c.gradient7(i / rrs.length), true));
            Region2D_5.Region2D.draw(rs, ctx, Color_5.Color.Black, false);
            Region2D_5.Region2D.draw(rd, ctx, Color_5.Color.White, false);
        };
        events.onDetach = () => { Pixels_4.Pixels.clear(ctx); ctx.canvas.style.backgroundColor = bgColor; };
        events.onMove = (x, y) => draw(x, y);
        events.onKey = (k) => {
            if (k == 'shift') {
                preferColumns = !preferColumns;
                console.log(preferColumns);
            }
            else {
                console.log('reset', k);
                reset();
            }
        };
        draw();
    }
    exports_35("testGetPartsTo", testGetPartsTo);
    function testGradient(ctx, events) {
        const c = new Color_5.Color();
        let ease = 0, easing = (v) => v;
        const draw = (x, y) => {
            Pixels_4.Pixels.withImageData(ctx, imd => {
                const d = imd.data, h = imd.height, w = imd.width;
                for (let y = 0; y < h; ++y) {
                    let yw4 = y * w * 4;
                    for (let x = 0; x < w; ++x) {
                        c.gradient7(easing(x / w));
                        let i = yw4 + x * 4;
                        d[i] = c.r;
                        d[i + 1] = c.g;
                        d[i + 2] = c.b;
                        d[i + 3] = c.a;
                    }
                }
            });
            DomHelper_4.DomHelper.crud('test', true, DomHelper_4.DomHelper.topLeft({ color: 'white', 'font-size': '16px' }), div => div.innerText = Easing_4.Easing.funcName(ease));
        };
        events.onDetach = () => DomHelper_4.DomHelper.crud('test', false);
        events.onKey = (k) => {
            easing = Easing_4.Easing.func(ease = ++ease > Easing_4.Easing.maxFuncIndex ? 0 : ease);
            draw();
        };
        draw();
    }
    exports_35("testGradient", testGradient);
    var Region2D_5, Color_5, Pixels_4, Easing_4, DomHelper_4;
    return {
        setters: [
            function (Region2D_5_1) {
                Region2D_5 = Region2D_5_1;
            },
            function (Color_5_1) {
                Color_5 = Color_5_1;
            },
            function (Pixels_4_1) {
                Pixels_4 = Pixels_4_1;
            },
            function (Easing_4_1) {
                Easing_4 = Easing_4_1;
            },
            function (DomHelper_4_1) {
                DomHelper_4 = DomHelper_4_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("shared/HMapMeshData", ["shared/HeightMapZones"], function (exports_36, context_36) {
    "use strict";
    var __moduleName = context_36 && context_36.id;
    var HeightMapZones_1, HMapMeshData;
    return {
        setters: [
            function (HeightMapZones_1_1) {
                HeightMapZones_1 = HeightMapZones_1_1;
            }
        ],
        execute: function () {
            HMapMeshData = class HMapMeshData {
                constructor() {
                    this.names = ['zones', 'noise', 'seams'];
                }
                getFaces(name) { return this.faces[this.merged ? 0 : this.names.indexOf(name)]; }
                static fromHeightMap(hmap, heightFactor, lod = 1, contourMinArea = 10, log = false, zones = true, noise = true, seams = true, updatable = false, merge = false) {
                    const hmz = HeightMapZones_1.HeightMapZones.compute(hmap, lod, contourMinArea, log);
                    const zt = (updatable || zones) && HMapMeshData.computeZonesTris(hmz);
                    const nt = (updatable || noise) && HMapMeshData.computeNoiseTris(hmz);
                    const st = (updatable || seams) && HMapMeshData.computeSeamsTris(hmz);
                    const result = new HMapMeshData();
                    if (updatable) {
                        result.data = hmz.data;
                        result.width = hmz.width;
                        result.lod = hmz.lod;
                        result.heightFactor = heightFactor;
                        result.zonesTris = zt;
                        result.noiseTris = nt;
                        result.seamsTris = st;
                    }
                    const pack = HMapMeshData.pack([zones && zt, noise && nt, seams && st], hmz.data, hmz.width, hmz.lod, heightFactor);
                    result.vertices = pack.vertices;
                    result.faces = merge ? Array.prototype.concat(...pack.faces) : pack.faces;
                    result.merged = merge;
                    return result;
                }
                update(heightFactor = this.heightFactor, zones = true, noise = true, seams = true) {
                    if (this.data) {
                        const pack = HMapMeshData.pack([zones && this.zonesTris, noise && this.noiseTris, seams && this.seamsTris], this.data, this.width, this.lod, heightFactor);
                        this.vertices = pack.vertices;
                        this.faces = pack.faces;
                    }
                    return this;
                }
                static computeZonesTris(hmz, indices = new Array()) {
                    hmz.zones.forEach(z => {
                        if (z.contour.length)
                            hmz.triangulate(z).getTriIndices(indices);
                    });
                    return indices;
                }
                static computeNoiseTris(hmz, indices = new Array()) {
                    const pt = hmz.pointTypeIndex, PTN = HeightMapZones_1.HMZPointType.Noise, PTC = HeightMapZones_1.HMZPointType.Contour;
                    const w = hmz.width, xl = w - 1, yl = hmz.height - 1;
                    for (let y = 0, yw; y < yl; y++) {
                        yw = y * w;
                        for (let x = 0; x < xl; x++) {
                            const ia = yw + x, ib = ia + w, ic = ib + 1, id = ia + 1, ta = pt[ia], tb = pt[ib], tc = pt[ic], td = pt[id];
                            if (ta == PTN || tb == PTN || tc == PTN || td == PTN) {
                                //if ((tb == PTN || tb == PTC) && (td == PTN || td == PTC)) 
                                {
                                    //if (ta == PTN || ta == PTC)
                                    indices.push(ia, ib, id);
                                    //  if (tc == PTN || tc == PTC)
                                    indices.push(ib, ic, id);
                                }
                            }
                            // if (ta == PTN || tb == PTN || td == PTN)
                            //     if ((ta == PTN || ta == PTC) && (tb == PTN || tb == PTC) && (td == PTN || td == PTC))
                            //         indices.push(ia, ib, id)
                            // if (tb == PTN || tc == PTN || td == PTN)
                            //     if ((tb == PTN || td == PTN) && (tc == PTN || tc == PTC) && (td == PTN || td == PTC))
                            //         indices.push(ib, ic, id)
                        }
                    }
                    return indices;
                }
                static computeSeamsTris(hmz, indices = new Array()) {
                    const w = hmz.width, h = hmz.height; //image dimensions
                    const xl = w - 1, yl = h - 1; //right and bottom borders
                    const pointType = hmz.pointTypeIndex; //each point type
                    const PT_CONTOUR = HeightMapZones_1.HMZPointType.Contour; //type of a contour point
                    const nh = [-w - 1, -w, -w + 1, 1, w + 1, w, w - 1, -1]; //neighborhood relative indices
                    //for each zone
                    for (let iz = 0, zones = hmz.zones, zl = zones.length; iz < zl; iz++) {
                        const zone = zones[iz], contour = zone.contour, cl = contour && contour.length || 0;
                        if (cl < 3)
                            continue; //we need 3 consecutive contour points
                        if (zone.levelIndex == hmz.maxLevelIndex)
                            continue; //those zones are bordered by noise
                        //for each contour point and back to start
                        let iP = contour[0]; //previous contour point index
                        let ia = contour[1]; //reference (current) contour point index
                        //let od = nh.indexOf(iP - ia)            //enter direction
                        for (let i = 2, cl1 = cl + 1; i < cl1; i++) {
                            const iN = contour[i % cl]; //next contour point index
                            const xa = ia % w, checkborders = xa == 0 || xa == xl;
                            //turn around the ref point
                            let ib = iP; //second vertex of the face, for now the start (previous) point
                            let dN = nh.indexOf(iN - ia); //next point direction
                            let ds = dN;
                            if (ds % 2 != 0) {
                                ds -= 2;
                                if (ds < 0)
                                    ds += 8;
                            }
                            let iS = ia + nh[ds]; //stop point
                            let ic = iP; //candidate point turning around ref point
                            let d = nh.indexOf(ic - ia); //direction turning around ref point
                            while (ic != iS) {
                                d = (d + 1) % 8; //turn clockwise
                                ic = ia + nh[d]; //candidate point
                                if (checkborders) {
                                    const xc = ic % w;
                                    if (xa == 0 && xc == xl || xc == 0)
                                        continue; //border to border
                                }
                                if (pointType[ic] == PT_CONTOUR) {
                                    const inv = false;
                                    if (inv)
                                        indices.push(ia, ib, ic); //
                                    else
                                        indices.push(ic, ib, ia); //add face
                                    ib = ic;
                                }
                            }
                            //next contour point
                            iP = ia;
                            ia = iN;
                        }
                    }
                    return indices;
                }
                static pack(triIndices, data, width, lod, heightFactor = 1) {
                    const dataWidth = width * lod, vertices = new Array(), map = new Map(), x0 = -dataWidth / 2, z0 = -(data.length / dataWidth) / 2, getHeight = typeof heightFactor == 'function' ? heightFactor
                        : (h) => h * heightFactor;
                    let vl = 0;
                    //console.log(width, lod, dataWidth, data)
                    function vertexIndex(i) {
                        let vi = map.get(i);
                        if (!vi) {
                            const u = (i % width) * lod;
                            const v = ~~(i / width) * lod;
                            const h = data[v * dataWidth + u];
                            vertices.push(x0 + u, getHeight(h), z0 + v);
                            map.set(i, vi = vl++);
                        }
                        return vi;
                    }
                    const faces = new Array();
                    triIndices.forEach((tis, ti) => {
                        if (tis) {
                            faces[ti] = new Array();
                            for (let i = 0, l = tis.length; i < l; i++)
                                faces[ti].push(vertexIndex(tis[i]));
                        }
                    });
                    return { vertices, faces };
                }
            };
            exports_36("HMapMeshData", HMapMeshData);
        }
    };
});
System.register("test/zone-test", ["shared/HMapMeshData", "shared/DomHelper", "shared/HeightMap", "shared/HeightMapZones", "shared/Color", "shared/Pixels", "shared/Region2D", "shared/Zoning"], function (exports_37, context_37) {
    "use strict";
    var __moduleName = context_37 && context_37.id;
    function testZones(ctx, events, fract2D) {
        const c = new Color_6.Color();
        let hmap, hmzd, ox, oy, drawMode = 0, lod = 1, fill = true, singleZone = true, contour = true, contourMethod = 1, removeColinears = false, triangulate = false, boundingBox = false, steiners = false, steinersDistanceFactor = 50, steinersEdgeDistance = 5, steinersClipping = true, allLevels = false, contourMinArea = 10, logWarnings = false, seams = false, smallZones = false, noiseZones = false, checkDuplicates = false;
        function getData() {
            fract2D.getHeightMapAsync()
                .then(hm => {
                hmap = hm;
                ctx = fract2D.getContext();
                getZones();
            });
        }
        function getZones() {
            hmzd = HeightMapZones_2.HeightMapZones.compute(hmap, lod, contourMinArea, true, true, contour && contourMethod || 0, removeColinears);
            draw();
        }
        function nextLod(reverse) {
            lod *= (reverse ? .5 : 2);
            if (lod > 32)
                lod = 1;
            else if (lod < 1)
                lod = 32;
            getZones();
        }
        function triangulateZone(zone) {
            if (!zone)
                return;
            const tr = hmzd.triangulate(zone, steiners && hmzd.width / steinersDistanceFactor, steinersEdgeDistance, steinersClipping, 100, logWarnings);
            if (!tr)
                return;
            const tris = tr.getTrianglesXys(), tl = tris && Math.max(1, tris.length - 1);
            if (tris)
                tris.forEach((t, ti) => Pixels_5.Pixels.drawPoly(ctx, t, c.gradient7(ti / tl), fill, true));
            if (tr.badPoints)
                tr.badPoints.map(i => hmzd.getPoint(i)).forEach(p => {
                    if (lod == 1)
                        Pixels_5.Pixels.drawCross(ctx, p.x, p.y, 5, Color_6.Color.Red);
                    else
                        Pixels_5.Pixels.drawRect(ctx, p.x * lod, p.y * lod, p.x * lod + lod, p.y * lod + lod, Color_6.Color.Red, true);
                });
            return tr;
        }
        function drawBoundingBox(zone) {
            Pixels_5.Pixels.drawBox(ctx, Region2D_6.Region2D.fromRMinMax(hmzd.computeBoundingBox(zone)).scale(lod), Color_6.Color.Red);
        }
        function drawSteiners(zoneSteiners, color = Color_6.Color.LBlue) {
            if (!zoneSteiners)
                return;
            zoneSteiners.map(i => hmzd.getPoint(i))
                .forEach(p => Pixels_5.Pixels.drawCross(ctx, p.x * lod, p.y * lod, 2, color));
        }
        function getSteiners(zone) {
            return hmzd.getSteinerPoints(zone, hmzd.width / steinersDistanceFactor, steinersEdgeDistance, steinersClipping);
        }
        class LInfo {
            constructor(points = 0, contour = 0, zones = 0, noContourZones = 0, noContourPoints = 0) {
                this.points = points;
                this.contour = contour;
                this.zones = zones;
                this.noContourZones = noContourZones;
                this.noContourPoints = noContourPoints;
            }
            add(linfo) {
                this.points += linfo.points;
                this.contour += linfo.contour;
                this.zones += linfo.zones;
                this.noContourZones += linfo.noContourZones;
                this.noContourPoints += linfo.noContourPoints;
            }
        }
        function draw(x, y) {
            if (x == undefined)
                x = ox;
            else
                ox = x;
            if (y == undefined)
                y = oy;
            else
                oy = y;
            const lx = Math.round(x / lod), ly = Math.round(y / lod);
            Pixels_5.Pixels.clear(ctx, Color_6.Color.LGray);
            const sli = hmzd.getLevelIndex(lx, ly), sZone = hmzd.getZone(lx, ly), levels = allLevels ? hmzd.getZonesByLevel() : [hmzd.getLevelZones(sli)], tzl = Math.max(1, hmzd.zonesCount - 1), colorize = Color_6.Color.getColorizer(c, true);
            let slInfo, alInfo = allLevels && new LInfo(), szn, zoneTri, nbSeams = 0, zn = 0;
            Pixels_5.Pixels.withImageData(ctx, imd => {
                if (noiseZones) {
                    const nzl = Math.max(1, hmzd.noiseZones.length - 1);
                    hmzd.noiseZones.forEach((nz, nzi) => hmzd.drawZone(nz, imd, c.gradient7(nzi / nzl), contour && Color_6.Color.MYellow, fill));
                }
                levels.forEach((lzs, li) => {
                    if (!lzs)
                        return;
                    const isSelectedLevel = !allLevels || li == sli;
                    const nclzs = lzs.filter(z => !z.contour.length), nclpn = nclzs.reduce((r, z) => r + z.points.length, 0), linfo = new LInfo(0, 0, lzs.length, nclzs.length, nclpn);
                    if (isSelectedLevel)
                        slInfo = linfo;
                    const lcc = isSelectedLevel ? Color_6.Color.MGreen : Color_6.Color.Green;
                    if (drawMode == 0) {
                        const lcp = isSelectedLevel ? Color_6.Color.Gray : Color_6.Color.LGray;
                        hmzd.drawLevel(lzs, imd, lcp, !contour ? undefined : t => c.gradient7(t), linfo, fill, false, true);
                        if (smallZones)
                            nclzs.forEach(z => hmzd.drawZone(z, imd, Color_6.Color.Magenta, null, fill));
                    }
                    else {
                        const zl = Math.max(1, lzs.length - 1);
                        lzs.forEach((z, iz) => {
                            linfo.points += z.points.length;
                            linfo.contour += z.contour.length;
                            if (z == sZone) {
                                szn = zn;
                                return;
                            }
                            if (smallZones && !z.contour.length)
                                c.copy(Color_6.Color.Magenta);
                            else if (drawMode == 1)
                                c.gradient7(iz / zl);
                            else if (drawMode == 2)
                                c.copy(iz % 2 == 0 ? Color_6.Color.Orange : Color_6.Color.Blue);
                            else if (drawMode == 3)
                                c.gradient7(zn / tzl);
                            const cp = drawMode == 4 ? colorize : c;
                            hmzd.drawZone(z, imd, cp, contour && lcc, fill);
                            zn++;
                        });
                    }
                    if (alInfo && linfo)
                        alInfo.add(linfo);
                });
                if (seams) {
                    const tris = HMapMeshData_1.HMapMeshData.computeSeamsTris(hmzd);
                    nbSeams = tris.length / 3;
                    hmzd.drawTris(tris, imd, t => c.gradient7(t));
                    if (noiseZones)
                        hmzd.drawTris(HMapMeshData_1.HMapMeshData.computeNoiseTris(hmzd), imd, c.copy(Color_6.Color.LBlue).setAlpha01(.3), true);
                }
            });
            //if (boundingBox || triangulate || steiners)
            levels.forEach(lzs => {
                if (lzs)
                    lzs.forEach(z => {
                        if (singleZone && z != sZone)
                            return;
                        if (boundingBox)
                            drawBoundingBox(z);
                        let zt = triangulate && triangulateZone(z);
                        if (steiners)
                            drawSteiners(zt && zt.steinerPoints || getSteiners(z), z == sZone ? Color_6.Color.LBlue : Color_6.Color.Blue);
                        if (z == sZone)
                            zoneTri = zt;
                    });
            });
            if (checkDuplicates) {
                const sumIndex = new Uint32Array(hmzd.width * hmzd.height);
                const zs = noiseZones ? hmzd.noiseZones : hmzd.zones;
                zs.forEach(z => z.points.forEach(i => sumIndex[i]++));
                const chm = HeightMap_3.HeightMap.fromData(hmzd.width, sumIndex);
                Pixels_5.Pixels.drawHeightMap(ctx, chm.data, chm.dataWidth, false, false, null, null, 1, .5);
            }
            DomHelper_5.DomHelper.crud('test', true, DomHelper_5.DomHelper.topLeft({
                padding: '3px',
                color: 'white',
                'font-size': '14px',
                'background-color': 'rgba(0,0,0,.5)'
            }), div => div.innerHTML = ''
                + '(D) drawMode: ' + drawMode
                + '<br/>(Z) singleZone: ' + singleZone
                + '<br/>(A) all levels: ' + allLevels
                + '<br/>(L) lod: ' + lod
                + '<br/>(F) fill: ' + fill
                + '<br/>(C) contour: ' + contour
                + (!contour ? '' : (''
                    + '<br/>(N) contouring: ' + Zoning_3.ContourMethod[contourMethod]
                    + '<br/>(R) nocolinears: ' + removeColinears))
                + '<br/>(B) boundingBox: ' + boundingBox
                + '<br/>(S) steiners: ' + steiners
                + (!steiners ? '' : ('<br/>(P) clip steiners: ' + steinersClipping))
                + '<br/>(G) triangulate: ' + triangulate
                + '<br/>(M) seams: ' + seams
                + '<br/>(0) small zones: ' + smallZones
                + '<br/>(I) noise zones: ' + noiseZones
                + '<br/>(K) check duplicates: ' + checkDuplicates
                + '<br/>'
                + '<br/>contour min area: ' + contourMinArea
                + (!steiners ? '' : (''
                    + '<br/>steinersGridDistance ' + Math.round(hmzd.width / steinersDistanceFactor)
                    + '<br/>steinersEdgeDistance ' + steinersEdgeDistance))
                + '<br/><br/>image:'
                + (!alInfo ? '' : (''
                    + '<br/>' + hmzd.levelZoneIndex.length + ' levels'
                    + '<br/>' + alInfo.zones + ' zones'
                    + '<br/>' + alInfo.points + ' points'
                    + '<br/>' + alInfo.contour + ' contour points'
                    + '<br/>' + alInfo.noContourZones + ' small zones'
                    + '<br/>' + alInfo.noContourPoints + ' smallzone points'
                    + '<br/>' + (alInfo.zones - alInfo.noContourZones) + ' contour zones'))
                + '<br/>' + (hmzd.noiseZones && hmzd.noiseZones.reduce((p, c) => p + c.points.length, 0) || 0) + ' noise points'
                + '<br/>' + (hmzd.noiseZones && hmzd.noiseZones.length || 0) + ' noise zones'
                + (!nbSeams ? '' : ('<br/>' + nbSeams + ' seams'))
                + (!slInfo ? '' : (''
                    + '<br/><br/>level:'
                    + '<br/>' + (sli + 1) + '/' + hmzd.levelZoneIndex.length
                    + '<br/>' + slInfo.zones + ' zones'
                    + '<br/>' + slInfo.points + ' points'
                    + '<br/>' + slInfo.contour + ' contour points'
                    + '<br/>' + slInfo.noContourZones + ' small zones'
                    + '<br/>' + slInfo.noContourPoints + ' smallzone points'))
                + (!sZone ? '' : (''
                    + '<br/><br/>zone:'
                    + (szn == undefined ? '' : ('<br/>' + szn + '/' + tzl))
                    + '<br/>' + (sZone.levelZoneIndex + 1) + '/' + hmzd.levelZoneIndex[sZone.levelIndex].length
                    + '<br/>' + sZone.points.length + ' points'
                    + '<br/>' + sZone.nbEdges + ' edge points'
                    + (!sZone.contour ? '' : ('<br/>' + sZone.contour.length + ' contour points'))
                    + (!zoneTri ? '' : (''
                        + (!zoneTri.steinerPoints ? '' : ('<br/>' + zoneTri.steinerPoints.length + ' steiners points'))
                        + (!zoneTri.nbTriangles ? '' : ('<br/>' + zoneTri.nbTriangles + ' triangles'))
                        + (!zoneTri.badPoints ? '' : ('<br/>' + zoneTri.badPoints.length + ' bad points')))))));
        }
        events.onDetach = () => DomHelper_5.DomHelper.crud('test', false);
        events.onSelect = () => setTimeout(getData, 200);
        events.onMove = (x, y) => draw(x, y);
        events.onKey = k => {
            switch (k) {
                case 'meta-R': return;
                case 'D':
                    if (++drawMode > 4)
                        drawMode = 0;
                    break;
                case 'L':
                    nextLod(false);
                    return;
                case 'shift-L':
                    nextLod(true);
                    return;
                case 'F':
                    fill = !fill;
                    break;
                case 'G':
                    triangulate = !triangulate;
                    break;
                case 'Z':
                    singleZone = !singleZone;
                    break;
                case 'C':
                    contour = !contour;
                    break;
                case 'N':
                    if (++contourMethod > 2)
                        contourMethod = 1;
                    getZones();
                    return;
                case 'R':
                    removeColinears = !removeColinears;
                    getZones();
                    return;
                case 'B':
                    boundingBox = !boundingBox;
                    break;
                case 'S':
                    steiners = !steiners;
                    break;
                case 'A':
                    allLevels = !allLevels;
                    break;
                case 'P':
                    steinersClipping = !steinersClipping;
                    break;
                case 'M':
                    seams = !seams;
                    break;
                case 'O':
                    smallZones = !smallZones;
                    break;
                case 'I':
                    noiseZones = !noiseZones;
                    break;
                case 'K':
                    checkDuplicates = !checkDuplicates;
                    break;
            }
            draw();
        };
        getData();
    }
    exports_37("testZones", testZones);
    var HMapMeshData_1, DomHelper_5, HeightMap_3, HeightMapZones_2, Color_6, Pixels_5, Region2D_6, Zoning_3;
    return {
        setters: [
            function (HMapMeshData_1_1) {
                HMapMeshData_1 = HMapMeshData_1_1;
            },
            function (DomHelper_5_1) {
                DomHelper_5 = DomHelper_5_1;
            },
            function (HeightMap_3_1) {
                HeightMap_3 = HeightMap_3_1;
            },
            function (HeightMapZones_2_1) {
                HeightMapZones_2 = HeightMapZones_2_1;
            },
            function (Color_6_1) {
                Color_6 = Color_6_1;
            },
            function (Pixels_5_1) {
                Pixels_5 = Pixels_5_1;
            },
            function (Region2D_6_1) {
                Region2D_6 = Region2D_6_1;
            },
            function (Zoning_3_1) {
                Zoning_3 = Zoning_3_1;
            }
        ],
        execute: function () {
        }
    };
});
System.register("client/fract2D/Fract2D", ["screenfull", "client/fract2D/Fract2DState", "client/fract2D/Fract2DKeys", "client/fract2D/Fract2DHelpers", "shared/fract/FractBase", "shared/fract/FractProcesr", "shared/fract/FractMiniMap", "shared/Transform2D", "shared/Viewport2D", "shared/MouseEvents", "shared/CanvasSelection", "shared/HeightMapZones", "shared/Pixels", "shared/Color", "shared/Region2D", "shared/DomHelper", "shared/nombres", "shared/UserEvents", "test/Cardioide-test", "test/Region2D-test", "test/zone-test"], function (exports_38, context_38) {
    "use strict";
    var __moduleName = context_38 && context_38.id;
    var screenfull, Fract2DState_1, Fract2DKeys_1, Fract2DHelpers_1, FractBase_1, FractProcesr_1, FractMiniMap_1, Transform2D_1, Viewport2D_1, MouseEvents_1, CanvasSelection_1, HeightMapZones_3, Pixels_6, Color_7, Region2D_7, DomHelper_6, nombres_2, UserEvents_1, Cardioide_test_1, Region2D_test_1, zone_test_1, Tools, Fract2D;
    return {
        setters: [
            function (screenfull_1) {
                screenfull = screenfull_1;
            },
            function (Fract2DState_1_1) {
                Fract2DState_1 = Fract2DState_1_1;
            },
            function (Fract2DKeys_1_1) {
                Fract2DKeys_1 = Fract2DKeys_1_1;
            },
            function (Fract2DHelpers_1_1) {
                Fract2DHelpers_1 = Fract2DHelpers_1_1;
            },
            function (FractBase_1_1) {
                FractBase_1 = FractBase_1_1;
            },
            function (FractProcesr_1_1) {
                FractProcesr_1 = FractProcesr_1_1;
            },
            function (FractMiniMap_1_1) {
                FractMiniMap_1 = FractMiniMap_1_1;
            },
            function (Transform2D_1_1) {
                Transform2D_1 = Transform2D_1_1;
            },
            function (Viewport2D_1_1) {
                Viewport2D_1 = Viewport2D_1_1;
            },
            function (MouseEvents_1_1) {
                MouseEvents_1 = MouseEvents_1_1;
            },
            function (CanvasSelection_1_1) {
                CanvasSelection_1 = CanvasSelection_1_1;
            },
            function (HeightMapZones_3_1) {
                HeightMapZones_3 = HeightMapZones_3_1;
            },
            function (Pixels_6_1) {
                Pixels_6 = Pixels_6_1;
            },
            function (Color_7_1) {
                Color_7 = Color_7_1;
            },
            function (Region2D_7_1) {
                Region2D_7 = Region2D_7_1;
            },
            function (DomHelper_6_1) {
                DomHelper_6 = DomHelper_6_1;
            },
            function (nombres_2_1) {
                nombres_2 = nombres_2_1;
            },
            function (UserEvents_1_1) {
                UserEvents_1 = UserEvents_1_1;
            },
            function (Cardioide_test_1_1) {
                Cardioide_test_1 = Cardioide_test_1_1;
            },
            function (Region2D_test_1_1) {
                Region2D_test_1 = Region2D_test_1_1;
            },
            function (zone_test_1_1) {
                zone_test_1 = zone_test_1_1;
            }
        ],
        execute: function () {
            (function (Tools) {
                Tools[Tools["Doc"] = 0] = "Doc";
                Tools[Tools["Switch"] = 1] = "Switch";
                Tools[Tools["Info"] = 2] = "Info";
                Tools[Tools["FullScreen"] = 3] = "FullScreen";
                Tools[Tools["Point"] = 4] = "Point";
                Tools[Tools["Map"] = 5] = "Map";
                Tools[Tools["Path"] = 6] = "Path";
                Tools[Tools["Fractions"] = 7] = "Fractions";
                Tools[Tools["Helpers"] = 8] = "Helpers";
                Tools[Tools["CarAngle"] = 9] = "CarAngle";
                Tools[Tools["Edges"] = 10] = "Edges";
            })(Tools || (Tools = {}));
            Fract2D = class Fract2D extends FractBase_1.FractBase {
                constructor(par, altAppInit) {
                    super(altAppInit);
                    this.tests = [
                        { name: 'Zones', method: zone_test_1.testZones, getArgs: () => [this] },
                        { name: 'ScaleFactor', method: Region2D_test_1.testSf },
                        { name: 'InCardioide', method: Cardioide_test_1.setupTestInCardioide },
                        { name: 'testGradient', method: Region2D_test_1.testGradient },
                        { name: 'GetPartsTo', method: Region2D_test_1.testGetPartsTo },
                    ];
                    this._allDivs = ['Doc', 'Info', 'InfoPoint', 'MiniMap'];
                    this.state = new Fract2DState_1.Fract2DState(par, () => this.update(), () => this.updateImage(), () => this.updateInfo(), this.log);
                    const canvas = document.querySelector('canvas.fract');
                    this.vp = new Viewport2D_1.Viewport2D(canvas, () => this.onResized(), this.log);
                    this.sel = new CanvasSelection_1.CanvasSelection(canvas, s => this.fromSel(s), false, (x, y, b) => this.getCursorColor(x, y, b), true, (x, y) => this.getCursorXYInfo(x, y), 'all');
                    this._userEvents = new UserEvents_1.UserEvents();
                    this.keys = new Fract2DKeys_1.Fract2DKeys(true, this.state, (name, shift) => this.tool(name, shift), () => this.cancelProcess(), e => this._userEvents.doKey(e), this.log, this.isDev);
                    this.mouse2D = new MouseEvents_1.MouseEvents(canvas, (x, y) => this.onClick(x, y), (x, y) => this.onDblClick(x, y), (x, y, e) => { this.onMove(x, y); this._userEvents.doMove(e); });
                    setTimeout(() => {
                        this.resize();
                        if (this.showDocAtStart)
                            this.showDoc(true, true);
                    }, 333);
                }
                getParams() { return this.state.current.clone(); }
                getHeightMap(flatten, info) {
                    const hmap = this.hmap.clone();
                    if (flatten)
                        hmap.flatten(info);
                    else if (info)
                        info.min = hmap.getMin();
                    return hmap;
                }
                getHeightMapAsync(flatten, info) {
                    const self = this;
                    return new Promise((resolve, reject) => {
                        function wait() {
                            if (self.processing)
                                setTimeout(() => wait(), 500);
                            else
                                resolve(self.getHeightMap(flatten, info));
                        }
                        wait();
                    });
                }
                getImage() {
                    if (!this.ctx)
                        return undefined;
                    const img = document.createElement('img');
                    img.src = this.ctx.canvas.toDataURL("image/jpg");
                    return img;
                }
                getContext() {
                    return this.ctx;
                }
                _attach() {
                    this.ctx.canvas.style.opacity = '0';
                    this.ctx.canvas.style.height = this.ctx.canvas.height + 'px';
                    this.sel.attach();
                    this.mouse2D.attach();
                    this.keys.attach();
                    this.vp.attach();
                }
                _attached() { console.log('attached'); }
                _detach() {
                    this.sel.detach();
                    this.mouse2D.detach();
                    this.keys.detach();
                    this.vp.detach();
                }
                _detached() { this.ctx.canvas.style.height = '0'; }
                _moreElements() { return [this.ctx.canvas]; }
                _loggers() {
                    return [this.miniMap, this.psr, this.state, this.sel, this.vp, this.keys];
                }
                _toolname(tool) { return Tools[tool]; }
                _getInfo() {
                    return this.state.toString()
                        + (this.processedMs ? ('\n' + this.processedMs.toFixed(3) + 'ms') : '');
                }
                drawPath(x, y) {
                    if (!this._pathData) {
                        this._pathData = new Array();
                    }
                    const par = this.state.current, path = this.psr.computePathFromView(x, y, par.m, par.r, this._pathData);
                    Pixels_6.Pixels.drawGradientPoly(this.ctx, path);
                }
                resize() {
                    this.lvp = this.vp.size;
                    this.ctx = this.vp.getContext();
                    if (this.tfm) {
                        this.tfm.updateView(this.vp.width, this.vp.height);
                    }
                    else {
                        this.tfm = new Transform2D_1.Transform2D(this.vp.width, this.vp.height);
                    }
                    this.sel.reset(this.ctx, true);
                    this.updateMiniMap(true);
                    this.update();
                }
                update() {
                    if (this.processing) {
                        return false;
                    }
                    const par = this.state.current;
                    this.tfm.setTarget(par.a, par.b, par.z);
                    return this.process();
                }
                process() {
                    if (this.processing) {
                        if (this.log)
                            console.log('busy');
                        return false;
                    }
                    this.processing = true;
                    this.cancelRequired = false;
                    this.updateShowBusy();
                    if (this.log)
                        console.log('---------------\n' + this.state.toString());
                    if (!this.psr) {
                        this.psr = new FractProcesr_1.FractProcesr(this.tfm, this.log);
                    }
                    const par = this.state.current, start = performance.now(), updateProgress = (p) => {
                        if (this.state.progress || (performance.now() - start) > 666)
                            this.updateProgress(p);
                    }, processing = this.psr.process(Region2D_7.Region2D.fromSize(this.vp.size), par, (hmap, canceled) => this.processed(start, hmap, canceled), this.hmap && this.hmap.clone(), this.state.parts && this.vp.isSame(this.lvp) && this.state.previousPar, this.state.worker, this.state.optimize, par.mask, p => { updateProgress(p); return this.cancelRequired; });
                    if (!processing) {
                        if (this.log)
                            console.log('procesr busy');
                        this.updateShowBusy();
                    }
                    return processing;
                }
                cancelProcess() {
                    if (!this.processing) {
                        return;
                    }
                    if (this.log)
                        console.log('canceling');
                    this.cancelRequired = true;
                    this.psr.cancelProcess();
                }
                processed(start, hmap, canceled) {
                    if (canceled) {
                        if (this.log)
                            console.log('canceled');
                        this.restoreImage();
                        this.state.restore();
                    }
                    else {
                        this.state.onRecomputed();
                        this.updateImage(hmap);
                        this.processedMs = performance.now() - start;
                        if (this.log)
                            console.log('done', this.processedMs.toFixed(3) + 'ms');
                        this._zonesData = undefined;
                    }
                    if (this.log)
                        console.log('---------------');
                    this.updateProgress(false);
                    this.processing = false;
                    this.cancelRequired = false;
                    //if (this.debug) hmap.computeIndicesByZoneByLevel(4, true, true)
                    this.updateShowBusy();
                }
                updateImage(hmap) {
                    if (hmap)
                        this.hmap = hmap;
                    else
                        hmap = this.hmap;
                    if (!hmap)
                        return;
                    const par = this.state.current;
                    this.imd = Pixels_6.Pixels.imageFromHeightMap(hmap.data, hmap.dataWidth, par.color, par.invert, par.easing);
                    this.restoreImage();
                }
                restoreImage(elseSave = false, isMove = false) {
                    if (this.imd) {
                        this.ctx.putImageData(this.imd, 0, 0);
                    }
                    else if (elseSave) {
                        this.imd = this.ctx.getImageData(0, 0, this.vp.width, this.vp.height);
                    }
                    if (this.isTool(Tools.Helpers)) {
                        this.drawHelpers();
                    }
                    if (!isMove) {
                        this.sel.reset(null, true);
                        this.updateMiniMap();
                    }
                    this.updateInfo();
                }
                onResized() {
                    this.cancelProcess();
                    this.resize();
                }
                onClick(x, y) {
                    if (this.log)
                        console.log('onClick', x, y);
                    DomHelper_6.DomHelper.showHideMouse(false, this.ctx.canvas);
                    this.showAll(false);
                }
                onDblClick(x, y) {
                    this.zoomAt(x, y);
                }
                onMove(x, y) {
                    DomHelper_6.DomHelper.showHideMouse(true, this.ctx.canvas);
                    this.showAll(true);
                    const localRefresh = this.isTool(Tools.Helpers, Tools.CarAngle, Tools.Path, Tools.Edges);
                    if (localRefresh && !this.sel.showCross) {
                        this.restoreImage(localRefresh, true);
                    }
                    if (this.isTool(Tools.Edges)) {
                        this.drawZones(x, y);
                    }
                    else {
                        this._zonesData = this._zonesInfo = undefined;
                    }
                    if (this.isTool(Tools.CarAngle)) {
                        this.drawCarAngle(x, y);
                    }
                    else {
                        this._carAngle = undefined;
                    }
                    if (this.isTool(Tools.Path)) {
                        this.drawPath(x, y);
                    }
                }
                tool(name, shift) {
                    if (super.basetool(name, shift))
                        return;
                    switch (name) {
                        case 'mask':
                            this.state.toggle('mask', this.isMaskVisible());
                            break;
                        case 'test':
                            this.nextTest();
                            break;
                        case 'save':
                            if (shift) {
                                const data = window.prompt("Paste content of fract .json file");
                                if (data) {
                                    this.state.update(data);
                                }
                            }
                            else {
                                DomHelper_6.DomHelper.download(this.state.current, 'fract-' + Date.now() + '.json');
                            }
                            break;
                        default:
                            const v = this.tools.get(name);
                            switch (name) {
                                case Tools[Tools.Point]:
                                    this.sel.showCross = v;
                                    this.showInfoPoint(null);
                                    break;
                                case Tools[Tools.FullScreen]:
                                    const sf = screenfull.screenfull;
                                    if (sf.enabled) {
                                        if (!sf.isFullscreen)
                                            sf.request();
                                        else
                                            sf.exit();
                                    }
                                    return;
                                default:
                                    this.updateImage();
                                    break;
                            }
                            break;
                    }
                }
                nextTest() {
                    this._userEvents.doDetach();
                    this._testNum = this._testNum == undefined ? 0 : this._testNum == this.tests.length - 1 ? undefined : 1 + this._testNum;
                    if (this._testNum == undefined) {
                        this.keys.bypass = false;
                        if (this.log)
                            console.log('---------------\ntests off\n---------------');
                        this.restoreImage();
                    }
                    else {
                        this.keys.bypass = true;
                        const t = this.tests[this._testNum];
                        if (this.log)
                            console.log('---------------\ntest: ' + t.name);
                        Pixels_6.Pixels.clear(this.ctx);
                        this.sel.reset(this.ctx);
                        t.method.apply(null, [this.ctx, this._userEvents].concat(t.getArgs ? t.getArgs() : undefined));
                    }
                }
                fromSel(r) {
                    this._userEvents.doSelect(r.x, r.y, r.width, r.height);
                    if (!this.tfm || this._testNum) {
                        return;
                    }
                    if (this.log)
                        console.log('zoomSel', r);
                    const loc = this.tfm.locFromViewRect(r, this.state.currentZoom);
                    this.state.update(loc);
                }
                zoomAt(x, y) {
                    if (this.log)
                        console.log('zoomAt', x, y);
                    const loc = this.tfm.locFromView(x, y, this.state.currentZoom, 2, true);
                    this.state.update(loc);
                }
                updateMiniMap(resized = false) {
                    if (!this.miniMap) {
                        this.miniMap = new FractMiniMap_1.FractMiniMap(this.log);
                    }
                    this.miniMap.updateMiniMap(this.isTool(Tools.Map), resized, this.vp.size, this.tfm, this.state.currentZoom);
                }
                computePoint(x, y) {
                    const par = this.state.current;
                    return this.psr.computePointFromView(x, y, par.m, par.r, par.mask, this.state.optimize);
                }
                getCursorColor(x, y, forSelectBox) {
                    if (forSelectBox || !this.psr) {
                        return Color_7.Color.MBlue;
                    }
                    const cp = this.computePoint(x, y), optim = this.state.optimize;
                    this.showInfoPoint(cp);
                    return cp.value == cp.max ?
                        cp.optimized ?
                            Color_7.Color.LGreen
                            : optim && cp.nooptim ?
                                Color_7.Color.LBlue
                                : Color_7.Color.MGreen
                        : cp.optimized ?
                            Color_7.Color.MidYellow
                            : optim && cp.nooptim ?
                                Color_7.Color.Pink
                                : Color_7.Color.MRed;
                }
                getCursorXYInfo(x, y) {
                    if (!this.tfm) {
                        return { x, y };
                    }
                    const fx = this.tfm.xToTarget(x, y), fy = this.tfm.yToTarget(x, y), df = this.isTool(Tools.Fractions), sx = df ? (nombres_2.fractionEntiere(fx) + ' ') : '', sy = df ? (nombres_2.fractionEntiere(fy) + ' ') : '';
                    return {
                        x: sx + fx + ' ' + x + '/' + this.vp.width,
                        y: sy + fy + ' ' + y + '/' + this.vp.height
                    };
                }
                showInfoPoint(cp) {
                    const show = !!cp;
                    DomHelper_6.DomHelper.crud('InfoPoint', show, show && DomHelper_6.DomHelper.topRight('all', 'info'), show && cp.value + '/' + cp.max
                        + ' ' + cp.elapsedMs.toFixed(3) + 'ms'
                        + (this.state.optimize && cp.nooptim ? ' nooptim' : '')
                        + (cp.optimized ? ' optimized' : '')
                        + (this._carAngle == undefined ? '' : (' ' + Fract2DHelpers_1.getCarAngleInfo(this._carAngle, this.isTool(Tools.Fractions))))
                        + (this._zonesInfo ? (' ' + this._zonesInfo) : ''));
                }
                drawHelpers() {
                    Fract2DHelpers_1.setupHelpers(this.ctx, this.tfm, this.state.current.r, this.isTool(Tools.CarAngle))
                        .forEach(h => h.draw());
                }
                drawCarAngle(x, y) {
                    this._carAngle = Fract2DHelpers_1.drawCarAngle(this.ctx, this.tfm, x, y);
                }
                drawZones(x, y) {
                    if (!this._zonesData) {
                        this.updateShowBusy(true);
                        this._zonesData = HeightMapZones_3.HeightMapZones.compute(this.hmap, 1, 100, this.log, this.debug);
                        this.updateShowBusy(false);
                    }
                    const zd = this._zonesData, lzs = zd.getLevelZones(zd.getLevelIndex(x, y)), lcounts = { points: 0, contour: 0 };
                    let zone = undefined;
                    Pixels_6.Pixels.withImageData(this.ctx, imd => {
                        Pixels_6.Pixels.setAlpha(imd, 128);
                        if (lzs.length == 1)
                            zone = lzs[0];
                        else
                            zd.drawLevel(lzs, imd, Color_7.Color.Gray, Color_7.Color.Green, lcounts);
                        zd.drawZone(zone = zd.getZone(x, y), imd, Color_7.Color.LGray, Color_7.Color.LGreen);
                    });
                    this._zonesInfo = ''
                        + 'level-edges:' + lcounts.contour
                        + ' level-points:' + lcounts.points
                        + ' level-zones:' + (lzs && lzs.length || 0)
                        + ' zone-edges:' + (zone && zone.contour.length || 0)
                        + ' zone-points:' + (zone && zone.points.length || 0);
                }
                isMaskVisible() {
                    return true; //todo compute(this.viewport.region, this.state.a, this.state.b, this.state.z)
                }
            };
            exports_38("Fract2D", Fract2D);
        }
    };
});
System.register("client/fract3D/Fract3DKeys", ["shared/KeyCommands"], function (exports_39, context_39) {
    "use strict";
    var __moduleName = context_39 && context_39.id;
    var KeyCommands_2, Fract3DKeys;
    return {
        setters: [
            function (KeyCommands_2_1) {
                KeyCommands_2 = KeyCommands_2_1;
            }
        ],
        execute: function () {
            Fract3DKeys = class Fract3DKeys extends KeyCommands_2.KeyCommands {
                constructor(inBrowser, tool, log = true, activateSpecials = false) {
                    super([
                        { key: 'I', action: () => tool('Info'), doc: "info panel" },
                        { key: 'R', action: () => tool('colorMode'), doc: "color mode" },
                        { key: 'C', action: () => tool('colors'), doc: "colors/gray gradient" },
                        { key: 'E', action: (shift) => tool('easing', shift), doc: "gradient easing type" },
                        { key: 'V', action: () => tool('invert'), doc: "invert colors" },
                        { key: 'T', action: () => tool('transparent'), doc: "transparency" },
                        { key: 'W', action: () => tool('wireframe'), doc: "wireframe" },
                        { key: 'N', action: () => tool('negateY'), doc: "invert height" },
                        { key: 'L', action: (shift) => tool('lod', shift), doc: "level of detail" },
                        { key: 'P', action: () => tool('points'), doc: "points/mesh" },
                        { key: 'alt-N', action: () => tool('fnh'), doc: "face normals helper" },
                        { key: 'alt-G', action: () => tool('grid'), doc: "grid" },
                        { key: 'alt-A', action: () => tool('axis'), doc: "axis" },
                        { key: 'alt-C', action: () => tool('crosshair'), doc: "crosshair" },
                        { key: 'alt-L', action: () => tool('lights'), doc: "lights helpers" },
                        { key: 'alt-S', action: () => tool('stats'), doc: "stats" },
                        { key: 'K', action: (shift) => tool('faceTracking', shift), doc: "face tracking" },
                        { key: 'F', action: () => tool('FullScreen'), doc: "fullscreen" },
                        { key: 'Y', action: () => tool('Switch'), doc: "switch to 2D" },
                        { key: 'H', action: () => tool('Doc'), doc: "commands list" },
                        { key: 'ctrl-O', action: () => tool('mesh.optimize'), doc: "mesh-optimize" },
                        { key: 'ctrl-Z', action: () => tool('mesh.zones'), doc: "mesh-zones" },
                        { key: 'ctrl-N', action: () => tool('mesh.noiseZones'), doc: "mesh-noiseZones" },
                        { key: 'ctrl-S', action: () => tool('mesh.seams'), doc: "mesh-seams" },
                    ], inBrowser, undefined, undefined, log, activateSpecials);
                }
            };
            exports_39("Fract3DKeys", Fract3DKeys);
        }
    };
});
System.register("shared/three/GUIs", [], function (exports_40, context_40) {
    "use strict";
    var __moduleName = context_40 && context_40.id;
    var GUIs;
    return {
        setters: [],
        execute: function () {
            GUIs = class GUIs {
                static vector3(v, parent, name, listen = false, reset = false, onChange) {
                    const v0 = v.clone(), gui = parent.addFolder(name), cx = gui.add(v, 'x'), cy = gui.add(v, 'y'), cz = gui.add(v, 'z');
                    if (listen) {
                        cx.listen();
                        cy.listen();
                        cz.listen();
                    }
                    if (onChange) {
                        cx.onChange((x) => onChange(v));
                        cy.onChange((y) => onChange(v));
                        cz.onChange((z) => onChange(v));
                    }
                    if (reset) {
                        gui.add({
                            reset: () => {
                                v.copy(v0);
                                if (listen)
                                    return;
                                cx.updateDisplay();
                                cy.updateDisplay();
                                cz.updateDisplay();
                                if (onChange)
                                    onChange(v);
                            }
                        }, "reset");
                    }
                    return gui;
                }
                static color(c, gui, name, listen = false, reset = false, onChange) {
                    const o = {}, c0 = c.clone();
                    Object.defineProperty(o, name, {
                        get: () => '#' + c.getHexString(),
                        set: v => c.setStyle(v)
                    });
                    const cc = gui.addColor(o, name);
                    if (listen)
                        cc.listen();
                    if (onChange)
                        cc.onChange((_) => onChange(c));
                    if (reset)
                        gui.add({
                            reset: () => {
                                c.copy(c0);
                                if (listen)
                                    return;
                                cc.updateDisplay();
                                if (onChange)
                                    onChange(c);
                            }
                        }, 'reset');
                    return gui;
                }
                static light(l, parent, name, listen = false, reset = false, onChange) {
                    const gui = parent.addFolder(name);
                    this.vector3(l.position, gui, 'position', listen, reset, onChange && (_ => onChange(l)));
                    this.color(l.color, gui, 'color', listen, reset, onChange && (_ => onChange(l)));
                    return gui;
                }
            };
            exports_40("GUIs", GUIs);
        }
    };
});
System.register("shared/three/ViewParams", [], function (exports_41, context_41) {
    "use strict";
    var __moduleName = context_41 && context_41.id;
    var ViewParams;
    return {
        setters: [],
        execute: function () {
            ViewParams = class ViewParams {
                constructor(viewport, fov = 40, near = 0.1, far = 10000) {
                    this.viewport = viewport;
                    this.fov = fov;
                    this.near = near;
                    this.far = far;
                }
                // get width() { return this.viewport.clientWidth; }
                // get height() { return this.viewport.clientHeight; }
                get width() { return window.innerWidth; }
                get height() { return window.innerHeight; }
                get aspect() { return this.width / this.height; }
                get size() { return { width: this.width, height: this.height }; }
            };
            exports_41("ViewParams", ViewParams);
        }
    };
});
System.register("shared/three/Helpers", ["THREE"], function (exports_42, context_42) {
    "use strict";
    var __moduleName = context_42 && context_42.id;
    var THREE_1, Helpers;
    return {
        setters: [
            function (THREE_1_1) {
                THREE_1 = THREE_1_1;
            }
        ],
        execute: function () {
            Helpers = class Helpers {
                static createCrosshair(length, colorOrMaterial, name) {
                    var l = length / 2, geometry = new THREE_1.Geometry();
                    geometry.vertices.push(new THREE_1.Vector3(-l, 0, 0), new THREE_1.Vector3(l, 0, 0), new THREE_1.Vector3(0, -l, 0), new THREE_1.Vector3(0, l, 0), new THREE_1.Vector3(0, 0, -l), new THREE_1.Vector3(0, 0, l));
                    const material = (colorOrMaterial && colorOrMaterial.isMaterial) ?
                        colorOrMaterial
                        : new THREE_1.LineBasicMaterial({ color: colorOrMaterial || this.randomColor() });
                    var mesh = new THREE_1.LineSegments(geometry, material);
                    mesh.name = name || 'crosshair';
                    return mesh;
                }
                static randomColor() {
                    return this.randomMinMaxInt(0, 0xffffff);
                }
                static randomMinMaxInt(min, max) {
                    min = Math.ceil(min);
                    max = Math.floor(max);
                    return Math.floor(Math.random() * (max - min + 1)) + min;
                }
            };
            exports_42("Helpers", Helpers);
        }
    };
});
System.register("shared/three/SceneHelpers", ["THREE", "shared/three/Helpers"], function (exports_43, context_43) {
    "use strict";
    var __moduleName = context_43 && context_43.id;
    var THREE_2, Helpers_1, HelperType, SceneHelpers;
    return {
        setters: [
            function (THREE_2_1) {
                THREE_2 = THREE_2_1;
            },
            function (Helpers_1_1) {
                Helpers_1 = Helpers_1_1;
            }
        ],
        execute: function () {
            (function (HelperType) {
                HelperType[HelperType["grid"] = 0] = "grid";
                HelperType[HelperType["axis"] = 1] = "axis";
                HelperType[HelperType["crosshair"] = 2] = "crosshair";
                HelperType[HelperType["light1"] = 3] = "light1";
                HelperType[HelperType["light2"] = 4] = "light2";
                HelperType[HelperType["light3"] = 5] = "light3";
            })(HelperType || (HelperType = {}));
            exports_43("HelperType", HelperType);
            SceneHelpers = class SceneHelpers {
                constructor(scene, lights, orbit, params, log = false) {
                    this.scene = scene;
                    this.log = log;
                    this._state = new Map();
                    this.createObjs(lights);
                    if (params == undefined)
                        params = {
                            grid: true,
                            axis: true,
                            crosshair: true,
                            light1: true,
                            light2: true,
                            light3: true,
                        };
                    this._state.set(HelperType.grid, !!params.grid);
                    this._state.set(HelperType.axis, !!params.axis);
                    this._state.set(HelperType.crosshair, !!params.crosshair);
                    if (lights)
                        lights.map((_, i) => 'light' + (i + 1)).forEach(ln => this._state.set(this.getHT(ln), !!params[ln]));
                    const n = SceneHelpers.names.reduce((p, c) => this.isVisible(c) ? p + 1 : p, 0);
                    if (this.log)
                        console.log(n + ' scene helpers');
                    this.attach(orbit);
                }
                createObjs(lights) {
                    this.grid = new THREE_2.GridHelper(1000, 20);
                    this.grid.name = 'gridHelper';
                    this.axis = new THREE_2.AxesHelper(100);
                    this.axis.name = 'axisHelper';
                    this.crosshair = Helpers_1.Helpers.createCrosshair(20, 0xf4ad42, 'crosshairHelper');
                    if (lights)
                        lights.forEach((l, i) => {
                            const lh = new THREE_2.PointLightHelper(l, 10), ln = 'light' + (i + 1);
                            lh.name = ln + 'Helper';
                            this[ln] = lh;
                        });
                }
                attach(orbit) {
                    if (this.attached)
                        return;
                    if (orbit && orbit !== this._orbit) {
                        this._orbit = orbit;
                        // this._onOrbitChange = (event: any) => this.updateCrossair()
                        // this._orbit.addEventListener('change', this._onOrbitChange)
                    }
                    this.setScene();
                    this.attached = true;
                }
                detach() {
                    if (!this.attached)
                        return;
                    // if (this._orbit && this._onOrbitChange)
                    //     this._orbit.removeEventListener('change', this._onOrbitChange)
                    this.unsetScene();
                    this.attached = false;
                }
                dispose() {
                    this.detach();
                }
                update() {
                    this.updateCrossair();
                }
                toggle(helper, force) {
                    if (helper == 'lights') {
                        const names = [1, 2, 3].map(i => 'light' + i);
                        const v = !names.some(n => this.isVisible(n));
                        names.forEach(n => this.toggle(n), v);
                        return;
                    }
                    const ht = this.getHT(helper), h = this.getH(ht);
                    if (!h)
                        return;
                    const isVisible = this.isVisible(ht), visible = force != undefined ? force : !isVisible;
                    //console.log(force, isVisible, visible)
                    if (isVisible == visible)
                        return;
                    if (visible)
                        this.scene.add(h);
                    else
                        this.scene.remove(h);
                    this._state.set(ht, visible);
                    if (this.log)
                        console.log(HelperType[ht], visible);
                }
                isVisible(helper) {
                    return !!this._state.get(this.getHT(helper));
                }
                datProxy() {
                    const r = {};
                    SceneHelpers.names.forEach(hn => {
                        Object.defineProperty(r, hn, {
                            get: () => this.isVisible(hn),
                            set: (v) => this.toggle(hn, v)
                        });
                    });
                    return r;
                }
                getHT(helper) {
                    return typeof helper == 'string' ? HelperType[helper] : helper;
                }
                getH(helper, log = false) {
                    const hn = typeof helper == 'string' && isNaN(parseInt(helper, 10)) ? helper : HelperType[helper];
                    return this[hn];
                }
                unsetScene() {
                    SceneHelpers.names.forEach(hn => this.scene.remove(this.getH(hn)));
                }
                setScene(scene) {
                    if (scene)
                        this.scene = scene;
                    this.unsetScene();
                    SceneHelpers.names
                        .filter(hn => this.isVisible(hn))
                        .map(hn => this.getH(hn, true))
                        .filter(o => !!o)
                        .forEach(o => this.scene.add(o));
                }
                updateCrossair() {
                    if (this.crosshair && this._orbit)
                        this.crosshair.position.copy(this._orbit.target);
                }
            };
            SceneHelpers.names = Object.keys(HelperType).filter(k => !isNaN(Number(HelperType[k])));
            exports_43("SceneHelpers", SceneHelpers);
        }
    };
});
System.register("shared/three/ThreeViewer", ["THREE", "dat.gui", "screenfull", "shared/three/GUIs", "shared/three/ViewParams", "shared/three/SceneHelpers", "shared/DomHelper"], function (exports_44, context_44) {
    "use strict";
    var __moduleName = context_44 && context_44.id;
    var THREE_3, THREE_4, dat_gui_1, screenfull, GUIs_1, ViewParams_1, SceneHelpers_1, DomHelper_7, StatMode, ThreeViewer;
    return {
        setters: [
            function (THREE_3_1) {
                THREE_3 = THREE_3_1;
                THREE_4 = THREE_3_1;
            },
            function (dat_gui_1_1) {
                dat_gui_1 = dat_gui_1_1;
            },
            function (screenfull_2) {
                screenfull = screenfull_2;
            },
            function (GUIs_1_1) {
                GUIs_1 = GUIs_1_1;
            },
            function (ViewParams_1_1) {
                ViewParams_1 = ViewParams_1_1;
            },
            function (SceneHelpers_1_1) {
                SceneHelpers_1 = SceneHelpers_1_1;
            },
            function (DomHelper_7_1) {
                DomHelper_7 = DomHelper_7_1;
            }
        ],
        execute: function () {
            (function (StatMode) {
                StatMode[StatMode["no"] = -1] = "no";
                StatMode[StatMode["fps"] = 0] = "fps";
                StatMode[StatMode["ms"] = 1] = "ms";
                StatMode[StatMode["mb"] = 2] = "mb";
            })(StatMode || (StatMode = {}));
            exports_44("StatMode", StatMode);
            ThreeViewer = class ThreeViewer {
                constructor(viewport, setScene, onRender = (dt, now) => { }, params = {}) {
                    this.onRender = onRender;
                    this.params = params;
                    this.log = true;
                    this._statMode = StatMode.no;
                    this._onresize = () => { this.onResize(); };
                    this._onbeforeunload = () => { this.dispose(); };
                    this._onOrbitChange = () => { this.requestRender(); };
                    window.addEventListener('beforeunload', this._onbeforeunload);
                    if (typeof viewport == 'string')
                        viewport = document.getElementById(viewport);
                    const vp = this.viewParams = new ViewParams_1.ViewParams(viewport);
                    console.log('viewport', vp.width + 'x' + vp.height);
                    this.animate = params.animate;
                    const renderer = new THREE_3.WebGLRenderer({ antialias: true, alpha: true });
                    renderer.setPixelRatio(window.devicePixelRatio);
                    renderer.setSize(vp.width, vp.height);
                    renderer.setClearColor(ThreeViewer.clearColor, 1);
                    this.renderer = renderer;
                    viewport.appendChild(renderer.domElement);
                    this.scene = new THREE_3.Scene();
                    this.scene.name = 'scene';
                    this.camera = new THREE_3.PerspectiveCamera(vp.fov, vp.aspect, vp.near, vp.far);
                    this.setVector(this.camera.position, params.camera && params.camera.position
                        || { x: 0, y: 300, z: 600 });
                    this.camera.lookAt(this.setVector(new THREE_3.Vector3(), params.camera && params.camera.target
                        || { x: 0, y: 0, z: 0 }));
                    this.orbit = new THREE_4.OrbitControls(this.camera, renderer.domElement);
                    this.lights = [[0, 1000, 0], [1000, 1000, 1000], [-1000, -1000, -1000]]
                        .map((p, i) => {
                        const l = new THREE_3.PointLight(0xffffff, 1, 0);
                        l.position.fromArray(p);
                        l.name = 'light' + (i + 1);
                        return l;
                    });
                    this.helpers = new SceneHelpers_1.SceneHelpers(this.scene, this.lights, this.orbit, params, this.log);
                    this.raycaster = new THREE_3.Raycaster();
                    this.statElement = (params ?
                        typeof params.statElement == 'string' ? document.getElementById(params.statElement)
                            : params.statElement : undefined) || document.body;
                    this.setStats(StatMode.no);
                    this.guiElement = params ?
                        typeof params.guiElement == 'string' ? document.getElementById(params.guiElement)
                            : params.guiElement : undefined;
                    this.guiData = params && params.guiData;
                    setScene(this.scene, this.camera, this.orbit);
                    this.attach();
                    this.attached();
                }
                get doms() {
                    return [
                        this.gui && this.gui.domElement,
                        this.stats && this.stats.dom
                    ].filter(o => !!o);
                }
                get statMode() { return this._statMode; }
                set statMode(mode) { this.setStats(this._statMode = mode); }
                dispose() {
                    if (this.disposing)
                        return;
                    this.disposing = true;
                    if (this.log)
                        console.log('disposing');
                    window.removeEventListener('beforeunload', this._onbeforeunload);
                    this.detach();
                    this.detached();
                    if (this.orbit)
                        this.orbit.dispose();
                    if (this.helpers)
                        this.helpers.dispose();
                    if (this.stats) {
                        this.stats.end();
                        this.stats.dom.remove();
                        if (this.statElement != document.body)
                            this.statElement.remove();
                    }
                    return new Promise((resolve, reject) => {
                        this.disposeObj(this.scene);
                        setTimeout(() => {
                            this.disposing = false;
                            if (this.log)
                                console.log('disposed');
                            resolve();
                        }, 100);
                    });
                }
                attach() {
                    if (this._attached)
                        return;
                    window.addEventListener('resize', this._onresize);
                    this.setScene();
                    this.startRendering();
                    if (this.orbit) {
                        if (this.helpers)
                            this.helpers.attach(this.orbit);
                        this.orbit.addEventListener('change', this._onOrbitChange);
                        this.orbit.enabled = true;
                    }
                    this._attached = true;
                    if (this.log)
                        console.log('attached');
                    if (this.stats)
                        this.stats.begin();
                }
                attached() { this.initGui(); }
                detach() {
                    if (!this._attached)
                        return;
                    window.removeEventListener('resize', this._onresize);
                    if (this.stats)
                        this.stats.end();
                    if (this.helpers)
                        this.helpers.detach();
                    if (this.orbit) {
                        this.orbit.enabled = false;
                        this.orbit.removeEventListener('change', this._onOrbitChange);
                    }
                    this.unsetScene();
                    this._attached = false;
                    if (this.log)
                        console.log('detached');
                }
                detached() { if (this.gui) {
                    this.gui.destroy();
                    delete this.gui;
                } }
                toggleStat() {
                    this.statMode = this.statMode == StatMode.no ? StatMode.fps : StatMode.no;
                }
                nextStat() {
                    this.statMode =
                        this.statMode == undefined ? StatMode.fps
                            : this.statMode >= StatMode.mb ? StatMode.no
                                : (this.statMode + 1);
                }
                toggleFullScreen(force) {
                    const sf = screenfull.screenfull, fs = force != undefined ? force : !sf.isFullscreen;
                    if (sf.enabled)
                        if (fs)
                            sf.request();
                        else
                            sf.exit();
                    this.requestRender();
                    return sf.enabled && fs;
                }
                toggleHelper(name, force) {
                    this.helpers.toggle(name, force);
                    this.requestRender();
                }
                setBackground(color) {
                    if (!(color instanceof THREE_3.Color))
                        color = new THREE_3.Color(color);
                    this.scene.background.copy(color);
                    this.requestRender();
                }
                getCamera() {
                    return {
                        position: this.camera.position.clone(),
                        target: this.orbit.target.clone()
                    };
                }
                setCamera(position, target) {
                    this.setVector(this.camera.position, position);
                    if (this.orbit) {
                        this.setVector(this.orbit.target, target);
                        this.orbit.update();
                    }
                    this.requestRender();
                }
                disposeObj(obj3D) {
                    const disposings = new Array();
                    function doDispose(obj) {
                        if (!obj)
                            return;
                        if (Array.isArray(obj)) {
                            obj.forEach(o => doDispose(o));
                            return;
                        }
                        if (obj.traverse)
                            obj.traverse(o => { if (o !== obj)
                                doDispose(o); });
                        const mesh = obj;
                        if (!mesh.geometry && !mesh.material)
                            return;
                        if (disposings.indexOf(obj) != -1)
                            return;
                        //console.log( 'disposing ' + ( obj.name || obj.id ) )
                        if (mesh.geometry)
                            mesh.geometry.dispose();
                        if (mesh.material)
                            if (Array.isArray(mesh.material))
                                mesh.material.forEach(m => m.dispose());
                            else
                                mesh.material.dispose();
                        disposings.push(obj);
                    }
                    doDispose(obj3D);
                }
                getPointedFace(event, mesh) {
                    const mouse = {
                        x: (event.clientX / this.renderer.domElement.clientWidth) * 2 - 1,
                        y: -(event.clientY / this.renderer.domElement.clientHeight) * 2 + 1
                    };
                    this.raycaster.setFromCamera(mouse, this.camera);
                    var intersects = this.raycaster.intersectObject(mesh);
                    return intersects.length && intersects[0].face;
                }
                setVector(v, p) {
                    if (!v || !p || p.x == undefined && p.y == undefined && p.z == undefined)
                        return;
                    v.set(p.x == undefined ? v.x : p.x, p.y == undefined ? v.y : p.y, p.z == undefined ? v.z : p.z);
                    return v;
                }
                startRendering() {
                    const self = this, renderer = this.renderer, dater = performance || Date;
                    let before = dater.now();
                    function render() {
                        if (self.animate) {
                            if (!self._attached || self.disposing) {
                                if (self.log)
                                    console.log('rendering stopped');
                                return;
                            }
                            requestAnimationFrame(render);
                        }
                        const now = dater.now(), dt = (now - before) * .001;
                        before = now;
                        self.onRender(dt, now);
                        if (self.stats)
                            self.stats.update();
                        if (self.helpers)
                            self.helpers.update();
                        //if (self.gui) self.updateGui(self.gui)
                        renderer.render(self.scene, self.camera);
                    }
                    this.render = render;
                    render();
                }
                requestRender() {
                    if (!this.animate)
                        requestAnimationFrame(this.render);
                }
                updateGui(gui) {
                    // if (!gui || gui.closed) return
                    // if (gui.__controllers) gui.__controllers.forEach(c => c.updateDisplay())
                    // if (gui.__folders && gui.__folders.length)  gui.__folders.forEach(f => this.updateGui(f))
                }
                onResize() {
                    if (this.log)
                        console.log('resized', this.viewParams.width + 'x' + this.viewParams.height);
                    this.renderer.setSize(this.viewParams.width, this.viewParams.height);
                    this.camera.aspect = this.viewParams.aspect;
                    this.camera.updateProjectionMatrix();
                }
                unsetScene() {
                    if (!this.scene)
                        return;
                    this.scene.children.forEach(o => this.disposeObj(o));
                }
                setScene() {
                    this.lights.forEach(l => this.scene.add(l));
                }
                setStats(mode) {
                    if (mode == StatMode.no) {
                        if (this.stats) {
                            this.stats.end();
                            this.stats.dom.remove();
                            this.stats = undefined;
                            if (this.log)
                                console.log('stats: ' + StatMode[mode]);
                        }
                        return;
                    }
                    if (!this.stats)
                        this.stats = new Stats();
                    DomHelper_7.DomHelper.reparent(this.stats.dom, this.statElement);
                    DomHelper_7.DomHelper.removeProperties(this.stats.dom, 'position', 'top', 'left');
                    this.stats.showPanel(mode || 0);
                    if (this.log)
                        console.log('stats: ' + StatMode[mode]);
                }
                initGui() {
                    if (this.gui)
                        return;
                    const update = () => this.requestRender(), gui = this.gui = new dat_gui_1.GUI({ autoPlace: this.guiElement == undefined });
                    if (this.guiElement)
                        DomHelper_7.DomHelper.reparent(gui.domElement, this.guiElement);
                    gui.closed = this.params && !this.params.guiOpen;
                    const cgui = gui.addFolder('camera');
                    GUIs_1.GUIs.vector3(this.camera.position, cgui, 'position', true, true, _ => this.orbit.update());
                    GUIs_1.GUIs.vector3(this.orbit.target, cgui, 'target', true, true, v => { this.camera.lookAt(v); update(); });
                    const lgui = gui.addFolder('lights');
                    this.lights.forEach((l, i) => GUIs_1.GUIs.light(this.lights[0], lgui, 'light' + (i + 1), false, true, update));
                    const hgui = gui.addFolder('helpers'), proxy = this.helpers.datProxy();
                    SceneHelpers_1.SceneHelpers.names.forEach(hn => hgui.add(proxy, hn).onChange(() => update()));
                    if (!this.scene.background)
                        this.scene.background = new THREE_3.Color(ThreeViewer.clearColor);
                    GUIs_1.GUIs.color(this.scene.background, gui, 'background', false, false, update);
                    const o = { fullScreen: false };
                    gui.add(o, 'fullScreen').onChange((fs) => o.fullScreen = this.toggleFullScreen(fs));
                }
            };
            ThreeViewer.clearColor = 0xE0E0E0; // 0x282424 0x595D98
            exports_44("ThreeViewer", ThreeViewer);
        }
    };
});
System.register("client/fract3D/RenderParams3D", ["THREE", "shared/Easing"], function (exports_45, context_45) {
    "use strict";
    var __moduleName = context_45 && context_45.id;
    var THREE_5, Easing_5, RenderParams3DMesh, RenderParams3D;
    return {
        setters: [
            function (THREE_5_1) {
                THREE_5 = THREE_5_1;
            },
            function (Easing_5_1) {
                Easing_5 = Easing_5_1;
            }
        ],
        execute: function () {
            RenderParams3DMesh = class RenderParams3DMesh {
                constructor() {
                    this.optimize = true;
                    this.zones = true;
                    this.seams = true;
                    this.noiseZones = true;
                }
                toString(prefix = '') {
                    return Object.keys(this).map(k => prefix + k + ':' + !!this[k]).join(' ');
                }
            };
            RenderParams3D = class RenderParams3D {
                constructor(par) {
                    this.mesh = new RenderParams3DMesh();
                    this.colors = true;
                    this.invert = false;
                    this.easing = 0;
                    this.maxHeight = 300;
                    this.minZoneArea = 10;
                    this.lod = 4;
                    this.negateY = true;
                    this.points = false;
                    this.pointSize = 1;
                    this.pointSizeAttenuation = true;
                    this.wireframe = false;
                    this.depthTest = true;
                    this.opacity = 0.25;
                    this.transparent = false;
                    this.vertexColors = true;
                    this.faceColors = false;
                    this.fnh = false;
                    this.background = new THREE_5.Color(0, 0, 0);
                    this.grid = false;
                    this.faceTracking = false;
                    this.trackedFace = false;
                    if (!par)
                        return;
                    Object.keys(this).forEach(k => {
                        const v = par[k];
                        if (v != undefined)
                            this[k] = v;
                    });
                    if (par.mesh)
                        this.mesh = par.mesh;
                    if (par.camera)
                        this.camera = par.camera;
                    if (this.negateY && this.camera && this.camera.target)
                        this.camera.target.y = -this.camera.target.y;
                }
                getVertexColors() {
                    return RenderParams3D.getVertexColors(this);
                }
                toString() {
                    return Object.keys(this).map(k => this.keyToString(k)).filter(s => s).join(' ');
                }
                keyToString(name) {
                    switch (name) {
                        case 'camera': return null;
                        case 'mesh': return !this.points && this.mesh.toString('mesh-');
                        default: return name + ':' + this.valToString(name);
                    }
                }
                valToString(name) {
                    const v = this[name];
                    switch (name) {
                        case 'background': return '#' + v.getHexString();
                        case 'easing': return Easing_5.Easing.funcName(v);
                        case 'lod': return '1/' + v;
                    }
                    return v;
                }
                static getVertexColors(par) {
                    return par.vertexColors ? THREE_5.VertexColors : par.faceColors ? THREE_5.FaceColors : THREE_5.NoColors;
                }
            };
            exports_45("RenderParams3D", RenderParams3D);
        }
    };
});
System.register("shared/three/MeshBuilder", ["THREE", "client/fract3D/RenderParams3D", "shared/Color"], function (exports_46, context_46) {
    "use strict";
    var __moduleName = context_46 && context_46.id;
    var THREE_6, RenderParams3D_1, Color_8, MeshBuilder;
    return {
        setters: [
            function (THREE_6_1) {
                THREE_6 = THREE_6_1;
            },
            function (RenderParams3D_1_1) {
                RenderParams3D_1 = RenderParams3D_1_1;
            },
            function (Color_8_1) {
                Color_8 = Color_8_1;
            }
        ],
        execute: function () {
            MeshBuilder = class MeshBuilder {
                static makePoints(hmap, par, log = false, debug = false) {
                    if (log)
                        console.log('makePoints');
                    return new THREE_6.Points(MeshBuilder.makeGeometry(hmap, par.maxHeight, par.lod, par.negateY, {
                        colored: par.colors,
                        invert: par.invert,
                        easing: par.easing
                    }, false, false, debug), new THREE_6.PointsMaterial({
                        vertexColors: THREE_6.VertexColors,
                        size: par.pointSize || 1,
                        sizeAttenuation: !!par.pointSizeAttenuation
                    }));
                }
                static makeMesh(data, par, log = false, debug = false) {
                    if (log)
                        console.log('makeMesh');
                    const material = new THREE_6.MeshBasicMaterial({
                        wireframe: par.wireframe,
                        depthTest: par.depthTest,
                        side: THREE_6.DoubleSide,
                        opacity: par.opacity,
                        transparent: par.transparent,
                        vertexColors: RenderParams3D_1.RenderParams3D.getVertexColors(par),
                    });
                    function makeGeo(geomData) {
                        return MeshBuilder.makeGeometry(geomData, par.maxHeight, par.lod, par.negateY, {
                            colored: par.colors,
                            invert: par.invert,
                            easing: par.easing
                        }, true, par.faceColors, debug);
                    }
                    const md = data.vertices && data;
                    if (!md)
                        return new THREE_6.Mesh(makeGeo(data), material);
                    else if (md.merged) {
                        const d = { vertices: md.vertices, faces: md.faces[0] };
                        return new THREE_6.Mesh(makeGeo(d), material);
                    }
                    else {
                        const obj = new THREE_6.Object3D();
                        md.faces.forEach((fs, i) => {
                            const m = new THREE_6.Mesh(makeGeo({ vertices: md.vertices, faces: fs }), material);
                            m.name = md.names[i];
                            obj.add(m);
                        });
                        return obj;
                    }
                }
                static makeGeometry(dataObj, maxHeight, lod = 1, negateY = false, colored, faces = false, nonIndexed = false, log = false) {
                    const hmap = dataObj.data && dataObj;
                    const geomData = dataObj.vertices && dataObj;
                    if (geomData)
                        nonIndexed = false; //could be non indexed as well
                    if (log)
                        console.log('makeGeometry', 'colored:', !!colored, 'faces:', !!faces, 'nonIndexed:', !!nonIndexed, 'meshData', !!geomData);
                    const geometry = new THREE_6.BufferGeometry();
                    geometry.rotateX(-Math.PI / 2);
                    let positions = new Array();
                    const mh = negateY ? -maxHeight : maxHeight;
                    let colors, color, colorize;
                    if (colored) {
                        colors = new Array();
                        color = new Color_8.Color();
                        colorize = Color_8.Color.getColorizer(color, colored.colored, colored.invert, colored.easing);
                    }
                    if (geomData) {
                        positions = geomData.vertices;
                        if (colored)
                            for (let iy = 1, l = positions.length; iy < l; iy += 3) {
                                colorize(positions[iy] / mh);
                                colors.push(color.r / 255, color.g / 255, color.b / 255);
                            }
                        geometry.setIndex(geomData.faces);
                    }
                    else {
                        const data = hmap.data, dlj = (data.length / hmap.dataWidth) | 0, dli = hmap.dataWidth | 0, lj = (dlj / lod) | 0, li = (dli / lod) | 0, x0 = (-li / 2) * lod, z0 = (-lj / 2) * lod, lj1 = lj - 1, li1 = li - 1;
                        if (!nonIndexed) {
                            for (let j = 0, jl, djli; j < lj; j++) {
                                jl = j * lod;
                                djli = jl * dli;
                                for (let i = 0, h, il; i < li; i++) {
                                    il = i * lod;
                                    h = data[Math.round(djli + il)];
                                    positions.push(x0 + il, h * mh, z0 + jl);
                                    if (!colored)
                                        continue;
                                    colorize(h);
                                    colors.push(color.r / 255, color.g / 255, color.b / 255);
                                }
                            }
                            if (faces) {
                                let indices;
                                if (log)
                                    console.log('indices');
                                indices = new Array();
                                let jli, jli1, a, b, c, d;
                                for (let j = 0; j < lj1; j++) {
                                    jli = j * li;
                                    jli1 = jli + li;
                                    for (let i = 0; i < li1; i++) {
                                        a = jli + i;
                                        b = jli1 + i;
                                        c = jli1 + i + 1;
                                        d = jli + i + 1;
                                        indices.push(a, b, d, b, c, d);
                                    }
                                }
                                geometry.setIndex(indices);
                            }
                        }
                        else {
                            let djli, djli1, jl, jl1, il, il1, ax, ay, az, ah, bx, by, bz, bh, br, bg, bb, cx, cy, cz, ch, dx, dy, dz, dh, dr, dg, db;
                            for (let j = 0; j < lj1; j++) {
                                jl = j * lod;
                                jl1 = (j + 1) * lod;
                                djli = jl * dli;
                                djli1 = jl1 * dli;
                                for (let i = 0; i < li1; i++) {
                                    il = i * lod;
                                    il1 = (i + 1) * lod;
                                    ah = data[Math.round(djli + il)];
                                    ax = x0 + il;
                                    ay = ah * mh;
                                    az = z0 + jl;
                                    bh = data[Math.round(djli1 + il)];
                                    bx = x0 + il;
                                    by = bh * mh;
                                    bz = z0 + jl1;
                                    ch = data[Math.round(djli1 + il1)];
                                    cx = x0 + il1;
                                    cy = ch * mh;
                                    cz = z0 + jl1;
                                    dh = data[Math.round(djli + il1)];
                                    dx = x0 + il1;
                                    dy = dh * mh;
                                    dz = z0 + jl;
                                    positions.push(ax, ay, az, bx, by, bz, dx, dy, dz);
                                    positions.push(bx, by, bz, cx, cy, cz, dx, dy, dz);
                                    if (colored) {
                                        colorize(ah);
                                        colors.push(color.r / 255, color.g / 255, color.b / 255);
                                        colorize(bh);
                                        br = color.r / 255;
                                        bg = color.g / 255;
                                        bb = color.b / 255;
                                        colors.push(br, bg, bb);
                                        colorize(dh);
                                        dr = color.r / 255;
                                        dg = color.g / 255;
                                        db = color.b / 255;
                                        colors.push(dr, dg, db);
                                        colors.push(br, bg, bb);
                                        colorize(ch);
                                        colors.push(color.r / 255, color.g / 255, color.b / 255);
                                        colors.push(dr, dg, db);
                                    }
                                }
                            }
                        }
                    }
                    geometry.addAttribute('position', new THREE_6.Float32BufferAttribute(positions, 3));
                    if (colored)
                        geometry.addAttribute('color', new THREE_6.Float32BufferAttribute(colors, 3));
                    if (faces) {
                        //if (log) console.log('computeVertexNormals')
                        geometry.computeVertexNormals();
                        //if (log) console.log('computeBoundingSphere')
                        geometry.computeBoundingSphere();
                    }
                    if (log)
                        console.log('geometry built');
                    return geometry;
                }
            };
            exports_46("MeshBuilder", MeshBuilder);
        }
    };
});
System.register("shared/three/ThreeUtil", ["THREE"], function (exports_47, context_47) {
    "use strict";
    var __moduleName = context_47 && context_47.id;
    var THREE_7, ThreeUtil;
    return {
        setters: [
            function (THREE_7_1) {
                THREE_7 = THREE_7_1;
            }
        ],
        execute: function () {
            ThreeUtil = class ThreeUtil {
                static normal(vA, vB, vC) {
                    return vC.clone().sub(vB).cross(vA.clone().sub(vB)).normalize();
                }
                static faceNormal(vs, f) {
                    return this.normal(vs[f.a], vs[f.b], vs[f.c]);
                }
                static makeTerrainFromCanvas(canvas) {
                    const texture = new THREE_7.CanvasTexture(canvas), 
                    //material = this.makeShaderTerrainMaterial(texture, texture)
                    material = new THREE_7.MeshBasicMaterial({ map: texture });
                    //https://github.com/mrdoob/three.js/blob/master/examples/webgl_geometry_terrain.html
                    const w = 1024, h = 512;
                    var geometry = new THREE_7.PlaneGeometry(w, h, 256, 256);
                    geometry.rotateX(-Math.PI / 2);
                    geometry.computeFaceNormals();
                    geometry.computeVertexNormals();
                    return new THREE_7.Mesh(geometry, material);
                }
            };
            exports_47("ThreeUtil", ThreeUtil);
        }
    };
});
System.register("shared/three/FaceHelper", ["THREE"], function (exports_48, context_48) {
    "use strict";
    var __moduleName = context_48 && context_48.id;
    var THREE_8, FaceHelper;
    return {
        setters: [
            function (THREE_8_1) {
                THREE_8 = THREE_8_1;
            }
        ],
        execute: function () {
            FaceHelper = class FaceHelper {
                constructor(vertices, log = false) {
                    this.vertices = vertices;
                    this.log = log;
                    this.centroid = new THREE_8.Vector3();
                    this.a = new THREE_8.Vector3();
                    this.b = new THREE_8.Vector3();
                    this.c = new THREE_8.Vector3();
                    this.geo = new THREE_8.BufferGeometry();
                }
                get hasObjects() { return !!this.mesh || !!this.centroHelper; }
                update(face) {
                    this.updateData(face);
                    this.geo.setFromPoints([this.a, this.b, this.c]);
                    if (!this.mesh) {
                        const matparam = { color: 0xff0000, wireframe: true };
                        this.mesh = new THREE_8.Mesh(this.geo, new THREE_8.MeshBasicMaterial(matparam));
                        this.centroHelper = new THREE_8.ArrowHelper(face.normal, this.centroid, 10, 0xffffff);
                    }
                    this.geo.attributes.position.needsUpdate = true;
                    this.centroHelper.position.copy(this.centroid);
                    this.centroHelper.setDirection(face.normal);
                }
                updateData(face) {
                    const ia = face.a * 3, ib = face.b * 3, ic = face.c * 3, vs = this.vertices;
                    this.a.set(vs[ia], vs[ia + 1], vs[ia + 2]);
                    this.b.set(vs[ib], vs[ib + 1], vs[ib + 2]);
                    this.c.set(vs[ic], vs[ic + 1], vs[ic + 2]);
                    FaceHelper.centroid(this.a, this.b, this.c, this.centroid);
                    if (this.log)
                        console.log(name, face, this.a, this.b, this.c, this.centroid);
                }
                static centroid(a, b, c, result = new THREE_8.Vector3()) {
                    return result.set((a.x + b.x + c.x) / 3, (a.y + b.y + c.y) / 3, (a.z + b.z + c.z) / 3);
                }
            };
            exports_48("FaceHelper", FaceHelper);
        }
    };
});
System.register("client/fract3D/Fract3D", ["shared/fract/FractBase", "client/fract3D/Fract3DKeys", "shared/DomHelper", "shared/three/ThreeViewer", "THREE", "three-shader-terrain", "shared/HeightMap", "shared/Easing", "client/fract3D/RenderParams3D", "shared/three/MeshBuilder", "shared/three/ThreeUtil", "shared/HMapMeshData", "shared/MouseEvents", "shared/three/FaceHelper"], function (exports_49, context_49) {
    "use strict";
    var __moduleName = context_49 && context_49.id;
    var FractBase_2, Fract3DKeys_1, DomHelper_8, ThreeViewer_1, THREE_9, HeightMap_4, Easing_6, RenderParams3D_2, MeshBuilder_1, ThreeUtil_1, HMapMeshData_2, MouseEvents_2, FaceHelper_1, Tools, Fract3D;
    return {
        setters: [
            function (FractBase_2_1) {
                FractBase_2 = FractBase_2_1;
            },
            function (Fract3DKeys_1_1) {
                Fract3DKeys_1 = Fract3DKeys_1_1;
            },
            function (DomHelper_8_1) {
                DomHelper_8 = DomHelper_8_1;
            },
            function (ThreeViewer_1_1) {
                ThreeViewer_1 = ThreeViewer_1_1;
            },
            function (THREE_9_1) {
                THREE_9 = THREE_9_1;
            },
            function (_1) {
            },
            function (HeightMap_4_1) {
                HeightMap_4 = HeightMap_4_1;
            },
            function (Easing_6_1) {
                Easing_6 = Easing_6_1;
            },
            function (RenderParams3D_2_1) {
                RenderParams3D_2 = RenderParams3D_2_1;
            },
            function (MeshBuilder_1_1) {
                MeshBuilder_1 = MeshBuilder_1_1;
            },
            function (ThreeUtil_1_1) {
                ThreeUtil_1 = ThreeUtil_1_1;
            },
            function (HMapMeshData_2_1) {
                HMapMeshData_2 = HMapMeshData_2_1;
            },
            function (MouseEvents_2_1) {
                MouseEvents_2 = MouseEvents_2_1;
            },
            function (FaceHelper_1_1) {
                FaceHelper_1 = FaceHelper_1_1;
            }
        ],
        execute: function () {
            (function (Tools) {
                Tools[Tools["Doc"] = 0] = "Doc";
                Tools[Tools["Switch"] = 1] = "Switch";
                Tools[Tools["Info"] = 2] = "Info";
                Tools[Tools["FullScreen"] = 3] = "FullScreen";
            })(Tools || (Tools = {}));
            Fract3D = class Fract3D extends FractBase_2.FractBase {
                constructor(fract2D) {
                    super(fract2D);
                    this.fract2D = fract2D;
                    this._allDivs = ['Doc'];
                    this.threeViewport =
                        DomHelper_8.DomHelper.div(undefined, {
                            height: window.innerHeight + 'px',
                            width: '100%', opacity: 0
                        });
                    this.keys = new Fract3DKeys_1.Fract3DKeys(true, (name, shift) => this.tool(name, shift), this.log, this.isDev);
                    this.mouseEvents = new MouseEvents_2.MouseEvents(this.threeViewport, (x, y) => this.onClick(x, y), (x, y) => this.onDblClick(x, y), (x, y, e) => { this.onMove(x, y, e); });
                }
                _attach() {
                    if (this.threeViewer) {
                        this.threeViewer.attach();
                        const c = this.threeViewport.querySelector('canvas');
                        this.threeViewport.style.height = c.style.height = c.height + 'px';
                    }
                    else
                        this.threeViewer = new ThreeViewer_1.ThreeViewer(this.threeViewport, scene => this.scene = scene, undefined, { statElement: DomHelper_8.DomHelper.div(undefined, DomHelper_8.DomHelper.topLeft()) });
                    this.keys.attach();
                    this.mouseEvents.attach();
                }
                _attached() {
                    this.threeViewer.attached();
                    this.updateFrom2D();
                }
                _detach() {
                    this.mouseEvents.detach();
                    this.keys.detach();
                    this.threeViewer.detach();
                }
                _detached() {
                    this.threeViewer.detached();
                    this.threeViewport.style.height = '0';
                }
                _loggers() {
                    return [this.keys];
                }
                _moreElements() {
                    return [this.threeViewport]
                        .concat(this.threeViewer.doms);
                }
                _toolname(tool) { return Tools[tool]; }
                _getInfo() {
                    let s = '';
                    if (this.dataObj instanceof HMapMeshData_2.HMapMeshData)
                        s = 'vertices:' + this.dataObj.vertices.length / 3
                            + ' triangles:' + this.dataObj.faces.reduce((p, c) => p + c.length / 3, 0)
                            + ' ';
                    else if (this.dataObj instanceof HeightMap_4.HeightMap) {
                        console.log('points', this.params.points);
                        const w = this.dataObj.dataWidth, h = this.dataObj.data.length / w;
                        const lw = Math.round(w / this.params.lod), lh = Math.round(h / this.params.lod);
                        if (this.params.points)
                            s = 'points:' + lw * lh;
                        else
                            s = 'vertices:' + (lw + 1) * (lh + 1) + ' triangles:' + lw * lh * 2;
                        s += ' ';
                    }
                    return s + this.params.toString();
                }
                drawPath(x, y) {
                    if (!this._pathData) {
                        this._pathData = new Array();
                    }
                    //const path = this.psr.computePathFromView(x, y, this.state.m, this.state.r, this._pathData)
                }
                updateFrom2D() {
                    if (!this.params)
                        this.params = new RenderParams3D_2.RenderParams3D({ camera: { position: { y: 700 } } });
                    if (this.log)
                        console.log('updateFrom2D');
                    const par = this.fract2D.getParams(), info = { min: 0 };
                    this.hmap = this.fract2D.getHeightMap(false, info);
                    const h = Math.min(par.m * (1 - info.min), 1000);
                    //console.log('info', info)
                    this.setup(this.hmap, {
                        colors: par.color,
                        invert: par.invert,
                        easing: par.easing,
                        maxHeight: h,
                        camera: { target: { y: h / 2 } }
                        //camera: { position: { x: -28, y: 106, z: -211 }, target: { x: -28, y: -13, z: -251 } }
                        //camera: { position: { x: -51, y: 18, z: -228 }, target: { x: -51, y: -8, z: -223 } }
                    }, true);
                }
                tool(name, shift) {
                    switch (name) {
                        case 'stats':
                            this.threeViewer.nextStat();
                            return;
                        case 'grid':
                        case 'axis':
                        case 'crosshair':
                        case 'lights':
                        case 'transparent':
                        case 'fnh':
                        case 'wireframe':
                        case 'negateY':
                        case 'colors':
                        case 'invert':
                        case 'mesh.optimize':
                        case 'mesh.zones':
                        case 'mesh.noiseZones':
                        case 'mesh.seams':
                        case 'points':
                            this.toggleParam(name);
                            return;
                        case 'easing':
                            this.nextParam(name, shift, Easing_6.Easing.nextEaser);
                            return;
                        case 'lod':
                            this.nextParam(name, !shift, this.nextLod);
                            return;
                        case 'colorMode':
                            this.nextColorMode();
                            return;
                        case 'faceTracking':
                            let tracking, tracked = false;
                            if (shift) {
                                tracked = this.toggleParam('trackedFace', null, true);
                                if (tracked)
                                    tracking = this.toggleParam('faceTracking', true, true);
                            }
                            else
                                tracking = this.toggleParam(name, null, true);
                            this.setupFaceTracking(tracking, tracked);
                            this.updateInfo();
                            return;
                    }
                    if (super.basetool(name, shift))
                        return;
                    const v = this.tools.get(name);
                    switch (name) {
                        case Tools[Tools.FullScreen]:
                            this.setTool(Tools.FullScreen, this.threeViewer.toggleFullScreen());
                            return;
                        default:
                            break;
                    }
                }
                nextParam(name, reverse = false, getValue, max = Infinity) {
                    if (!getValue)
                        getValue = v => (v += (reverse ? -1 : 1)) > (max || Infinity) ? 0 : v < 0 ? (max || 0) : v;
                    const v = this.getParam(name);
                    this.setParam(name, getValue(v, reverse));
                    this.setup(this.dataObj, this.params, name == 'lod');
                }
                nextColorMode() {
                    let vc = this.getParam('vertexColors'), fc = this.getParam('faceColors');
                    if (!vc && !fc)
                        vc = !(fc = false);
                    else if (vc)
                        vc = !(fc = true);
                    else
                        vc = fc = false;
                    this.setParam('vertexColors', vc);
                    this.setParam('faceColors', fc);
                    //this.setMatProp('vertexColors', this.params.getVertexColors())
                    this.setup(this.dataObj, this.params);
                }
                toggleParam(name, force = undefined, noSetup = false) {
                    const v = force != undefined ? force : !this.getParam(name);
                    this.setParam(name, v);
                    switch (name) {
                        case 'grid':
                        case 'axis':
                        case 'crosshair':
                        case 'lights':
                            this.threeViewer.toggleHelper(name, v);
                            return;
                        case 'transparent':
                            this.setMatProp(name, v);
                            return;
                        case 'wireframe':
                            if (!this.getParam('points')) {
                                this.setMatProp(name, v);
                                return;
                            }
                            this.setParam('points', false);
                            break;
                        case 'negateY':
                            const { target: { y } } = this.threeViewer.getCamera();
                            this.threeViewer.setCamera(undefined, { y: -y });
                            break;
                    }
                    if (!noSetup)
                        this.setup(this.dataObj, this.params);
                    return this.getParam(name);
                }
                getParam(name) {
                    if (name.indexOf('.') > -1) {
                        let res, o = this.params;
                        name.split('.').forEach((n, i, a) => {
                            if (!o)
                                return;
                            if (i == a.length - 1)
                                res = o[n];
                            else
                                o = o[n];
                        });
                        return res;
                    }
                    else
                        return this.params[name];
                }
                setParam(name, value) {
                    if (name.indexOf('.') > -1) {
                        let o = this.params;
                        name.split('.').forEach((n, i, a) => {
                            if (!o)
                                return;
                            if (i == a.length - 1)
                                o[n] = value;
                            else
                                o = o[n];
                        });
                    }
                    else
                        this.params[name] = value;
                    if (this.log)
                        console.log(name + ':', value);
                    return value;
                }
                setMatProp(name, value, traverse = true) {
                    function setProp(o) { if (o.material)
                        o.material[name] = value; }
                    if (traverse)
                        this.object.traverse(setProp);
                    else
                        setProp(this.object);
                    this.threeViewer.requestRender();
                    this.updateInfo();
                }
                nextLod(lod, reverse) {
                    let lods = [1, 2, 4, 8, 16, 32];
                    if (reverse)
                        lods = lods.reverse();
                    return lods.find(l => reverse ? l < lod : l > lod) || lods[0];
                }
                setup(dataObject, params, isMeshDirty = false) {
                    if (!dataObject)
                        return;
                    if (this.log)
                        if (this.debug)
                            console.log('----------\nsetup', dataObject, params);
                        else
                            console.log('----------\nsetup');
                    this.dataObj = dataObject;
                    //console.log(params, this.params)
                    this.params = params = new RenderParams3D_2.RenderParams3D(params);
                    const start = performance.now();
                    let obj3d;
                    if (this.dataObj instanceof HTMLCanvasElement)
                        obj3d = ThreeUtil_1.ThreeUtil.makeTerrainFromCanvas(this.dataObj);
                    else if (params.points)
                        obj3d = MeshBuilder_1.MeshBuilder.makePoints(this.dataObj = this.hmap, params, this.log, this.debug);
                    else {
                        const p = this.params, pm = p.mesh;
                        if (pm && pm.optimize) {
                            let meshHeight = p.negateY ? -p.maxHeight : p.maxHeight;
                            let heightFactor = meshHeight; //h => meshHeight * (h == 0 ? 0 : (1 / h))
                            if (isMeshDirty || !(this.dataObj instanceof HMapMeshData_2.HMapMeshData))
                                this.dataObj = HMapMeshData_2.HMapMeshData.fromHeightMap(this.hmap, heightFactor, p.lod, p.minZoneArea, this.log, pm.zones, pm.noiseZones, pm.seams, true);
                            else
                                this.dataObj
                                    .update(heightFactor, pm.zones, pm.noiseZones, pm.seams);
                        }
                        else
                            this.dataObj = this.hmap;
                        obj3d = MeshBuilder_1.MeshBuilder.makeMesh(this.dataObj, p, this.log, this.debug);
                    }
                    if (this.log)
                        console.log('object built');
                    if (this.debug)
                        console.log(this.dataObj);
                    if (obj3d) {
                        if (this.object) {
                            this.scene.remove(this.object);
                            this.threeViewer.disposeObj(this.object);
                        }
                        if (params.fnh && !params.points)
                            obj3d.add(new THREE_9.VertexNormalsHelper(obj3d, 5, 0XFFFF00));
                        this.scene.add(this.object = obj3d);
                    }
                    if (params.grid != undefined)
                        this.threeViewer.toggleHelper('grid', params.grid);
                    if (params.background)
                        this.threeViewer.setBackground(params.background);
                    if (params.camera)
                        this.threeViewer.setCamera(params.camera.position, params.camera.target);
                    params.camera = undefined;
                    if (this.log)
                        console.log((performance.now() - start).toFixed(3) + 'ms\n----------');
                    this.updateInfo();
                }
                onClick(x, y) {
                }
                onDblClick(x, y) {
                    if (this.faceHelper)
                        this.threeViewer.setCamera(undefined, this.faceHelper.centroid);
                }
                onMove(x, y, e) {
                    if (this.params.faceTracking)
                        this.trackFace(e);
                }
                trackFace(e) {
                    if (!this.faceHelper)
                        return;
                    if (this.dataObj instanceof HMapMeshData_2.HMapMeshData) {
                        const hmd = this.dataObj;
                        const update = (obj) => {
                            const face = this.threeViewer.getPointedFace(e, obj);
                            if (!face)
                                return false;
                            this.faceHelper.update(face);
                            this.threeViewer.requestRender();
                            return true;
                        };
                        if (hmd.merged)
                            update(this.object);
                        else
                            hmd.names.some(n => update(this.object.getObjectByName(n)));
                    }
                }
                setupFaceTracking(tracking = false, tracked = false) {
                    if (!this.faceHelper) {
                        if (this.dataObj instanceof HMapMeshData_2.HMapMeshData)
                            this.faceHelper = new FaceHelper_1.FaceHelper(this.dataObj.vertices);
                    }
                    else if (this.faceHelper.hasObjects) {
                        if (tracked) {
                            this.scene.add(this.faceHelper.mesh);
                            this.scene.add(this.faceHelper.centroHelper);
                        }
                        else {
                            this.scene.remove(this.faceHelper.mesh);
                            this.scene.remove(this.faceHelper.centroHelper);
                        }
                        this.threeViewer.requestRender();
                    }
                }
            };
            exports_49("Fract3D", Fract3D);
        }
    };
});
System.register("client/Fract", ["client/fract2D/Fract2D", "client/fract3D/Fract3D"], function (exports_50, context_50) {
    "use strict";
    var __moduleName = context_50 && context_50.id;
    var Fract2D_1, Fract3D_1;
    return {
        setters: [
            function (Fract2D_1_1) {
                Fract2D_1 = Fract2D_1_1;
            },
            function (Fract3D_1_1) {
                Fract3D_1 = Fract3D_1_1;
            }
        ],
        execute: function () {
            (() => {
                console.log('starting');
                const f2 = new Fract2D_1.Fract2D(null, () => new Fract3D_1.Fract3D(f2));
                console.log('started');
            })();
        }
    };
});
