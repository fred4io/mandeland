import { HMapMeshData } from './../shared/HMapMeshData';
import { TriResult } from '../shared/Triangulation'
import { DomHelper } from '../shared/DomHelper'
import { HeightMap, IHeightMapProducer } from '../shared/HeightMap'
import { HeightMapZones, HMapZone, HMZPointType } from '../shared/HeightMapZones'
import { IUserEvents } from '../shared/UserEvents'
import { Color } from '../shared/Color'
import { Pixels } from '../shared/Pixels'
import { Region2D } from '../shared/Region2D'
import { ContourMethod, Zoning } from '../shared/Zoning'
import { NumberArrays } from '../shared/NumberArrays';

declare type IGet2DContext = {
    getContext(): CanvasRenderingContext2D
}
export function testZones(ctx: CanvasRenderingContext2D, events: IUserEvents, fract2D: IHeightMapProducer & IGet2DContext) {

    const c = new Color()

    let hmap: HeightMap,
        hmzd: HeightMapZones,
        ox: number,
        oy: number,
        drawMode = 0,
        lod = 1,
        fill = true,
        singleZone = true,
        contour = true,
        contourMethod = 1,
        removeColinears = false,
        triangulate = false,
        boundingBox = false,
        steiners = false,
        steinersDistanceFactor = 50,
        steinersEdgeDistance = 5,
        steinersClipping = true,
        allLevels = false,
        contourMinArea = 10,
        logWarnings = false,
        seams = false,
        smallZones = false,
        noiseZones = false,
        checkDuplicates = false,
        checkSeamsNeighbors = false,
        neighborZones = false,
        zoneEdges = false
        //holesContours = false

    function getData() {
        fract2D.getHeightMapAsync()
            .then(hm => {
                hmap = hm
                ctx = fract2D.getContext()
                getZones()
            })
    }

    function getZones() {
        hmzd = HeightMapZones.compute(hmap, lod, contourMinArea, true, true,
            contour && contourMethod || 0, 
            removeColinears, 
            neighborZones, 
            zoneEdges)
        draw()
    }

    function nextLod(reverse: boolean) {
        lod *= (reverse ? .5 : 2); if (lod > 32) lod = 1; else if (lod < 1) lod = 32
        getZones()
    }

    function triangulateZone(zone: HMapZone) {
        if (!zone) return
        const tr = hmzd.triangulate(zone,
            steiners && hmzd.width / steinersDistanceFactor,
            steinersEdgeDistance,
            steinersClipping,
            100, logWarnings)
        if (!tr) return
        const tris = tr.getTrianglesXys(), tl = tris && Math.max(1, tris.length - 1)
        if (tris)
            tris.forEach((t, ti) => Pixels.drawPoly(ctx, t, c.gradient7(ti / tl), fill, true))
        if (tr.badPoints)
            tr.badPoints.map(i => hmzd.getPoint(i)).forEach(p => {
                if (lod == 1) Pixels.drawCross(ctx, p.x, p.y, 5, Color.Red)
                else Pixels.drawRect(ctx, p.x * lod, p.y * lod, p.x * lod + lod, p.y * lod + lod, Color.Red, true)
            })
        return tr
    }

    function drawBoundingBox(zone: HMapZone) {
        Pixels.drawBox(ctx,
            Region2D.fromRMinMax(hmzd.computeBoundingBox(zone)).scale(lod),
            Color.Red)
    }

    function drawSteiners(zoneSteiners: number[], color = Color.LBlue) {
        if (!zoneSteiners) return
        zoneSteiners.map(i => hmzd.getPoint(i))
            .forEach(p => Pixels.drawCross(ctx, p.x * lod, p.y * lod, 2, color))
    }

    function getSteiners(zone: HMapZone) {
        return hmzd.getSteinerPoints(zone,
            hmzd.width / steinersDistanceFactor,
            steinersEdgeDistance,
            steinersClipping)
    }
    function drawSeamsNeighbors(z: HMapZone) {
        Pixels.withImageData(ctx, imd => {
            const contour = new Array<number>()
            Zoning.removeColinears(z.contour, hmzd.width, true, contour)
            if (!contour || contour.length < 3) return
            const il = contour.length - 1
            const counts = HMapMeshData.getSeamsNeighborCounts(contour,
                hmzd.pointLevelIndex, z.levelIndex,
                hmzd.pointTypeIndex, HMZPointType.Contour, hmzd.width)
            if (!counts) return
            console.log(z.levelIndex, NumberArrays.getDistinctValues(counts, true))
            hmzd.draw(contour, imd, t => c.gradient7(counts[~~(t * il)] / 8), true)
        })
    }

    function drawNeighborZones(z: HMapZone) {
        Pixels.withImageData(ctx, imd => {
            hmzd.drawZone(z, imd, undefined, Color.Green, true);
            const n = z.neighborZones.size
            z.neighborZones.forEach((nzi, i) => {
                const t = (n == 1 ? 0 : (i / (n - 1)))
                hmzd.drawZone(hmzd.zones[nzi], imd, c.gradient7(t),
                    z.interiorZones && z.interiorZones.has(nzi) ? Color.Red : Color.Blue, true)
            })
        })
    }
    function drawZoneEdges(z: HMapZone) {
        const c = Color.pink().setAlpha01(.5)
        Pixels.withImageData(ctx, imd => {
            hmzd.draw(z.edges, imd, c, true, true)
        })
    }

    class LInfo {
        constructor(
            public points = 0,
            public contour = 0,
            public zones = 0,
            public noContourZones = 0,
            public noContourPoints = 0
        ) { }

        add(linfo: LInfo) {
            this.points += linfo.points
            this.contour += linfo.contour
            this.zones += linfo.zones
            this.noContourZones += linfo.noContourZones
            this.noContourPoints += linfo.noContourPoints
        }
    }
    function draw(x?: number, y?: number) {
        if (x == undefined) x = ox; else ox = x
        if (y == undefined) y = oy; else oy = y
        const lx = Math.round(x / lod), ly = Math.round(y / lod)

        Pixels.clear(ctx, Color.LGray)

        const
            sli = hmzd.getLevelIndex(lx, ly),
            sZone = hmzd.getZone(lx, ly),
            levels = allLevels ? hmzd.getZonesByLevel() : [hmzd.getLevelZones(sli)],
            tzl = Math.max(1, hmzd.zonesCount - 1),
            colorize = Color.getColorizer(c, true)

        let slInfo: LInfo, alInfo = allLevels && new LInfo(),
            szn: number, zoneTri: TriResult, nbSeams = 0, zn = 0

        Pixels.withImageData(ctx, imd => {
            if (noiseZones) {
                const nzl = Math.max(1, hmzd.noiseZones.length - 1)
                hmzd.noiseZones.forEach((nz, nzi) =>
                    hmzd.drawZone(nz, imd, c.gradient7(nzi / nzl), contour && Color.MYellow, fill))
            }
            levels.forEach((lzs, li) => {
                if (!lzs) return
                const isSelectedLevel = !allLevels || li == sli
                const nclzs = lzs.filter(z => !z.contour.length),
                    nclpn = nclzs.reduce((r, z) => r + z.points.length, 0),
                    linfo = new LInfo(0, 0, lzs.length, nclzs.length, nclpn)
                if (isSelectedLevel) slInfo = linfo
                const lcc = isSelectedLevel ? Color.MGreen : Color.Green
                if (drawMode == 0) {
                    const lcp = isSelectedLevel ? Color.Gray : Color.LGray
                    hmzd.drawLevel(lzs, imd, lcp, !contour ? undefined : t => c.gradient7(t), linfo, fill, false, true)
                    if (smallZones) nclzs.forEach(z => hmzd.drawZone(z, imd, Color.Magenta, null, fill))
                } else {
                    const zl = Math.max(1, lzs.length - 1)
                    lzs.forEach((z, iz) => {
                        linfo.points += z.points.length
                        linfo.contour += z.contour.length
                        if (z == sZone) { szn = zn; return }
                        if (smallZones && !z.contour.length) c.copy(Color.Magenta)
                        else if (drawMode == 1) c.gradient7(iz / zl)
                        else if (drawMode == 2) c.copy(iz % 2 == 0 ? Color.Orange : Color.Blue)
                        else if (drawMode == 3) c.gradient7(zn / tzl)
                        const cp = drawMode == 4 ? colorize : c
                        hmzd.drawZone(z, imd, cp, contour && lcc, fill)
                        zn++
                    })
                }
                if (alInfo && linfo) alInfo.add(linfo)
            })
            if (noiseZones)
                hmzd.drawTris(
                    HMapMeshData.computeNoiseTris(hmzd), imd,
                    c.copy(Color.LBlue).setAlpha01(.3), true)
            if (seams) {
                const tris = HMapMeshData.computeSeamsTris(hmzd)
                nbSeams = tris.length / 3
                hmzd.drawTris(tris, imd, t => c.gradient7(t))
            }
        })

        //if (boundingBox || triangulate || steiners)
        levels.forEach(lzs => {
            if (lzs) lzs.forEach(z => {
                if (singleZone && z != sZone) return
                if (boundingBox) drawBoundingBox(z)
                let zt = triangulate && triangulateZone(z)
                if (steiners) drawSteiners(zt && zt.steinerPoints || getSteiners(z), z == sZone ? Color.LBlue : Color.Blue)
                if (z == sZone) zoneTri = zt
            })
        })
        if (singleZone) {
            if (checkSeamsNeighbors) drawSeamsNeighbors(sZone)
            if (neighborZones) drawNeighborZones(sZone)
            if (zoneEdges) drawZoneEdges(sZone)
        }

        if (checkDuplicates) {
            const sumIndex = new Uint32Array(hmzd.width * hmzd.height)
            const zs = noiseZones ? hmzd.noiseZones : hmzd.zones
            zs.forEach(z => z.points.forEach(i => sumIndex[i]++))
            const chm = HeightMap.fromData(hmzd.width, sumIndex)
            Pixels.drawHeightMap(ctx, chm.data, chm.dataWidth,
                false, false, null, null, 1, .5)
        }

        DomHelper.crud('test', true,
            DomHelper.topLeft({
                padding: '3px',
                color: 'white',
                'font-size': '14px',
                'background-color': 'rgba(0,0,0,.5)'
            }),
            div => div.innerHTML = ''
                + '(D) drawMode: ' + drawMode
                + '<br/>(Z) singleZone: ' + singleZone
                + '<br/>(A) all levels: ' + allLevels
                + '<br/>(L) lod: ' + lod
                + '<br/>(F) fill: ' + fill
                + '<br/>(C) contour: ' + contour
                + (!contour ? '' : (''
                    + '<br/>(N) contouring: ' + ContourMethod[contourMethod]
                    + '<br/>(R) nocolinears: ' + removeColinears
                ))
                + '<br/>(B) boundingBox: ' + boundingBox
                + '<br/>(S) steiners: ' + steiners
                + (!steiners ? '' : ('<br/>(P) clip steiners: ' + steinersClipping))
                + '<br/>(G) triangulate: ' + triangulate
                + '<br/>(M) seams: ' + seams
                + '<br/>(E) seams neighbors: ' + checkSeamsNeighbors
                + '<br/>(0) small zones: ' + smallZones
                + '<br/>(I) noise zones: ' + noiseZones
                + '<br/>(H) neighbor zones: ' + neighborZones
                + '<br/>(K) check duplicates: ' + checkDuplicates
                + '<br/>(J) zone edges: ' + zoneEdges
                //+ '<br/>(U) zone holes: ' + holesContours

                + '<br/>'
                + '<br/>contour min area: ' + contourMinArea
                + (!steiners ? '' : (''
                    + '<br/>steinersGridDistance ' + Math.round(hmzd.width / steinersDistanceFactor)
                    + '<br/>steinersEdgeDistance ' + steinersEdgeDistance
                ))

                + '<br/><br/>image:'
                + '<br/>' + hmzd.zonesCount + ' zones'
                + (!alInfo ? '' : (''
                    + '<br/>' + hmzd.levelZoneIndex.length + ' levels'
                    //+ '<br/>' + alInfo.zones + ' zones'
                    + '<br/>' + alInfo.points + ' points'
                    + '<br/>' + alInfo.contour + ' contour points'
                    + '<br/>' + alInfo.noContourZones + ' small zones'
                    + '<br/>' + alInfo.noContourPoints + ' smallzone points'
                    + '<br/>' + (alInfo.zones - alInfo.noContourZones) + ' contour zones'
                ))
                + '<br/>' + (hmzd.noiseZones && hmzd.noiseZones.reduce((p, c) => p + c.points.length, 0) || 0) + ' noise points'
                + '<br/>' + (hmzd.noiseZones && hmzd.noiseZones.length || 0) + ' noise zones'
                + (!nbSeams ? '' : ('<br/>' + nbSeams + ' seams'))

                + (!slInfo ? '' : (''
                    + '<br/><br/>level:'
                    + '<br/>' + sli + '/' + (hmzd.levelZoneIndex.length - 1)
                    + '<br/>' + slInfo.zones + ' zones'
                    + '<br/>' + slInfo.points + ' points'
                    + '<br/>' + slInfo.contour + ' contour points'
                    + '<br/>' + slInfo.noContourZones + ' small zones'
                    + '<br/>' + slInfo.noContourPoints + ' smallzone points'
                ))

                + (!sZone ? '' : (''
                    + '<br/><br/>zone:'
                    + (szn == undefined ? '' : ('<br/>' + szn + '/' + tzl))
                    + '<br/>' + sZone.zoneIndex + '/' + (hmzd.zonesCount - 1)
                    + '<br/>' + sZone.levelZoneIndex + '/' + (hmzd.levelZoneIndex[sZone.levelIndex].length - 1) + ' (level)'
                    + '<br/>' + sZone.points.length + ' points'
                    + (!sZone.edges ? '' : ('<br/>' + sZone.edges.length + ' edge points'))
                    + (!sZone.contour ? '' : ('<br/>' + sZone.contour.length + ' contour points'))
                    + (!sZone.neighborZones ? '' : ('<br/>' + sZone.neighborZones.size + ' neighbors'
                        + (!sZone.interiorZones ? '' : ('(' + sZone.interiorZones.size + ' interiors)'))))
                    //+ (!sZone.holesContours ? '' : ('<br/>' + sZone.holesContours.length + ' holes'))
                    + (!zoneTri ? '' : (''
                        + (!zoneTri.steinerPoints ? '' : ('<br/>' + zoneTri.steinerPoints.length + ' steiners points'))
                        + (!zoneTri.nbTriangles ? '' : ('<br/>' + zoneTri.nbTriangles + ' triangles'))
                        + (!zoneTri.badPoints ? '' : ('<br/>' + zoneTri.badPoints.length + ' bad points'))
                    ))
                ))
        )
    }

    events.onDetach = () => DomHelper.crud('test', false)
    events.onSelect = () => setTimeout(getData, 200)
    events.onMove = (x, y) => draw(x, y)
    events.onKey = k => {
        switch (k) {
            case 'meta-R': return
            case 'D': if (++drawMode > 4) drawMode = 0; break
            case 'L': nextLod(false); return
            case 'shift-L': nextLod(true); return
            case 'F': fill = !fill; break
            case 'G': triangulate = !triangulate; break
            case 'Z': singleZone = !singleZone; break
            case 'C': contour = !contour; getZones(); return
            case 'N': if (++contourMethod > 2) contourMethod = 1; getZones(); return
            case 'R': removeColinears = !removeColinears; getZones(); return
            case 'B': boundingBox = !boundingBox; break
            case 'S': steiners = !steiners; break
            case 'A': allLevels = !allLevels; break
            case 'P': steinersClipping = !steinersClipping; break
            case 'M': seams = !seams; break
            case 'O': smallZones = !smallZones; break
            case 'I': noiseZones = !noiseZones; break
            case 'K': checkDuplicates = !checkDuplicates; break
            case 'E': checkSeamsNeighbors = !checkSeamsNeighbors; break
            case 'H': neighborZones = !neighborZones; getZones(); return
            case 'J': zoneEdges = !zoneEdges; getZones(); return
            //case 'U': holesContours = !holesContours; getZones(); return

        }
        draw()
    }

    getData()
}