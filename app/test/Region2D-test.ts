import { Region2D } from "../shared/Region2D";
import { Color } from "../shared/Color";
import { Pixels } from "../shared/Pixels";
import { IUserEvents } from "../shared/UserEvents";
import { Easing } from "../shared/Easing";
import { DomHelper } from "../shared/DomHelper";

export function testSfMany(ctx?: CanvasRenderingContext2D) {
    const n = 1000000;
    for (let i = 0; i < n; ++i) {
        testSf(ctx);
    }
    console.log('testSfMany Done ' + n);
}

export function testSf(ctx: CanvasRenderingContext2D, events?: IUserEvents) {
    let fitOut = true
    function draw() {
        Pixels.clear(ctx)
        const rnd = (n?: number) => Math.random() * (n || 1);
        const w = ctx ? ctx.canvas.width : 1000, h = ctx ? ctx.canvas.height : 1000;
        const rs = Region2D.fromXYWH(rnd(w / 2), rnd(w / 2), rnd(w / 2), rnd(w / 2)),
            rd = Region2D.fromXYWH(rnd(h / 2), rnd(h / 2), rnd(h / 2), rnd(h / 2)),
            rr = rs.clone()
        rr.fitTo(rd, fitOut)

        const p = 7, b = rs.ratio.toPrecision(p) != rr.ratio.toPrecision(p);
        if (ctx) {
            Region2D.draw(rs, ctx, Color.White);
            Region2D.draw(rd, ctx, Color.Blue);
            Region2D.draw(rr, ctx, b ? Color.Red : Color.Green);
            console.log('testSf', rs.ratio, rr.ratio);
        }
        else if (b) { console.log('testSf', rs.ratio.toPrecision(p), rr.ratio.toPrecision(p)); }
    }

    events.onKey = k => { if (k == 'shift') fitOut = !fitOut; draw() }
    draw();
}


export function testGetPartsTo(ctx: CanvasRenderingContext2D, events: IUserEvents) {

    const c = new Color().setAlpha01(.5),
        red = Color.red().setAlpha01(.5),
        green = Color.green().setAlpha01(.5),
        bgColor = ctx.canvas.style.backgroundColor

    ctx.canvas.style.backgroundColor = 'lightgray'

    let rs: Region2D, rd: Region2D

    function reset() {
        const rnd = (n?: number) => Math.random() * (n || 1),
            w = ctx ? ctx.canvas.width : 1000, 
            h = ctx ? ctx.canvas.height : 1000,
            ld = 4, ls = 3

        rs = Region2D.fromXYWH(rnd(w / 2), rnd(h / 2), rnd(w / ls), rnd(h / ls)).round(),
            rd = Region2D.fromCenterAndSize(w / 2, h / 2, w / ld, h / ld).round()
    }
    reset()

    let preferColumns = false;

    const draw = (x?: number, y?: number) => {

        if (x && y) {
            const r = rs, tw = r.width, th = r.height
            r.xmin = x
            r.ymin = y
            r.xmax = x + tw
            r.ymax = y + th
        }

        Pixels.clear(ctx, Color.Gray)

        const rrs = rs.getPartsTo(rd, preferColumns)

        console.log(rrs.length)
        rrs.forEach((r, i) => r.draw(ctx, c.gradient7(i / rrs.length), true))
        Region2D.draw(rs, ctx, Color.Black, false)
        Region2D.draw(rd, ctx, Color.White, false)
    }

    events.onDetach = () => { Pixels.clear(ctx); ctx.canvas.style.backgroundColor = bgColor }
    events.onMove = (x, y) => draw(x, y)
    events.onKey = (k) => {
        if (k == 'shift') { preferColumns = !preferColumns; console.log(preferColumns) }
        else { console.log('reset', k); reset() }
    }
    draw()
}

export function testGradient(ctx: CanvasRenderingContext2D, events: IUserEvents) {

    const c = new Color()
    let ease = 0, easing = (v: number) => v

    const draw = (x?: number, y?: number) => {

        Pixels.withImageData(ctx, imd => {
            const d = imd.data, h = imd.height, w = imd.width
            for (let y = 0; y < h; ++y) {
                let yw4 = y * w * 4
                for (let x = 0; x < w; ++x) {
                    c.gradient7(easing(x / w))
                    let i = yw4 + x * 4
                    d[i] = c.r
                    d[i + 1] = c.g
                    d[i + 2] = c.b
                    d[i + 3] = c.a
                }
            }
        })

        DomHelper.crud('test', true,
            DomHelper.topLeft({ color: 'white', 'font-size': '16px' }),
            div => div.innerText = Easing.funcName(ease))
    }

    events.onDetach = () => DomHelper.crud('test', false)
    events.onKey = (k) => {
        easing = Easing.func(ease = ++ease > Easing.maxFuncIndex ? 0 : ease)
        draw()
    }
    draw()
}