import { ParamSet } from "../shared/ParamSet";


enum EP {
    v1 = 0,
    v2,
    v3,
    v4,
    v5
}

class C1 {

    notDefined: number

    num1 = 0

    bool1 = false

    @ParamSet.boolean()
    bool2 = true

    @ParamSet.number({ max: 3, isCyclic: true, isInteger: true, excludeZero: true })
    cyclicNum = 1

    @ParamSet.enum(EP)
    ep = EP.v2

    @ParamSet.object()
    obj = new C2()

    @ParamSet.skip()
    leaveMeAlone = 2

    @ParamSet.custom((v: number) => Math.floor(Math.random() * 10))
    random = 0
}
class C2 {
    subString: string

    @ParamSet.custom((v: number) => Math.floor(Math.random() * 1000))
    subNum = 0
}


export class ParamSetTest {
    static test() {

        const c = new C1()
        const greedy = true, logOnChange = true, logWarnings = true
        const ps = ParamSet.generate(c, 'test', greedy, logOnChange, logWarnings)

        ps.attach('ep',
            (v: number, pl: any) => console.log('action', v, EP[v], pl))

        console.log(ps)
        for (let i = 0; i < 5; i++) {
            ps.next('ep', true, false, i, i % 2 == 0)
            ps.next('obj.subNum')
        }

    }
}