import { Vector2D } from "./lib/Vector2D";
import { Circle } from "../shared/Circle";
import { Color } from "../shared/Color";
import { Pixels } from "../shared/Pixels";
import { Cardioide } from "../shared/Cardioide";
import { Region2D } from "../shared/Region2D";
import { UserEvents } from "../shared/UserEvents";

export function setupTestInCardioide(ctx: CanvasRenderingContext2D, events: UserEvents) {

    let car: Cardioide, cir: Circle, box: Region2D

    function refresh() {
        car = Cardioide.randomIn(ctx.canvas)
        cir = Circle.fromCxyr(car.getCircleIn())
        box = Region2D.fromRMinMax(car.getBox())
        clear()
    }

    function clear() {
        Pixels.clear(ctx)
        car.draw(ctx, 100, Color.Gray);
        cir.draw(ctx, Color.MGreen);
        box.draw(ctx, Color.MRed);
    }

    refresh()

    events.onKey = _ => refresh()
    events.onMove = (x, y) => {
        clear();
        const p = { x, y };
        let color = (
            car.containsPoint(p) ?
                cir.containsPoint(p) ?
                    Color.MGreen
                    : Color.LBlue
                : box.containsPoint(p) ?
                    Color.MidYellow
                    : Color.MRed
        )

        Pixels.drawCross(ctx, p.x, p.y, 10, color);

        Pixels.drawLine(ctx, car.x, car.y, p.x, p.y, Color.White);

        let angle = new Vector2D(1, 0).angleWith(new Vector2D(p.x - car.x, p.y - car.y))
        if (car.r < 0) { angle += Math.PI; }
        if (p.y < car.y) { angle = - angle }

        const q = car.getPointAtAngle(angle);
        Pixels.drawCross(ctx, q.x, q.y, 5, Color.White);
    }


}