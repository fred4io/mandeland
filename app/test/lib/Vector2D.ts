export class Vector2D {

    static fromPoint(p: { x: number, y: number }) {
        return new Vector2D(p.x, p.y);
    }
    static random(xmax: number, ymax: number) {
        return new Vector2D(Math.random() * xmax, Math.random() * ymax);
    }
    static randomIn(r: { width: number, height: number }) {
        return Vector2D.random(r.width, r.height);
    }

    constructor(
        public x = 0,
        public y = 0
    ) { }

    clone() {
        return new Vector2D(this.x, this.y);
    }

    length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    dot(v: Vector2D) {
        return this.x * v.x + this.y * v.y;
    }

    angleWith(v: Vector2D) {
        return Math.acos(this.dot(v) / (this.length() * v.length()));
    }
}