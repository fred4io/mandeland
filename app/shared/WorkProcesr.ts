import { WorkerLoader } from './WorkerLoader';
import { WorkerProxy } from './WorkerProxy';

declare type TPartInput = any
declare type TPartOutput = any
declare type TPartResult = { progress: number, data?: TPartOutput }
declare type TComputeFunction = (data: TPartInput, onProgress: (result: TPartResult) => boolean) => void

export interface IWorkPart { data: TPartInput, weight?: number, id?: number | string }

class WorkPart {
    public result: TPartResult
    public started: number
    public completed: number
    public canceled: boolean
    constructor(
        public data: TPartInput,
        public weight: number,
        public id: number | string,
    ) { }
}

export class WorkProcesr {

    static readonly maxWorkers = navigator.hardwareConcurrency || 4
    private blobUrl: string
    private workers: WorkerProxy[]
    private cancelRequired: boolean
    private canceled: boolean
    private started: number
    private workParts: WorkPart[]
    private globalProgress: number
    private _busy = false
    private useWorkers: boolean
    private onComplete: (result: TPartResult[], canceled: boolean) => void
    private onProgressCancel: (progress: number) => boolean

    get busy() { return this._busy }

    get log() { return this._log }
    set log(value: boolean) { this._log = value; this.workers.forEach(w => w.log = value) }

    get maxWorkers() { return this.workers.length }
    set maxWorkers(value: number) { this.initWorkers(value) }

    constructor(
        private computeFunction: TComputeFunction,
        dependencies: Array<any>,
        computeClassName: string,
        maxWorkers = WorkProcesr.maxWorkers,
        private _log = false,
        private verbose = false,
        private progressStep = .01,
        private workerIdPrefix = 'worker'
    ) {
        this.blobUrl = WorkerLoader.buildBlobUrl(computeFunction, dependencies, computeClassName, _log, verbose)
        this.initWorkers(maxWorkers)
    }
    private debug = false

    protected say(...args: any[]) {
        if (this.log) { console.log(this.constructor.name, ...args) }
    }
    protected dbg(...args: any[]) {
        if (this.log && this.debug) { console.log(this.constructor.name, ...args) }
    }
    protected sayMs(start: number, ...args: any[]) {
        if (this.log) { this.say(...args, (performance.now() - start).toFixed(3) + 'ms') }
    }

    private initWorkers(maxWorkers: number) {
        this.dbg('initWorkers', maxWorkers)
        if (this._busy) { this.say('busy'); return false }
        maxWorkers = Math.max(0, maxWorkers)
        this.workers = new Array<WorkerProxy>(maxWorkers)
        for (let i = 0; i < maxWorkers; i++) {
            this.workers[i] = new WorkerProxy(
                this.blobUrl,
                w => this.oneCompleted(w.extData as WorkPart, w.canceled),
                w => this.oneProgressed(w.result, w.extData as WorkPart),
                `${this.workerIdPrefix} ${i + 1}/${maxWorkers}`,
                this._log
            )
        }
    }

    dispose() {
        this.say('disposing')
        this.workers.forEach(w => w.terminate())
    }

    cancel() {
        if (!this._busy || this.cancelRequired) return
        this.say('canceling')
        this.cancelAll()
    }

    startOne(workInput: TPartInput, useWorker: boolean,
        onComplete: (result: TPartResult, canceled: boolean) => void,
        onProgressCancel?: (p: number) => boolean,
        id?: string | number
    ) {
        return this.start([{ data: workInput, id: id }], useWorker,
            (rs, c) => onComplete(rs && rs[0], c),
            onProgressCancel
        )
    }

    start(workInput: IWorkPart[], useWorkers: boolean,
        onComplete: (result: TPartResult[], canceled: boolean) => void,
        onProgressCancel?: (p: number) => boolean
    ) {
        if (this._busy) { this.say('busy'); return false }

        this._busy = true
        this.canceled = false
        this.cancelRequired = false
        this.useWorkers = useWorkers
        this.onComplete = onComplete
        this.onProgressCancel = onProgressCancel
        this.globalProgress = 0
        this.started = performance.now()

        const pl = workInput.length
        this.workParts = new Array<WorkPart>(pl)
        workInput.forEach((p, pi) => {
            this.workParts[pi] = new WorkPart(p.data,
                p.weight || (1 / pl),
                p.id != undefined ? p.id : `${pi + 1}/${pl}`
            )
        })

        const started = useWorkers ? this.startWorkers() : this.startNoWorker()
        if (!started) {
            if (this.debug || this._log && this.verbose) console.warn('nothing to start', this.workParts, useWorkers)
            this._busy = false
        }

        return started
    }

    private startWorkers(parts = this.workParts, isFirst = true) {
        const workers = this.workers.filter(p => !p.busy)
        const l = Math.min(parts && parts.length || 0, workers.length)
        if (l < 1) { return false }
        if (isFirst) { this.say('starting', l) }
        for (let i = 0; i < l; i++) {
            const wp = parts[i]
            workers[i].start(wp.data, wp.id, wp)
            wp.started = performance.now()
        }
        return true
    }

    private startNoWorker(isFirst = true) {
        const part = this.workParts.find(p => !p.started)
        if (!part) { return false }
        setTimeout(() => {
            if (isFirst) { this.say('starting (no worker)', this.workParts.length) }
            this.say('compute part', part.id)
            part.started = performance.now()
            try {
                const progressed = (wr: TPartResult) => {
                    if (wr && wr.data != undefined) {
                        part.result = wr
                        this.oneCompleted(part, false)
                    } else {
                        return this.oneProgressed(wr, part)
                    }
                }
                this.computeFunction(part.data, progressed)
            } catch (e) {
                if (this._log || this.debug) console.error(e, part)
                if (isFirst) { this.oneCompleted(part, true) }
            }
        }, 0)
        return true
    }

    private oneProgressed(wr: TPartResult, wp: WorkPart) {
        this.dbg('oneProgressed', wr && wr.progress, wp)
        wp.result = wr

        let cancel = this.cancelRequired
        if (!cancel && this.onProgressCancel && wr && wr.progress) {
            const defaultWeight = 1 / this.workParts.length
            const getProgress = (wr: TPartResult, wp: WorkPart) => wr && wr.progress && wr.progress * (wp.weight || defaultWeight) || 0
            const ngp = this.workParts.reduce((p, c) => p + getProgress(c.result, c), 0)
            const ogp = this.globalProgress
            this.globalProgress = ngp

            if (ngp - ogp > this.progressStep) {
                cancel = this.onProgressCancel(ngp)
            }
        }
        if (cancel) { this.cancelAll() }
        return cancel
    }
    private oneCompleted(part: WorkPart, canceled: boolean) {
        this.dbg('oneCompleted', part, canceled)
        if (!part.completed) { part.completed = performance.now() }
        if (canceled) {
            part.canceled = true
            this.cancelAll()
        } else if (this.workParts.every(p => p.result && p.result.data != undefined)) {
            this.allCompleted(false)
        } else if (this.useWorkers) {
            this.startWorkers(this.workParts.filter(p => !p.started && !p.canceled), false)
        } else {
            this.startNoWorker(false)
        }
    }

    private allCompleted(canceled: boolean) {
        this.sayMs(this.started, 'computed')
        this._busy = false
        const results = canceled ? null : this.workParts.map(p => p.result)
        this.dbg('allCompleted', canceled, results)
        this.onComplete(results, canceled)
    }

    private cancelAll() {
        if (this.canceled) return
        this.canceled = true
        this.workers.forEach(w => { if (!w.canceled) { w.cancel() } })
        const now = performance.now()
        this.workParts.forEach(p => { if (!p.completed) { p.canceled = true; p.completed = now } })
        this.allCompleted(true)
    }
}

