import { FractInput, FractWorker } from "./FractWorker";
import { Transform2D } from "../Transform2D";
import { HeightMap } from "../HeightMap";
import { DomHelper } from "../DomHelper";
import { Region2D } from "../Region2D";
import { Matrix2D } from "../Matrix2D";
import { Pixels } from "../Pixels";
import { Color } from "../Color";

declare type Size = { width: number, height: number }

export class FractMiniMap {

    private _minimapData: ImageData
    private _mmwv: Matrix2D
    private debug = false

    constructor(public log = false) { }

    protected say(...args: any[]) {
        if(this.log) { console.log(this.constructor.name, ...args) }
    }
    protected dbg(...args: any[]) {
        if(this.log && this.debug) { console.log(this.constructor.name, ...args) }
    }
    protected sayMs(start: number, ...args: any[]) {
        if(this.log) { this.say(...args, (performance.now() - start).toFixed(3) + 'ms') }
    }

    updateMiniMap(doShowMap: boolean, resized: boolean, vp: Size, tfm: Transform2D, zoom: number) {
        if (!doShowMap || resized) { this._minimapData = this._mmwv = undefined; }
        DomHelper.crud('MiniMap', doShowMap, DomHelper.topLeft('all'), (div, isNew) => {
            const start = performance.now(),
                c = isNew ? div.appendChild(document.createElement('canvas')) : div.querySelector('canvas')
            let mmd = this._minimapData; if (!mmd) { mmd = this._minimapData = this.makeMiniMapData(vp) }
            if (isNew || resized) { DomHelper.setupCanvas(c, mmd.width, mmd.height) }
            const ctx = c.getContext('2d'); ctx.putImageData(this._minimapData, 0, 0)
            if (zoom > 1) { 
                const rv = this._mmwv.transformRegion(tfm.regionToTarget(Region2D.fromSize(vp)))
                Pixels.drawCrossBox(ctx, rv.xmin, rv.xmax, rv.ymin, rv.ymax, 15, Color.Red)
            }
            this.sayMs(start)
        })
    }

    private makeMiniMapData(vp: Size, maxWidth = 200, maxHeight = 150) {
        const start = performance.now(),
            vm = Region2D.fromSize(vp).maxTo(maxWidth, maxHeight).round(),
            l = 1.2, x = -.5, rw = new Region2D(x - l, x + l, -l, l, true),
            ro = vm.clone().fitTo(rw, true),
            fi = FractInput.fromRegion(ro, vm.width, vm.height),
            fo = FractWorker.compute(fi),
            hmap = HeightMap.fromData(vm, fo.data, fo.min, fo.max),
            imd = Pixels.imageFromHeightMap(hmap.data, vm.width, false, true)
        this.sayMs(start, imd.width + 'x' + imd.height)
        this._mmwv = this.makeMiniMapMatrix(vm, ro)
        return imd
    }

    private makeMiniMapMatrix(rv: Region2D, rt: Region2D) {
        return Matrix2D.transformLR(
            Matrix2D.translation(-rv.cx, -rv.cy),
            Matrix2D.flipY(),
            Matrix2D.scale(rv.scaleFactorToFit(rt, true)),
            Matrix2D.translation(rt.cx, rt.cy),
        ).getInverse()
    }
}