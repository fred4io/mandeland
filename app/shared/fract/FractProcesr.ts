import { Region2D } from "../Region2D"
import { HeightMap } from "../HeightMap"
import { WorkProcesr, IWorkPart } from "../WorkProcesr"
import { FractInput, FractOutput, FractWorker } from "./FractWorker"
import { Transform2D } from "../Transform2D"
import { FractPar } from "./FractPar"

class ComputeData {
    constructor(
        public target: Region2D,
        public width: number,
        public height: number,
        public max = 50,
        public optimize = true,
        public radius = 2,
        public mask = false,
    ) { }
}

export class ComputedPoint {
    constructor(
        public value: number,
        public max: number,
        public nooptim: boolean,
        public optimized: boolean,
        public elapsedMs: number
    ) { }
}

export class FractProcesr {

    private _busy: boolean
    private wpsr: WorkProcesr
    private _log: boolean
    private debug = false

    public get log() { return this._log }
    public set log(value: boolean) {
        this._log = value
        this.wpsr.log = value
    }

    constructor(
        public tfm: Transform2D,
        log = false
    ) {
        this._log = log
        this.initGpuCompute()
        this.wpsr = new WorkProcesr(
                FractWorker.compute,
                [FractInput, FractOutput, FractWorker],
                FractWorker.name,
                WorkProcesr.maxWorkers - 1,
                this.log)
    }

    protected say(...args: any[]) {
        if (this.log) { console.log(this.constructor.name, ...args) }
    }
    protected dbg(...args: any[]) {
        if (this.log && this.debug) {
            args.forEach((arg, i) => { if (typeof arg == 'function') { args[i] = arg() } })
            console.log(this.constructor.name, ...args)
        }
    }
    protected sayMs(start: number, ...args: any[]) {
        if (this.log) { this.say(...args, (performance.now() - start).toFixed(3) + 'ms') }
    }

    computePointFromView(x: number, y: number, max: number, radius = 2, mask = false, optimize = true) {
        const a = this.tfm.xToTarget(x, y), b = this.tfm.yToTarget(x, y),
            fo = FractWorker.computePoint(a, b, max, radius, optimize, mask)
        return new ComputedPoint(fo.data[0], fo.max, !fo.optimized, fo.optims > 0, fo.elapsedMs)
    }

    computePathFromView(x: number, y: number, max = 50, radius = 2, dataHolder?: Array<number>) {
        const a = this.tfm.xToTarget(x, y), b = this.tfm.yToTarget(x, y),
            xys = FractWorker.computePath(a, b, max, radius, dataHolder)
        return this.tfm.xysToView(xys)
    }

    private cancelRequired = false
    cancel() {
        this.say('canceling')
        this.cancelRequired = true
        this.wpsr.cancel()
    }

    computeHeightMap(
        vp: Region2D,
        par: FractPar,
        onComplete: (hmap: HeightMap, canceled: boolean) => void,
        prevHmap?: HeightMap,
        prevPar?: FractPar,
        useWorker = false,
        optimize = true,
        mask = false,
        onProgressCancel?: (p: number) => boolean,
        subdivide = false,
        useGpu = false,
        useGpuDynamicOutput = false,
    ) {
        if (this._busy) { this.say('busy!'); return false }
        this._busy = true
        this.say('processing')

        const start = performance.now()

        const processed = (hmap: HeightMap, canceled: boolean) => {
            if (canceled || this.cancelRequired) { this.say('canceled') }
            else { this.sayMs(start, 'processed') }
            onComplete(hmap, canceled || this.cancelRequired)
            this._busy = false
        }
        const progressed = (p: number) => this.cancelRequired || onProgressCancel(p)

        let processing = false

        const target = this.tfm.toTarget(vp.clone(), true),
            data = new ComputeData(target, vp.width, vp.height, par.m, optimize, par.r, mask)

        const prevTarget =
            prevHmap && prevHmap.data
            && prevPar && par.isSameMR(prevPar) && !par.isSameABZ(prevPar)
            && this.tfm.toOtherTarget(vp.clone(), prevPar.a, prevPar.b, prevPar.z, true)

        const useGpuWithDynOut = useGpu && useGpuDynamicOutput
        const canUseParts = !useGpu || useGpuWithDynOut
        if (canUseParts && prevTarget && !prevTarget.surroundsR(target)) {
            this.say('using partial')
            const combine = (newParts: HeightMap[], commonPart: Region2D, canceled: boolean) => {
                if (!canceled) this.combine(prevHmap, newParts, commonPart)
                processed(prevHmap, canceled)
            }
            processing = this.computePartial(prevTarget, data, combine, progressed, useWorker, useGpuWithDynOut)
            if (processing) { this.say('processing partial') }
        } else if (canUseParts && subdivide) {
            this.say('using subdivided')
            const combine = (newParts: HeightMap[], canceled: boolean) => {
                const hmap = !canceled && HeightMap.fromRegion(vp)
                if (!canceled) this.combine(hmap, newParts)
                processed(hmap, canceled)
            }
            processing = this.computeSubdivided(data, combine, progressed, subdivide, useWorker, useGpuWithDynOut)
            if (processing) { this.say('processing subdivided') }
        } else {
            this.say('using whole')
            const adapt = (hmap: HeightMap, canceled: boolean) => {
                if (!canceled) this.tfm.regionToView(hmap.region).flipY().round()
                processed(hmap, canceled)
            }
            processing = this.computeWhole(data, adapt, progressed, useWorker, useGpu, !useGpuDynamicOutput)
            if (processing) { this.say('processing whole') }
        }
        if (!processing) { this.say('compute busy') }

        this._busy = processing
        return processing
    }

    private buildInput(cd: ComputeData, progress: boolean, tr?: Region2D, dw = cd.width, dh = cd.height) {
        const ps = progress ? cd.max > 10000 ? 100 : cd.max > 5000 ? 50 : cd.max > 2000 ? 20 : 10 : 0
        const fi = FractInput.fromRegion(tr || cd.target, dw, dh, cd.max, cd.radius, cd.optimize, ps, cd.mask)
        this.dbg('buildInput', fi, dw, dh, cd, tr, dw, dh)
        return fi
    }

    private combine(result: HeightMap, newParts: HeightMap[], commonPart?: Region2D) {
        const isDezoom = commonPart && newParts.length == 4, start = performance.now()
        this.say('combine', newParts.length, !!commonPart, !!isDezoom)
        newParts.forEach((part, i) => {
            //const before = part.region.clone()
            this.tfm.regionToView(part.region).flipY().round()
            if (part.region.width != part.dataWidth) {
                console.warn('Data size mismatch before combine for part ' + i
                    + '. Region width is ' + part.region.width + ' but data width is ' + part.dataWidth
                    + '; Result width is ' + result.dataWidth + '.'
                    //+ '\nbefore tranform: ', before, 'after tranform:', part.region, result
                )
            }
        })
        if (commonPart) { this.tfm.regionToView(commonPart).flipY().round() }
        result.combine(newParts, commonPart, this.log)
        this.sayMs(start, 'combined')
    }

    private computePartial(
        fromTarget: Region2D,
        data: ComputeData,
        onComplete: (newParts: HeightMap[], commonPart: Region2D, canceled: boolean) => void,
        onProgressCancel?: (p: number) => boolean,
        useWorker?: boolean,
        useGpu = false
    ) {
        const trs = fromTarget.getPartsTo(data.target)
        this.say('computePartial', trs.length)

        let commonPart: Region2D, ta = data.target.area
        if (trs.length > 1) {
            commonPart = trs.shift()
            ta -= commonPart.area
        }

        const kw = data.width / data.target.width,
            kh = data.height / data.target.height

        const completed = (fos: FractOutput[], canceled: boolean) => {
            const newParts = this.getHeightMaps(fos, trs, i => Math.round(trs[i].width * kw))
            this.say('computePartial-completed', fos, newParts)
            onComplete(newParts, commonPart, canceled)
        }
        const wps = trs.map((tr, i) => {
            const dw = Math.round(tr.width * kw)
            const dh = Math.round(tr.height * kh)
            return {
                data: this.buildInput(data, !!onProgressCancel, tr, dw, dh),
                weight: tr.area / ta,
                id: 'part ' + (i + 1) + '/' + trs.length
            } as IWorkPart
        })

        return this.computeParts(wps, useGpu, useWorker, completed, onProgressCancel)
    }

    private computeSubdivided(
        data: ComputeData,
        onComplete: (hmaps: HeightMap[], canceled: boolean) => void,
        onProgressCancel?: (p: number) => boolean,
        subdivide = false,
        useWorker = false,
        useGpu = false
    ) {
        const subdivisions = subdivide ? this.wpsr.maxWorkers : 1
        const dg = Region2D.fromSize(data).getGrid(subdivisions, 1, true, this.debug)
        const tg = data.target.getGridLike(dg, this.debug)
        this.dbg('computeSubdivided', useWorker ? 'using worker' : useGpu ? 'using gpu' : '', data, subdivisions, () => Region2D.gridToString(dg, 3), () => Region2D.gridToString(tg, 3))

        const drs = dg.parts, trs = tg.parts, ta = data.target.area
        const wps = trs.map((tr, i) => {
            const dr = drs[i]
            const part = {
                data: this.buildInput(data, !!onProgressCancel, tr, dr.width, dr.height),
                weight: tr.area / ta,
                id: 'part ' + (i + 1) + '/' + trs.length
            } as IWorkPart
            //this.dbg('computeSubdivided-part', part, dr)
            return part
        })

        const completed = (fos: FractOutput[], canceled: boolean) => {
            const newParts = this.getHeightMaps(fos, trs, i => drs[i].width)
            this.say('computeSubdivided-completed', fos, newParts)
            onComplete(newParts, canceled)
        }

        return this.computeParts(wps, useGpu, useWorker, completed, onProgressCancel)
    }

    private computeWhole(
        data: ComputeData,
        onComplete: (hmap: HeightMap, canceled: boolean) => void,
        onProgressCancel?: (p: number) => boolean,
        useWorker = false,
        useGpu = false,
        useGpuFixedOutputSize = false
    ) {
        this.say('computeWhole',
            useWorker ? 'using worker'
            : useGpu ? ('using gpu with ' + (useGpuFixedOutputSize ? 'static' : 'dynamic') + ' output')
            : '')
        const fi = this.buildInput(data, !!onProgressCancel)
        const completed = (fo: FractOutput, canceled: boolean) => {
            this.dbg('completed', fi, fo)
            if (fo && this.log) this.say('optim ' + (fo.optimized ? ((100 * fo.optims / fo.data.length).toFixed(0) + '%') : false))
            const hmap = !canceled && fo && HeightMap.fromData(data.target, fo.data, 0, fo.max, data.width)
            onComplete(hmap, canceled)
        }
        if(useGpu) {
            this.initGpuCompute(useGpuFixedOutputSize ? fi : undefined)
            this.gpuCompute(fi, this.debug).then(fo => completed(fo, false))
            return true
        } else {
            return this.wpsr.startOne(fi, useWorker, completed, onProgressCancel)
        }
    }

    private getHeightMaps(fos: FractOutput[], trs: Region2D[], getDataWidth: (i: number) => number) {
        if(!fos) { return }
        const max = Math.max(...fos.map(fo => fo.max))
        const newParts = fos.map((fo, i) => HeightMap.fromData(trs[i], fo.data, 0, max, getDataWidth(i)))
        return newParts
    }

    private computeParts(wps: IWorkPart[], useGpu: boolean, useWorker: boolean,
        completed: (fos: FractOutput[], canceled: boolean) => void,
        onProgressCancel?: (p: number) => boolean
    ) {
        if(useGpu) {
            this.initGpuCompute()
            Promise.all(wps.map(wp => this.gpuCompute(wp.data as FractInput, this.debug)))
                .then(fos => completed(fos, false))
            return true
        } else {
            return this.wpsr.start(wps, useWorker, completed, onProgressCancel)
        }
    }

    private gpuCompute: (fi: FractInput, log?: boolean) => Promise<FractOutput>
    private gpuFixedOutput?: { width: number, height: number }

    private initGpuCompute(fixedOutputSize?: { width: number, height: number }) {
        const s1 = fixedOutputSize, s2 = this.gpuFixedOutput
        if(this.gpuCompute && (!s1 && !s2 || s1 && s2 && s1.width == s2.width && s1.height == s2.height )) { return }
        this.gpuCompute = FractWorker.buildComputeWithGpu(fixedOutputSize, this.log)
    }
}
