import { Easing, Easer } from "../Easing";
import { FractPar, FractLoc, Par, Loc } from "./FractPar";

export declare type RenderPar = {
    color?: boolean, invert?: boolean, easing?: number,
    mask?: boolean,
    optimize?: boolean
}

export class FractRenderPar extends FractPar {

    private static _bools = ['invert', 'color', 'optimize', 'mask']
    private static _nums = ['easing']

    private static _imagers = ['invert', 'color', 'easing']
    private static _computers = ['optimize', 'mask']

    private static allRProps() { return this._bools.concat(this._nums) }
    private static allProps(filter?: (p: string) => boolean) {
        const props = this.allRProps().concat('a', 'b', 'z', 'm', 'r')
        return filter ? props.filter(filter) : props
    }

    static isComputer(name: string) { return this._computers.some(c => c == name) }
    static isImager(name: string) { return this._imagers.some(c => c == name) }

    private static copyProps<T1, T2>(tgt: T1, src: T2, names: string[]) {
        if (!src || !tgt) { return tgt }
        names.forEach(pn => { const v = (<any>src)[pn]; if (v != undefined) { (<any>tgt)[pn] = v } })
        return tgt
    }

    static loc(n: number) {
        let rp: RenderPar
        const colorInv: RenderPar = { invert: true, color: true },
            grayInv: RenderPar = { invert: true, color: false }

        switch (n) {
            //zmax? 24947790800071

            //Region2D.zeroCentered(4, 2.25),
            //new Region2D(-2.1, .6, -1.2, 1.2, true),
            //l = 1.2, x = -.5, rw = new Region2D(x - l, x + l, -l, l, true),

            case 1: return FractRenderPar.from(new FractPar(-.75, 0, 1.75, 50, 2.1915), { invert: false, color: false })
            case 2: return FractRenderPar.from(new FractPar(-0.19616829451540196, 0.4127473077886301, 3.993000000000001, 50), grayInv)
            case 3: return FractRenderPar.from(new FractPar(0.336558573388203, 0.047351543209876584, 47174.924165824064, 666))
            case 4: return FractRenderPar.from(new FractPar(-1.2388170062348742, 0.09860571193872883, 3274.8273685494632, 312), grayInv)
            case 5: return FractRenderPar.from(new FractPar(-1.235306354684753, 0.09734096086361498, 8711.36344343207, 624), colorInv)
            case 6: return FractRenderPar.from(new FractPar(-0.23006461896505995, 0.755071578437906, 25055.581752642876, 6400), colorInv)
            case 7: return FractRenderPar.from(new FractPar(-1.0076379502907715, 0.32123299594109134, 1145001726661.055, 2520), colorInv)
            case 8: return FractRenderPar.from(new FractPar(-1.6749999284744286, 0, 33554432, 400))
            case 9: return FractRenderPar.from(new FractPar(0.33679259259259253, -0.05131851851851849, 254.9858223062382, 226))
            case 10: return FractRenderPar.from(new FractPar(-0.7449300342878324, 0.11786674080252069, 527.3664710198092, 1352))
            case 11: return FractRenderPar.from(new FractPar(0.13543610074626849515, 0.63736007462686583569, 2144.0, 2294))
            case 12: return FractRenderPar.from(new FractPar(-0.23006461896505994713, 0.75507157843790595475, 25055.6, 1600), colorInv)
            case 13: return FractRenderPar.from(new FractPar(-0.19654267896927768433, 0.65101409324616743568, 451.7, 271), { easing: Easer.InCubic });
            case 14: return FractRenderPar.from(<FractRenderPar>{ a: -1.4153616375100906, b: 0, z: 18.98003571428572, m: 200, invert: true, color: true })
            case 15: return FractRenderPar.from(<FractRenderPar>{ a: 0.13530936722694523, b: 0.637802768154106, z: 319335.46175115206, m: 2294, invert: false, color: true, easing: Easer.OutQuadratic })
            case 16: return FractRenderPar.from(<FractRenderPar>{ a: -1.2329096703980087, b: 0.09688811800373118, z: 15832.615384615385, m: 535, invert: false, color: true })
            case 17: return FractRenderPar.from(<FractRenderPar>{ a: -0.1876314942699442, b: -0.6660138362911809, z: 567.0315789473684, m: 114, invert: false, color: true, easing: Easer.OutQuadratic })
            case 18: return FractRenderPar.from(<FractRenderPar>{ a: -0.18814477216649397, b: -0.6665049598853583, z: 3279.8085573646345, m: 228, invert: false, color: true, easing: Easer.OutQuadratic })
            case 19: return FractRenderPar.from(<FractRenderPar>{ a: -0.188018476643721, b: -0.6663422069213536, z: 4824627678263.674, m: 912, invert: false, color: true, easing: Easer.OutQuadratic })
            case 20: return FractRenderPar.from(<FractRenderPar>{ a: 0.3046954988934953, b: 0.030101333015795297, z: 101525.31108654167, m: 1332, invert: true, color: true })
            case 21: return FractRenderPar.from(<FractRenderPar>{ a: 0.3047108697039768, b: 0.030109264665048836, z: 453877.86132806865, m: 2664, invert: true, color: true })
            case 22: return FractRenderPar.from(<FractRenderPar>{ a: 0.3047064632321694, b: 0.03011190854813334, z: 453877.86132806865, m: 5328, invert: true, color: true, easing: Easer.OutQuadratic })
            case 23: return FractRenderPar.from(<FractRenderPar>{ a: -0.10877497353977791, b: 0.8990408851353562, z: 96.46954900000007, m: 154, invert: true, color: true, easing: Easer.InBounce })
            case 24: return FractRenderPar.from(<FractRenderPar>{ a: -0.676783883766736, b: 0.3090823719480107, z: 4607302.936125294, m: 711, invert: true, color: true })
            case 25: return FractRenderPar.from(<FractRenderPar>{ a: 0.23364057564812218, b: 0.5501889297385614, z: 377.85310734463275, m: 76, invert: true, color: true })
            default: return FractRenderPar.from(new FractPar())
        }
    }

    static from(p: FractLoc | FractPar | FractRenderPar | string, rp?: RenderPar) {
        if (typeof p == 'string') { p = JSON.parse(p) as FractRenderPar }
        const o = new FractRenderPar(); if (p) { o.copy(p) } if (rp) { o.copyRPar(rp) } return o
    }

    getComputeProfile() {
        const r = this.clone();
        FractRenderPar.allProps(p => !FractRenderPar.isComputer(p)).forEach(p => (<any>r)[p] = undefined)
        return r
    }

    constructor(
        public invert = false,
        public color = true,
        public easing = 0,
        public mask = false,
        a?: number, b?: number, z?: number, m?: number, r?: number
    ) {
        super(a, b, z, m, r)
    }

    copyRPar(o: RenderPar) {
        return FractRenderPar.copyProps(this, o, FractRenderPar.allRProps())
    }
    copy(o: FractRenderPar | Par | Loc) {
        FractRenderPar.copyProps(this, o, FractRenderPar.allProps())
        return this
    }
    clone() {
        const o = new FractRenderPar().copy(this); return o
    }
    toString() {
        return super.toString() + '\n'
            + FractRenderPar._bools.reduce((p, c) => p + ' ' + ((<any>this)[c] ? '' : '!') + c, '')
            + FractRenderPar._nums.reduce((p, c) => p + ' ' + c + ':' + this.valToString(c), '')
    }
    setPar(a?: number, b?: number, z?: number, m?: number, r?: number) {
        return super.set(a, b, z, m, r)
    }
    isSamePar(o: Par) { return super.equals(o) }

    valToString(name: string): any {
        const v = (<any>this)[name]
        if (name == 'easing') { const fn = Easing.funcName(<number>v); return fn.startsWith('ease') ? fn.substr(4) : fn }
        return v
    }

}