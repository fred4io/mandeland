import { GPU, IGPUKernelSettings, IKernelRunShortcut } from "gpu.js"
import { NumberArrays } from "../NumberArrays"

declare type Region = { xmin: number, xmax: number, ymin: number, ymax: number }

export class FractInput {

    static fromRegion(r: Region, width: number, height: number,
        max = 50, radius = 2, optimize = true, progressSteps = 10, mask = false
    ) {
        return new FractInput(r.xmin, r.xmax, r.ymin, r.ymax, width, height, max, radius, optimize, progressSteps, mask)
    }

    static forPoint(
        a = 0, b = 0, max = 50, radius = 2, optimize = true, mask = false
    ) {
        return new FractInput(a, a, b, b, 1, 1, max, radius, optimize, 0, mask)
    }

    constructor(
        public amin: number,
        public amax: number,
        public bmin: number,
        public bmax: number,
        public width: number,
        public height: number,
        public max: number,
        public radius: number,
        public optimize = true,
        public progressSteps = 10,
        public mask = false
    ) { }

    setForPoint(a: number, b: number, max: number,
        radius = 2, optimize = true, mask = false
    ) {
        this.amin = a, this.amax = a
        this.bmin = b, this.bmax = b
        this.width = 1, this.height = 1
        this.max = max
        this.radius = radius
        this.optimize = optimize
        this.progressSteps = 0
        this.mask = mask
        return this
    }
}

export class FractOutput {
    constructor(
        public progress: number,
        public data?: Uint16Array,
        public min?: number,
        public max?: number,
        public optimized?: boolean,
        public optims?: number,
        public elapsedMs?: number
    ) { }
}

export class FractWorker {

    private static _fi = FractInput.forPoint()

    public static computePoint(a: number, b: number, m: number, r = 2, optimize = true, mask = false) {
        return FractWorker.generate(FractWorker._fi.setForPoint(a, b, m, r, optimize, mask)).next().value
    }

    public static compute(fi: FractInput, onProgress?: (fo: FractOutput) => boolean, dataHolder?: Uint16Array) {
        let lwr: FractOutput
        const progress = (r: FractOutput) => {
            lwr = r
            if (r.data) { onProgress?.(r); return }
            else if (r.progress == 0 || r.progress == 1) return false
            else return onProgress?.(r) === true
        }
        if (onProgress && fi.progressSteps) { FractWorker.computeWithEvents(fi, progress, dataHolder) }
        else { FractWorker.computeWithoutEvents(fi, progress, dataHolder) }
        return lwr
    }

    private static computeWithoutEvents(fi: FractInput, onProgress: (fo: FractOutput) => boolean, dataHolder?: Uint16Array) {
        let canceled: boolean
        for (let wr of FractWorker.generate(fi, dataHolder)) {
            canceled = onProgress?.(wr) === true
            if (canceled) { break }
        }
    }
    private static computeWithEvents(fi: FractInput, onProgress: (fo: FractOutput) => boolean, dataHolder?: Uint16Array) {
        const iwr = FractWorker.generate(fi, dataHolder)
        const next = () => {
            const ir = iwr.next()
            if (ir.done) { return }
            const canceled = onProgress?.(ir.value) === true
            if (!canceled) { setTimeout(next) }
        }
        next()
    }

    private static * generate(fi: FractInput, dataHolder?: Uint16Array) {
        const start = performance.now(),
            w = Math.trunc(fi.width),
            h = Math.trunc(fi.height),
            data = dataHolder || new Uint16Array(w * h),
            amin = fi.amin, amax = fi.amax, da = (amax - amin) / w,
            bmin = fi.bmin, bmax = fi.bmax, db = (bmax - bmin) / h,
            max = Math.round(Math.abs(fi.max)), nmax = max - 1,
            ps = fi.progressSteps > 0 && Math.trunc(h / fi.progressSteps),
            r2 = fi.radius * fi.radius,
            PI = Math.PI,
            fo = new FractOutput(0)
        let min = Infinity
        const mask = fi.mask, omax = mask ? max + 1 : max
        let optimize = !!fi.optimize, optims = 0, testInC1: boolean, testInC2: boolean, testInCa: boolean
        let c1x: number, c1y: number, c1r2: number, c1x1: number, c1x2: number, c1y1: number, c1y2: number
        let c2x: number, c2y: number, c2r2: number, c2x1: number, c2x2: number, c2y1: number, c2y2: number
        let cax1: number, cay1: number, cax2: number, cay2: number, b2: number
        let dx: number, dy: number, c1dy2: number, c2dy2: number

        if (optimize) {
            let y = 0, r = .25, x = -1
            c1x = x, c1y = y, c1r2 = r * r
            c1x1 = x - r, c1x2 = x + r, c1y1 = y - r, c1y2 = y + r

            r = .5, x = -.25
            c2x = x, c2y = y, c2r2 = r * r
            c2x1 = x - r, c2x2 = x + r, c2y1 = y - r, c2y2 = y + r

            cax1 = -.75, cax2 = .375
            const t = Math.tan(Math.PI / 6), d = 1 + t * t
            cay1 = -2 * t / (d * d), cay2 = -cay1

            if (bmax < cay1 || cay2 < bmin
                || amax < c1x1 || cax2 < amin
                || amax < cax1 && (bmax < c1y1 || c1y2 < bmin)
            ) { optimize = false }
        }

        for (let y = 0; y < h; y++) {
            const b = bmin + y * db
            const yw = y * w

            if (optimize) {
                if (cay1 <= b && b <= cay2) {
                    testInCa = true
                    b2 = b * b
                    if (c2y1 <= b && b <= c2y2) {
                        testInC2 = true
                        dy = b - c2y
                        c2dy2 = dy * dy

                        if (c1y1 <= b && b <= c1y2) {
                            testInC1 = true
                            dy = b - c1y
                            c1dy2 = dy * dy
                        } else { testInC1 = false }

                    } else { testInC1 = testInC2 = false }
                } else { testInCa = false }
            }

            for (let x = 0; x < w; x++) {
                const a = amin + x * da

                if (optimize) {
                    if (testInC1 && c1x1 <= a && a <= c1x2) {
                        dx = a - c1x
                        if (dx * dx + c1dy2 <= c1r2) {
                            data[yw + x] = omax
                            optims++
                            continue
                        }
                    }
                    if (testInCa && cax1 <= a && a <= cax2) {
                        if (testInC2 && c2x1 <= a && a <= c2x2) {
                            dx = a - c2x
                            if (dx * dx + c2dy2 <= c2r2) {
                                data[yw + x] = omax
                                optims++
                                continue
                            }
                        }
                        const vx = a - .25, lcp = Math.sqrt(vx * vx + b2)
                        let phi = Math.acos(vx / lcp); phi = (b < 0 ? -phi : phi) + PI
                        const t = Math.tan(phi / 2), t2 = t * t, d = 1 + t2, d2 = d * d
                        const ux = ((1 - t2) / d2), uy = (2 * t) / d2
                        if (lcp * lcp < ux * ux + uy * uy) {
                            data[yw + x] = omax
                            optims++
                            continue
                        }
                    }
                }

                let n = 0, za = a, zb = b, za2 = za * za, zb2 = zb * zb
                while (za2 + zb2 <= r2 && n++ < nmax) {
                    const za0 = za
                    za = za2 - zb2 + a
                    zb = 2 * za0 * zb + b
                    za2 = za * za
                    zb2 = zb * zb
                }
                data[yw + x] = n
                if (n < min) min = n
            }
            if (ps && y % ps == 0) { fo.progress = y / h; yield fo }
        }
        yield new FractOutput(1, data, min, omax, optimize, optims, performance.now() - start)
        return null as FractOutput
    }

    static computePath(a: number, b: number, max: number, r: number = 2, result: Array<number>) {
        max = Math.ceil(max)
        const xys = result || new Array<number>(2 * max), r2 = r * r
        let i = 0, n = 0, za = a, zb = b, za2 = za * za, zb2 = zb * zb
        while (za2 + zb2 <= r2 && n++ < max) {
            const za0 = za
            za = za2 - zb2 + a
            zb = 2 * za0 * zb + b
            za2 = za * za, zb2 = zb * zb
            xys[i] = za
            xys[i + 1] = zb
            i += 2
        }
        xys.length = i > max ? i - 2 : i
        return xys
    }

    static buildComputeWithGpu(size?: { width: number, height: number }, logTime = false) {
        logTime && console.time('buildComputeWithGpu')

        function computePointInGpu(w: number, amin: number, bmin: number, da: number, db: number, nmax: number, r2: number) {
            const i = this.thread.x
            const x = i % w
            const y = ~~(i / w)
            const a = amin + x * da
            const b = bmin + y * db
            let n = 0, za = a, za2 = za * za, zb = b, zb2 = zb * zb
            while (za2 + zb2 <= r2 && n++ < nmax) {
                const za0 = za
                za = za2 - zb2 + a
                zb = 2 * za0 * zb + b
                za2 = za * za
                zb2 = zb * zb
            }
            return n
        }

        const kernel = new GPU().createKernel(computePointInGpu)
        let W: number, H: number
        if(size) {
            W = Math.trunc(size.width)
            H = Math.trunc(size.height)
            kernel.setOutput([W * H])
        } else { kernel.setDynamicOutput(true) }
        const exec = kernel.exec as unknown as (...args: any[]) => Promise<Float32Array>

        const computeWithGpu = async (fi: FractInput, log = false) => {
            const
                w = size ? W : Math.trunc(fi.width),
                h = size ? H : Math.trunc(fi.height),
                da = (fi.amax - fi.amin) / w,
                db = (fi.bmax - fi.bmin) / h,
                nmax = fi.max - 1,
                r2 = fi.radius * fi.radius,
                start = performance.now()
            log && console.log('computeWithGpu-before', !!size, fi, w, fi.amin, fi.bmin, da, db, nmax, r2)
            !size && kernel.setOutput([w * h])
            const gpuOutput = await exec(w, fi.amin, fi.bmin, da, db, nmax, r2)
            log && console.log('computeWithGpu-after', gpuOutput)
            const data = Uint16Array.from(gpuOutput), { min, max } = NumberArrays.getMinMax(data)
            return new FractOutput(1, data, min, max, false, 0, performance.now() - start)
        }

        logTime && console.timeEnd('buildComputeWithGpu')
        return computeWithGpu
    }
}

