export declare type Loc = { a?: number, b?: number, z?: number }
export declare type Par = Loc & { m?: number, r?: number }

export class FractLoc {

    static fromLoc(p: Loc) { return new FractLoc(p.a, p.b, p.z) }

    constructor(
        public a = 0,
        public b = 0,
        public z = 1
    ) { }

    copy(l: Loc) { return this.set(l.a, l.b, l.z) }
    clone() { return new FractLoc().copy(this); }
    toString() { return `a=${this.a.toFixed(20)} b=${this.b.toFixed(20)} z=${this.z.toFixed(1)}` }

    set(a?: number, b?: number, z?: number) {
        if (a != undefined) this.a = a
        if (b != undefined) this.b = b
        if (z != undefined) this.z = z
        return this
    }

    equals(l: Loc) {
        return l && l.a == this.a && l.b == this.b && l.z == this.z
    }
}

export class FractPar extends FractLoc {

    static fromPar(l: FractLoc | FractPar, p?: Par) {
        const o = new FractPar(); if (l) { o.copy(l); } if (p) { o.copyPar(p); } return o;
    }

    constructor(
        a = 0, b = 0, z = 1,
        public m = 50,
        public r = 2
    ) {
        super(a, b, z)
    }

    copyPar(o: Par) {
        super.copy(o)
        if (o.m != undefined) { this.m = o.m }
        if (o.r != undefined) { this.r = o.r }
        return this
    }
    copy(o: Par | Loc) {
        this.copyPar(o);
        return this
    }
    clone() { return new FractPar().copy(this) }
    toString() { return super.toString() + ` m=${this.m} r=${this.r}` }

    set(a?: number, b?: number, z?: number, m?: number, r?: number) {
        super.set(a, b, z)
        if (m != undefined) { this.m = m }
        if (r != undefined) { this.r = r }
        return this
    }

    setLoc(a?: number, b?: number, z?: number) { return super.set(a, b, z); }
    isSameMR(o: Par) { return this.m == o.m && this.r == o.r }
    equals(o: Par) { return super.equals(o) && this.isSameMR(o) }
    isSameABZ(o: Loc) { return super.equals(o) }
}