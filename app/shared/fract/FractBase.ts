import { KeyCommands } from '../KeyCommands';
import { DomHelper } from '../DomHelper';

export declare interface ILogger { log: boolean }


declare type Doc = { key: string, doc: string, section?: string, special?: boolean }
declare type DocSection = { name: string, docs: Doc[] }
enum Tools { Doc, Switch, Info }

export abstract class FractBase {
    protected showDocAtStart = !this.isDev
    protected altApp: FractBase
    protected log = this.isDev
    protected debug = true
    protected keys: KeyCommands
    protected tools: Map<string, boolean>
    protected abstract _allDivs = ['Doc', 'Info']

    get isDocShown() { return this.isTool(Tools.Doc) }

    get isDev() { return document.location.hash.toLowerCase() == '#dev' }

    constructor(protected altAppInit?: FractBase | (() => FractBase)) {
        this.tools = new Map<string, boolean>()
    }

    protected say(...args: any[]) {
        if(this.log) { console.log(this.constructor.name, ...args) }
    }
    protected sayMs(start: number, ...args: any[]) {
        if(this.log) { this.say(...args, (performance.now() - start).toFixed(3) + 'ms') }
    }
    protected dbg(...args: any[]) {
        if(this.log && this.debug) { console.log(this.constructor.name, ...args) }
    }
    protected withTime(label: string, action: () => void) {
        if (this.log && this.debug) {
            label = this.constructor.name + ' ' + label
            console.time(label)
            try { action() }
            finally { console.timeEnd(label) }
        } else action()
    }

    attach() {
        this._attach()
        this.allShown = false
        this.showAll(true, ...this._moreElements())
        this.updateInfo(true)
        this._attached()
        return this
    }
    detach() {
        this._detach()
        this.setTool(Tools.Doc, false)
        this.allShown = true
        return this.showAll(false, ...this._moreElements())
            .then(() => { DomHelper.remove('Info'); this._detached() })
    }
    showDoc(force?: boolean, fade = false) {
        const show = force || this.isDocShown
        if (force) this.setTool(Tools.Doc, true)
        const docs = show && this.keys.getDoc(this.isDev)
        this.doShowDoc(show && docs, fade || undefined)
    }
    
    protected _attach() { }
    protected _attached() { }
    protected _detach() { }
    protected _detached() { }

    protected abstract _loggers(): ILogger[]

    protected abstract _toolname(tool: number): string

    protected isTool(...name: (string | number)[]) {
        return name.reduce((p, c) => p || this.tools.get(typeof c == 'string' ? c : this._toolname(c)), false)
    }

    protected setTool(tool: number, v: boolean) {
        this.tools.set(this._toolname(tool), v)
        return v
    }

    protected switch() {
        if (!this.altAppInit) return
        const showDoc = this.isDocShown || !this.altApp && this.showDocAtStart
        this.detach().then(() => {
            if (!this.altApp) this.altApp = (typeof this.altAppInit == 'function') ? this.altAppInit() : this.altAppInit
            this.altApp.attach()
            if (showDoc) this.altApp.showDoc(true, true)
        })
    }

    protected basetool(name: string, shift: boolean) {
        switch (name) {
            case 'log':
                this.log = !this.log; this._loggers().forEach(l => l.log = this.log)
                this.say('logging: ' + (this.log ? 'on' : 'off'))
                this.updateInfo()
                return true
            case Tools[Tools.Switch]: this.switch()
                return true
            default:
                let v: boolean; this.tools.set(name, v = !this.tools.get(name))
                this.say(name, v)
                switch (name) {
                    case Tools[Tools.Doc]: this.showDoc(); return true
                    case Tools[Tools.Info]: this.updateInfo(); return true
                    default: break
                }
        }
    }


    protected _pathData: Array<number>
    protected drawPath(x: number, y: number) {
        if (!this._pathData) { this._pathData = new Array<number>() }
        //const path = this.psr.computePathFromView(x, y, this.state.m, this.state.r, this._pathData)
    }

    protected _divProgress: HTMLElement
    protected updateProgress(p: number | false) {
        const show = p !== false //&& this.state.progress
        this._divProgress = DomHelper.crud('Progress', show, !this._divProgress
            && DomHelper.topLeft('progressBar'),
            (el, isNew) => {
                // if (isNew) {
                //     const p = DomHelper.create('progress', el, 'progress') as HTMLProgressElement
                //     p.max = 1, p.value = 0;
                // }
                //(el.firstElementChild as HTMLProgressElement).value = p || 0
                //setTimeout(() =>
                DomHelper.setStyle(el,
                    //    DomHelper.transition('width 0.5s ease',
                    { width: Math.ceil(100 * (p || 0)) + '%' })
                //)
                //    , 0)
            })
    }

    protected processing: boolean
    protected _divInfo: HTMLElement
    protected updateShowBusy(force?: boolean) {
        if (force != undefined) this.processing = force
        //if (this._divInfo) setTimeout(() => this._divInfo.classList.toggle('busy', this.processing), 0)
        if (this._divInfo) this._divInfo.classList.toggle('busy', this.processing)
    }

    protected abstract _getInfo(): string

    protected updateInfo(fadeIn = false) {
        const show = this.isTool(Tools.Info)
        this._divInfo = DomHelper.crud('Info', show ? fadeIn ? DomHelper.fader(true) : true : false,
            show && DomHelper.bottomLeft('all', 'info', 'green'),
            show && this._getInfo())
    }

    protected allShown: boolean
    protected abstract _moreElements(): HTMLElement[]

    protected showAll(show: boolean, ...more: (string | HTMLElement)[]) {
        if (this.allShown == show) {
            return DomHelper.fade(show, .5, ...more)
        } else {
            this.allShown = show
            return DomHelper.fade(show, .5, ...more.concat(this._allDivs))
        }
    }

    private doShowDoc(docs: Doc[], forceFade?: boolean) {
        const div = DomHelper.crud('Doc', forceFade != undefined ? DomHelper.fader(forceFade) : !!docs,
            !!docs && DomHelper.bottomRight('all', 'info', { 'width': '250px', 'z-index': 10 }),
            div => {
                const sections =
                    docs.reduce((p, c) => {
                        const sn = c.special ? 'specials' : c.section || '',
                            s = p.find(s => s.name == sn)
                        if (s) s.docs.push(c)
                        else p.push({ name: sn, docs: [c] })
                        return p
                    }, new Array<DocSection>())
                this.say(sections)
                DomHelper.append(div, true,
                    ...sections.map(s =>
                        DomHelper.div(null,
                            { display: 'block' },
                            DomHelper.span(s.name, { 'font-weight': 'bold' }, null),
                            ...s.docs.map(c =>
                                DomHelper.div(null,
                                    { display: 'block' },
                                    DomHelper.span(c.key, { 'font-weight': 'bold' }, null),
                                    DomHelper.span(c.doc, { 'margin-left': '5px' }, null)))

                        )))
            })
    }
}
