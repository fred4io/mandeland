
declare type TProgressResult = { progress: number, data?: any }
declare type TOnProgressCancel = (progressResult: TProgressResult) => boolean
function compute(data: any, onResult: TOnProgressCancel): any { }
export class WorkerLoader {

    static setup(
        computeFunction: (data: any, onResult: TOnProgressCancel) => any,
        dependencies?: Array<any>,
        computeClassName?: string,
        log = false, debug = false, verbose = false,
        onWorkerReady?: (worker: Worker) => void
    ) {
        if (log) console.log('building')
        const blobUrl = WorkerLoader.buildBlobUrl(computeFunction, dependencies, computeClassName, log, debug, verbose)
        if (log) console.log('loading')
        const worker = new Worker(blobUrl)
        worker.onmessage = e => {
            if (log) console.log('worker: ' + e.data)
            if (e.data == 'ready' && typeof onWorkerReady == 'function') { onWorkerReady(worker) }
        }
    }

    static buildBlobUrl(
        computeFunction: (data: any, onResult: TOnProgressCancel) => any,
        dependencies: any[],
        computeClassName: string,
        log?: boolean, debug?: boolean, verbose?: boolean
    ) {
        let comp = computeFunction.toString().trim(), cn = computeClassName
        if (cn) comp = comp.replace(new RegExp('\\w+\\.' + cn, 'g'), cn)
        if (comp.indexOf('function ') != 0) comp = 'function ' + comp
        comp = `function compute(data, onProgress){\n(${comp})(data, onProgress)\n}`
        const func = `(function ${this.webWorker.toString().trim()})(${log && debug},${log && debug && verbose})`
        const blobParts = dependencies.concat(comp, func).map(o => o + '\n')
        return URL.createObjectURL(new Blob(blobParts, { type: 'text/javascript' }))
    }

    private static webWorker(log = false, verbose = false) {
        var canceled = false, busy = false
        function unbusy() { setTimeout(() => busy = false, 0) }
        onmessage = e => {
            switch (e.data) {
                case 'cancel': if (log) { console.log('canceling') }; canceled = true; break
                case 'silent': log = false; break
                case 'debug': log = true, verbose = false; break
                case 'verbose': log = verbose = true; break
                default:
                    if (typeof e.data != 'object') {
                        console.warn('unknown request: ' + e.data);
                        break
                    }
                    if (busy) {
                        if (log) { console.log('(busy)') }
                        break
                    }
                    canceled = false
                    if (log) { console.log('starting') }
                    compute(e.data, (wr: TProgressResult) => {
                        if (canceled) { unbusy(); return true }
                        if (log && verbose) { console.log('computing', wr) }
                        postMessage(wr, undefined, [wr.data])
                        if (wr.progress >= 1 && wr.data) { unbusy() }
                        return false
                    })
                    if (log) { console.log('started') }
                    break
            }
        }
        if (log) console.log('loaded')
        postMessage('ready', undefined)
    }

}
