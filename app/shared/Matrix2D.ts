declare type Point = { x: number, y: number }
declare type Rect = { x: number, y: number, width: number, height: number }
declare type Region = { xmin: number, xmax: number, ymin: number, ymax: number }

/** 3x2, columns first */
export class Matrix2D {

    static multiply(m1: Matrix2D, m2: Matrix2D, result?: Matrix2D) {
        return (result || new Matrix2D(false)).set(
            m1.a * m2.a + m1.c * m2.b,
            m1.b * m2.a + m1.d * m2.b,

            m1.a * m2.c + m1.c * m2.d,
            m1.b * m2.c + m1.d * m2.d,

            m1.a * m2.e + m1.c * m2.f + m1.e,
            m1.b * m2.e + m1.d * m2.f + m1.f
        )
    }

    static inverse(m: Matrix2D, result?: Matrix2D) {

        const ad_bc = m.a * m.d - m.b * m.c

        return (result || new Matrix2D(false)).set(
            m.d / ad_bc,
            m.b / -ad_bc,

            m.c / -ad_bc,
            m.a / ad_bc,

            (m.d * m.e - m.c * m.f) / -ad_bc,
            (m.b * m.e - m.a * m.f) / ad_bc
        )
    }

    static translation(tx: number, ty = 0) {
        return new Matrix2D(false).setAsTranslate(tx, ty)
    }
    static scale(sx: number, sy?: number) {
        return new Matrix2D(false).setAsScale(sx, sy)
    }
    static rotation(angle: number) {
        return new Matrix2D(false).setAsRotate(angle)
    }
    /** a, b, c => i.m(c).m(b).m(a) */
    static transformLR(...matrices: Matrix2D[]) {
        switch (matrices.length) {
            case 0: return new Matrix2D()
            case 1: return new Matrix2D(matrices[0])
            default: return matrices.reduceRight((p, c) => c ? p.multiplyBy(c) : p, new Matrix2D())
        }
    }
    /** a, b, c => i.m(a).m(b).m(c) */
    static transformRL(...matrices: Matrix2D[]) {
        switch (matrices.length) {
            case 0: return new Matrix2D()
            case 1: return new Matrix2D(matrices[0])
            default: return matrices.reduce((p, c) => c ? p.multiplyBy(c) : p, new Matrix2D())
        }
    }
    static rotationAround(cx: number, cy: number, angle: number) {
        return Matrix2D.transformLR(
            Matrix2D.translation(cx, cy),
            Matrix2D.rotation(angle),
            Matrix2D.translation(-cx, -cy)
        )
    }
    static flipX() {
        return Matrix2D.scale(-1, 1)
    }
    static flipY() {
        return Matrix2D.scale(1, -1)
    }
    static flipXY() {
        return Matrix2D.scale(-1, -1)
    }

    public a: number; public c: number; public e: number
    public b: number; public d: number; public f: number

    constructor(m?: Matrix2D | false) {
        if (m == undefined) { this.setIdentity() }
        else if (m !== false) { this.copy(m) }
    }

    set(a: number, b: number, c: number, d: number, e: number, f: number) {
        this.a = a; this.c = c; this.e = e
        this.b = b; this.d = d; this.f = f
        return this;
    }

    setIdentity() {
        this.a = 1; this.c = 0; this.e = 0
        this.b = 0; this.d = 1; this.f = 0
        return this
    }

    copy(m: Matrix2D) {
        this.a = m.a; this.c = m.c; this.e = m.e
        this.b = m.b; this.d = m.d; this.f = m.f
        return this
    }

    setAsTranslate(tx: number, ty = 0) {
        this.a = 1; this.c = 0; this.e = tx
        this.b = 0; this.d = 1; this.f = ty
        return this
    }

    setAsScale(sx: number, sy?: number) {
        if (sy == undefined) { sy = sx }
        this.a = sx; this.c = 0; this.e = 0
        this.b = 0; this.d = sy; this.f = 0
        return this
    }

    setAsRotate(angle: number) {
        const cos = Math.cos(angle)
        const sin = Math.sin(angle)
        this.a = cos; this.c = -sin; this.e = 0
        this.b = sin; this.d = cos; this.f = 0
        return this
    }

    multiplyBy(m: Matrix2D) {
        return Matrix2D.multiply(this, m, this)
    }
    inverse(): Matrix2D {
        return Matrix2D.inverse(this, this)
    }

    clone(): Matrix2D {
        return new Matrix2D(this)
    }

    getInverse(): Matrix2D {
        return this.clone().inverse()
    }

    getMultiplyBy(m: Matrix2D) {
        return this.clone().multiplyBy(m)
    }


    transformX(x: number, y: number) {
        return this.a * x + this.c * y + this.e
    }
    transformY(x: number, y: number) {
        return this.b * x + this.d * y + this.f
    }

    transformPoint<T extends Point>(p: T): T {
        const x = p.x, y = p.y
        p.x = this.a * x + this.c * y + this.e
        p.y = this.b * x + this.d * y + this.f
        return p
    }

    transformRegion<T extends Region>(r: T): T {
        const { xmin, ymin, xmax, ymax } = r
        r.xmin = this.a * xmin + this.c * ymin + this.e
        r.ymin = this.b * xmin + this.d * ymin + this.f
        r.xmax = this.a * xmax + this.c * ymax + this.e
        r.ymax = this.b * xmax + this.d * ymax + this.f
        return r
    }
    transformRect<T extends Rect>(r: T): T {
        let x = r.x, y = r.y
        r.x = this.a * x + this.c * y + this.e
        r.y = this.b * x + this.d * y + this.f
        x += r.width; y += r.height
        const a = this.a * x + this.c * y + this.e
        const b = this.b * x + this.d * y + this.f
        r.width = a - r.x
        r.height = b - r.y
        return r
    }
    transformXYs(xys: Array<number>, result?: Array<number>) {
        result = result || xys
        const l = xys && xys.length
        for (let i = 0; i < l; i += 2) {
            let x = xys[i], y = xys[i + 1]
            result[i] = this.a * x + this.c * y + this.e
            result[i + 1] = this.b * x + this.d * y + this.f
        }
        return result
    }

}
