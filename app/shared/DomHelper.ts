declare type Style = string | object
declare type El = string | HTMLElement
declare type TFader = {
    show: boolean,
    seconds?: number,
    opacity?: number | undefined
}
export class Fader {
    constructor(
        public show: boolean,
        public seconds = .5,
        public opacity = show ? 1 : 0
    ) {
    }
}
export class DomHelper {
    private static _divs = new Map<string, HTMLDivElement>()
    private static _abs = { position: 'absolute' }
    private static _inb = { display: 'inline-block' }
    private static _noevents = { 'pointer-events': 'none' }
    private static _tl = { top: 0, left: 0 }
    private static _bl = { bottom: 0, left: 0 }
    private static _tr = { top: 0, right: 0 }
    private static _br = { bottom: 0, right: 0 }
    private static _top = { top: 0, left: 0, right: 0 }
    private static _btm = { bottom: 0, left: 0, right: 0 }
    private static _lft = { top: 0, bottom: 0, left: 0 }
    private static _rgt = { top: 0, bottom: 0, right: 0 }
    private static _prefixes = ['-webkit-', '-moz-', '-ms-', '-o-', '']

    private static _fader(_in: boolean, seconds = .5, toOpacity01 = _in ? 1 : 0) {
        return this.transition(`opacity ${seconds}s ease-${_in ? 'in' : 'out'}`, { opacity: toOpacity01 })
    }
    private static _fade(el: HTMLElement, fader: TFader) {
        this.setStyle(el, this._fader(fader.show, fader.seconds, fader.opacity))
        return new Promise<void>(resolve => setTimeout(() => resolve(), 1000 * fader.seconds))
    }
    private static _unfade(el: HTMLElement) {
        this._prefixes.forEach(p => el.style.removeProperty(p + 'transition'))
    }

    private static absDiv(o: object) { return this.merge(this._abs, this._inb, this._noevents, o) }

    private static getEl(el: El, elseBody = false) {
        return ((typeof el == 'object') ? el : this._divs.get(el)) || (elseBody ? document.body : undefined)
    }

    static get(name: string, elseBody = false) { return this.getEl(name, elseBody) }

    static merge(...objs: object[]) { return <object>objs.reduce(Object.assign, {}) }

    static mergeIn(o: object, ...stylesOrClasses: Style[]) {
        stylesOrClasses.filter(sc => !!sc).forEach(sc => {
            if (typeof sc == 'string') {
                let a = (<any>o).classNames;
                if (a == undefined) { (<any>o).classNames = a = new Array<string>() }
                if (a) { a.push(sc) }
            } else { Object.assign(o, sc) }
        })
        return o;
    }

    static windowSize() { return { width: window.innerWidth, height: window.innerHeight } }

    static top(...style: Style[]) { return this.mergeIn(this.absDiv(this._top), ...style) }
    static bottom(...style: Style[]) { return this.mergeIn(this.absDiv(this._btm), ...style) }
    static left(...style: Style[]) { return this.mergeIn(this.absDiv(this._lft), ...style) }
    static right(...style: Style[]) { return this.mergeIn(this.absDiv(this._rgt), ...style) }
    static topLeft(...style: Style[]) { return this.mergeIn(this.absDiv(this._tl), ...style) }
    static topRight(...style: Style[]) { return this.mergeIn(this.absDiv(this._tr), ...style) }
    static bottomLeft(...style: Style[]) { return this.mergeIn(this.absDiv(this._bl), ...style) }
    static bottomRight(...style: Style[]) { return this.mergeIn(this.absDiv(this._br), ...style) }

    static fader(_in: boolean, seconds = .5, opacity?: number) { return new Fader(_in, seconds, opacity) }

    static setStyle(el: El, style?: Style, className?: string) {
        if (!el) return
        el = this.getEl(el)
        if (className != undefined) { el.className = className }
        if (!style) { return el }
        if (typeof style == 'string') { el.classList.add(style); return el }
        for (let pn in style) {
            const v: Style | Array<string> = (<any>style)[pn]
            if (pn == 'className' && typeof v == 'string') { el.classList.add(v) }
            else if (pn == 'classNames' && Array.isArray(v)) { v.forEach(cn => (<HTMLElement>el).classList.add(cn)) }
            else if (pn && v === null) { el.style.removeProperty(pn) }
            else if (v != undefined) { el.style.setProperty(pn, v.toString()) }
        }
        return el
    }

    static transition(value: string, style: Style = {}) {
        return this._prefixes.reduce((p, c) => { (<any>p)[c + 'transition'] = value; return p }, style)
    }


    static append(el: El, replace: boolean, ...content: HTMLElement[]) {
        el = this.getEl(el)
        if (!el) return el
        if (replace) el.innerHTML = ''
        content.forEach(c => (<HTMLElement>el).appendChild(c))
        return el
    }

    static setContent(el: El, content: string, nospaces = false) {
        el = this.getEl(el)
        if (el && content) el.innerHTML = content.replace(nospaces ? /\s+|\n/g : /\s|\n/g, '<br>')
        return el
    }
    static remove(...els: El[]) {
        els.forEach(el => { const e = this.getEl(el); if (e) e.remove() })
    }
    static removeProperties(el: El, ...names: string[]) {
        el = this.getEl(el)
        if (el) names.filter(pn => !!pn).forEach(pn => (<HTMLElement>el).style.removeProperty(pn))
        return el
    }
    static reparent(el: El, parent: El) {
        el = this.getEl(el), parent = this.getEl(parent)
        if (el && parent) { el.remove(); parent.appendChild(el) }
        return el
    }

    static div(parent?: El | null, style?: Style, ...content: HTMLElement[]) {
        return this.create('div', parent, style, ...content) as HTMLDivElement
    }
    static span(text: string, style: Style, parent: El | null): HTMLSpanElement {
        return this.create('span', parent, style, document.createTextNode(text))
    }
    static create(elementName: string,
        parent?: El | null, style?: Style, ...content: (HTMLElement | Text)[]
    ) {
        const el = document.createElement(elementName)
        this.setStyle(el, style)
        parent = parent !== null && this.getEl(parent, true)
        if (parent) parent.appendChild(el)
        content.filter(e => !!e).forEach(c => el.appendChild(c))
        return el
    }

    static crud(key: string, show: boolean | TFader,
        style?: Style,
        content?: string | ((div: HTMLDivElement, isNew: boolean) => void)
    ) {
        const divs = this._divs, fader = typeof show == 'object' ? show : undefined;
        let div = divs.get(key), isNew = false
        if (!show || fader && !fader.show) {
            if (!div) return
            divs.delete(key)
            if (fader) { this._fade(div, fader).then(() => div.remove()) }
            else div.remove()
            return
        } else if (!div) {
            divs.set(key, div = this.div(null, style))
            if (fader) div.style.opacity = '0'
            document.body.appendChild(div)
            isNew = true
        }
        div.style.visibility = 'visible';
        this._unfade(div)
        if (fader) {
            if (fader && !isNew) div.style.opacity = fader.show ? '0' : '1'
            setTimeout(() => this._fade(div, fader), 0)
        }
        if (typeof content == 'function') content(div, isNew)
        else this.setContent(div, content)
        return div
    }

    static showHide(show: boolean, ...keys: string[]) {
        keys.map(k => this._divs.get(k)).filter(el => !!el).forEach(el =>
            el.style.visibility = show ? 'visible' : 'hidden')
    }
    static fade(_in: boolean, seconds: number, ...els: El[]) {
        els.map(k => this.getEl(k)).filter(el => !!el).forEach(el =>
            this.setStyle(el, this._fader(_in, seconds)))

        return new Promise<void>((resolve, reject) =>
            setTimeout(() => resolve(), 1000 * (seconds || .5)))
    }

    static showHideMouse(show: boolean, el: El) {
        el = this.getEl(el); if (el) { el.style.cursor = show ? 'default' : 'none' }
        return el
    }


    static setSize(el: El, w: number, h: number) {
        this.setStyle(el, { 'width': w + 'px', 'height': h + 'px' })
        return el
    }

    static setupCanvas(c: HTMLCanvasElement, w?: number, h?: number, style?: Style) {
        this.setStyle(c, style)
        if (w == undefined || w <= 0) w = window.innerWidth + (w || 0)
        if (h == undefined || h <= 0) h = window.innerHeight + (h || 0)
        this.setSize(c, c.width = w, c.height = h)
        return c
    }

    static download(obj: object, fileName: string) {
        fileName = fileName || 'file.json'
        var a = document.createElement('a')
        a.setAttribute('style', 'display:none')
        a.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(obj)))
        a.setAttribute('download', fileName)
        document.body.appendChild(a)
        try { a.click() }
        finally { document.body.removeChild(a) }
    }
}