import { PointLight, GridHelper, AxesHelper, PointLightHelper, Scene, OrbitControls, LineSegments, Object3D } from "three"
import { Helpers } from "./Helpers"

declare type THelper = HelperType | string
export enum HelperType {
    grid,
    axis,
    crosshair,
    light1,
    light2,
    light3
}
export type SceneHelpersParam = {
    grid?: boolean,
    axis?: boolean,
    crosshair?: boolean,
    light1?: boolean,
    light2?: boolean,
    light3?: boolean,
}
export class SceneHelpers {

    static names = Object.keys(HelperType).filter(k => !isNaN(Number((<any>HelperType)[k])))
    private _state = new Map<HelperType, boolean>()
    private attached: boolean

    grid: GridHelper
    axis: AxesHelper
    crosshair: LineSegments
    light1: PointLightHelper
    light2: PointLightHelper
    light3: PointLightHelper

    private _onOrbitChange: (event: any) => void
    private _orbit: OrbitControls

    constructor(
        private scene: Scene,
        lights?: PointLight[],
        orbit?: OrbitControls,
        params?: SceneHelpersParam,
        public log = false) {

        this.createObjs(lights)

        if (params == undefined) params = {
            grid: true,
            axis: true,
            crosshair: true,
            light1: true,
            light2: true,
            light3: true,
        }

        this._state.set(HelperType.grid, !!params.grid)
        this._state.set(HelperType.axis, !!params.axis)
        this._state.set(HelperType.crosshair, !!params.crosshair)

        if (lights)
            lights.map((_, i) => 'light' + (i + 1)).forEach(ln =>
                this._state.set(this.getHT(ln), !!(<any>params)[ln]))

        const n = SceneHelpers.names.reduce((p, c) => this.isVisible(c) ? p + 1 : p, 0)
        this.say(n + ' scene helpers')

        this.attach(orbit)
    }

    protected say(...args: any[]) {
        if(this.log) { console.log(this.constructor.name, ...args) }
    }

    private createObjs(lights?: PointLight[]) {

        this.grid = new GridHelper(1000, 20)
        this.grid.name = 'gridHelper';

        this.axis = new AxesHelper(100)
        this.axis.name = 'axisHelper'

        this.crosshair = Helpers.createCrosshair(20, 0xf4ad42, 'crosshairHelper')

        if (lights)
            lights.forEach((l, i) => {
                const lh = new PointLightHelper(l, 10), ln = 'light' + (i + 1);
                lh.name = ln + 'Helper';
                (<any>this)[ln] = lh
            })
    }

    attach(orbit?: OrbitControls) {
        if (this.attached) return
        if (orbit && orbit !== this._orbit) {
            this._orbit = orbit
            // this._onOrbitChange = (event: any) => this.updateCrossair()
            // this._orbit.addEventListener('change', this._onOrbitChange)
        }
        this.setScene()
        this.attached = true
    }
    detach() {
        if (!this.attached) return
        // if (this._orbit && this._onOrbitChange)
        //     this._orbit.removeEventListener('change', this._onOrbitChange)
        this.unsetScene()
        this.attached = false
    }

    dispose() {
        this.detach()
    }

    update() {
        this.updateCrossair()
    }

    toggle(helper: THelper, force?: boolean) {
        if (helper == 'lights') {
            const names = [1, 2, 3].map(i => 'light' + i)
            const v = !names.some(n => this.isVisible(n))
            names.forEach(n => this.toggle(n), v)
            return
        }
        const ht = this.getHT(helper), h = this.getH(ht)
        if (!h) return
        const isVisible = this.isVisible(ht), visible = force != undefined ? force : !isVisible
        //console.log(force, isVisible, visible)
        if (isVisible == visible) return
        if (visible) this.scene.add(h)
        else this.scene.remove(h)
        this._state.set(ht, visible)
        this.say(HelperType[ht], visible)
    }

    isVisible(helper: THelper) {
        return !!this._state.get(this.getHT(helper))
    }

    datProxy() {
        const r = {}
        SceneHelpers.names.forEach(hn => {
            Object.defineProperty(r, hn,
                {
                    get: () => this.isVisible(hn),
                    set: (v) => this.toggle(hn, v)
                })
        })
        return r
    }


    private getHT(helper: THelper): HelperType {
        return typeof helper == 'string' ? (<any>HelperType)[helper] : helper
    }
    private getH(helper: THelper, log = false) {
        const hn = typeof helper == 'string' && isNaN(parseInt(helper, 10)) ? helper : HelperType[<number>helper]
        return (<any>this)[hn] as Object3D
    }

    private unsetScene() {
        SceneHelpers.names.forEach(hn => this.scene.remove(this.getH(hn)))
    }

    private setScene(scene?: Scene) {
        if (scene) this.scene = scene
        this.unsetScene()
        SceneHelpers.names
            .filter(hn => this.isVisible(hn))
            .map(hn => this.getH(hn, true))
            .filter(o => !!o)
            .forEach(o => this.scene.add(o))
    }

    private updateCrossair() {
        if (this.crosshair && this._orbit)
            this.crosshair.position.copy(this._orbit.target)
    }

}