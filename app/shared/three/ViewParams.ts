
export class ViewParams {

    // get width() { return this.viewport.clientWidth; }
    // get height() { return this.viewport.clientHeight; }
    get width() { return window.innerWidth; }
    get height() { return window.innerHeight; }
    get aspect() { return this.width / this.height }
    get size() { return { width: this.width, height: this.height } }

    constructor(
        public viewport: HTMLElement,
        public fov = 40,
        public near = 0.1,
        public far = 10000,
    ) { }

}

