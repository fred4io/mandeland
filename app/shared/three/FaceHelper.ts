import { Vector3, ArrowHelper, Face3, Mesh, BufferGeometry, MeshBasicMaterial } from "three";

export class FaceHelper {

    mesh: Mesh
    centroHelper: ArrowHelper
    centroid = new Vector3()
    get hasObjects() { return !!this.mesh }
    get wireFrame() { return this.matparam.wireframe }
    set wireFrame(value: boolean) {
        this.matparam.wireframe = value
        if (this.mat) this.mat.wireframe = value
    }

    private a = new Vector3()
    private b = new Vector3()
    private c = new Vector3()
    private geo: BufferGeometry
    private mat: MeshBasicMaterial
    private matparam = { color: 0xff0000, wireframe: true }

    constructor(
        public vertices: Array<number>,
        private log = false
    ) { }

    reset(vertices: Array<number>) {
        this.dispose()
        this.vertices = vertices
    }

    update(face: Face3) {
        this.updateData(face)
        if (!this.geo) this.geo = new BufferGeometry()
        this.geo.setFromPoints([this.a, this.b, this.c])
        if (!this.mesh) {
            this.mat = new MeshBasicMaterial(this.matparam)
            this.mesh = new Mesh(this.geo, this.mat)
            this.centroHelper = new ArrowHelper(face.normal, this.centroid, 10, 0xffffff)
        }
        (this.geo.attributes as any).position.needsUpdate = true
        this.centroHelper.position.copy(this.centroid)
        this.centroHelper.setDirection(face.normal)
    }

    dispose() {
        this.centroHelper = this.mesh = null
        if (this.geo) this.geo.dispose()
        if (this.mat) this.mat.dispose()
        this.geo = this.mat = null
    }

    private updateData(face: Face3) {
        this.a.fromArray(this.vertices, face.a * 3)
        this.b.fromArray(this.vertices, face.b * 3)
        this.c.fromArray(this.vertices, face.c * 3)
        FaceHelper.centroid(this.a, this.b, this.c, this.centroid)
        if (this.log) console.log(face, this.a, this.b, this.c, this.centroid)
    }

    private static centroid(a: Vector3, b: Vector3, c: Vector3, result = new Vector3()) {
        return result.set(
            (a.x + b.x + c.x) / 3,
            (a.y + b.y + c.y) / 3,
            (a.z + b.z + c.z) / 3)
    }
}