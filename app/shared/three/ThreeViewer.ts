import { Scene, Renderer, WebGLRenderer, Camera, PerspectiveCamera, PointLight, Object3D, Mesh, Color, Vector3, Points, Raycaster } from 'three'
import { OrbitControls } from 'three'
import { GUI } from 'dat.gui'
import * as screenfull from 'screenfull'

import { GUIs } from './GUIs'
import { ViewParams } from './ViewParams'
import { SceneHelpers, SceneHelpersParam } from './SceneHelpers'
import { DomHelper } from '../DomHelper'

export enum StatMode { no = -1, fps, ms, mb }
export declare type TVectorParam = { x?: number, y?: number, z?: number }

export declare interface ICameraParam {
    position?: TVectorParam
    target?: TVectorParam
}

declare type ThreeViewerParams = {
    animate?: boolean,
    guiOpen?: boolean,
    statElement?: string | HTMLElement,
    guiElement?: string | HTMLElement,
    guiData?: object,
    fullScreen?: boolean,
    camera?: { position?: TVectorParam, target?: TVectorParam }
} & SceneHelpersParam

export class ThreeViewer {

    static clearColor = 0xE0E0E0 // 0x282424 0x595D98

    public log = true
    public get doms() {
        return [
            this.gui && this.gui.domElement,
            this.stats && this.stats.dom
        ].filter(o => !!o)
    }

    private scene: Scene
    private viewParams: ViewParams
    private camera: PerspectiveCamera
    private renderer: Renderer
    private orbit: OrbitControls
    private lights: PointLight[]
    private animate: boolean
    private helpers: SceneHelpers
    private disposing: boolean
    private _attached: boolean
    private _onresize: EventListener
    private _onbeforeunload: EventListener
    private _onOrbitChange: EventListener
    private render: () => void

    private stats: Stats
    private statElement: HTMLElement
    private _statMode = StatMode.no
    get statMode() { return this._statMode }
    set statMode(mode: StatMode) { this.setStats(this._statMode = mode) }

    private gui: GUI
    private guiElement: HTMLElement
    private guiData: Object

    constructor(viewport: string | HTMLElement,
        setScene: (scene: Scene, camera: Camera, orbit: OrbitControls) => void,
        private onRender = (dt: number, now: number) => { },
        private params: ThreeViewerParams = {},
        private onParamChanged?: (name: string, value: any) => void
    ) {
        this._onresize = () => { this.onResize() }
        this._onbeforeunload = () => { this.dispose() }
        this._onOrbitChange = () => { this.requestRender() }

        window.addEventListener('beforeunload', this._onbeforeunload)

        if (typeof viewport == 'string') viewport = document.getElementById(viewport)

        const vp = this.viewParams = new ViewParams(viewport)
        this.say('viewport', vp.width + 'x' + vp.height)

        this.animate = params.animate

        const renderer = new WebGLRenderer({ antialias: true, alpha: true })
        renderer.setPixelRatio(window.devicePixelRatio)
        renderer.setSize(vp.width, vp.height)
        renderer.setClearColor(ThreeViewer.clearColor, 1)
        this.renderer = renderer

        viewport.appendChild(renderer.domElement)

        this.scene = new Scene()
        this.scene.name = 'scene'

        this.camera = new PerspectiveCamera(vp.fov, vp.aspect, vp.near, vp.far)
        this.setVector(this.camera.position, params.camera && params.camera.position
            || { x: 0, y: 300, z: 600 })
        this.camera.lookAt(this.setVector(new Vector3(), params.camera && params.camera.target
            || { x: 0, y: 0, z: 0 }))

        this.orbit = new OrbitControls(this.camera, renderer.domElement)

        this.lights = [[0, 1000, 0], [1000, 1000, 1000], [-1000, -1000, -1000]]
            .map((p, i) => {
                const l = new PointLight(0xffffff, 1, 0)
                l.position.fromArray(p)
                l.name = 'light' + (i + 1)
                return l
            })

        this.helpers = new SceneHelpers(this.scene, this.lights, this.orbit, params, this.log)

        this.statElement = (params ?
            typeof params.statElement == 'string' ? document.getElementById(params.statElement)
                : params.statElement : undefined) || document.body
        this.setStats(StatMode.no)

        this.guiElement = params ?
            typeof params.guiElement == 'string' ? document.getElementById(params.guiElement)
                : params.guiElement : undefined
        this.guiData = params && params.guiData

        setScene(this.scene, this.camera, this.orbit)
        this.attach()
        this.attached()
    }

    protected say(...args: any[]) {
        if(this.log) { console.log(this.constructor.name, ...args) }
    }

    dispose() {
        if (this.disposing) return
        this.disposing = true
        this.say('disposing')
        window.removeEventListener('beforeunload', this._onbeforeunload)
        this.detach()
        this.detached()
        if (this.orbit) this.orbit.dispose()
        if (this.helpers) this.helpers.dispose()
        if (this.stats) { this.stats.end(); this.stats.dom.remove(); if (this.statElement != document.body) this.statElement.remove() }
        return new Promise<void>((resolve, reject) => {
            this.disposeObj(this.scene)
            setTimeout(() => {
                this.disposing = false
                this.say('disposed')
                resolve()
            }, 100)
        })
    }

    attach() {
        if (this._attached) return
        window.addEventListener('resize', this._onresize)
        this.setScene()
        this.startRendering()
        if (this.orbit) {
            if (this.helpers) this.helpers.attach(this.orbit)
            this.orbit.addEventListener('change', this._onOrbitChange)
            this.orbit.enabled = true
        }
        this._attached = true
        this.say('attached')
        if (this.stats) this.stats.begin()
    }
    attached() { this.initGui() }
    detach() {
        if (!this._attached) return
        window.removeEventListener('resize', this._onresize)
        if (this.stats) this.stats.end()
        if (this.helpers) this.helpers.detach()
        if (this.orbit) {
            this.orbit.enabled = false
            this.orbit.removeEventListener('change', this._onOrbitChange)
        }
        this.unsetScene()
        this._attached = false
        this.say('detached')
    }
    detached() { if (this.gui) { this.gui.destroy(); delete this.gui } }

    toggleStat() {
        this.statMode = this.statMode == StatMode.no ? StatMode.fps : StatMode.no
    }
    nextStat() {
        this.statMode =
            this.statMode == undefined ? StatMode.fps
                : this.statMode >= StatMode.mb ? StatMode.no
                    : (this.statMode + 1)
    }
    toggleFullScreen(force?: boolean) {
        const sf = (<any>screenfull).screenfull, fs = force != undefined ? force : !sf.isFullscreen
        if (sf.enabled) if (fs) sf.request(); else sf.exit()
        this.requestRender()
        return sf.enabled && fs
    }
    toggleHelper(name: string, force?: boolean) {
        this.helpers.toggle(name, force)
        this.requestRender()
    }
    setBackground(color: Color | number) {
        if (!(color instanceof Color)) color = new Color(color)
        const sb = this.scene.background as Color
        if (!sb.equals(color)) {
            (this.scene.background as any).copy(color) 
            this.requestRender()
        }
    }
    getCamera() {
        return {
            position: this.camera.position.clone(),
            target: this.orbit.target.clone()
        }
    }
    setCamera(position?: TVectorParam, target?: TVectorParam) {
        this.setVector(this.camera.position, position)
        if (this.orbit) {
            this.setVector(this.orbit.target, target)
            this.orbit.update()
        }
        this.requestRender()
    }

    disposeObj(obj3D: Object3D | Object3D[]) {

        const disposings = new Array<Object3D>()

        function doDispose(obj: Object3D | Object3D[]) {

            if (!obj) return

            if (Array.isArray(obj)) {
                obj.forEach(o => doDispose(o))
                return
            }

            if (obj.traverse)
                obj.traverse(o => { if (o !== obj) doDispose(o) })

            const mesh = obj as (Mesh | Points)
            if (!mesh.geometry && !mesh.material) return
            if (disposings.indexOf(obj) != -1) return

            //console.log( 'disposing ' + ( obj.name || obj.id ) )

            if (mesh.geometry)
                mesh.geometry.dispose()

            if (mesh.material)
                if (Array.isArray(mesh.material)) mesh.material.forEach(m => m.dispose())
                else mesh.material.dispose()

            disposings.push(obj)

        }

        doDispose(obj3D)
    }

    private raycaster: Raycaster

    getPointedPoint(event: MouseEvent, mesh: Object3D) {
        const int = this.getIntersection(event, mesh)
        return int && int.point
    }

    getPointedFace(event: MouseEvent, mesh: Object3D) {
        const int = this.getIntersection(event, mesh)
        return int && int.face
    }
    private getIntersection(event: MouseEvent, mesh: Object3D) {
        if (!mesh) return
        if (!this.raycaster) this.raycaster = new Raycaster()
        const mouse = {
            x: (event.clientX / this.renderer.domElement.clientWidth) * 2 - 1,
            y: - (event.clientY / this.renderer.domElement.clientHeight) * 2 + 1
        }
        this.raycaster.setFromCamera(mouse, this.camera)
        var intersects = this.raycaster.intersectObject(mesh)
        return intersects.length && intersects[0]
    }

    private setVector(v: Vector3, p: TVectorParam) {
        if (!v || !p || p.x == undefined && p.y == undefined && p.z == undefined) return
        v.set(
            p.x == undefined ? v.x : p.x,
            p.y == undefined ? v.y : p.y,
            p.z == undefined ? v.z : p.z
        )
        return v
    }

    private startRendering() {
        const self = this, renderer = this.renderer, dater = performance || Date
        let before = dater.now()

        function render() {
            if (self.animate) {
                if (!self._attached || self.disposing) {
                    self.say('rendering stopped')
                    return
                }
                requestAnimationFrame(render)
            }

            const now = dater.now(), dt = (now - before) * .001
            before = now

            self.onRender(dt, now)
            if (self.stats) self.stats.update()
            if (self.helpers) self.helpers.update()
            //if (self.gui) self.updateGui(self.gui)

            renderer.render(self.scene, self.camera)
        }
        this.render = render
        render()
    }
    requestRender() {
        if (!this.animate) requestAnimationFrame(this.render)
    }

    private updateGui(gui: GUI) {
        // if (!gui || gui.closed) return
        // if (gui.__controllers) gui.__controllers.forEach(c => c.updateDisplay())
        // if (gui.__folders && gui.__folders.length)  gui.__folders.forEach(f => this.updateGui(f))
    }

    private onResize() {
        this.say('resized', this.viewParams.width + 'x' + this.viewParams.height)
        this.renderer.setSize(this.viewParams.width, this.viewParams.height)
        this.camera.aspect = this.viewParams.aspect
        this.camera.updateProjectionMatrix()
    }

    private unsetScene() {
        if (!this.scene) return
        this.scene.children.forEach(o => this.disposeObj(o))
    }

    private setScene() {
        this.lights.forEach(l => this.scene.add(l))
    }

    private setStats(mode: StatMode) {

        if (mode == StatMode.no) {
            if (this.stats) {
                this.stats.end()
                this.stats.dom.remove()
                this.stats = undefined
                this.say('stats: ' + StatMode[mode])
            }
            return
        }

        if (!this.stats) this.stats = new Stats()

        DomHelper.reparent(this.stats.dom, this.statElement)
        DomHelper.removeProperties(this.stats.dom, 'position', 'top', 'left')
        this.stats.showPanel(mode || 0)

        this.say('stats: ' + StatMode[mode])
    }

    private initGui() {
        if (this.gui) return

        const self = this
        const update = (name: string, value: any) => {
            this.requestRender()
            if (this.onParamChanged) self.onParamChanged(name, value)
        }
        const gui = this.gui = new GUI({ autoPlace: this.guiElement == undefined })
        if (this.guiElement) DomHelper.reparent(gui.domElement, this.guiElement)
        gui.closed = this.params && !this.params.guiOpen

        const cgui = gui.addFolder('camera')
        GUIs.vector3(this.camera.position, cgui, 'position', true, true, v => {
            this.orbit.update()
            update('camera.position', v)
        })
        GUIs.vector3(this.orbit.target, cgui, 'target', true, true, v => {
            this.camera.lookAt(v)
            update('camera.target', v)
        })

        const lgui = gui.addFolder('lights')
        this.lights.forEach((l, i) => {
            const name = 'light' + (i + 1)
            GUIs.light(this.lights[0], lgui, name, false, true, l => update(name, l))
        })

        const hgui = gui.addFolder('helpers'), proxy = this.helpers.datProxy()
        SceneHelpers.names.forEach(hn =>
            hgui.add(proxy, hn)
                .onChange((v: any) =>
                    update('helpers.' + hn, v)))

        if (!this.scene.background) this.scene.background = new Color(ThreeViewer.clearColor)
        GUIs.color(this.scene.background as Color, gui, 'background', false, false, c => update('background', c))

        const o = { fullScreen: false }
        gui.add(o, 'fullScreen').onChange((fs: boolean) => {
            o.fullScreen = this.toggleFullScreen(fs)
            update('fullScreen', o.fullScreen)
        })
    }
}