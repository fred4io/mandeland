import { GUI } from 'dat.gui'
import { Vector3, Color, Light } from 'three'

export class GUIs {

    static vector3(v: Vector3, parent: GUI, name: string,
        listen = false, reset = false, onChange?: (v: Vector3) => void
    ) {
        const v0 = v.clone(), gui = parent.addFolder(name),
            cx = gui.add(v, 'x'),
            cy = gui.add(v, 'y'),
            cz = gui.add(v, 'z')
        if (listen) { cx.listen(); cy.listen(); cz.listen() }
        if (onChange) {
            cx.onChange((x: number) => onChange(v))
            cy.onChange((y: number) => onChange(v))
            cz.onChange((z: number) => onChange(v))
        }
        if (reset) {
            gui.add({
                reset: () => {
                    v.copy(v0)
                    if (listen) return
                    cx.updateDisplay(); cy.updateDisplay(); cz.updateDisplay()
                    if (onChange) onChange(v)
                }
            }, "reset")
        }
        return gui
    }

    static color(c: Color, gui: GUI, name: string,
        listen = false, reset = false, onChange?: (c: Color) => void
    ) {
        const o = {}, c0 = c.clone()
        Object.defineProperty(o, name, {
            get: () => '#' + c.getHexString(),
            set: v => c.setStyle(v)
        })
        const cc = gui.addColor(o, name)
        if (listen) cc.listen()
        if (onChange) cc.onChange((_: any) => onChange(c))
        if (reset) gui.add({
            reset: () => {
                c.copy(c0)
                if (listen) return
                cc.updateDisplay()
                if (onChange) onChange(c)
            }
        }, 'reset')
        return gui
    }

    static light(l: Light, parent: GUI, name: string,
        listen = false, reset = false, onChange?: (c: Light) => void
    ) {
        const gui = parent.addFolder(name)
        this.vector3(l.position, gui, 'position', listen, reset, onChange && (_ => onChange(l)))
        this.color(l.color, gui, 'color', listen, reset, onChange && (_ => onChange(l)))
        return gui
    }

}