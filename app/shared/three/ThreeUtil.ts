import { Vector3, Face3, CanvasTexture, MeshBasicMaterial, PlaneGeometry, Mesh } from "three"

export class ThreeUtil {

    static normal(vA: Vector3, vB: Vector3, vC: Vector3) {
        return vC.clone().sub(vB).cross(vA.clone().sub(vB)).normalize()
    }

    static faceNormal(vs: Array<Vector3>, f: Face3) {
        return this.normal(vs[f.a], vs[f.b], vs[f.c])
    }

    /** TODO */
    static makeTerrainFromCanvas(canvas: HTMLCanvasElement) {
        const texture = new CanvasTexture(canvas),
            material = new MeshBasicMaterial({ map: texture })

        //https://github.com/mrdoob/three.js/blob/master/examples/webgl_geometry_terrain.html

        const w = 1024, h = 512
        var geometry = new PlaneGeometry(w, h, 256, 256)
        geometry.rotateX(-Math.PI / 2)
        geometry.computeFaceNormals()
        geometry.computeVertexNormals()

        return new Mesh(geometry, material)
    }

}