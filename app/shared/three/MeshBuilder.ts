import { BufferGeometry, Mesh, DoubleSide, VertexColors, Float32BufferAttribute, Points, PointsMaterial, Object3D, MeshStandardMaterial } from "three"
import { IRenderParams3D, RenderParams3D } from "../../client/fract3D/RenderParams3D"
import { Color as FColor } from "../Color"

declare type THeightMap = {
    data: Float32Array,
    dataWidth: number
}
declare type TMeshData = {
    vertices: Array<number>,
    faces: Array<Array<number>>,
    merged: boolean,
    names: string[]
}
declare type TGeometryData = {
    vertices: Array<number>,
    faces: Array<number>
}

export class MeshBuilder {

    protected static say(log: boolean, ...args: any[]) {
        if (log) { console.log(MeshBuilder.name, ...args) }
    }

    static makePoints(hmap: THeightMap, par: IRenderParams3D,
        log = false, debug = false
    ) {
        MeshBuilder.say(log, 'makePoints')
        return new Points(
            MeshBuilder.makeGeometry(hmap, par.maxHeight, par.lod, par.negateY,
                {
                    colored: par.colors,
                    invert: par.invert,
                    easing: par.easing
                }, false, false, debug),
            new PointsMaterial(
                {
                    vertexColors: VertexColors,
                    size: par.pointSize || 1,
                    sizeAttenuation: !!par.pointSizeAttenuation
                }))
    }

    static makeMesh(data: THeightMap | TMeshData, par: IRenderParams3D,
        log = false, debug = false
    ) {
        MeshBuilder.say(log, 'makeMesh')
        const mat1 =
            new MeshStandardMaterial({
                wireframe: par.wireframe,
                depthTest: par.depthTest,
                side: DoubleSide,
                opacity: par.opacity,
                transparent: par.transparent,
                vertexColors: RenderParams3D.getVertexColors(par),
                color: 0x888888
            })
        //const mat2 = new MeshStandardMaterial({ color: 0x000088  })
        const mats = mat1 // [mat1, mat2]

        const makeGeo = (geomData: THeightMap | TGeometryData) =>
            MeshBuilder.makeGeometry(geomData, par.maxHeight, par.lod, par.negateY,
                {
                    colored: par.colors,
                    invert: par.invert,
                    easing: par.easing
                },
                true, par.faceColors, log, debug)

        const md = (data as TMeshData).vertices && data as TMeshData
        if (!md) return new Mesh(makeGeo(data as THeightMap), mats)
        if (md.merged) {
            const d = { vertices: md.vertices, faces: md.faces[0] }
            return new Mesh(makeGeo(d), mats)
        }
        else {
            const obj = new Object3D()
            md.faces.forEach((fs, i) => {
                const d = { vertices: md.vertices, faces: fs }
                const m = new Mesh(makeGeo(d), mats)
                m.name = md.names[i]
                obj.add(m)
            })
            return obj
        }
    }

    private static makeGeometry(
        dataObj: THeightMap | TGeometryData,
        maxHeight: number,
        lod = 1,
        negateY = false,
        colored?: { colored: boolean, invert: boolean, easing: number },
        faces = false, nonIndexed = false,
        log = false,
        debug = false
    ) {
        const hmap = dataObj && (dataObj as THeightMap).data && dataObj as THeightMap
        const geomData = dataObj && (dataObj as TGeometryData).vertices && dataObj as TGeometryData

        if (geomData) nonIndexed = false //could be non indexed as well

        MeshBuilder.say(log, 'makeGeometry',
            'colored:', !!colored,
            'faces:', !!faces,
            'nonIndexed:', !!nonIndexed,
            'meshData', !!geomData)

        const geometry = new BufferGeometry()
        geometry.rotateX(- Math.PI / 2)

        let positions: Array<number>
        const mh = negateY ? -maxHeight : maxHeight

        let colors: Array<number>, color: FColor, colorize: (t: number) => void
        if (colored) {
            colors = new Array<number>()
            color = new FColor()
            colorize = FColor.getColorizer(color, colored.colored, colored.invert, colored.easing)
        }

        const withTime = (txt: string, action: () => void) => {
            if (log && debug) {
                const label = `${MeshBuilder.name} ${MeshBuilder.makeGeometry.name} ${txt}`
                console.time(label)
                action()
                console.timeEnd(label)
            } else action()
        }

        if (geomData) {
            withTime('setIndex' + (colored ? '+colors' : ''), () => {
                positions = geomData.vertices
                if (colored) //why is it not working from 0 ?
                    for (let iy = 1, l = positions.length; iy < l; iy += 3) {
                        colorize(positions[iy] / mh)
                        colors.push(color.r / 255, color.g / 255, color.b / 255)
                    }
                geometry.setIndex(geomData.faces)
            })
        }
        else {
            positions = new Array<number>()
            const
                data = hmap.data,
                dlj = (data.length / hmap.dataWidth) | 0,
                dli = hmap.dataWidth | 0,
                lj = (dlj / lod) | 0,
                li = (dli / lod) | 0,
                x0 = (- li / 2) * lod,
                z0 = (- lj / 2) * lod,
                lj1 = lj - 1,
                li1 = li - 1

            if (!nonIndexed) {
                withTime('indexed-positions' + (colored ? '+colors' : '') + (faces ? '+indices' : ''), () => {
                    for (let j = 0, jl: number, djli: number; j < lj; j++) {
                        jl = j * lod
                        djli = jl * dli
                        for (let i = 0, h: number, il: number; i < li; i++) {
                            il = i * lod
                            h = data[Math.round(djli + il)]
                            positions.push(x0 + il, h * mh, z0 + jl)
                            if (!colored) continue
                            colorize(h)
                            colors.push(color.r / 255, color.g / 255, color.b / 255)
                        }
                    }
                    if (faces) {
                        const indices = new Array<number>()
                        let jli: number, jli1: number, a: number, b: number, c: number, d: number
                        for (let j = 0; j < lj1; j++) {
                            jli = j * li
                            jli1 = jli + li
                            for (let i = 0; i < li1; i++) {
                                a = jli + i
                                b = jli1 + i
                                c = jli1 + i + 1
                                d = jli + i + 1
                                indices.push(a, b, d, b, c, d)
                            }
                        }
                        geometry.setIndex(indices)
                    }
                })
            }
            else {
                withTime('nonindexed-positions' + (colored ? '+colors' : ''), () => {

                    let djli: number, djli1: number, jl: number, jl1: number, il: number, il1: number,
                        ax: number, ay: number, az: number, ah: number,
                        bx: number, by: number, bz: number, bh: number, br: number, bg: number, bb: number,
                        cx: number, cy: number, cz: number, ch: number,
                        dx: number, dy: number, dz: number, dh: number, dr: number, dg: number, db: number

                    for (let j = 0; j < lj1; j++) {
                        jl = j * lod
                        jl1 = (j + 1) * lod
                        djli = jl * dli
                        djli1 = jl1 * dli
                        for (let i = 0; i < li1; i++) {
                            il = i * lod
                            il1 = (i + 1) * lod

                            ah = data[Math.round(djli + il)]
                            ax = x0 + il
                            ay = ah * mh
                            az = z0 + jl

                            bh = data[Math.round(djli1 + il)]
                            bx = x0 + il
                            by = bh * mh
                            bz = z0 + jl1

                            ch = data[Math.round(djli1 + il1)]
                            cx = x0 + il1
                            cy = ch * mh
                            cz = z0 + jl1

                            dh = data[Math.round(djli + il1)]
                            dx = x0 + il1
                            dy = dh * mh
                            dz = z0 + jl

                            // a d
                            // b c

                            positions.push(ax, ay, az, bx, by, bz, dx, dy, dz)
                            positions.push(bx, by, bz, cx, cy, cz, dx, dy, dz)

                            if (colored) {
                                colorize(ah)
                                colors.push(color.r / 255, color.g / 255, color.b / 255)

                                colorize(bh)
                                br = color.r / 255
                                bg = color.g / 255
                                bb = color.b / 255
                                colors.push(br, bg, bb)

                                colorize(dh)
                                dr = color.r / 255
                                dg = color.g / 255
                                db = color.b / 255
                                colors.push(dr, dg, db)

                                colors.push(br, bg, bb)

                                colorize(ch)
                                colors.push(color.r / 255, color.g / 255, color.b / 255)

                                colors.push(dr, dg, db)
                            }
                        }
                    }
                })
            }

        }

        geometry.addAttribute('position', new Float32BufferAttribute(positions, 3))
        if (colored) geometry.addAttribute('color', new Float32BufferAttribute(colors, 3))

        if (faces) {
            withTime('computeVertexNormals', () =>
                geometry.computeVertexNormals())
            withTime('computeBoundingSphere', () =>
                geometry.computeBoundingSphere())

            MeshBuilder.say(log && debug,
                'boundingSphere', geometry.boundingSphere,
                'normals', geometry.getAttribute('normal').count)
        }

        MeshBuilder.say(log, 'geometry built')
        return geometry
    }

}