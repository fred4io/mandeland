import { Material, Color, Geometry, Vector3, LineBasicMaterial, LineSegments, LineDashedMaterial, ShaderMaterial, LineMaterialType } from "three";

declare type TColor = string | number | Color


export class Helpers {

    static createCrosshair(length: number,
        colorOrMaterial: TColor | LineMaterialType,
        name: string) {

        var l = length / 2, geometry = new Geometry();

        geometry.vertices.push(
            new Vector3(-l, 0, 0), new Vector3(l, 0, 0),
            new Vector3(0, -l, 0), new Vector3(0, l, 0),
            new Vector3(0, 0, -l), new Vector3(0, 0, l)
        );

        const material =
            (colorOrMaterial && (colorOrMaterial as Material).isMaterial) ?
                colorOrMaterial as LineMaterialType
                : new LineBasicMaterial(
                    { color: colorOrMaterial as Color | number || this.randomColor() });

        var mesh = new LineSegments(geometry, material);

        mesh.name = name || 'crosshair';

        return mesh;
    }


    static randomColor() {
        return this.randomMinMaxInt(0, 0xffffff);
    }
    static randomMinMaxInt(min: number, max: number) {
        min = Math.ceil(min); max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}