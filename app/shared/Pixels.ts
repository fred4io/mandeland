import { Easer, Easing } from './Easing';
import { Color, IColor } from './Color'
declare type StrokeStyle = string | { r: number, g: number, b: number, a?: number }

function rgba(c: StrokeStyle) { return typeof c == 'string' ? c : Color.text(c.r, c.g, c.b, c.a) }

export class Pixels {

    static noLog = true

    static clear(ctx: CanvasRenderingContext2D, c: IColor = Color.Black) {
        ctx.beginPath()
        ctx.fillStyle = c.toString()
        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    }

    static clearAlpha(ctx: CanvasRenderingContext2D, c = 0) {
        if (!Pixels.noLog) console.log('Pixels clearAlpha', c)
        Pixels.withImageData(ctx, imd => Pixels.setAlpha(imd, c))
    }

    static withImageData(ctx: CanvasRenderingContext2D, withData: (imd: ImageData) => void) {
        const w = ctx.canvas.width, h = ctx.canvas.height
        //console.log('Pixels withImageData', w, h)
        const imd = ctx.getImageData(0, 0, w, h)
        //console.log('withImageData before', imd.data)
        withData(imd)
        //console.log('withImageData after', imd.data)
        ctx.putImageData(imd, 0, 0)
    }

    static setAlpha(imd: ImageData, alpha: number = 0) {
        const data = imd.data, l = data.length
        for (let i = 0; i < l; i += 4) {
            data[i + 3] = alpha
        }
    }


    static invert(ctx: CanvasRenderingContext2D) {
        if (!Pixels.noLog) console.log('Pixels invert')
        Pixels.withImageData(ctx, imd => {
            const data = imd.data, l = data.length
            for (var i = 0; i < l; i += 4) {
                data[i] = 255 - data[i] // red
                data[i + 1] = 255 - data[i + 1] // green
                data[i + 2] = 255 - data[i + 2] // blue
            }
        })
    }

    static getColoriser(c: Color,
        colors: boolean | ((v: number, c: IColor) => void),
        invert?: boolean, easing?: Easer
    ) {
        return typeof colors == 'boolean' ?
            Color.getColorizer(c, colors, invert, easing)
            : invert ?
                (v: number) => { colors(v, c); c.invert() }
                : (v: number) => colors(v, c)
    }

    static drawHeightMap(ctx: CanvasRenderingContext2D, data: Float32Array, width: number,
        colors: boolean | ((v: number, c: Color) => void),
        invert?: boolean, easing?: Easer, alpha01?: number,
        threshold = 0, alphaIfUnderThreshold?: number
    ) {
        const height = data.length / width,
            c = new Color().setAlpha01(alpha01),
            colorize = Pixels.getColoriser(c, colors, invert, easing),
            alpha = Math.floor(255 * alphaIfUnderThreshold)

        Pixels.withImageData(ctx, imd => {
            const img = imd.data
            for (let y = 0, yw: number, yw4: number; y < height; y++) {
                yw = y * width, yw4 = yw * 4
                for (let x = 0; x < width; x++) {
                    const v = data[yw + x]
                    if (threshold && v < threshold) {
                        if (alphaIfUnderThreshold != undefined)
                            img[yw4 + x * 4 + 3] = alpha
                        continue
                    }
                    colorize(v)
                    const i = yw4 + x * 4
                    img[i] = c.r
                    img[i + 1] = c.g
                    img[i + 2] = c.b
                    img[i + 3] = c.a
                }
            }
        })
    }

    static imageFromHeightMap(data: Float32Array, width: number,
        colors: boolean | ((v: number, c: Color) => void),
        invert?: boolean, easing?: Easer,
        alpha01?: number
    ) {
        width = Math.round(width)
        const height = data.length / width,
            imd = new ImageData(width, height),
            d = imd.data,
            c = new Color().setAlpha01(alpha01),
            colorize = Pixels.getColoriser(c, colors, invert, easing)

        for (let y = 0, yw: number, yw4: number; y < height; y++) {
            yw = y * width, yw4 = yw * 4
            for (let x = 0; x < width; x++) {
                colorize(data[yw + x])
                const i = yw4 + x * 4
                d[i] = c.r
                d[i + 1] = c.g
                d[i + 2] = c.b
                d[i + 3] = c.a
            }
        }

        return imd
    }

    static drawCircle(ctx: CanvasRenderingContext2D, r?: number, x?: number, y?: number, strokeStyle?: StrokeStyle) {
        if (!Pixels.noLog) console.log('Pixels drawCircle', r)
        ctx.beginPath()
        if (strokeStyle) { ctx.strokeStyle = rgba(strokeStyle) }
        const c = ctx.canvas
        if (x == undefined) { x = c.width / 2 }
        if (y == undefined) { y = c.height / 2 }
        if (r == undefined) { r = Math.min(c.width, c.height) / 2 }
        ctx.arc(x - .5, y - .5, r, 0, 2 * Math.PI)
        ctx.stroke()
    }

    static drawLine(ctx: CanvasRenderingContext2D, x1: number, y1: number, x2: number, y2: number, strokeStyle?: StrokeStyle) {
        if (!Pixels.noLog) console.log('Pixels drawLine', x1, y1, x2, y2)
        ctx.beginPath()
        if (strokeStyle) { ctx.strokeStyle = rgba(strokeStyle) }
        ctx.moveTo(x1 - .5, y1 - .5)
        ctx.lineTo(x2 - .5, y2 - .5)
        ctx.stroke()
    }

    static drawBox(ctx: CanvasRenderingContext2D, r: { xmin: number, ymin: number, xmax: number, ymax: number }, strokeStyle?: StrokeStyle, fill = false) {
        if (!r) return
        Pixels.drawRect(ctx, r.xmin, r.ymin, r.xmax, r.ymax, strokeStyle, fill)
    }

    static drawRect(ctx: CanvasRenderingContext2D, x1: number, y1: number, x2: number, y2: number, strokeStyle?: StrokeStyle, fill = false) {
        if (!Pixels.noLog) console.log('Pixels drawRect', x1, y1, x2, y2)
        ctx.beginPath()
        if (strokeStyle)
            if (fill) ctx.fillStyle = rgba(strokeStyle)
            else ctx.strokeStyle = rgba(strokeStyle)
        if (fill) ctx.fillRect(x1 - .5, y1 - .5, x2 - x1, y2 - y1)
        else ctx.strokeRect(x1 - .5, y1 - .5, x2 - x1, y2 - y1)
    }

    static randomCross(ctx: CanvasRenderingContext2D, r = 5, strokeStyle: string | Color = 'rgba(255,255,255,255)') {
        const x = Math.random() * ctx.canvas.width, y = Math.random() * ctx.canvas.height
        this.drawCross(ctx, x, y, r, strokeStyle)
        return { x, y }
    }

    static drawCross(ctx: CanvasRenderingContext2D, cx: number, cy: number, r: number, strokeStyle?: StrokeStyle) {
        if (!Pixels.noLog) console.log('Pixels drawCross', cx, cy, r)
        ctx.beginPath()
        if (strokeStyle) { ctx.strokeStyle = rgba(strokeStyle) }
        ctx.moveTo(cx - .5, cy - r - .5)
        ctx.lineTo(cx - .5, cy + r - .5)
        ctx.moveTo(cx - r - .5, cy - .5)
        ctx.lineTo(cx + r - .5, cy - .5)
        ctx.stroke()
    }

    static drawPoly(ctx: CanvasRenderingContext2D, xys: Array<number>, strokeStyle?: StrokeStyle, fill = false, close = fill) {
        if (!Pixels.noLog) console.log('Pixels drawPoly', xys)
        ctx.beginPath()
        if (strokeStyle)
            if (fill) ctx.fillStyle = rgba(strokeStyle)
            else ctx.strokeStyle = rgba(strokeStyle)
        const l = xys.length, correct = true
        if (correct) {
            ctx.moveTo(xys[0] - .5, xys[1] - .5)
            for (let i = 2; i < l; i += 2)
                ctx.lineTo(xys[i] - .5, xys[i + 1] - .5)
        }
        else {
            ctx.moveTo(xys[0], xys[1])
            for (let i = 2; i < l; i += 2)
                ctx.lineTo(xys[i], xys[i + 1])
        }
        if (close) ctx.closePath()
        if (fill) ctx.fill()
        else ctx.stroke()
    }
    static drawGradientPoly(ctx: CanvasRenderingContext2D, xys: Array<number>) {
        const c = new Color()
        const l = xys.length
        for (let i = 2; i < l; i += 2) {
            ctx.beginPath()
            ctx.strokeStyle = c.gradient7(i / l).toString()
            ctx.moveTo(xys[i - 2] - .5, xys[i - 1] - .5)
            ctx.lineTo(xys[i] - .5, xys[i + 1] - .5)
            ctx.stroke()
        }
    }

    static drawCrossBox(ctx: CanvasRenderingContext2D,
        xmin: number, xmax: number,
        ymin: number, ymax: number,
        minSize: number = 0,
        strokeStyle?: StrokeStyle
    ) {
        xmin = Math.round(xmin), xmax = Math.round(xmax), ymin = Math.round(ymin), ymax = Math.round(ymax)
        //console.log('drawCrossBox', xmin, xmax, ymin, ymax)
        ctx.beginPath()
        if (strokeStyle) { ctx.strokeStyle = rgba(strokeStyle) }
        const w = xmax - xmin, h = ymax - ymin
        ctx.strokeRect(xmin - .5, ymin - .5, w, h)
        if (!(minSize > 0 && (w < minSize || h < minSize))) { return }
        let cx = xmin + w / 2, cy = ymin + h / 2
        if (w % 2 != 0) cx -= .5; if (h % 2 != 0) cy -= .5
        ctx.moveTo(cx - .5, 0); ctx.lineTo(cx - .5, ymin - .5)
        ctx.moveTo(cx - .5, ctx.canvas.height); ctx.lineTo(cx - .5, ymax - .5)
        ctx.moveTo(0, cy - .5); ctx.lineTo(xmin - .5, cy - .5)
        ctx.moveTo(xmax, cy - .5); ctx.lineTo(ctx.canvas.width, cy - .5)
        ctx.stroke()
    }
}
