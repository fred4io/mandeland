declare type Command = {
    key: string | number,
    action?: (shift?: boolean, ctrl?: boolean, alt?: boolean, meta?: boolean, digit?: number) => any,
    doc?: string, default?: string, shift?: string, ctrl?: string, alt?: string, meta?: string
    special?: boolean,
    section?: string
}

export class KeyCommands {

    static readonly _arrows = -2
    static readonly _digits = -1
    static readonly _tab = 9
    static readonly _shift = 16
    static readonly _alt = 17
    static readonly _ctrl = 18
    static readonly _esc = 27
    static readonly _left = 37
    static readonly _up = 38
    static readonly _right = 39
    static readonly _down = 40
    static readonly _meta = 91
    static readonly _lt = 188

    private static readonly _digitChars = Array.from('0123456789')

    private static readonly _mods =
        [
            { s: 'shift', v: KeyCommands._shift },
            { s: 'alt', v: KeyCommands._alt },
            { s: 'ctrl', v: KeyCommands._ctrl },
            { s: 'meta', v: KeyCommands._meta }
        ]

    private static _ks = new Array<string>()
    private static modedKeyName(c: number, kc: string | number, e: KeyboardEvent) {
        this._ks.length = 0
        const b = this._mods.reduce((p, m) => {
            if ((<any>e)[m.s + 'Key']) this._ks.push(m.s)
            return p && c != m.v
        }, true)
        if (b) this._ks.push('' + kc)
        return this._ks.join('-')
    }

    private kd: EventListener
    private isAttached: boolean

    bypass = false

    constructor(
        public commands: Command[],
        public inBrowser = false,
        public bypassKey?: string | number,
        public bypassed = (e: KeyboardEvent) => { },
        public log = true,
        public activateSpecials = false
    ) {
        const self = this
        this.kd = (e: KeyboardEvent) => self.onKeyDown(e)
        this.attach()
    }

    public attach() {
        if (this.isAttached) { return }
        window.addEventListener('keydown', this.kd)
        this.isAttached = true
    }

    public detach() {
        if (!this.isAttached) { return }
        window.removeEventListener('keydown', this.kd)
        this.isAttached = false
    }

    public getDoc(includeSpecials = false) {
        const docs = this.commands.filter(c => !!c.doc && (includeSpecials || !c.special))
        const makeDoc = (c: Command) => ({
            key: this.keyName(c.key),
            section: c.section,
            special: c.special,
            doc: c.doc
                + (c.default ? (' ' + c.default) : '')
                + (c.shift ? (', shift: ' + c.shift) : '')
                + (c.ctrl ? (', ctrl: ' + c.ctrl) : '')
                + (c.alt ? (', alt: ' + c.alt) : '')
                + (c.meta ? (', meta: ' + c.meta) : '')
        })
        return docs.map(makeDoc)
    }

    private keyName(k: string | number) {
        if (typeof k == 'string') { return k }
        switch (k) {
            case KeyCommands._arrows: return 'arrows'
            case KeyCommands._digits: return 'digits'
            case KeyCommands._tab: return 'tab'
            case KeyCommands._shift: return 'shift'
            case KeyCommands._alt: return 'alt'
            case KeyCommands._ctrl: return 'ctrl'
            case KeyCommands._esc: return 'esc'
            case KeyCommands._left: return 'left arrow'
            case KeyCommands._up: return 'up arrow'
            case KeyCommands._right: return 'right arrow'
            case KeyCommands._down: return 'down arrow'
            case KeyCommands._meta: return 'meta/windows'
            case KeyCommands._lt: return '<'
        }
    }

    private _onUserEvent(e: KeyboardEvent) { if (this.bypassed) this.bypassed(e) }

    private onKeyDown(e: KeyboardEvent) {
        const c = e.charCode || e.keyCode, k = String.fromCharCode(c)
        const kc = /[A-Z|0-9]/.test(k) ? k : c
        if (this.bypass) {
            this._onUserEvent(e)
            if (k != this.bypassKey) return
        }
        const shift = e.shiftKey, ctrl = e.ctrlKey, alt = e.altKey, meta = e.metaKey
        if (this.isModifier(kc)
            || this.isOs(kc, shift, ctrl, alt, meta)
            || this.inBrowser && this.isBrowser(kc, shift, ctrl, alt, meta)
        ) {
            return
        }

        const action = (filter: (cmd: Command) => boolean, isDigit = false) => {
            const cmd = this.commands.find(c => filter(c))
            if (!cmd) return false
            if (cmd.special && !this.activateSpecials) return true
            if (isDigit) cmd.action(shift, ctrl, alt, meta, parseInt(<string>kc, 10))
            else cmd.action(shift, ctrl, alt, meta)
            return true
        }

        const mkn = KeyCommands.modedKeyName(c, kc, e)
        if (!action(c => c.key == mkn)
            && (!this.isDigit(k) || !action(c => c.key == KeyCommands._digits, true))
            && !action(c => c.key == kc)
            && this.log
        ) console.log('key: ' + k, c)
    }


    private isModifier(k: string | number) {
        return k == KeyCommands._shift
            || k == KeyCommands._ctrl
            || k == KeyCommands._alt
            || k == KeyCommands._meta
    }

    private isDigit(k: string | number) {
        return typeof k == 'string' && KeyCommands._digitChars.some(d => d == k)
    }

    private isOs(k: string | number, shift: boolean, ctrl: boolean, alt: boolean, meta: boolean) {
        return ctrl && k == KeyCommands._tab
    }

    private isBrowser(k: string | number, shift: boolean, ctrl: boolean, alt: boolean, meta: boolean) {
        return meta && k == 'R'
            || alt && meta && k == 'I'

    }

}