import { SweepContext, PointError, Triangle } from "poly2tri"
import { Zoning } from "./Zoning"

type TIndicedPoint = { x: number, y: number, i: number }

export type TBoundingBox = { xmin: number, xmax: number, ymin: number, ymax: number }

export class Triangulation {

    static triangulate(contour: number[], width: number, lod: number,
        steinerPointsIndices: number[],
        log = false, maxRemoveBadPoints = 100
    ): TriResult {
        if (!contour) return
        const indices = Zoning.removeColinears(contour, width)
        if (!indices) return

        function getPoints(indices: number[]) {
            return indices.map(i => ({
                x: (i % width) * lod,
                y: ~~(i / width) * lod,
                i
            }))
        }

        const steinerPoints =
            steinerPointsIndices
            && steinerPointsIndices.length
            && getPoints(steinerPointsIndices)

        let sc: SweepContext, bps = new Array<number>()
        while (--maxRemoveBadPoints) {
            try {
                sc = new SweepContext(getPoints(indices))
                if (steinerPoints) sc.addPoints(steinerPoints)
                sc.triangulate()
                break
            }
            catch (e) {
                if (log) console.warn(e)
                const pe = e as PointError, pps = pe && pe.points as TIndicedPoint[],
                    ids = pps && pps.length && pps.map(p => p.i)
                if (ids) {
                    //if triplet remove mid point
                    const pi = ids[ids.length === 3 ? 1 : 0]
                    const ii = indices.indexOf(pi)
                    indices.splice(ii, 1)
                    bps.push(pi)
                }
            }
        }

        return new TriResult(
            sc.getTriangles(),
            bps && bps.length ? bps : undefined,
            steinerPointsIndices)
    }

}

export class TriResult {

    get nbTriangles() { return this.tris && this.tris.length || 0 }

    constructor(
        private tris: Triangle[],
        public badPoints?: number[],
        public steinerPoints?: number[]
    ) { }

    /** [ia, ib, ic,  ia, ib, ic ] */
    getTriIndices(result = new Array<number>()) {
        const tris = this.tris
        for (let i = 0, l = tris.length; i < l; i++) {
            const t = tris[i]
            result.push( //2, 1, 0 to get normal vectors upwards
                (<TIndicedPoint>t.getPoint(2)).i,
                (<TIndicedPoint>t.getPoint(1)).i,
                (<TIndicedPoint>t.getPoint(0)).i
            )
        }
        return result
    }

    /** [[[x,y], [x,y], [x,y]], [[x,y], [x,y], [x,y]], ...] */
    getTriangles(result = new Array<Array<Array<number>>>()) {
        const tris = this.tris
        for (let i = 0, l = tris.length; i < l; i++) {
            const t = tris[i].getPoints(),
                a = t[0], b = t[1], c = t[2]
            result.push([[a.x, a.y], [b.x, b.y], [c.x, c.y]])
        }
        return result
    }

    /** [[xa,ya,xb,yb,xc,yc], [xa,ya,xb,yb,xc,yc], ...] */
    getTrianglesXys(result = new Array<Array<number>>()) {
        const triangles = this.tris
        for (let i = 0, l = triangles && triangles.length || 0; i < l; i++) {
            const t = triangles[i].getPoints(),
                a = t[0], b = t[1], c = t[2]
            result.push([a.x, a.y, b.x, b.y, c.x, c.y])
        }
        return result
    }
}
