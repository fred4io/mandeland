import { Matrix2D } from "./Matrix2D";
import { Region2D } from "./Region2D";

declare type Point = { x: number, y: number }
declare type Rect = { x: number, y: number, width: number, height: number }
declare type Region = { xmin: number, xmax: number, ymin: number, ymax: number }

export class Transform2D {

    static makeTarget(wx: number, wy: number, zoom: number, vw: number, vh: number, result?: Region2D) {
        const mvw = this.makeViewToWorld(vw, vh), mwt = this.makeWorldToTarget(wx, wy, zoom)
        result = result || Region2D.fromScreen(vw, vh)
        return this.transformToTarget(result, mvw, mwt)
    }

    static makeViewToWorld(vw: number, vh: number, 
        ww = 4, wh = 4, worldIsZeroCentered = true, worldTopIsYmax = true
    ) {
        return Matrix2D.transformLR(
            Matrix2D.translation(-vw / 2, -vh / 2),
            worldTopIsYmax ? Matrix2D.flipY() : null,
            Matrix2D.scale(Region2D.scaleFactorToFit(true, vw, vh, ww, wh)),
            worldIsZeroCentered ? null : Matrix2D.translation(-ww / 2, -wh / 2)
        )
    }
    static makeWorldToTarget(wx: number, wy: number, zoom: number) {
        return Matrix2D.transformLR(
            Matrix2D.scale(1 / zoom),
            Matrix2D.translation(wx, wy))
    }
    static makeViewToTarget(mvw: Matrix2D, mwt: Matrix2D) {
        return Matrix2D.transformLR(mvw, mwt)
    }

    static transformToTarget(vr: Region2D, mvw: Matrix2D, mwt: Matrix2D, flipY = true) {
        const tr = this.makeViewToTarget(mvw, mwt).transformRegion(vr)
        if (flipY) { tr.flipY() }
        return tr
    }

    private mvw: Matrix2D
    private mwt: Matrix2D
    private mvt: Matrix2D
    private mtv: Matrix2D

    constructor(
        private vw: number,
        private vh: number,
        private ww: number = 4,
        private wh: number = 4,
        private worldIsZeroCentered = true,
        private worldTopIsYmax = true
    ) {
        this.mwt = Transform2D.makeWorldToTarget(0, 0, 1)
        this.updateView(vw, vh)
    }

    updateView(vw: number, vh: number) {
        this.vw = vw, this.vh = vh
        this.mvw = Transform2D.makeViewToWorld(vw, vh, this.ww, this.wh, this.worldIsZeroCentered, this.worldTopIsYmax)
        this.mvt = Transform2D.makeViewToTarget(this.mvw, this.mwt)
        this.mtv = this.mvt.getInverse()
        return this
    }

    setTarget(wx: number, wy: number, zoom?: number) {
        const mwt = Transform2D.makeWorldToTarget(wx, wy, zoom || 1)
        return this.setTargetTransform(mwt)
    }

    setTargetTransform(mwt: Matrix2D) {
        this.mwt = mwt
        this.mvt = Transform2D.makeViewToTarget(this.mvw, this.mwt)
        this.mtv = this.mvt.getInverse()
        return this
    }

    toTarget(vr: Region2D, flipY: boolean) {
        return this.mvt.transformRegion(vr).flip(false, flipY)
    }

    toOtherTarget(vr: Region2D, wx: number, wy: number, zoom: number, flipY: boolean) {
        const mvt = Transform2D.makeViewToTarget(this.mvw, Transform2D.makeWorldToTarget(wx, wy, zoom))
        return mvt.transformRegion(vr).flip(false, flipY)
    }

    xToTarget(x: number, y: number) {
        return this.mvt.transformX(x, y)
    }
    yToTarget(x: number, y: number) {
        return this.mvt.transformY(x, y)
    }
    pointToTarget<T extends Point>(p: T): T {
        return this.mvt.transformPoint(p)
    }
    regionToTarget<T extends Region>(r: T): T {
        return this.mvt.transformRegion(r)
    }
    rectToTarget<T extends Rect>(r: T): T {
        return this.mvt.transformRect(r)
    }
    xysToTarget(xys: Array<number>, result?: Array<number>) {
        return this.mvt.transformXYs(xys, result)
    }

    toView(rt: Region2D, flipY: boolean) {
        return this.mtv.transformRegion(rt).flip(false, flipY)
    }
    xToView(x: number, y: number) {
        return this.mtv.transformX(x, y)
    }
    yToView(x: number, y: number) {
        return this.mtv.transformY(x, y)
    }
    pointToView<T extends Point>(p: T): T {
        return this.mtv.transformPoint(p)
    }
    regionToView<T extends Region>(r: T): T {
        return this.mtv.transformRegion(r)
    }
    rectToView<T extends Rect>(r: T): T {
        return this.mtv.transformRect(r)
    }
    xysToView(xys: Array<number>, result?: Array<number>) {
        return this.mtv.transformXYs(xys, result)
    }

    locFromViewRect(r: Rect, currentZoom: number) {
        const rv = Region2D.fromRect(r),
            zoomFactor = Region2D.scaleFactorToFit(false, rv.width, rv.height, this.vw, this.vh)
        return this.locFromView(rv.cx, rv.cy, currentZoom, zoomFactor)
    }
    locFromView(x: number, y: number, currentZoom: number, zoomFactor = 2, inplace = false) {
        if (inplace) {
            const v = Region2D.fromScreen(this.vw, this.vh),
                c = this.pointToTarget(v.center()),
                p = this.pointToTarget({ x, y }),
                tx = (c.x - p.x) * currentZoom,
                ty = (c.y - p.y) * currentZoom,
                newZoom = currentZoom * zoomFactor,
                mwt = Matrix2D.transformLR(
                    Matrix2D.translation(tx, ty),
                    Matrix2D.scale(1 / newZoom),
                    Matrix2D.translation(p.x, p.y)
                ),
                mvt = Transform2D.makeViewToTarget(this.mvw, mwt),
                q = mvt.transformPoint(v.center())
            return { a: q.x, b: q.y, z: newZoom }
        } else {
            return {
                a: this.xToTarget(x, y),
                b: this.yToTarget(x, y),
                z: currentZoom * zoomFactor
            }
        }
    }
}