
export enum ContourMethod {
    NoContour = 0,
    Moore,
    Square
}

export class Zoning {

    /** flood fill, DFS */
    static getZone(
        startPointIndex: number,
        pointLabel: number,
        zoneLabel: number,
        pointsIndex: Uint32Array,
        zonesIndex: Uint32Array,
        defaultZoneLabel: number,
        paddedWidth: number,
        zone: Array<number>,
        edges: Array<number>,
        pointTypeIndex?: Uint32Array,
        PT_INTERIOR?: number,
        PT_EDGE?: number,
        neighborZones?: Set<number>
    ) {
        edges.length = 0
        const nrs = [1, paddedWidth, -1, -paddedWidth] // relative von neumann neighborhood; E, S, W, N
        const stack = new Array<number>()
        stack.push(startPointIndex)
        while (stack.length) { //at least one point to visit

            const pi = stack.pop()

            if (zonesIndex[pi] != defaultZoneLabel) continue //already visited

            //mark point as visited
            zonesIndex[pi] = zoneLabel
            zone.push(pi)

            //visit each of its 4 neighbours
            let nn = 0
            for (let inr = 0; inr < 4; inr++) {
                const ni = pi + nrs[inr]
                if (pointsIndex[ni] == pointLabel) { //same colour
                    nn++
                    if (zonesIndex[ni] == defaultZoneLabel) //not visited yet
                        stack.push(ni) //add it to visit
                } else { //other zone point
                    if (neighborZones && zonesIndex[ni] != defaultZoneLabel)
                        neighborZones.add(zonesIndex[ni])
                }
            }

            if (nn < 4) { //less than 4 neighbours, it's an edge
                edges.push(pi)
                if (pointTypeIndex) pointTypeIndex[pi] = PT_EDGE
            }
            else if (pointTypeIndex) pointTypeIndex[pi] = PT_INTERIOR
        }
    }

    static getContour(firstEdgeIdx: number,
        zoneIdx: number,
        zoneIndex: Uint32Array,
        paddedWidth: number,
        contourMethod = ContourMethod.Moore,
        removeColinears: boolean = false,
    ) {
        let contour = contourMethod == ContourMethod.Moore ?
            Zoning.mooreContour(firstEdgeIdx, zoneIdx, zoneIndex, paddedWidth)
            : Zoning.squareContour(firstEdgeIdx, zoneIdx, zoneIndex, paddedWidth)

        if (contour.length > 1) {
            if (contour[contour.length - 1] == contour[0]) {
                //duplicate first/last point
                contour.splice(contour.length - 1)
            }
            //first point is second
            contour.unshift(...contour.splice(contour.length - 1)) //moves last to first

            if (removeColinears)
                contour = Zoning.removeColinears(contour, paddedWidth)
        }
        return contour
    }

    private static squareContour(firstEdgePointIndex: number,
        zoneIdx: number,
        zoneIndex: Uint32Array,
        paddedWidth: number
    ) {

        ///http://www.imageprocessingplace.com/downloads_V3/root_downloads/tutorials/contour_tracing_Abeer_George_Ghuneim/square.html

        const w = paddedWidth,
            nrs = [1, w, -1, -w] // relative von neumann neighborhood; E, S, W, N

        let p: number, d: number
        for (d = 0; d < 4; d++) {
            p = firstEdgePointIndex + nrs[d]
            if (zoneIndex[p] == zoneIdx)
                break
        }

        const p0 = p, d0 = d, boundary = new Array<number>()
        boundary.push(p0)

        do {
            if (zoneIndex[p] == zoneIdx) {
                boundary.push(p)
                d = d == 0 ? 3 : d - 1
            }
            else d = (d + 1) % 4
            p += nrs[d]
        } while (p != p0 || d != d0)

        return boundary
    }

    private static mooreContour(firstEdgePointIndex: number,
        zoneIdx: number,
        zoneIndex: Uint32Array,
        paddedWidth: number,
    ) {

        /// https://fr.mathworks.com/matlabcentral/fileexchange/42144-moore-neighbor-boundary-trace

        const initial_entry = firstEdgePointIndex

        /// Designate a directional offset array for search positions
        const w = paddedWidth
        const neighborhood = [-1, -w - 1, -w, -w + 1, 1, w + 1, w, w - 1]
        const exit_direction = [7, 7, 1, 1, 3, 3, 5, 5]

        /// Find the first point in the boundary, Moore-Neighbor of entry point
        let c: number, n: number, found = false
        for (n = 0; n < 8; n++) {
            c = initial_entry + neighborhood[n]
            if (zoneIndex[c] == zoneIdx) {
                found = true
                break
            }
        }
        if (!found) { return [initial_entry] }
        
        const initial_position = c

        /// Set next direction based on found pixel ( i.e. 3 -> 1)
        const initial_direction = exit_direction[n]

        /// Start the boundary set with this pixel
        const boundary = new Array<number>()
        boundary.push(initial_position)

        /// Initialize variables for boundary search
        let position = initial_position
        let direction = initial_direction

        /// Return a list of the ordered boundary pixels
        while (true) {

            /// Find the next neighbor with a clockwise search
            for (let i = 0; i < 8; i++) {
                n = (i + direction) % 8
                c = position + neighborhood[n]
                if (zoneIndex[c] == zoneIdx) {
                    position = c
                    break
                }
            }

            /// Neighbor found, save its information
            direction = exit_direction[n]
            boundary.push(position)

            /// Entered the initial pixel the same way twice, the end
            if (position == initial_position && direction == initial_direction)
                break
        }

        return boundary
    }

    static removeColinears(indices: number[], width: number,
        isClosed = true, result = new Array<number>()
    ) {
        if (!indices) return result
        if (indices.length < 3) { result.push(...indices); return result }

        let x1: number, y1: number
        let x2: number, y2: number, i2: number

        //for each point of contour (and back to start if closed)
        for (let i = 0, l = indices.length, il = isClosed ? (l + 1) : l; i < il; i++) {

            let i3 = indices[i % l] //if closed, last point is first point
            let x3 = i3 % width     //x coordinate
            let y3 = ~~(i3 / width) //y coordinate

            if (i < 2) {                        //init phase
                if (i == 1) {
                    i2 = i3, x2 = x3, y2 = y3   //second point of first triplet
                } else { //i == 0
                    result.push(i3)             //store first point index
                    x1 = x3, y1 = y3            //first point of first triplet
                }
                continue                        //3 points needed to check colinearity
            }

            if ((y1 - y2) * (x1 - x3) == (y1 - y3) * (x1 - x2)) {
                //colinears => forget middle point
            } else {
                //not colinears
                result.push(i2)             //store middle point index
                x1 = x2, y1 = y2            //second becomes first
            }

            i2 = i3, x2 = x3, y2 = y3       //third becomes second
        }

        return result
    }

}