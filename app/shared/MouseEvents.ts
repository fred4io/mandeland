
export class MouseEvents {

    private cl: EventListener
    private dc: EventListener
    private mm: EventListener
    private md: EventListener
    private cm: EventListener
    private isAttached: boolean
    private mdown: number
    private clicking: any

    constructor(
        private element: HTMLElement,
        public onClick?: (x: number, y: number, e?: MouseEvent) => void,
        public onDblClick?: (x: number, y: number, e?: MouseEvent) => void,
        public onMove?: (x: number, y: number, e?: MouseEvent) => void,
    ) {
        const self = this
        this.cl = (e: TMouseEvent) => { self._onClick(e) }
        this.dc = (e: TMouseEvent) => { self._onDblClick(e) }
        this.mm = (e: TMouseEvent) => { self._onMouseMove(e) }
        this.md = (e: MouseEvent) => { self.mdown = performance.now() }
        this.cm = (e: MouseEvent) => { e.preventDefault() }
        this.attach()
    }

    attach() {
        if (this.isAttached || !this.element) { return }
        this.element.addEventListener('click', this.cl)
        this.element.addEventListener('dblclick', this.dc)
        this.element.addEventListener('mousemove', this.mm)
        this.element.addEventListener('mousedown', this.md)
        this.element.addEventListener('contextmenu', this.cm)
        this.isAttached = true
    }
    detach() {
        if (!this.isAttached || !this.element) { return }
        this.element.removeEventListener('click', this.cl)
        this.element.removeEventListener('dblclick', this.dc)
        this.element.removeEventListener('mousemove', this.mm)
        this.element.removeEventListener('mousedown', this.md)
        this.element.removeEventListener('contextmenu', this.cm)
        this.isAttached = false
    }

    private _onClick(e: TMouseEvent) {
        clearTimeout(this.clicking)
        if (this.mdown && performance.now() - this.mdown > 700) { return }
        this.clicking = setTimeout(() => {
            if (this.onClick) this.onClick(e.layerX, e.layerY, e)
        }, 200)
    }
    private _onDblClick(e: TMouseEvent) {
        clearTimeout(this.clicking)
        if (this.onDblClick) this.onDblClick(e.layerX, e.layerY, e)
    }
    private _onMouseMove(e: TMouseEvent) {
        if (this.onMove) { this.onMove(e.layerX, e.layerY, e) }
    }

}

export type TMouseEvent = MouseEvent & { layerX?: number, layerY?: number }