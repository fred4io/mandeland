import { TMouseEvent } from "./MouseEvents"

export interface IUserEvents {
    onMove?: (x: number, y: number) => void,
    onKey?: (k: string) => void,
    onSelect?: (x: number, y: number, w: number, h: number) => void,
    onDetach?: () => void,
}
export class UserEvents implements IUserEvents {
    private static _mods =
        [
            { s: 'shift', v: 16 },
            { s: 'alt', v: 17 },
            { s: 'ctrl', v: 18 },
            { s: 'meta', v: 91 }
        ]

    public onMove: (x: number, y: number) => void
    public onKey: (k: string) => void
    public onSelect: (x: number, y: number, w: number, h: number) => void
    public onDetach: () => void

    public doMove(e: TMouseEvent) {
        if (!this.onMove) { return }
        try { this.onMove(e.layerX, e.layerY) }
        catch (e) { console.warn('onMove', e) }
    }
    public doKey(e: KeyboardEvent) {
        if (!this.onKey) { return }
        const c = e.charCode || e.keyCode, k = String.fromCharCode(c),
            kc = /[A-Z|0-9]/.test(k) ? k : c, ks = new Array<string>()
        if (UserEvents._mods.reduce((p, m) => {
            if ((<any>e)[m.s + 'Key']) { ks.push(m.s) } return p && c != m.v
        }, true)) { ks.push('' + kc) }
        try { this.onKey(ks.join('-')) }
        catch (e) { console.warn('onKey', e) }
    }
    public doSelect(x: number, y: number, w: number, h: number) {
        if (!this.onSelect) { return }
        try { this.onSelect(x, y, w, h) }
        catch (e) { console.warn('onSelect', e) }
    }
    public doDetach() {
        this.onMove = this.onKey = this.onSelect = undefined
        if (!this.onDetach) { return }
        try { this.onDetach() }
        catch (e) { console.warn('doDetach', e) }
    }

}