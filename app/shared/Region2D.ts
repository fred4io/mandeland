declare type Point = { x: number, y: number }
declare type Size = { width: number, height: number }
declare type Rect = { x: number, y: number, width: number, height: number }
declare type Region = { xmin: number, xmax: number, ymin: number, ymax: number }
declare type OrientedRegion = { xmin: number, xmax: number, ymin: number, ymax: number, topIsYMax?: boolean }
declare type CenteredRegion = { cx: number, cy: number, width: number, height: number }
declare type TopLeftRegion = { width: number, height: number, top: number, left: number }
declare type StrokeStyle = string | { r: number, g: number, b: number, a?: number }
declare type Grid = { parts: Region2D[], nx: number, ny: number }

function rgba(ss: StrokeStyle) {
    return typeof ss == 'string' ? ss
        : ss.a == undefined ? `rgb(${ss.r},${ss.g},${ss.b})`
            : `rgba(${ss.r},${ss.g},${ss.b},${(ss.a / 255).toFixed(2)})`
}

export class Region2D {

    private static say(log: boolean, ...args: any[]) {
        if (log) { console.log(Region2D.name, ...args) }
    }

    static scaleFactorToFit(out: boolean, sw: number, sh: number, dw: number, dh: number) {
        return sh * (out ? sw / dw : dw / sw) > dh ? (dh / sh) : (dw / sw)
    }

    static draw(r: Region2D, ctx: CanvasRenderingContext2D, strokeStyle?: StrokeStyle, fill = false) {
        ctx.beginPath()
        if (fill) {
            if (strokeStyle) { ctx.fillStyle = rgba(strokeStyle) }
            ctx.fillRect(r.left - .5, r.top - .5, r.width, r.height)
        } else {
            if (strokeStyle) { ctx.strokeStyle = rgba(strokeStyle) }
            ctx.strokeRect(r.left - .5, r.top - .5, r.width, r.height)
        }
    }


    static zeroCentered(width: number = 1, height?: number) {
        if (height == undefined) { height = width }
        return new Region2D(-width / 2, width / 2, -height / 2, height / 2, true)
    }
    static fromScreen(widthOrSize: number | Size, height?: number) {
        const size = typeof widthOrSize == 'object' ? widthOrSize : { width: widthOrSize, height: height }
        return Region2D.fromSize(size, false)
    }
    static fromSize(r: Size, topIsYMax = false) {
        return Region2D.fromXYWH(0, 0, r.width, r.height, topIsYMax)
    }
    static fromCenterAndSize(x: number, y: number, w: number, h: number, topIsYMax = false) {
        return Region2D.fromXYWH(x - w / 2, y - w / 2, w, h, topIsYMax)
    }
    static fromRect(r: Rect, topIsYMax = false) {
        return Region2D.fromXYWH(r.x, r.y, r.width, r.height, topIsYMax)
    }
    static fromXYWH(x: number, y: number, w: number, h: number, topIsYMax = false) {
        return new Region2D(x, x + w, y, y + h, topIsYMax)
    }
    static fromRMinMax(r: OrientedRegion) {
        return new Region2D(r.xmin, r.xmax, r.ymin, r.ymax, r.topIsYMax)
    }
    static fromPoints(p1: Point, p2: Point, topIsYMax?: boolean) {
        return new Region2D(
            p1.x < p2.x ? p1.x : p2.x,
            p1.x > p2.x ? p1.x : p2.x,
            p1.y < p2.y ? p1.y : p2.y,
            p1.y > p2.y ? p1.y : p2.y,
            topIsYMax)
    }

    constructor(
        public xmin: number,
        public xmax: number,
        public ymin: number,
        public ymax: number,
        public topIsYMax: boolean
    ) {
        //console.log(xmin, xmax, ymin, ymax, topIsYMax)
    }

    toString(toFixed?: number) {
        return toFixed ?
            `x:${this.xmin.toFixed(toFixed)},${this.xmax.toFixed(toFixed)} y:${this.ymin.toFixed(toFixed)},${this.ymax.toFixed(toFixed)}`
            : `x:${this.xmin},${this.xmax} y:${this.ymin},${this.ymax}`
    }


    set x(value: number) { this.xmin = value }
    set y(value: number) { if (this.topIsYMax) { this.ymax = value } else { this.ymin = value } }
    set width(value: number) { this.xmax = this.xmin + Math.abs(value) }
    set height(value: number) { this.ymax = this.ymin + Math.abs(value) }


    get x() { return this.xmin }
    get y() { return this.topIsYMax ? this.ymax : this.ymin }
    get width(): number { return Math.abs(this.xmax - this.xmin) }
    get height(): number { return Math.abs(this.ymax - this.ymin) }

    get top(): number { return this.topIsYMax ? this.ymax : this.ymin }
    get left(): number { return this.xmin }
    get bottom(): number { return this.topIsYMax ? this.ymin : this.ymax }
    get right(): number { return this.xmax }

    get cx(): number { return (this.xmax + this.xmin) / 2 }
    get cy(): number { return (this.ymax + this.ymin) / 2 }

    get ratio(): number { return this.width / this.height }
    get isPortrait(): boolean { return this.ratio < 1 }
    get isSquare(): boolean { return this.ratio == 1 }
    get isLandscape(): boolean { return this.ratio > 1 }

    get area(): number { return this.width * this.height }

    get maxLength(): number { return Math.max(this.width, this.height) }
    get minLength(): number { return Math.min(this.width, this.height) }


    containsX(x: number): boolean { return this.xmin <= x && x <= this.xmax }
    containsY(y: number): boolean { return this.ymin <= y && y <= this.ymax }
    containsXY(x: number, y: number): boolean {
        return this.xmin <= x && x <= this.xmax
            && this.ymin <= y && y <= this.ymax
    }
    containsPoint(p: Point): boolean { return this.containsXY(p.x, p.y) }

    scaleFactorToFit(r: Size, out: boolean) {
        return Region2D.scaleFactorToFit(out, this.width, this.height, r.width, r.height)
    }

    equals(r: Region2D, strict = false) {
        if (this.xmin == r.xmin && this.xmax == r.xmax
            && this.ymin == r.ymin && this.ymax == r.ymax) {
            return true
        }
        if (!strict) {
            return this.xmax == r.xmin && this.xmin == r.xmax
                && this.ymax == r.ymin && this.ymin == r.ymax
        }
    }

    is(xmin: number, xmax: number, ymin: number, ymax: number, strict = true) {
        return this.xmin == xmin && this.xmax == xmax && (strict || this.xmax == xmin && this.xmin == xmax)
            && this.ymin == ymin && this.ymax == ymax && (strict || this.ymax == ymin && this.ymin == ymax)
    }


    isSameSize(r: Size) { return this.width == r.width && this.height == r.height }
    hasBiggerDimensionThan(r: Size): boolean { return this.width > r.width || this.height > r.height }

    intersects(r: Region2D) {
        return this.xmax < r.xmin || r.xmax < this.xmin
            || r.ymax < this.ymin || this.ymax < r.ymin ? false : true
    }

    surrounds(xmin: number, xmax: number, ymin: number, ymax: number, strict = false) {
        return strict ?
            this.xmin < xmin && xmax < this.xmax
            && this.ymin < ymin && ymax < this.ymax
            : this.xmin <= xmin && xmax <= this.xmax
            && this.ymin <= ymin && ymax <= this.ymax
    }
    surroundsR(r: Region2D, strict = false) {
        return this.surrounds(r.xmin, r.xmax, r.ymin, r.ymax, strict)
    }

    getRatioAsXY() { return { x: this.width, y: this.height } }

    center() { return { x: this.cx, y: this.cy } }
    topLeft() { return { x: this.top, y: this.left } }
    topRight() { return { x: this.top, y: this.right } }
    bottomLeft() { return { x: this.bottom, y: this.left } }
    bottomRight() { return { x: this.bottom, y: this.right } }


    clone() { return new Region2D(this.xmin, this.xmax, this.ymin, this.ymax, this.topIsYMax) }

    getIntersection(r: Region2D) { return this.clone().intersect(r) }

    getUnion(r: Region2D) { return this.clone().union(r) }

    getPartsTo(dst: Region2D, preferColumns = false) {

        const parts = new Array<Region2D>()

        if (!this.intersects(dst)) { return parts }

        const inter = this.getIntersection(dst)

        if (inter.width == 0 || inter.height == 0) { return parts }

        function sortDistinct(ns: number[]) {
            return ns.reduce((p, c) => {
                if (!p.some(o => o == c)) { p.push(c) }
                return p
            }, new Array<number>()).sort((a, b) => a - b)
        }

        const xs = sortDistinct([this.xmin, this.xmax, dst.xmin, dst.xmax]),
            ys = sortDistinct([this.ymin, this.ymax, dst.ymin, dst.ymax])

        //consecutive x-pairs
        for (let i = 1; i < xs.length; i++) {

            const xmin = xs[i - 1], xmax = xs[i]

            //consecutive y-pairs
            for (let j = 1; j < ys.length; j++) {

                const ymin = ys[j - 1], ymax = ys[j]

                //each small rect

                //exclude if not in destination or is intersection
                if (!dst.surrounds(xmin, xmax, ymin, ymax) || inter.is(xmin, xmax, ymin, ymax, true)) {
                    continue
                }

                //find neighbour
                const t = parts.find(t => preferColumns ?
                    t.xmin == xmin && t.xmax == xmax && (t.ymin == ymax || t.ymax == ymin)
                    : t.ymin == ymin && t.ymax == ymax && (t.xmin == xmax || t.xmax == xmin))

                if (t) { //merge with neighbour
                    if (preferColumns) { t.ymin = Math.min(t.ymin, ymin), t.ymax = Math.max(t.ymax, ymax) }
                    else { t.xmin = Math.min(t.xmin, xmin), t.xmax = Math.max(t.xmax, xmax) }
                }
                else { //add to result
                    parts.push(new Region2D(xmin, xmax, ymin, ymax, this.topIsYMax))
                }
            }
        }

        //intersection first
        parts.unshift(inter)

        return parts
    }

    getGrid(nx = 1, ny = 1, round = false, log = false) {
        if (round) { return this.getRoundedGrid(nx, ny, log) }
        const il = Math.trunc(Math.abs(nx)), jl = Math.trunc(Math.abs(ny)),
            r = this, topIsYMax = r.topIsYMax,
            x0 = r.xmin, y0 = r.ymin, w = r.width / il, h = r.height / jl,
            parts = new Array<Region2D>(il * jl)
        for (let i = 0, x = x0; i < il; i++ , x += w) {
            for (let j = 0, y = y0; j < jl; j++ , y += h) {
                parts[i * j + i] = Region2D.fromXYWH(x, y, w, h, topIsYMax)
            }
        }
        Region2D.say(log, this.getGrid.name, r.width, r.height, parts)
        return { parts, nx: il, ny: jl } as Grid
    }
    getRoundedGrid(nx = 1, ny = 1, log = false) {
        const getRoundParts = (dividend: number, divisor: number) => {
            divisor = Math.trunc(Math.abs(divisor))
            const a = Math.trunc(dividend / divisor),
                b = dividend - a * (divisor - 1),
                result = new Array<number>(divisor)
            result[0] = b
            result.fill(a, 1)
            return result
        }
        const r = this, topIsYMax = r.topIsYMax,
            x0 = r.xmin, y0 = r.ymin,
            ws = getRoundParts(r.width, nx),
            hs = getRoundParts(r.height, ny),
            il = ws.length, jl = hs.length,
            parts = new Array<Region2D>(il * jl)
        for (let i = 0, x = x0, w = ws[i]; i < il; i++ , x += (w = ws[i])) {
            for (let j = 0, y = y0, h = hs[j]; j < jl; j++ , y += (h = hs[j])) {
                parts[i * (j + 1)] = Region2D.fromXYWH(x, y, w, h, topIsYMax)
            }
        }
        Region2D.say(log, this.getRoundedGrid.name, r.width, r.height, parts)
        return { parts, nx: il, ny: jl } as Grid
    }
    getGridLike(t: Grid, log = false) {
        const il = t.nx, jl = t.ny, ws = new Array<number>(il), hs = new Array<number>(jl),
            r = this, x0 = r.xmin, y0 = r.ymin, rw = r.width, rh = r.height, topIsYMax = r.topIsYMax,
            parts = new Array<Region2D>(il * jl)
        let tw = 0; for (let i = 0; i < il; i++) { const w = t.parts[i].width; ws[i] = w; tw += w }
        let th = 0; for (let j = 0; j < jl; j++) { const h = t.parts[j * il].height; hs[j] = h; th += h }
        for (let j = 0, y = y0; j < jl; j++) {
            const h = rh * hs[j] / th
            for (let i = 0, x = x0; i < il; i++) {
                const w = rw * ws[i] / tw
                parts[i * (j + 1)] = Region2D.fromXYWH(x, y, w, h, topIsYMax)
                x += w
            }
            y += h
        }
        Region2D.say(log, this.getGridLike.name, rw, rh, il, jl, tw, th, ws, hs)
        return { parts, nx: il, ny: jl } as Grid
    }
    static gridToString(g: Grid, toFixed?: number) {
        return `(${g.nx},${g.ny}:[${g.parts.map(r => r.toString(toFixed)).join(', ')}])`
    }

    copy(r: Region2D) {
        this.xmin = r.xmin
        this.xmax = r.xmax
        this.ymin = r.ymin
        this.ymax = r.ymax
        this.topIsYMax = r.topIsYMax
        return this
    }

    setMin(x: number, y: number) { this.xmin = x, this.ymin = y; return this }
    setMax(x: number, y: number) { this.xmax = x, this.ymax = y; return this }
    setX(min: number, max: number) { this.xmin = min, this.xmax = max; return this }
    setY(min: number, max: number) { this.ymin = min, this.ymax = max; return this }

    floorX() { this.xmin = Math.floor(this.xmin); this.xmax = Math.floor(this.xmax); return this }
    floorY() { this.ymin = Math.floor(this.ymin); this.ymax = Math.floor(this.ymax); return this }
    ceilX() { this.xmin = Math.ceil(this.xmin); this.xmax = Math.ceil(this.xmax); return this }
    ceilY() { this.ymin = Math.ceil(this.ymin); this.ymax = Math.ceil(this.ymax); return this }
    roundX() { this.xmin = Math.round(this.xmin); this.xmax = Math.round(this.xmax); return this }
    roundY() { this.ymin = Math.round(this.ymin); this.ymax = Math.round(this.ymax); return this }
    truncX() { this.xmin = Math.trunc(this.xmin); this.xmax = Math.trunc(this.xmax); return this }
    truncY() { this.ymin = Math.trunc(this.ymin); this.ymax = Math.trunc(this.ymax); return this }
    floor() { return this.floorX().floorY() }
    ceil() { return this.ceilX().ceilY() }
    round() { return this.roundX().roundY() }
    trunc() { return this.truncX().truncY() }

    floorMinCeilMax() {
        this.xmin = Math.floor(this.xmin)
        this.ymin = Math.floor(this.ymin)
        this.xmax = Math.ceil(this.xmax)
        this.ymax = Math.ceil(this.ymax)
        return this
    }

    flipX() {
        const t = this.xmin
        this.xmin = this.xmax
        this.xmax = t
        return this
    }
    flipY() {
        const t = this.ymin
        this.ymin = this.ymax
        this.ymax = t
        return this
    }
    flip(flipX = false, flipY = false) {
        if (flipY) { this.flipY() }
        if (flipX) { this.flipX() }
        return this
    }
    conform() {
        return this.flip(this.xmin > this.xmax, this.ymin > this.ymax)
    }
    translate(x: number, y: number) {
        this.xmin += x
        this.xmax += x
        this.ymin += y
        this.ymax += y
        return this
    }
    scale(sf: number, sy?: number) {
        if (sy == undefined) { sy = sf }
        this.xmin *= sf
        this.xmax *= sf
        this.ymin *= sy
        this.ymax *= sy
        return this
    }

    centerOn(x?: number, y?: number) {
        return this.translate(
            x == undefined ? 0 : x - this.cx,
            y == undefined ? 0 : y - this.cy)
    }
    topLeftOn(x?: number, y?: number) {
        return this.translate(
            x == undefined ? 0 : x - this.left,
            y == undefined ? 0 : y - this.top)
    }

    maxTo(widthOrSize: number | Size, height?: number) {
        const size = typeof widthOrSize == 'object' ? widthOrSize : { width: widthOrSize, height: height }
        const sf = this.scaleFactorToFit(size, false)
        this.width *= sf
        this.height *= sf
        return this
    }
    maxToCentered(r: Rect) {
        return this.maxTo(r)
            .translate(r.x + r.width / 2 - this.cx, r.y + r.height / 2 - this.cy)
    }

    clamp(maxWidth: number | false, maxHeight?: number | false) {
        if (maxWidth != false && this.width < maxWidth) { this.width = maxWidth }
        if (maxHeight == undefined) maxHeight = maxWidth
        if (maxHeight != false && this.height < maxHeight) { this.height = maxHeight }
        return this
    }

    scaleOnCenter(sf: number, sy?: number) {
        const x = this.cx, y = this.cy
        return this.scale(sf, sy).centerOn(x, y)
    }
    scaleOnTopLeft(sf: number, sy?: number) {
        const x = this.left, y = this.top
        return this.scale(sf, sy).topLeftOn(x, y)
    }

    fitTo(r: Region2D, out: boolean, topLeft = false) {
        const sf = this.scaleFactorToFit(r, out)
        return topLeft ? this.scale(sf).topLeftOn(r.left, r.top)
            : this.scale(sf).centerOn(r.cx, r.cy)
    }

    intersect(r: Region2D) {
        this.xmin = Math.max(this.xmin, r.xmin)
        this.xmax = Math.min(this.xmax, r.xmax)
        this.ymin = Math.max(this.ymin, r.ymin)
        this.ymax = Math.min(this.ymax, r.ymax)
        return this
    }

    union(r: Region2D) {
        this.xmin = Math.min(this.xmin, r.xmin)
        this.xmax = Math.min(this.ymin, r.ymin)
        this.ymin = Math.max(this.xmax, r.ymax)
        this.ymax = Math.max(this.ymax, r.ymax)
        return this
    }


    draw(ctx: CanvasRenderingContext2D, strokeStyle?: StrokeStyle, fill = false) {
        Region2D.draw(this, ctx, strokeStyle, fill)
    }

}