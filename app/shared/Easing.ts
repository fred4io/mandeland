
export enum EaseDir { In, Out, InOut }

//export enum EaseType { Quad, Cubic, Quart, Quint }
export enum EaseType { Quadratic, Cubic, Quartic, Quintic, Sinusoidal, Exponential, Elastic, Back, Bounce }

// export enum Easer {
//     Linear,
//     InQuad, InCubic, InQuart, InQuint,
//     OutQuad, OutCubic, OutQuart, OutQuint,
//     InOutQuad, InOutCubic, InOutQuart, InOutQuint
// }
export enum Easer {
    Linear,
    InQuadratic, InCubic, InQuartic, InQuintic, InSinusoidal, InExponential, InElastic, InBack, InBounce,
    OutQuadratic, OutCubic, OutQuartic, OutQuintic, OutSinusoidal, OutExponential, OutElastic, OutBack, OutBounce,
    InOutQuadratic, InOutCubic, InOutQuartic, InOutQuintic, InOutSinusoidal, InOutExponential, InOutElastic, InOutBack, InOutBounce,
}

export class Easing {

    private static readonly useTween = true
    private static readonly types = Easing.useTween ? 9 : 4
    
    static readonly maxFuncIndex = Easing.types * 3

    static func(ed: Easer | EaseDir, et?: EaseType): (t: number) => number {
        if (ed == undefined && et == undefined || ed == 0) return t => t //linear
        if (et == undefined) {
            const n = Math.max(0, Math.min(Easing.maxFuncIndex, ed || 0))
            ed = n ? ~~((n - 1) / Easing.types) : undefined
            et = n ? ((n - 1) % Easing.types) : undefined
        }
        const tn = EaseType[et], dn = EaseDir[ed]
        return Easing.useTween ?
            (<any>TWEEN.Easing)[EaseType[et]][EaseDir[ed]]
            : (<any>Easing)['ease' + dn + tn]
    }

    static funcName(ed: Easer | EaseDir, et?: EaseType) {
        if (et == undefined) {
            const n = Math.max(0, Math.min(Easing.maxFuncIndex, ed || 0))
            ed = n ? ~~((n - 1) / Easing.types) : undefined
            et = n ? ((n - 1) % Easing.types) : undefined
        }
        if (et == undefined || ed == undefined) return 'linear'
        return EaseType[et] + '.' + EaseDir[ed]
    }

    static _test() {
        for (let i = 0; i <= Easing.maxFuncIndex; i++) {
            console.log(Easing.funcName(i), Easing.func(i)(.25))
        }
    }

    static nextEaser(ed: Easer, reverse = false) {
        return (ed += (reverse ? -1 : 1)) > Easing.maxFuncIndex ? 0 : ed < 0 ? Easing.maxFuncIndex : ed
    }

    /*
    * From https://gist.github.com/gre/1650294
    * Easing Functions - inspired from http://gizma.com/easing/
    * only considering the t value for the range [0, 1] => [0, 1]
    */

    // no easing, no acceleration
    static linear(t: number) { return t }

    // accelerating from zero velocity
    static easeInQuad(t: number) { return t * t }

    // decelerating to zero velocity
    static easeOutQuad(t: number) { return t * (2 - t) }

    // acceleration until halfway, then deceleration
    static easeInOutQuad(t: number) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t }

    // accelerating from zero velocity 
    static easeInCubic(t: number) { return t * t * t }

    // decelerating to zero velocity 
    static easeOutCubic(t: number) { return (--t) * t * t + 1 }

    // acceleration until halfway, then deceleration 
    static easeInOutCubic(t: number) { return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1 }

    // accelerating from zero velocity 
    static easeInQuart(t: number) { return t * t * t * t }

    // decelerating to zero velocity 
    static easeOutQuart(t: number) { return 1 - (--t) * t * t * t }

    // acceleration until halfway, then deceleration
    static easeInOutQuart(t: number) { return t < .5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t }

    // accelerating from zero velocity
    static easeInQuint(t: number) { return t * t * t * t * t }

    // decelerating to zero velocity
    static easeOutQuint(t: number) { return 1 + (--t) * t * t * t * t }

    // acceleration until halfway, then deceleration 
    static easeInOutQuint(t: number) { return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t }

}