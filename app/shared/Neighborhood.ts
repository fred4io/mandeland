
export class Neighborhood {

    private static _nrRangeCache: Map<string, number[]>
    private static _dirsRightFrom = [7, 7, 1, 1, 3, 3, 5, 5]
    private static _dirsLeftFrom = [1, 3, 3, 5, 5, 7, 7, 1]

    static prepare(imageWidth: number) {

        const nrs = Neighborhood.get8Wcw(imageWidth)

        function getDir(fi: number, ti: number) {
            return nrs.indexOf(ti - fi)
        }
        function getIndex(ri: number, dir: number) {
            return ri - nrs[dir]
        }
        function getIndexRight(fi: number, ti: number) {
            return ti + nrs[Neighborhood._dirsRightFrom[nrs.indexOf(ti - fi)]]
        }
        function getIndexLeft(fi: number, ti: number) {
            return ti + nrs[Neighborhood._dirsLeftFrom[nrs.indexOf(ti - fi)]]
        }


        return {
            getDir, getIndex, getIndexRight, getIndexLeft
        }

    }


    /** Von Neumann neighborhood: 4 relative indices, from right, clockwise (like E, S, W, N), at the given distance or 1 */
    static get4Ecw(imgWidth: number, distance = 1) {
        return [distance, imgWidth * distance, -distance, -imgWidth * distance]
    }

    /** Moore neighborhood: 8 relative indices, from top-left, clockwise, at the given distance or 1 */
    static get8Wcw(imgWidth: number, distance = 1) {
        const l = distance, w = imgWidth * l
        return [-w - l, -w, -w + l, l, w + l, w, w - l, -l]
    }

    /** neighbors relative indices within a radial range */
    static getRange(range: number, imgWidth: number, lod = 1) {
        if (range < 1) return
        const neighborhood = new Array<number>(), nr = -range, r2 = range * range
        for (let y = nr; y <= range; y++) {
            let y2 = y * y, ywl = y * imgWidth * lod
            for (let x = nr; x <= range; x++)
                if (x != 0 && y != 0 && y2 + x * x <= r2)
                    neighborhood.push(ywl + x * lod)
        }
        return neighborhood
    }

    /**
     * neighbors relative indices within a radial range, with caching
     * @param range radius of a circle
     * @param imgWidth image width
     * @param lod level of detail (ie distance between grid points)
     * @param log message logging to console
     */
    static getCachedRange(range: number, imgWidth: number, lod = 1, log = false) {
        if (range < 1) return
        let map = Neighborhood._nrRangeCache
        if (!map) Neighborhood._nrRangeCache = map = new Map<string, number[]>()
        const key = range + '/' + imgWidth + '/' + lod
        let neighborhood = map.get(key)
        if (neighborhood) return neighborhood
        neighborhood = Neighborhood.getRange(range, imgWidth, lod)
        map.set(key, neighborhood)
        if (log) console.log('getCachedRange', key, neighborhood)
        return neighborhood
    }


    /**
     * returns the direction id, from the given input pixel index
     * to the given neighbor pixel index.
     * 0=NW, 1=N incrementing clockwise up to 7=W
     * @param fi from pixel index
     * @param ti to neighbour pixel index
     * @param w image width
     */
    static getDir(fi: number, ti: number, w: number) {
        switch (ti - fi) {
            case - w - 1: return 0  //NW
            case - w: return 1      //N
            case - w + 1: return 2  //NE
            case 1: return 3        //E
            case w + 1: return 4    //SE
            case w: return 5        //S
            case w - 1: return 6    //SW
            case - 1: return 7      //W
        }
    }

    /** 
     * returns the index of the neighbor pixel in the given direction from the given reference pixel
     * @param ri: reference pixel index
     * @param dir: direction; 0=NW, incrementing clockwise up to 7=W
     * @param w: image width
    */
    static getIndex(ri: number, dir: number, w: number) {
        switch (dir) {
            case 0: return ri - w - 1
            case 1: return ri - w
            case 2: return ri - w + 1
            case 3: return ri + 1
            case 4: return ri + w + 1
            case 5: return ri + w
            case 6: return ri + w - 1
            case 7: return ri - 1
        }
    }

    /** W|NW => N; N|NE => E; E|SE => S; S|SW => W */
    static getDirLeftFrom(dir: number) {
        return Neighborhood._dirsLeftFrom[dir]
    }
    /** NW|N => W; NE|E => N; SE,S => E; SW,W => S */
    static getDirRightFrom(dir: number) {
        return Neighborhood._dirsRightFrom[dir]
    }

    /** NW => N; N => NE, ... */
    static getDirLeftForwardFrom(dir: number) {
        return (dir + 1) % 8
    }
    /** NW => W; W => SW, ... */
    static getDirRightForwardFrom(dir: number) {
        return dir == 0 ? 7 : dir - 1
    }
    /** NW => SE; W => E, ... */
    static getDirOpposite(dir: number) {
        return (dir + 4) % 8
    }

    static getMidDir(dirIn: number, dirOut: number) {
        return (dirIn + ~~((dirOut - dirIn) / 2)) % 8
    }

    static getRandomDir() {
        return ~~(Math.random() * 8)
    }

    /** returns true is the turn is to the left  */
    static isTurnLeft(dirIn: number, dirOut: number) {
        switch (dirIn) {
            case 0: return dirOut > 0 && dirOut < 4
            case 1: return dirOut > 1 && dirOut < 5
            case 2: return dirOut > 2 && dirOut < 6
            case 3: return dirOut > 3 && dirOut < 7
            case 4: return dirOut > 4 && dirOut <= 7
            case 5: return dirOut > 5 || dirOut < 1
            case 6: return dirOut > 6 || dirOut < 2
            case 7: return dirOut < 3
        }
    }

    /** returns true is the turn is to the right */
    static isTurnRight(dirIn: number, dirOut: number) {
        switch (dirIn) {
            case 0: return dirOut > 4
            case 1: return dirOut < 1 || dirOut > 5
            case 2: return dirOut < 2 || dirOut > 6
            case 3: return dirOut < 3
            case 4: return dirOut > 0 && dirOut < 4
            case 5: return dirOut > 1 && dirOut < 5
            case 6: return dirOut > 2 && dirOut < 6
            case 7: return dirOut > 3 && dirOut < 7
        }
    }

    /** returns true if the given directions are opposites  */
    static isNoTurn(dirIn: number, dirOut: number) {
        return dirOut == (dirIn < 4 ? (dirIn + 4) : (dirIn - 4))
    }

    static isDiagonal(dir: number) {
        return dir % 2 == 0
    }
    static isStraight(dir: number) {
        return dir % 2 != 0
    }

    


    /**
     * returns the index of the pixel on the right of the reference given pixel 
     * when coming from the given neighbor pixel
     * @param ri reference pixel index
     * @param pi previous pixel index
     * @param w image width
     */
    static getIndexRightFrom(ri: number, pi: number, w: number) {
        const dir = Neighborhood.getDir(pi, ri, w)
        const right = Neighborhood.getDirRightFrom(dir)
        return Neighborhood.getIndex(ri, right, w)
    }

    /**
     * returns the index of the pixel on the left of the reference given pixel 
     * when coming from the given neighbor pixel
     * @param ri center pixel index
     * @param pi previous pixel index
     * @param w image width
     */
    static getIndexLeftFrom(ri: number, pi: number, w: number) {
        const dir = Neighborhood.getDir(pi, ri, w)
        const left = Neighborhood.getDirLeftFrom(dir)
        return Neighborhood.getIndex(ri, left, w)
    }

    /**
     * returns the index of the pixel on the right and forward of the reference given pixel 
     * when coming from the given neighbor pixel
     * @param ri reference pixel index
     * @param pi previous pixel index
     * @param w image width
     */
    static getIndexRightForwardFrom(ri: number, pi: number, w: number) {
        const dir = Neighborhood.getDir(pi, ri, w)
        const right = Neighborhood.getDirRightForwardFrom(dir)
        return Neighborhood.getIndex(ri, right, w)
    }

    /**
     * returns the index of the pixel on the left and forward of the reference given pixel 
     * when coming from the given neighbor pixel
     * @param ri reference pixel index
     * @param pi previous pixel index
     * @param w image width
     */
    static getIndexLeftForwardFrom(ri: number, pi: number, w: number) {
        const dir = Neighborhood.getDir(pi, ri, w)
        const left = Neighborhood.getDirLeftForwardFrom(dir)
        return Neighborhood.getIndex(ri, left, w)
    }

    /**
     * returns the index of the neighbour pixel of the reference given pixel,
     * at the opposite direction from the given previous pixel
     * @param ri reference pixel index
     * @param pi previous pixel index
     * @param w image width
     */
    static getIndexOppositeFrom(ri: number, pi: number, w: number) {
        const dir = Neighborhood.getDir(pi, ri, w)
        const opp = (dir + 4) % 8
        return Neighborhood.getIndex(ri, opp, w)
    }

    /**
     * return the angle id:
     * 4 = flat; 0 = u-turn; >0 = CW, <0 = CCW; 
     * 0=0°, 1=45°; 2=90°; 3=135°; 4=180°; 5=225°; 6=270; 7=315° 
     * @param pi previous pixel index (entering)
     * @param ri reference pixel index
     * @param ni next pixel index (exiting)
     * @param w image width
     */
    static getAngleId(pi: number, ri: number, ni: number, w: number) {
        const dirIn = Neighborhood.getDir(pi, ri, w)
        const dirOut = Neighborhood.getDir(ri, ni, w)
        return dirOut - dirIn
    }

    /** 
     * 4 = flat; 0 = u-turn; >0 = CW, <0 = CCW; 
     * 1 = 45°; 2 = 90°; 3=135°; 4=180°; 5=225°; 6=270; 7=315° 
    */
    private static angleOfDirs(dirIn: number, dirOut: number) {
        return dirOut - dirIn
    }
}