import { HeightMap } from "./HeightMap"
import { HMZPointType, HeightMapZones } from "./HeightMapZones"
import { TriResult } from './Triangulation'
import { Zoning } from "./Zoning";

declare type THeightFactor = number | ((h: number) => number)

export class HMapMeshData {

    public vertices: Array<number>
    public faces: Array<Array<number>>
    public merged: boolean
    public names = ['zones', 'noise', 'seams']
    public getFaces(name: string) { return this.faces[this.merged ? 0 : this.names.indexOf(name)] }

    private zonesTris: Array<number>
    private noiseTris: Array<number>
    private seamsTris: Array<number>

    private data: Float32Array
    private width: number
    private lod: number
    private heightFactor: THeightFactor

    protected static say(log: boolean, ...args: any[]) {
        if(log) { console.log(HMapMeshData.name, ...args) }
    }

    static fromHeightMap(hmap: HeightMap, heightFactor: THeightFactor,
        lod = 1, contourMinArea = 10, log = false,
        zones = true, noise = true, seams = true,
        updatable = false, merge = false
    ) {
        const hmz = HeightMapZones.compute(hmap, lod, contourMinArea, log)
        const zt = (updatable || zones) && HMapMeshData.computeZonesTris(hmz)
        const nt = (updatable || noise) && HMapMeshData.computeNoiseTris(hmz)
        const st = (updatable || seams) && HMapMeshData.computeSeamsTris(hmz)

        const result = new HMapMeshData()

        if (updatable) {
            result.data = hmz.data
            result.width = hmz.width
            result.lod = hmz.lod
            result.heightFactor = heightFactor
            result.zonesTris = zt
            result.noiseTris = nt
            result.seamsTris = st
        }

        const pack = HMapMeshData.pack(
            [zones && zt, noise && nt, seams && st],
            hmz.data, hmz.width, hmz.lod, heightFactor, log)

        result.vertices = pack.vertices
        result.faces = merge ? [Array.prototype.concat(...pack.facesGroups)] : pack.facesGroups
        result.merged = merge
        HMapMeshData.say(log, 'fromHeightMap', result)
        return result
    }

    update(heightFactor = this.heightFactor,
        zones = true, noise = true, seams = true,
        merge = false, log = false
    ) {
        if (!this.data) {
            console.warn('HMapMeshData', 'not updatable')
            return this
        }
        const pack = HMapMeshData.pack(
            [zones && this.zonesTris, noise && this.noiseTris, seams && this.seamsTris],
            this.data, this.width, this.lod, heightFactor, log)
        this.vertices = pack.vertices
        this.faces = merge ? [Array.prototype.concat(...pack.facesGroups)] : pack.facesGroups
        this.merged = merge
        this.heightFactor = heightFactor

        return this
    }

    static computeZonesTris(hmz: HeightMapZones, indices = new Array<number>()) {
        hmz.zones.forEach(z => {
            if (z.contour.length)
                hmz.triangulate(z).getTriIndices(indices)
        })
        return indices
    }

    static computeNoiseTris(hmz: HeightMapZones, indices = new Array<number>()) {

        const pt = hmz.pointTypeIndex, PTN = HMZPointType.Noise, PTC = HMZPointType.Contour
        const w = hmz.width, xl = w - 1, yl = hmz.height - 1
        for (let y = 0, yw: number; y < yl; y++) {
            yw = y * w
            for (let x = 0; x < xl; x++) {
                const
                    ia = yw + x, ta = pt[ia],
                    ib = ia + w, tb = pt[ib],
                    ic = ib + 1, tc = pt[ic],
                    id = ia + 1, td = pt[id]

                // if (ta == PTN || tb == PTN || tc == PTN || td == PTN) {
                //     indices.push(ia, ib, id)
                //     indices.push(ib, ic, id)
                // }

                let n = 0
                if ((ta == PTN || tb == PTN || tc == PTN)
                    && (ta == PTN || ta == PTC)
                    && (tb == PTN || tb == PTC)
                    && (tc == PTN || tc == PTC)
                ) {
                    indices.push(ia, ib, ic)
                    n++
                }

                if ((tc == PTN || td == PTN || ta == PTN)
                    && (tc == PTN || tc == PTC)
                    && (td == PTN || td == PTC)
                    && (ta == PTN || ta == PTC)
                ) {
                    indices.push(ic, id, ia)
                    n++
                }

                if (n == 0) {
                    if ((td == PTN || ta == PTN || tb == PTN)
                        && (td == PTN || td == PTC)
                        && (ta == PTN || ta == PTC)
                        && (tb == PTN || tb == PTC)
                    ) {
                        indices.push(id, ia, ib)
                    }

                    if ((tb == PTN || tc == PTN || td == PTN)
                        && (tb == PTN || tb == PTC)
                        && (tc == PTN || tc == PTC)
                        && (td == PTN || td == PTC)
                    ) {
                        indices.push(ib, ic, id)
                    }

                }
            }
        }

        return indices
    }

    static getSeamsNeighborCounts(
        contour: Array<number>,
        pointLabel: Uint32Array, zoneLabel: number,
        pointType: Uint32Array, PT_CONTOUR: number,
        w: number, counts = new Array<number>(contour.length).fill(0)
    ) {

        const xl = w - 1
        const nh = [-w - 1, -w, -w + 1, 1, w + 1, w, w - 1, -1]

        if (!contour || contour.length < 3)
            return

        for (let i = 0, cl = contour.length; i < cl; i++) {

            const iR = contour[i]

            for (let d = 0; d < 8; d++) {

                let ic = iR + nh[d]

                if (pointType[ic] == PT_CONTOUR && pointLabel[ic] != zoneLabel)
                    counts[i]++
            }
        }

        return counts
    }

    static computeSeamsTris(hmz: HeightMapZones, indices = new Array<number>()) {

        const w = hmz.width, h = hmz.height         //image dimensions
        const xl = w - 1, yl = h - 1                //right and bottom borders
        const pointType = hmz.pointTypeIndex        //each point type
        const PT_CONTOUR = HMZPointType.Contour     //type of a contour point
        const nh = [-w - 1, -w, -w + 1, 1, w + 1, w, w - 1, -1] //neighborhood relative indices
        const pointLabel = hmz.pointLevelIndex
        //for each zone
        for (let iz = 0, zones = hmz.zones, zl = zones.length; iz < zl; iz++) {
            const zone = zones[iz], contour = zone.contour, cl = contour && contour.length || 0
            const zoneLabel = zone.levelIndex
            if (cl < 3)
                continue                            //we need 3 consecutive contour points

            if (zone.levelIndex == hmz.maxLevelIndex)
                continue                            //those zones are bordered by noise

            let iP = contour[0]                     //previous contour point index
            let ia = contour[1]                     //reference (current) contour point index
            let dP = nh.indexOf(ia - iP)            //entering direction

            //let pib: number, pic: number
            for (let i = 2, cl1 = cl + 1; i < cl1; i++) { //for each contour point and back to start

                const iN = contour[i % cl]          //next contour point index

                const xa = ia % w, border = xa == 0 || xa == xl

                let ib = iP                         //second vertex of the face, for now the start (previous) point

                let dN = nh.indexOf(iN - ia)        //next point direction

                let ds = dN                         //stop direction
                
                if (ds % 2 != 0)                    //if not diagonal, 
                    ds += (ds < 2 ? 6 : -2)         // stop at perp. before

                let iS = ia + nh[ds]                //stop point

                let ic = iP                         //candidate point turning around ref point
                let d = nh.indexOf(ic - ia)         //direction turning around ref point

                let nf = 0
                while (ic != iS) {                  //let's turn around the ref point

                    d = (d + 1) % 8                     //turn clockwise
                    ic = ia + nh[d]                     //candidate point

                    if (border) {
                        const xc = ic % w
                        if (xa == 0 && xc == xl || xc == 0)
                            continue                    //border to border
                    }

                    if (pointType[ic] == PT_CONTOUR     //contour point
                        && pointLabel[ic] != zoneLabel  //of another zone
                    ) {
                        indices.push(ic, ib, ia)    //add face
                        ib = ic
                    }
                }

                //next contour point
                iP = ia
                ia = iN
                dP = dN
            }
        }

        return indices
    }

    private static pack(triIndicesGroups: Array<Array<number>>, data: Float32Array,
        width: number, lod: number,
        heightFactor: THeightFactor = 1,
        log = false
    ) {
        const dataWidth = width * lod,
            vertices = new Array<number>(),
            map = new Map<number, number>(),
            x0 = -dataWidth / 2,
            z0 = -(data.length / dataWidth) / 2,
            getHeight =
                typeof heightFactor == 'function' ? heightFactor
                    : (h: number) => h * heightFactor
        let vl = 0
        //console.log(width, lod, dataWidth, data)

        function vertexIndex(i: number) {
            let vi = map.get(i)
            if (!vi) {
                const u = (i % width) * lod
                const v = ~~(i / width) * lod
                const h = data[dataWidth * v + u]
                vertices.push(x0 + u, getHeight(h), z0 + v)
                map.set(i, vi = vl++)
            }
            return vi
        }

        const facesGroups = new Array<Array<number>>()

        HMapMeshData.say(log, 'pack-triIndices', triIndicesGroups)

        triIndicesGroups.forEach((tg, ti) => {
            if (!tg) return
            const fg = new Array<number>(tg.length)
            facesGroups.push(fg)
            for (let i = 0, l = tg.length; i < l; i++)
                fg[i] = vertexIndex(tg[i])
        })

        return { vertices, facesGroups }
    }
}