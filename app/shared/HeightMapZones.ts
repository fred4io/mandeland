import { HeightMap } from "./HeightMap"
import { Triangulation } from "./Triangulation";
import { Zoning, ContourMethod } from "./Zoning";
import { Neighborhood } from "./Neighborhood";

declare type TBoundingBox = { xmin: number, xmax: number, ymin: number, ymax: number }
declare type TColor = { r: number, g: number, b: number, a?: number }
declare type TGetColor = ((i: number) => TColor)
declare type TPoint = { x: number, y: number }

export enum HMZPointType {
    Nothing = 0,
    Interior,
    Edge,
    Contour,
    Noise
}
export class HMapZone {

    points: Array<number>
    contour: Array<number>
    edges?: Array<number>
    boundingBox?: TBoundingBox
    neighborZones?: Set<number>
    interiorZones?: Set<number>

    constructor(
        public zoneIndex: number,
        public levelIndex?: number,
        public levelZoneIndex?: number
    ) {
        this.points = new Array<number>()
    }
}

export class HeightMapZones {

    static compute(
        hmap: HeightMap,
        lod = 1,
        contourMinArea = 1,
        log = false,
        debug = false,
        contourMethod = ContourMethod.Moore,
        removeColinears = false,
        computeNeighborZones = false,
        keepEdges = false,
        holesContours = false
    ) {

        if (lod > 1) hmap.makeEven(lod, log && debug)

        const result = HeightMapZones.zonesByLevel(
            hmap.data, hmap.dataWidth,
            lod, contourMethod, contourMinArea,
            removeColinears, computeNeighborZones, keepEdges,
            log, debug)

        if (log && debug) console.log('HeightMapZones',
            !result ? result : result.levelZoneIndex.length + ' levels', result.zones.length + ' zones')


        return result
    }

    private constructor(
        public lod = 1,
        public contourMinArea = 1,
        private log = false,
        private debug = false,
        public data: Float32Array,
        public dataWidth: number,
        public width: number,
        public height: number,
        public zones: Array<HMapZone>,
        public levelZoneIndex: Array<Array<number>>,
        public pointLevelIndex: Uint32Array,
        public pointZoneIndex: Uint32Array,
        public pointTypeIndex: Uint32Array,
        public noiseZones: Array<HMapZone>,
        public pointNoiseZoneIndex: Uint32Array,
        public maxLevelIndex: number
    ) {
    }

    get levelsCount() { return this.levelZoneIndex.length }
    get zonesCount() { return this.zones.length }

    getZonesByLevel() {
        return this.levelZoneIndex.map(zis => zis.map(zi => this.zones[zi]))
    }

    getPointTypeCounts() {
        return this.pointTypeIndex.reduce((p, c) => { p[c]++; return p }, new Array<number>(4).fill(0))
    }

    getPoint(index: number) {
        return { x: index % this.width, y: ~~(index / this.width) }
    }

    getLevelIndex(x: number, y: number) {
        return this.pointLevelIndex[this.width * y + x]
    }
    getLevelZones(levelIndex: number) {
        const zis = this.levelZoneIndex[levelIndex]
        return zis && zis.map(zi => this.zones[zi])
    }
    drawLevel(level: Array<HMapZone>, imd: ImageData,
        pointsColor?: TColor | TGetColor, contourColor?: TColor | TGetColor,
        info?: { points?: number, contour?: number },
        fillLod = false, mixColor = false, byZone = false
    ) {
        if (pointsColor) {
            if (byZone) {
                if (info) info.points = 0
                level.forEach(z => {
                    this.draw(z.points, imd, pointsColor, fillLod, mixColor)
                    if (info) info.points += z.contour.length
                })
            } else {
                const indices = Array.prototype.concat(...level.map(z => z.points))
                this.draw(indices, imd, pointsColor, fillLod, mixColor)
                if (info) info.points = indices.length
            }
        }
        if (contourColor) {
            if (byZone) {
                if (info) info.contour = 0
                level.forEach(z => {
                    this.draw(z.contour, imd, contourColor, fillLod, mixColor)
                    if (info) info.contour += z.contour.length
                })
            }
            else {
                const indices = Array.prototype.concat(...level.map(z => z.contour))
                this.draw(indices, imd, contourColor, fillLod, mixColor)
                if (info) info.contour = indices.length
            }
        }
    }

    getZone(x: number, y: number) {
        return this.zones[this.pointZoneIndex[this.width * y + x]]
    }
    drawZone(zone: HMapZone, imd: ImageData,
        pointsColor?: TColor | TGetColor,
        contourColor?: TColor | TGetColor,
        fillLod = false, mixColor = false
    ) {
        if (pointsColor) this.draw(zone.points, imd, pointsColor, fillLod, mixColor)
        if (contourColor) this.draw(zone.contour, imd, contourColor, fillLod, mixColor)
    }


    drawTris(tris: Array<number>, imd: ImageData, color: TColor | TGetColor, mixColor = false) {
        this.draw(tris, imd, color, true, mixColor)
    }

    triangulate(zone: HMapZone,
        steinersGridDistance?: number,
        steinersEdgeDistance?: number,
        steinersClipInterior = false,
        maxRemovedBadPoints = 100,
        logWarnings = true
    ) {
        if (!zone || !zone.contour || !zone.contour.length) return

        const steiners = steinersGridDistance &&
            this.getSteinerPoints(zone, steinersGridDistance, steinersEdgeDistance, steinersClipInterior)

        return Triangulation.triangulate(zone.contour, this.width, this.lod,
            steiners, this.debug && logWarnings, maxRemovedBadPoints)
    }

    computeBoundingBox(zone: HMapZone) {
        if (!zone.boundingBox)
            zone.boundingBox = HeightMapZones.getBoundingBox(zone.contour, this.width)
        return zone.boundingBox
    }

    /** return points in the polygon described by the given zone contour */
    getSteinerPoints(zone: HMapZone,
        gridMinDistance: number, edgeMinDistance: number,
        clipInterior = true
    ) {
        gridMinDistance = Math.round(gridMinDistance)
        const indices = new Array<number>(),
            bb = this.computeBoundingBox(zone),
            neighborhood = edgeMinDistance &&
                Neighborhood.getCachedRange(
                    edgeMinDistance, this.width, 1, this.debug),
            zoneIdx = zone.zoneIndex,
            imgWidth = this.width,
            zoneIndex = this.pointZoneIndex,
            pointType = this.pointTypeIndex,
            hgd = Math.round(gridMinDistance / 2),
            y0 = bb.ymin + hgd, yl = bb.ymax,
            x0 = bb.xmin + hgd, xl = bb.xmax,
            PT_INTERIOR = HMZPointType.Interior,
            PT_CONTOUR = HMZPointType.Contour

        for (let y = y0; y < yl; y += gridMinDistance) {
            for (let x = x0; x < xl; x += gridMinDistance) {
                const pi = y * imgWidth + x
                if (pointType[pi] == PT_INTERIOR
                    && zoneIndex[pi] == zoneIdx
                    && (!neighborhood || !neighborhood.some(ri => {
                        const ni = pi + ri
                        return pointType[ni] == PT_CONTOUR
                            && zoneIndex[ni] == zoneIdx
                    }))
                ) {
                    indices.push(pi)
                }
            }
        }

        if (!clipInterior) return indices

        const nrs = Neighborhood.get4Ecw(imgWidth, gridMinDistance)
        return indices.reduce((sis, si) => {
            if (nrs.reduce((sum, ri) =>
                indices.some(i => i == si + ri)
                    ? (sum + 1) : sum
                , 0) < 4
            ) {
                sis.push(si)
            }
            return sis
        }, new Array<number>())
    }

    draw(indices: number[], imd: ImageData, color: TColor | TGetColor, fillLod: boolean,
        mixColor = false
    ) {
        if (!indices || indices.length == 0) return
        const lod = this.lod, lf = fillLod ? lod : 1,
            lw = this.width, w = imd.width,
            il = Math.max(1, indices.length - 1)
        let c: TColor, isFunc = typeof color == 'function'
        if (!isFunc) c = <TColor>color
        let an: number, ao: number, mix: boolean
        indices.forEach((pi, i) => {
            if (isFunc) c = (<TGetColor>color)(i / il)
            mix = mixColor && c.a != undefined
            if (mix) { an = c.a / 255, ao = 1 - an }
            for (let ly = 0; ly < lf; ly++) {
                for (let lx = 0; lx < lf; lx++) {
                    const p = 4 * (lod == 1 ? pi : (~~(pi / lw) * lod + ly) * w + (pi % lw) * lod + lx)
                    if (mix) {
                        imd.data[p] = ~~(imd.data[p] * ao + c.r * an)
                        imd.data[p + 1] = ~~(imd.data[p + 1] * ao + c.g * an)
                        imd.data[p + 2] = ~~(imd.data[p + 2] * ao + c.b * an)
                        imd.data[p + 3] = 255
                    } else {
                        imd.data[p] = c.r
                        imd.data[p + 1] = c.g
                        imd.data[p + 2] = c.b
                        if (c.a != undefined) imd.data[p + 3] = c.a
                    }
                }
            }
        })
    }

    private static getBoundingBox(indices: number[], width: number, unpad = false) {
        let xmin = Infinity, xmax = -Infinity, ymin = xmin, ymax = xmax
        indices.forEach(i => {
            const x = i % width, y = ~~(i / width)
            if (x < xmin) xmin = x
            if (x > xmax) xmax = x
            if (y < ymin) ymin = y
            if (y > ymax) ymax = y
        })
        if (unpad) { xmin-- , xmax-- , ymin-- , ymax-- }
        return { xmin, xmax, ymin, ymax }
    }

    private static zonesByLevel(data: Float32Array, dataWidth: number,
        lod = 1,
        contourMethod: ContourMethod,
        contourMinArea = 1,
        removeContourColinears = false,
        computeNeighborZones = false,
        keepEdges = false,
        log = false, debug = false
    ) {
        if (!data) return null

        let levelIndex: Uint32Array,
            zoneIndex: Uint32Array,
            typeIndex: Uint32Array,
            noiseZoneIndex: Uint32Array,
            maxLevelIndex: number

        const
            levelZoneIndex = new Array<Array<number>>(),
            zones = new Array<HMapZone>(),
            noiseZones = new Array<HMapZone>(),
            w = Math.round(dataWidth / lod),
            h = Math.round((data.length / dataWidth) / lod),
            wp2 = w + 2, hp2 = h + 2, whp2 = wp2 * hp2,
            wp1 = w + 1, hp1 = h + 1,
            NOTHING = -1 >>> 0 // Uint32 max value

        function init() { //lod + padding
            typeIndex = new Uint32Array(whp2)
            levelIndex = new Uint32Array(whp2).fill(NOTHING)
            zoneIndex = new Uint32Array(whp2).fill(NOTHING)
            noiseZoneIndex = new Uint32Array(whp2).fill(NOTHING)
            const levelsMap = new Map<number, number>() //value -> index
            const lw = lod * dataWidth
            let ywp2: number, yn1lw: number, v: number, li: number
            for (let y = 1; y < hp1; y++) {
                ywp2 = y * wp2
                yn1lw = (y - 1) * lw
                for (let x = 1; x < wp1; x++) {
                    v = data[yn1lw + (x - 1) * lod]
                    li = levelsMap.get(v)
                    if (li == undefined) {
                        li = levelZoneIndex.length
                        levelZoneIndex.push(new Array<number>())
                        levelsMap.set(v, li)
                    }
                    levelIndex[ywp2 + x] = li
                }
            }
            maxLevelIndex = levelsMap.get(1)
        }

        function parcour() {

            const PT_CONTOUR = HMZPointType.Contour,
                PT_EDGE = HMZPointType.Edge,
                noContour = new Array<number>(),
                edges = new Array<number>(),
                noise = new Array<number>()

            for (let y = 1; y < hp1; y++) {
                const ywp2 = y * wp2
                for (let x = 1; x < wp1; x++) {
                    const i = ywp2 + x
                    if (zoneIndex[i] == NOTHING) {
                        const li = levelIndex[i],
                            lzis = levelZoneIndex[li],
                            zi = zones.length,
                            lzi = lzis.length,
                            zone = new HMapZone(zi, li, lzi)
                        lzis.push(zi)
                        zones.push(zone)

                        edges.length = 0

                        if (computeNeighborZones)
                            zone.neighborZones = new Set<number>()

                        Zoning.getZone(i, li, zi,
                            levelIndex, zoneIndex,
                            NOTHING, wp2, zone.points, edges,
                            typeIndex, HMZPointType.Interior, HMZPointType.Edge,
                            zone.neighborZones)

                        if (keepEdges) zone.edges = edges.slice()

                        if (contourMethod == ContourMethod.NoContour) {
                            zone.contour = noContour
                        } else if (zone.points.length < contourMinArea) {
                            zone.contour = noContour
                            noise.push(...zone.points)
                        }
                        else {
                            zone.contour = Zoning.getContour(edges[0], zi, zoneIndex, wp2, contourMethod, removeContourColinears)
                            zone.contour.forEach(i => typeIndex[i] = PT_CONTOUR)
                        }

                    }
                }
            }

            if (computeNeighborZones)
                zones.forEach(z => {
                    const zi = z.zoneIndex
                    z.neighborZones.forEach(nzi => {
                        const nz = zones[nzi]
                        if (!nz.neighborZones.has(zi)) {
                            nz.neighborZones.add(zi)
                            if (!nz.interiorZones) nz.interiorZones = new Set<number>()
                            nz.interiorZones.add(zi)
                        }
                    })
                })

            const NOISE = HMZPointType.Noise
            noise.forEach(i => typeIndex[i] = NOISE)
            noise.forEach(i => {
                if (noiseZoneIndex[i] == NOTHING) {
                    const nzi = noiseZones.length
                    const zone = new HMapZone(nzi)
                    noiseZones.push(zone)
                    Zoning.getZone(i, NOISE, nzi,
                        typeIndex, noiseZoneIndex, NOTHING, wp2, zone.points, edges)
                    zone.contour = Zoning.getContour(edges[0], nzi, noiseZoneIndex, wp2)

                    //zone.contour.forEach(i => typeIndex[i] = PT_CONTOUR)
                }
            })

            if (debug) {
                const c = noiseZones.reduce((p, c) => p + c.points.length, 0)
                if (c != noise.length) console.warn('HeightMapZones', 'noise', noiseZones.length, c, noiseZones.map(z => z.points.length))
            }
        }

        function unpad() {
            function unpadImage(image: Uint32Array) {
                const result = new Uint32Array(h * w)
                let yw: number, ywp: number
                for (let y = 0; y < h; y++) {
                    yw = y * w
                    ywp = (y + 1) * wp2 + 1
                    for (let x = 0; x < w; x++)
                        result[yw + x] = image[ywp + x]
                }
                return result
            }
            function unpadIndices(indices: Array<number>) {
                if (!indices) return
                for (let ii = 0, l = indices.length; ii < l; ii++) {
                    const pi = indices[ii]
                    indices[ii] = (~~(pi / wp2) - 1) * w + (pi % wp2) - 1
                }
            }
            levelIndex = unpadImage(levelIndex)
            zoneIndex = unpadImage(zoneIndex)
            typeIndex = unpadImage(typeIndex)
            zones.forEach(zone => {
                unpadIndices(zone.points)
                if (zone.contour) unpadIndices(zone.contour)
                if (zone.edges) unpadIndices(zone.edges)
            })
            noiseZones.forEach(zone => {
                unpadIndices(zone.points)
                if (zone.contour) unpadIndices(zone.contour)
            })
        }

        function trace(id: string, work: () => void) {
            const label = HeightMapZones.name + ' ' + id
            if (log) console.time(label)
            work()
            if (log) console.timeEnd(label)
        }

        trace('zonesByLevel', () => {
            trace('init', init)
            trace('parcour', parcour)
            trace('unpad', unpad)
        })

        return new HeightMapZones(
            lod, contourMethod, log, debug,
            data, dataWidth,
            w, h,
            zones, levelZoneIndex, levelIndex, zoneIndex, typeIndex,
            noiseZones, noiseZoneIndex, maxLevelIndex
        )
    }

}
