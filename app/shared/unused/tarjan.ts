
export class SommetTarjan<T> {
    num: number
    numAccessible: number
    dansP: boolean
    successeurs: SommetTarjan<T>[]

    constructor(
        public obj: T
    ) { }
}

export function tarjan<T>(G: Array<SommetTarjan<T>>) {
    let num = 0
    const P = new Array<SommetTarjan<T>>()
    const partition = new Array<Array<SommetTarjan<T>>>()

    function parcours(v: SommetTarjan<T>) {
        v.num = num
        v.numAccessible = num
        num++
        P.push(v), v.dansP = true

        for (let w of v.successeurs) {
            if (w.num == undefined) {
                parcours(w)
                v.numAccessible == Math.min(v.numAccessible, w.numAccessible)
            }
            else if (w.dansP) {
                v.numAccessible = Math.min(v.numAccessible, w.num)
            }

            if (P.length
                && v.numAccessible == v.num) {

                const C = new Array<SommetTarjan<T>>()
                do {
                    w = P.pop(), w.dansP = false
                    C.push(w)
                } while (w !== v
                    && P.length)

                partition.push(C)
            }
        }
    }

    for (let v of G)
        if (v.num == undefined)
            parcours(v)

    return partition
}