
//declare global {
    interface Set<T> {
        map<T2>(mapper: (v: T) => T2): Set<T>
        reduce<T2>(reducer: (p: T2, c: T) => T2, initial: T2): T2
    }
//}
Set.prototype.map = function <T, T2>(mapper: (v: T) => T2) {
    const result = new Set<T2>()
    for (var v of this) result.add(mapper(v))
    return result;
}
Set.prototype.reduce = function <T2, T>(reducer: (p: T2, c: T) => T2, initial: T2) {
    let result = initial;
    for (var v of this) result = reducer(result, v);
    return result;
}

//declare global {
    interface Map<K, V> {
        map<T>(mapper: (v: V) => T): Array<T>
        reduce<T>(reducer: (p: T, c: V) => T, initial: T): T
    }
//}
Map.prototype.map = function <V, T>(mapper: (v: V) => T) {
    const result = new Array<T>()
    for (var v of this) result.push(mapper(v))
    return result;
}
Map.prototype.reduce = function <V, T>(reducer: (p: T, c: V) => T, initial: T) {
    let result = initial;
    for (var v of this) result = reducer(result, v);
    return result;
}