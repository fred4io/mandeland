//import { Texture, UniformsUtils, ShaderMaterial, Color } from "three";

// function makeShaderTerrainMaterial(texture: Texture, detailTexture: Texture) {

//     // the following configuration defines how the terrain is rendered
//     var terrainShader = ShaderTerrain().terrain;
//     var uniformsTerrain = UniformsUtils.clone(terrainShader.uniforms);

//     console.log('uniformsTerrain', uniformsTerrain)

//     // how to treat abd scale the normal texture
//     uniformsTerrain.tNormal.texture = detailTexture;
//     uniformsTerrain.uNormalScale.value = 1;

//     // the displacement determines the height of a vector, mapped to
//     // the heightmap
//     uniformsTerrain.tDisplacement.texture = texture;
//     uniformsTerrain.uDisplacementScale.value = 100;

//     // the following textures can be use to finetune how
//     // the map is shown. These are good defaults for simple
//     // rendering
//     uniformsTerrain.tDiffuse1.texture = detailTexture;
//     uniformsTerrain.tDetail.texture = detailTexture;
//     uniformsTerrain.enableDiffuse1.value = true;
//     uniformsTerrain.enableDiffuse2.value = true;
//     uniformsTerrain.enableSpecular.value = true;

//     // diffuse is based on the light reflection
//     uniformsTerrain.diffuse.value.setHex(0xcccccc);
//     uniformsTerrain.specular.value.setHex(0xff0000);

//     // is the base color of the terrain
//     uniformsTerrain.ambientLightColor.value.push(new Color(0x0000cc))

//     // how shiny is the terrain
//     uniformsTerrain.shininess.value = 3;

//     // handles light reflection
//     uniformsTerrain.uRepeatOverlay.value.set(3, 3);

//     // configure the material that reflects our terrain
//     var material = new ShaderMaterial({
//         uniforms: uniformsTerrain,
//         vertexShader: terrainShader.vertexShader,
//         fragmentShader: terrainShader.fragmentShader,
//         lights: true,
//         fog: false
//     });

//     return material
// }