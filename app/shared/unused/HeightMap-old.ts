import { Region2D } from "../Region2D";

export class Zone {
    points: Array<number>
    edges: Array<number>
    constructor() {
        this.points = new Array<number>()
    }
}
//declare type Zone = Array<number>
declare type Contour = Array<number>
declare type Level = Array<Zone>
declare type TColor = { r: number, g: number, b: number, a?: number }

export interface IHeightMapProducer {
    getHeightMap(flatten?: boolean, info?: { min: number }): HeightMap1
}

export class HeightMap1 {

    static fromUint16(region: Region2D, data: Uint16Array,
        min?: number, max?: number,
        dataWidth = region.width, dataHeight = region.height,
        dataHolder?: Float32Array, log = false, getValues = false
    ) {
        if (min == undefined) { min = Infinity; data.forEach(d => { if (d < min) min = d }) }
        if (max == undefined) { max = -Infinity; data.forEach(d => { if (d > max) max = d }) }
        const l = data.length, h = max - min, d = dataHolder || new Float32Array(l)
        for (let i = 0; i < l; i++) d[i] = (data[i] - min) / h
        const hmap = new HeightMap1(region, d, dataWidth, dataHeight)
        if (log) console.log('fromUint16', {
            length: l, width: dataWidth, height: dataHeight,
            min: min, max: max, values: getValues && hmap.countValues()
        })
        return hmap
    }

    constructor(
        public region: Region2D,
        public data: Float32Array,
        public dataWidth = region.width,
        public dataHeight = region.height,
    ) { }

    clone() {
        return new HeightMap1(
            this.region.clone(),
            this.data.slice(),
            this.dataWidth, this.dataHeight)
    }
    copy(hmap: HeightMap1, byRef: boolean) {
        this.region = byRef ? hmap.region : hmap.region.clone()
        this.data = byRef ? hmap.data : hmap.data.slice()
        this.dataWidth = hmap.dataWidth
        this.dataHeight = hmap.dataHeight
        return this
    }

    getMin() { return this.data.reduce((p, c) => c < p ? c : p, Infinity) }
    getMax() { return this.data.reduce((p, c) => c > p ? c : p, -Infinity) }
    countNans() { return this.data.reduce((p, c) => isNaN(c) ? p + 1 : p, 0) }
    getValues() { return Array.from(new Set(this.data).values()) }
    countValues() { return this.getValues().length }
    counts() {
        return {
            width: this.dataWidth,
            height: this.dataHeight,
            length: this.data.length,
            min: this.getMin(),
            max: this.getMax(),
            nans: this.countNans(),
            values: this.countValues()
        }
    }

    flatten(info?: { min: number }) {
        const min = this.getMin(), d = this.data, l = d.length
        for (let i = 0; i < l; i++) d[i] = d[i] - min
        if (info) info.min = min
        return this
    }

    combine(newParts: HeightMap1[], commonPart: Region2D, log = false) {
        if (log) console.log('combine', newParts.length, !!commonPart)
        const isDezoom = commonPart && newParts && newParts.length == 4
        if (commonPart)
            if (isDezoom) this.dezoom(commonPart, log)
            else this.move(commonPart, log)
        newParts.forEach(part => this.copyPart(part.region, part.data, log))
        return this
    }

    private copyPart(sr: Region2D, sd: Float32Array, log = false) {
        const dd = this.data, dr = this.region, dw = dr.width
        let y0 = sr.ymin, x0 = sr.xmin, h = sr.height, w = sr.width
        const xodd = sr.xmin % 2 !== 0
        if (log) console.log('copy part', x0, y0, w, h)
        for (let y = 0, d, s; y < h; y++) {
            d = dw * (y0 + y) + x0
            s = w * y
            for (let x = 0; x < w; x++) {
                dd[d + x] = sd[s + x]
            }
        }
    }

    private move(dr: Region2D, log = false) {
        const d = this.data, sr = this.region,
            txp = dr.xmin - sr.xmin, txn = dr.xmax - sr.xmax,
            typ = dr.ymin - sr.ymin, tyn = dr.ymax - sr.ymax
        if (log) console.log('move part', txp || txn, typ || tyn)
        const s = sr.width, h = dr.height, w = dr.width, t = 1,
            dx = txp ? -1 : 1, dy = typ ? -1 : 1,
            x0 = txp ? (sr.xmax - txp - t) : sr.xmin,
            y0 = typ ? (sr.ymax - typ - t) : sr.ymin
        for (let j = 0, y = y0, syw, dyw; j < h; j++ , y += dy) {
            syw = s * (y - tyn)
            dyw = s * (y + typ)
            for (let i = 0, x = x0; i < w; i++ , x += dx) {
                d[dyw + x + txp] = d[syw + x - txn]
            }
        }
    }

    private dezoom(dr: Region2D, log = false) {
        const dd = this.data, sd = dd.slice(),
            sr = this.region, sw = sr.width, sh = sr.height,
            dw = dr.width, dh = dr.height, dx0 = dr.xmin, dy0 = dr.ymin,
            ax = sw / dw,
            ay = sh / dh

        if (log) console.log('dezoom',
            (100 / ax).toFixed(1) + '%',
            (100 / ay).toFixed(1) + '%')

        for (let y = 0, sy: number, dywx0, syw; y < dh; y++) {
            sy = Math.round(y * ay)
            syw = sy * sw
            dywx0 = (dy0 + y) * sw + dx0
            for (let x = 0, sx: number; x < dw; x++)
                dd[dywx0 + x] = sd[syw + Math.round(x * ax)]
        }
    }

    computeZonesData(lod = 1, log = false, debug = false) {
        const zonesInfo = HeightMap1.zonesByLevel(
            this.data, this.dataWidth, this.dataHeight, lod, log, debug)
        return new HMapZonesData(this, zonesInfo)
    }

    private static zonesByLevel(data: Float32Array, width: number, height: number,
        lod = 1, log = false, debug = false
    ) {
        if (!data) return null
        if (height * width != data.length) throw 'size mismatch'

        const
            w = Math.round(width / lod),
            h = Math.round(height / lod),
            l = w * h,
            levelIndex = new Uint32Array(l),
            zoneIndex = new Array<number>(l),
            pointIsEdge = new Array<boolean>(l),
            levels = new Array<Level>()

        function makeLevels() {
            const levelsMap = new Map<number, number>() //value -> index
            let levelIdx: number
            for (let j = 0, idx: number, jlw: number, jw: number; j < h; j++) {
                jlw = j * w
                jw = j * lod * width
                for (let i = 0, v: number; i < w; i++) {
                    v = data[jw + i * lod]
                    levelIdx = levelsMap.get(v)
                    if (levelIdx == undefined) {
                        levelIdx = levels.length
                        levels.push(new Array<Zone>())
                        levelsMap.set(v, levelIdx)
                    }
                    levelIndex[jlw + i] = levelIdx
                }
            }
        }

        function makeZones() {

            function floodFillDF(i: number, levelIdx: number, zoneIdx: number, zone: Zone) {
                const zonePoints = zone.points, stack = new Array<number>()
                stack.push(i)
                while (stack.length) {
                    const pi = stack.pop()
                    zoneIndex[pi] = zoneIdx
                    zonePoints.push(pi)

                    const x = pi % w, y = ~~(pi / w)
                    let ni: number, nn = 0
                    if (y > 0) {
                        ni = pi - w  //top
                        if (levelIndex[ni] == levelIdx) {
                            nn++
                            if (zoneIndex[ni] == undefined)
                                stack.push(ni)
                        }
                    }
                    if (x < w) {
                        ni = pi + 1 //right
                        if (levelIndex[ni] == levelIdx) {
                            nn++
                            if (zoneIndex[ni] == undefined)
                                stack.push(ni)
                        }
                    }
                    if (y < h) {
                        ni = pi + w //bottom
                        if (levelIndex[ni] == levelIdx) {
                            nn++
                            if (zoneIndex[ni] == undefined)
                                stack.push(ni)
                        }
                    }
                    if (x > 0) {
                        ni = pi - 1 //left
                        if (levelIndex[ni] == levelIdx) {
                            nn++
                            if (zoneIndex[ni] == undefined)
                                stack.push(ni)
                        }
                    }

                    if (nn < 4) {
                        pointIsEdge[pi] = true
                    }
                }
            }

            for (let i = 0; i < l; i++) {
                if (zoneIndex[i] != undefined)
                    continue
                const levelIdx = levelIndex[i],
                    level = levels[levelIdx],
                    zone = new Zone()
                level.push(zone)
                floodFillDF(i, levelIdx, level.length - 1, zone)
            }
        }

        let contours: TContourInfo
        function makeContours() {
            contours = getContoursByLevel(levelIndex, w, h)
        }

        function trace<T>(id: string, work: () => T,
            msg = () => (undefined as any), isdebug = false
        ) {
            const start = performance.now(), result = work()
            if (log && (debug || !isdebug))
                console.log(id, (performance.now() - start).toFixed(0) + 'ms', (msg ? ('' + msg()) : ''))
            return result
        }

        trace("zonesByLevel", () => {
            trace("levels", makeLevels, () => levels.length, true)
            trace("zones", makeZones, () => ''/*levels.reduce((p, c) => p + c.length, 0)*/, true)
            trace("contours", makeContours, () => '', true)
        }, () => 'lod: ' + lod + ' ' + w + 'x' + h)

        return new ZonesInfo(lod, w, h, levels, levelIndex, zoneIndex, pointIsEdge, contours)
    }
}

export class ZonesInfo {
    constructor(
        public lod: number,
        public width: number,
        public height: number,
        public levels: Array<Level>,
        public pointLevelIndex: Uint32Array,
        public pointLevelZoneIndex: Array<number>,
        public pointIsEdge: Array<boolean>,
        public contours: TContourInfo
    ) { }

    levelPointsIndices(level: Level, edgesElseInners: boolean) {
        const isEdge = this.pointIsEdge
        return level.reduce((p, z) =>
            z.points.reduce((p, i) => {
                if (!!isEdge[i] == edgesElseInners) p.push(i)
                return p
            }, p), new Array<number>())
    }

    zonePointsIndices(zone: Zone, edgesElseInners: boolean) {
        const isEdge = this.pointIsEdge
        return zone.points.reduce((p, i) => {
            if (!!isEdge[i] == edgesElseInners) p.push(i)
            return p
        }, new Array<number>())
    }
}


export class HMapZonesData {
    constructor(
        public hmap: HeightMap1,
        public info: ZonesInfo
    ) { }
    getPointIndex(x: number, y: number) {
        return this.hmap.dataWidth * y + x
    }

    getLevel(x: number, y: number) {
        const w = this.hmap.dataWidth, i = y * w + x
        return this.info.levels[this.info.pointLevelIndex[i]]
    }
    drawLevel(l: Level, imd: ImageData,
        edges?: TColor,
        inners?: TColor,
        info?: { edges: number, inners: number }
    ) {
        if (edges) {
            const indices = this.info.levelPointsIndices(l, true)
            if (info) info.edges = indices.length
            this.draw(indices, imd, edges)
        }
        if (inners) {
            const indices = this.info.levelPointsIndices(l, false)
            if (info) info.inners = indices.length
            this.draw(indices, imd, inners)
        }
    }

    getZone(x: number, y: number) {
        const w = this.hmap.dataWidth, i = y * w + x, info = this.info,
            level = info.levels[info.pointLevelIndex[i]]
        return level && level[info.pointLevelZoneIndex[i]]
    }
    drawZone(zone: Zone, imd: ImageData, edges: TColor, inners: TColor = edges,
        info?: { edges: number, inners: number }
    ) {
        if (edges) {
            const indices = this.info.zonePointsIndices(zone, true)
            if (info) info.edges = indices.length
            this.draw(indices, imd, edges)
        }
        if (inners) {
            const indices = this.info.zonePointsIndices(zone, false)
            if (info) info.inners = indices.length
            this.draw(indices, imd, inners)
        }
    }

    getContourLevel(xOrIndex: number, y?: number) {
        const i = y == undefined ? xOrIndex : this.getPointIndex(xOrIndex, y)
        return this.info.contours.levels.get(this.info.contours.levelIndex[i])
    }
    getContour(xOrIndex: number, y?: number) {
        const i = y == undefined ? xOrIndex : this.getPointIndex(xOrIndex, y),
            info = this.info.contours,
            level = info.levels.get(info.levelIndex[i])
        return level && level[info.contourIndex[i]]
    }

    draw(indices: number[], imd: ImageData, color: TColor) {
        if (!indices || indices.length == 0) return
        let p: number
        indices.forEach(i => {
            p = i * 4
            imd.data[p] = color.r
            imd.data[p + 1] = color.g
            imd.data[p + 2] = color.b
            if (color.a != undefined)
                imd.data[p + 3] = color.a
        })
    }
}

declare type TContourInfo = {
    levels: Map<number, Array<Array<number>>>,
    levelIndex: Uint32Array,
    contourIndex: Uint32Array
}

function getContoursByLevel(image: Uint32Array, width: number, height: number, isPadded = false): TContourInfo {

    //adapted from :
    // https://www.eriksmistad.no/moore-neighbor-contour-tracing-algorithm-in-c/

    const levels = new Map<number, Array<Array<number>>>()
    let level: Array<Array<number>>, contour: Array<number>

    // Defines the neighborhood offset position from current position and the neighborhood
    // position we want to check next if we find a new border at checkLocationNr
    const neighborhood = [
        [-1, 7],
        [-3 - width, 7],
        [-width - 2, 1],
        [-1 - width, 1],
        [1, 3],
        [3 + width, 3],
        [width + 2, 5],
        [1 + width, 5]
    ]

    // Need to start by padding the image by 1 pixel
    const NOLABEL = 4294967295, //(uint32) -1,
        paddedImage = isPadded ? image : padImage(image, width, height, NOLABEL),
        wp2hp2 = (width + 2) * (height + 2),
        borderImage = new Uint32Array(wp2hp2).fill(NOLABEL),
        contourIndex = new Uint32Array(wp2hp2).fill(NOLABEL)

    let firstNonWhite = width + 3,
        LABEL = paddedImage[firstNonWhite],
        inside = false

    for (let pos = firstNonWhite; pos < wp2hp2; pos++) {

        // Scan for BLACK pixel
        if (!inside && borderImage[pos] == LABEL)	// Entering an already discovered border
            inside = true
        else if (inside && paddedImage[pos] == LABEL)	// Already discovered border point
            continue
        else if (inside && paddedImage[pos] != LABEL)	// Leaving a border
        {
            inside = false

            let i = pos; while (paddedImage[i] == NOLABEL) i++
            LABEL = paddedImage[i]
            pos--
        }
        else if (!inside && paddedImage[pos] == LABEL)	// Undiscovered border point
        {
            borderImage[pos] = LABEL 	// Mark the start pixel
            let checkLocationNr = 1,	// The neighbor number of the location we want to check for a new border point
                checkPosition: number,	// The corresponding absolute array address of checkLocationNr
                newCheckLocationNr: number, // Variable that holds the neighborhood position we want to check if we find a new border at checkLocationNr
                startPos = pos,			// Set start position
                counter = 0, 			// Counter is used for the jacobi stop criterion
                counter2 = 0 			// Counter2 is used to determine if the point we have discovered is one single point

            level = levels.get(LABEL)
            if (!level) levels.set(LABEL, level = new Array<Array<number>>())
            level.push(contour = new Array<number>())
            contour.push(pos)
            const contourIdx = level.length - 1
            contourIndex[pos] = contourIdx

            // Trace around the neighborhood
            while (true) {
                checkPosition = pos + neighborhood[checkLocationNr - 1][0]
                newCheckLocationNr = neighborhood[checkLocationNr - 1][1]

                if (paddedImage[checkPosition] == LABEL) // Next border point found
                {
                    if (checkPosition == startPos) {
                        counter++

                        // Stopping criterion (jacob)
                        if (newCheckLocationNr == 1 || counter >= 3) {
                            // Close loop
                            inside = true // Since we are starting the search at were we first started we must set inside to true
                            break
                        }
                    }

                    // Update which neighborhood position we should check next
                    checkLocationNr = newCheckLocationNr
                    pos = checkPosition
                    // Reset the counter that keeps track of how many neighbors we have visited
                    counter2 = 0
                    // Set the border pixel 						    
                    borderImage[pos] = LABEL
                    contour.push(pos)
                    contourIndex[pos] = contourIdx
                }
                else {
                    // Rotate clockwise in the neighborhood
                    checkLocationNr = 1 + (checkLocationNr % 8)
                    if (counter2 > 8) {
                        // If counter2 is above 8 we have traced around the neighborhood and
                        // therefor the border is a single black pixel and we can exit
                        counter2 = 0
                        break
                    }
                    else {
                        counter2++
                    }
                }
            }
        }
    }

    console.log('nocontour', contourIndex.reduce((p, c) => c == NOLABEL ? p : (p + 1), 0))

    return {
        levels: unpadBorders(levels, width),
        levelIndex: unpadImage(borderImage, width, height),
        contourIndex: unpadImage(contourIndex, width, height)
    }
}

function padImage(image: Uint32Array, width: number, height: number, paddingColor: number) {
    const wp2 = width + 2, hp2 = height + 2, wp1 = width + 1, hp1 = height + 1
    const paddedImage = new Uint32Array(wp2 * hp2)
    let ywp2: number, yn1wn1: number
    for (let y = 0; y < hp2; y++) {
        ywp2 = y * wp2
        yn1wn1 = (y - 1) * width - 1
        for (let x = 0; x < wp2; x++)
            paddedImage[ywp2 + x] =
                (x == 0 || y == 0 || x == wp1 || y == hp1) ?
                    paddingColor
                    : image[yn1wn1 + x]
    }
    return paddedImage
}

function unpadImage(image: Uint32Array, width: number, height: number) {
    const result = new Uint32Array(height * width)
    let yw: number, wp2 = width + 2, yp1wp2p1: number
    for (let y = 0; y < height; y++) {
        yw = y * width
        yp1wp2p1 = (y + 1) * wp2 + 1
        for (let x = 0; x < width; x++)
            result[yw + x] = image[yp1wp2p1 + x]
    }
    return result
}
function unpadIndices(indices: Array<number>, width: number) {
    const wp2 = width + 2
    for (let ii = 0, l = indices.length; ii < l; ii++) {
        const pi = indices[ii]
        indices[ii] = (~~(pi / wp2) - 1) * width + (pi % wp2) - 1
    }
}
function unpadBorders(levels: Map<number, Array<Array<number>>>, width: number) {
    for (let level of levels.values())
        for (let li = 0, ll = level.length; li < ll; li++)
            unpadIndices(level[li], width)
    return levels
}

