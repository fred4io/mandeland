//https://www.npmjs.com/package/three-shader-terrain

declare function ShaderTerrain() : {
    terrain: {
        vertexShader: any
        fragmentShader: any
        uniforms: any
    }
}

declare module 'three-shader-terrain' {

    export = ShaderTerrain;

}

