export class Complex {

    static getPathInMandelbrot = (function () {

        return function (a: number, b: number, max: number, r: number = 2) {
            max = Math.ceil(max)
            const xys = new Array<number>(2 * max), r2 = r * r
            let i = 0, n = 0, za = a, zb = b, za2 = za * za, zb2 = zb * zb, t
            while (za2 + zb2 <= r2 && n++ < max) {
                t = za
                za = za2 - zb2 + a
                zb = 2 * t * zb + b
                za2 = za * za, zb2 = zb * zb
                xys[i] = za
                xys[i + 1] = zb
                i += 2
            }
            xys.length = i > max ? i - 2 : i
            return xys
        }

    })()

    constructor(public a?: number, public b?: number) {
        this.a = a || 0;
        this.b = b || 0
    }

    set(a?: number, b?: number): Complex {
        if (a != undefined) { this.a = a; }
        if (b != undefined) { this.b = b; }
        return this;
    }

    add(c: Complex): Complex {
        this.a += c.a;
        this.b += c.b;
        return this;
    }

    mult(c: Complex): Complex {
        this.a = this.a * c.a - this.b * c.b;
        this.b = this.a * c.b + this.b * c.a;
        return this;
    }

    squared(): Complex {
        const a = this.a, b = this.b;
        this.a = a * a - b * b;
        this.b = 2 * a * b;
        return this;
    }

    squaredPlus(c: Complex): Complex {
        const a = this.a, b = this.b;
        this.a = a * a - b * b + c.a;
        this.b = 2 * a * b + c.b;
        return this;
    }
}