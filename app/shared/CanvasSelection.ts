import { TMouseEvent } from "./MouseEvents"

declare type Rect = { x: number, y: number, width: number, height: number }
declare type StrokeStyle = string | { r: number, g: number, b: number }
function rgb(c: StrokeStyle) { return typeof c == 'string' ? c : `rgb(${c.r},${c.g},${c.b})` }

export class CanvasSelection {
    private x: number
    private y: number
    private cx: number
    private cy: number

    private ctx: CanvasRenderingContext2D
    private imd: ImageData

    private isStarted: boolean
    private isAttached: boolean

    private md: EventListener
    private mm: EventListener
    private mu: EventListener
    private kd: EventListener

    private tmdown: number
    private tmup: number

    private divInfoX: HTMLElement
    private divInfoY: HTMLElement

    public log = false

    public getStrokeStyle: (x: number, y: number, forSelectionBox?: boolean) => StrokeStyle

    public get showCross(): boolean { return this._showCross }
    public set showCross(show: boolean) {
        this.showXYInfo = this._showCross = show
        if (!show) { this.cancelSelection() }
    }

    public get showXYInfo(): boolean { return this._showXYInfo }
    public set showXYInfo(show: boolean) { this._showXYInfo = show; if (!show) { this.removeInfoDivs() } }

    public onUpdated = () => { }

    constructor(
        private canvas: HTMLCanvasElement,
        private onSelected: (cs: Rect) => void,
        private _showCross = false,
        private strokeStyle: StrokeStyle | ((x: number, y: number, b?: boolean) => StrokeStyle) = 'rgb(255,0,0)',
        private _showXYInfo = false,
        public getInfoXY: (x: number, y: number) => { x: any, y: any } = (x, y) => { return { x, y } },
        private _infoXYClassName?: string
    ) {
        const getX = (e:TMouseEvent) => e.layerX
        const getY = (e:TMouseEvent) => e.layerY
        this.md = (e: TMouseEvent) => { this.start(getX(e), getY(e)) }
        this.mm = (e: TMouseEvent) => { this.update(getX(e), getY(e)) }
        this.mu = (e: TMouseEvent) => { this.end(getX(e), getY(e)) }
        this.kd = (e: KeyboardEvent) => { if (e.code == 'Escape') this.cancelSelection() }
        this.getStrokeStyle = typeof strokeStyle == 'function' ? strokeStyle : () => strokeStyle
        this.attach()
    }

    public attach() {
        if (this.isAttached) { return }
        const canvas = this.canvas
        canvas.addEventListener('mousedown', this.md)
        canvas.addEventListener('mousemove', this.mm)
        canvas.addEventListener('mouseup', this.mu)
        document.addEventListener('keydown', this.kd)
        this.isAttached = true
    }
    public detach() {
        if (!this.isAttached) { return }
        const canvas = this.canvas
        canvas.removeEventListener('mousedown', this.md)
        canvas.removeEventListener('mousemove', this.mm)
        canvas.removeEventListener('mouseup', this.mu)
        document.removeEventListener('keydown', this.kd)
        this.removeInfoDivs()
        this.clear()
        this.isAttached = false
    }

    public cancel() {
        const wasStarted = this.isStarted
        this.reset()
        if (wasStarted) { if (this.log) console.log('selection canceled') }
    }

    public reset(ctx?: CanvasRenderingContext2D, noUpdate = false) {
        if (ctx) { this.ctx = ctx }
        this.imd = null
        this.isStarted = false
        this.x = this.y = undefined
        if (this._showCross && !noUpdate) { this.update(this.cx, this.cy) }
        else if (noUpdate) { this.removeInfoDivs() }
    }

    private start(x: number, y: number) {
        this.tmdown = performance.now()
        if (this.isStarted) { return }
        this.isStarted = true
        if (this.log) console.log('selection start')
        this.x = x
        this.y = y
        this.clear()
    }
    private update(x: number, y: number) {
        if (this._showCross && !this.isStarted) {
            this.updateCross(x, y)
        } else if (this.isStarted && this.imd && x != this.x && y != this.y) {
            this.updateBox(x, y)
        }
    }
    private end(x: number, y: number) {
        this.tmup = performance.now()
        if (!this.isStarted) { return }
        if (this.log) console.log('selection end')
        if (x != this.x || y != this.y) {
            this.putImage()
            this.imd = null
            const x1 = Math.min(this.x, x), y1 = Math.min(this.y, y)
            const x2 = Math.max(this.x, x), y2 = Math.max(this.y, y)
            this.onSelected({ x: x1, y: y1, width: x2 - x1, height: y2 - y1 })
        } else { this.removeInfoDivs() }
        this.isStarted = false
    }

    private putImage() { if (this.imd) { this.getCtx().putImageData(this.imd, 0, 0); return true } }
    private getImage() { this.imd = this.getCtx().getImageData(0, 0, this.canvas.width, this.canvas.height) }
    private clear() { this.putImage() || this.getImage() }


    private cancelSelection() {
        this.putImage()
        this.cancel()
    }
    private updated() {
        if (this.onUpdated) { this.onUpdated() }
    }

    private getCtx() {
        if (!this.ctx) this.ctx = this.canvas.getContext('2d')
        return this.ctx
    }
    private updateCross(x: number, y: number) {
        if (x == undefined || y == undefined) { return }
        this.cx = x, this.cy = y
        const ctx = this.getCtx()
        this.clear()
        ctx.beginPath()
        ctx.strokeStyle = rgb(this.getStrokeStyle(x, y, false))
        ctx.moveTo(x - .5, -.5)
        ctx.lineTo(x - .5, ctx.canvas.height)
        ctx.moveTo(-.5, y - .5)
        ctx.lineTo(ctx.canvas.width, y - .5)
        ctx.stroke()
        if (this._showXYInfo) { this.showPosition(this.cx, this.cy) }
        this.updated()
    }
    private updateBox(x: number, y: number) {
        const ctx = this.getCtx()
        this.putImage()
        ctx.beginPath()
        ctx.strokeStyle = rgb(this.getStrokeStyle(x, y, true))
        ctx.strokeRect(this.x - .5, this.y - .5, x - this.x, y - this.y)
        this.cx = x, this.cy = y
        this.updated()
    }

    private showPosition(x: number, y: number) {
        this.createInfoDivs()
        const elX = this.divInfoX, elY = this.divInfoY
        if (!elX || !elY) { return }
        const info = this.getInfoXY(x, y),
            w = this.ctx.canvas.width, h = this.ctx.canvas.height,
            ww = window.innerWidth, wh = window.innerHeight,
            isLeft = x < w / 2, isTop = y < h / 2,
            sx = elX.style, sy = elY.style
        sx.setProperty('left', isLeft ? ((x + 1) + 'px') : 'auto')
        sx.setProperty('right', isLeft ? 'auto' : ((ww - x + 2) + 'px'))
        sx.setProperty('top', isTop ? 'auto' : '0')
        sx.setProperty('bottom', isTop ? ((wh - h) + 'px') : 'auto')
        elX.innerText = '' + info.x
        sy.setProperty('top', isTop ? ((y + 1) + 'px') : 'auto')
        sy.setProperty('bottom', isTop ? 'auto' : ((wh - y + 3) + 'px'))
        sy.setProperty('left', isLeft ? 'auto' : '0')
        sy.setProperty('right', isLeft ? ((ww - w) + 'px') : 'auto')
        elY.innerText = '' + info.y
    }
    private createInfoDiv() {
        const el = document.createElement('div')
        if (this._infoXYClassName) { el.className = this._infoXYClassName }
        const es = el.style
        es.setProperty('position', 'absolute')
        es.setProperty('top', '0')
        es.setProperty('left', '0')
        es.setProperty('background-color', 'lightgray')
        es.setProperty('padding', '0 2px')
        document.body.appendChild(el)
        return el
    }
    private createInfoDivs() {
        if (!this.divInfoX) { this.divInfoX = this.createInfoDiv() }
        if (!this.divInfoY) { this.divInfoY = this.createInfoDiv() }
    }
    private removeInfoDivs() {
        if (this.divInfoX) { document.body.removeChild(this.divInfoX) }
        this.divInfoX = undefined
        if (this.divInfoY) { document.body.removeChild(this.divInfoY) }
        this.divInfoY = undefined
    }

}