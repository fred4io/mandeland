import { Easing, Easer } from "./Easing";

export class Color {

    private static _instances = new Map<string, IColoR>()

    static text(r: number, g: number, b: number, a?: number) {
        return a == undefined ? `rgb(${r},${g},${b})` : `rgba(${r},${g},${b},${(a / 255).toFixed(2)}`
    }

    private static get(key: string) {
        let cr = this._instances.get(key)
        if (cr != undefined) { return cr }
        cr = new Color((<any>this)[key]())
        this._instances.set(key, cr)
        return cr
    }

    static get Black() { return this.get('black') }
    static get Red() { return this.get('red') }
    static get LRed() { return this.get('pink') }
    static get MRed() { return this.get('mred') }
    static get DRed() { return this.get('dred') }
    static get Yellow() { return this.get('yellow') }
    static get MYellow() { return this.get('myellow') }
    static get Green() { return this.get('green') }
    static get MGreen() { return this.get('mgreen') }
    static get LGreen() { return this.get('lgreen') }
    static get DGreen() { return this.get('dgreen') }
    static get Cyan() { return this.get('cyan') }
    static get MCyan() { return this.get('mcyan') }
    static get Blue() { return this.get('blue') }
    static get MBlue() { return this.get('mblue') }
    static get LBlue() { return this.get('lblue') }
    static get DBlue() { return this.get('dblue') }
    static get Magenta() { return this.get('magenta') }
    static get MMagenta() { return this.get('mmagenta') }
    static get White() { return this.get('white') }
    static get DGray() { return this.get('dgray') }
    static get Gray() { return this.get('gray') }
    static get LGray() { return this.get('lgray') }

    static get Pink() { return this.get('pink') }
    static get MidYellow() { return this.get('midyellow') }
    static get Orange() { return this.get('orange') }

    static Bool(b: boolean) {
        return b == undefined ? this.Blue : b ? this.Green : this.Red
    }

    static black() { return new Color(0, 0, 0) }
    static red() { return new Color(255, 0, 0) }
    static mred() { return new Color(128, 0, 0) }
    static dred() { return new Color(64, 0, 0) }
    static yellow() { return new Color(255, 255, 0) }
    static myellow() { return new Color(128, 128, 0) }
    static green() { return new Color(0, 255, 0) }
    static mgreen() { return new Color(0, 128, 0) }
    static dgreen() { return new Color(0, 64, 0) }
    static cyan() { return new Color(0, 255, 255) }
    static mcyan() { return new Color(0, 128, 128) }
    static blue() { return new Color(0, 0, 255) }
    static mblue() { return new Color(0, 0, 128) }
    static dblue() { return new Color(0, 0, 64) }
    static magenta() { return new Color(255, 0, 255) }
    static mmagenta() { return new Color(128, 0, 128) }
    static white() { return new Color(255, 255, 255) }
    static dgray() { return new Color(64, 64, 64) }
    static gray() { return new Color(128, 128, 128) }
    static lgray() { return new Color(192, 192, 192) }

    static pink() { return new Color(128, 80, 80) }
    static lgreen() { return new Color(128, 255, 128) }
    static lblue() { return new Color(173, 216, 230) }
    static midyellow() { return new Color(160, 160, 0) }
    static orange() { return new Color(255, 165, 0) }

    static bool(b: boolean) {
        return b == undefined ? this.blue() : b ? this.green() : this.red()
    }

    static lerp(a: Color, b: Color, t: number) {
        return new Color(
            Math.round(a.r + (b.r - a.r) * t),
            Math.round(a.g + (b.g - a.g) * t),
            Math.round(a.b + (b.b - a.b) * t),
            Math.round(a.a + (b.a - a.a) * t)
        )
    }

    static getColorizer(color: Color, colors: boolean, invert = false, easing?: Easer) {
        const ease = Easing.func(easing),
            colorize = colors ?
                (t: number) => color.gradient7(ease(t))
                : (t: number) => color.gray01(ease(t))
        return invert ? (t: number) => colorize(t).invert() : colorize
    }

    static getNormalizedColorizer(color: Float32Array, colors: boolean, invert: boolean, easing?: Easer) {
        const colorize = this.getColorizer(new Color(), colors, invert, easing)
        return color.length > 3 ?
            (t: number) => colorize(t).normalizedRGBA(color)
            : (t: number) => colorize(t).normalizedRGB(color)
    }

    static getNormalizedRGBArrayColorizer(colored: boolean, invert = false, easing?: Easer) {
        const colorize = Color.getColorizer(new Color(), colored, invert, easing)
        const colors = new Array<number>()
        const rgb = new Float32Array(3)
        return { colors, rgb, push: (t: number) => { colors.push(...colorize(t).normalizedRGB(rgb)) } }
    }

    constructor(
        public r = 0,
        public g = 0,
        public b = 0,
        public a = 255
    ) { }

    toString(withAlpha = true) {
        return withAlpha ? Color.text(this.r, this.g, this.b, this.a) : Color.text(this.r, this.g, this.b)
    }

    average(withAlpha = false) {
        return withAlpha ? (this.r + this.g + this.b + this.a) / 4 : (this.r + this.g + this.b) / 3
    }

    is(c: Color, checkAlpha = false) {
        return c
            && c.r == this.r
            && c.g == this.g
            && c.b == this.b
            && (!checkAlpha || c.a == this.a)
    }

    copy(c: IColor) {
        return this.set(c.r, c.g, c.b, c.a)
    }
    set(r?: number, g?: number, b?: number, a?: number) {
        if (r != undefined) { this.r = r }
        if (g != undefined) { this.g = g }
        if (b != undefined) { this.b = b }
        if (a != undefined) { this.a = a }
        return this
    }
    setFromImageData(imd: ImageData, x: number, y: number) {
        const d = imd.data, i = y * imd.width + x
        return this.set(d[i], d[i + 1], d[i + 2], d[i + 3])
    }
    setFromUint8ClampedArray(array: Uint8ClampedArray, start = 0) {
        return this.set(array[start], array[start + 1], array[start + 2], array[start + 3])
    }
    gray(n: number, alpha?: number) {
        return this.set(n, n, n, alpha)
    }
    gray01(t: number, alpha?: number) {
        return this.gray(Math.round(t * 255), alpha)
    }
    gradient7(t: number, start = 0, end = 7, alpha?: number) {
        const a = t / (1 / Math.min(7, end - start))
        const x = Math.floor(a)
        const y = Math.floor(255 * (a - x))
        let r: number, g: number, b: number
        switch (x + start) {
            case 0: r = y; g = 0; b = 0; break //noir - rouge
            case 1: r = 255; g = y; b = 0; break //rouge - jaune
            case 2: r = 255 - y; g = 255; b = 0; break //jaune - vert
            case 3: r = 0; g = 255; b = y; break //vert - cyan
            case 4: r = 0; g = 255 - y; b = 255; break //cyan - bleu
            case 5: r = y; g = 0; b = 255; break //bleu - magenta
            case 6: r = 255; g = y; b = 255; break //magenta - blanc
            default: r = 255; g = 255; b = 255; break //blanc
        }
        return this.set(r, g, b, alpha)
    }
    setLerp(a: IColor, b: IColor, t: number) {
        this.r = Math.round(a.r + (b.r - a.r) * t)
        this.g = Math.round(a.g + (b.g - a.g) * t)
        this.b = Math.round(a.b + (b.b - a.b) * t)
        this.a = Math.round(a.a + (b.a - a.a) * t)
        return this
    }

    randomize() {
        this.r = Math.round(Math.random() * 255)
        this.g = Math.round(Math.random() * 255)
        this.b = Math.round(Math.random() * 255)
        return this
    }

    invert() {
        this.r = 255 - this.r
        this.g = 255 - this.g
        this.b = 255 - this.b
        return this
    }

    setAlpha01(t: number) {
        if (t != undefined) { this.a = Math.round(255 * t) }
        return this
    }

    normalizedRGB(result: Float32Array) {
        result[0] = this.r / 255
        result[1] = this.g / 255
        result[2] = this.b / 255
        return result
    }

    normalizedRGBA(result: Float32Array) {
        result[0] = this.r / 255
        result[1] = this.g / 255
        result[2] = this.b / 255
        result[3] = this.a / 255
        return result
    }
}

export interface IColor {
    r: number,
    g: number,
    b: number,
    a: number,
}
export interface IColoR {
    readonly r: number,
    readonly g: number,
    readonly b: number,
    readonly a: number,
}
export type TColor = Color | IColoR