import { DomHelper } from "./DomHelper";

declare type Size = { width: number, height: number }

export class Viewport2D {
    private rs: EventListener
    private attached: boolean
    private resizing: any

    protected _trace = false

    get width() { return this.canvas.width }
    get height() { return this.canvas.height }
    get size() { return { width: this.width, height: this.height } }

    constructor(
        private canvas: HTMLCanvasElement,
        public onResized: () => void,
        public log = true,
        public debug = false
    ) {
        const self = this
        this.rs = _ => self._onResize()
        this.attach(true)
        this.resize(true)
    }

    protected say(...args: any[]) {
        if(this.log) { console.log(this.constructor.name, ...args) }
    }
    protected dbg(...args: any[]) {
        if(this.log && this.debug) { console.log(this.constructor.name, ...args) }
    }

    getContext() { return this.canvas.getContext('2d') }

    isSame(size: Size) { return size && this.width == size.width && this.height == size.height }

    attach(noResize = false) {
        if (this.attached) { return }
        window.addEventListener('resize', this.rs)
        this.attached = true
        this.dbg('resize attached')
        if (!noResize && !this.isSame(DomHelper.windowSize())) this.resize()
    }
    detach() {
        if (!this.attached) { return }
        window.removeEventListener('resize', this.rs)
        this.dbg('resize detached')
        this.attached = false
    }

    private _onResize() {
        clearTimeout(this.resizing)
        this.resizing = setTimeout(() => this.resize(), 300)
    }

    private resize(nodispatch = false) {
        DomHelper.setupCanvas(this.canvas)
        this.say('resized', this.toString())
        if (this.onResized && !nodispatch) { this.onResized() }
    }

    public toString() {
        return this.width + 'x' + this.height
    }
}