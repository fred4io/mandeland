declare type Point = { x: number, y: number }
declare type TCircle = { x: number, y: number, r: number }
declare type Size = { width: number, height: number }
declare type StrokeStyle = string | { r: number, g: number, b: number }
function rgb(c: StrokeStyle) { return typeof c == 'string' ? c : `rgb(${c.r},${c.g},${c.b})` }

export class Circle {
    public static IsIn(px: number, py: number, cx: number, cy: number, r: number) {
        const x = px - cx, y = py - cy
        return x * x + y * y <= r * r
    }
    public static draw(ctx: CanvasRenderingContext2D, x: number, y: number, r: number, strokeStyle?: StrokeStyle) {
        ctx.beginPath()
        if (strokeStyle) { ctx.strokeStyle = rgb(strokeStyle) }
        ctx.arc(x - .5, y - .5, r, 0, 2 * Math.PI)
        ctx.stroke()
    }
    public static random(xmax: number, ymax: number, rmax?: number) {
        const x = Math.random() * xmax
        const y = Math.random() * ymax
        const r = Math.random() * Math.abs(rmax || (ymax / 2))
        return new Circle(r, x, y)
    }
    public static randomIn(r: Size) {
        return Circle.random(r.width, r.height, Math.min(r.width, r.height))
    }
    public static fromCxyr(c: TCircle) {
        return new Circle(c.r, c.x, c.y)
    }
    public static getBox(r: number, x: number, y: number) {
        const x1 = x - r, x2 = x + r, y1 = y - r, y2 = y + r
        return {
            xmin: x1 < x2 ? x1 : x2,
            xmax: x1 > x2 ? x1 : x2,
            ymin: y1 < y2 ? y1 : y2,
            ymax: y1 > y2 ? y1 : y2
        }
    }

    constructor(
        public r: number = 1,
        public x: number = 0,
        public y: number = 0
    ) { }

    public draw(ctx: CanvasRenderingContext2D, strokeStyle?: StrokeStyle) {
        Circle.draw(ctx, this.x, this.y, this.r, strokeStyle)
    }

    public contains(px: number, py: number): boolean {
        const dx = px - this.x, dy = py - this.y, r = this.r
        return dx * dx + dy * dy <= r * r
    }

    public containsPoint(p: Point): boolean {
        return this.contains(p.x, p.y)
    }

    public getBox() {
        return Circle.getBox(this.r, this.x, this.y)
    }
}