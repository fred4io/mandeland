
export declare type TNumberArray = Uint8Array | Uint16Array | Uint32Array | Float32Array | Array<number>

export interface INumberArrayInfo {
    min?: number
    max?: number
    nans?: number
}
export class NumberArrayInfo {
    constructor(
        public min?: number,
        public max?: number,
        public nans?: number
    ) { }
}
export interface ICheckNaN {
    throwOnNaN: boolean
    replaceNanBy: number,
    warnOnNaN: boolean
}

export class NumberArrays {

    /** returns result containing values in range [0.0, 1.0] */
    static normalize(data: TNumberArray,
        result = new Float32Array(data.length),
        min?: number, max?: number, checkNaN?: ICheckNaN,
        info?: INumberArrayInfo
    ) {
        if (min == undefined || max == undefined) {
            const mm = NumberArrays.getMinMax(data, info)
            min = mm.min, max == mm.max
        }

        if (checkNaN)
            NumberArrays.checkNaN(data,
                checkNaN.throwOnNaN,
                checkNaN.replaceNanBy,
                checkNaN.warnOnNaN,
                info)

        const h = (max - min) || 1

        for (let i = 0, l = data.length; i < l; i++) {
            result[i] = (data[i] - min) / h
        }

        return result
    }

    static getMinMax(data: TNumberArray, info: INumberArrayInfo = new NumberArrayInfo()
    ) {
        let min = Infinity, max = -Infinity
        for (let i = 0, l = data.length; i < l; i++) {
            const v = data[i]
            if (v < min) min = v
            if (v > max) max = v
        }
        info.min = min
        info.max = max
        return info
    }

    static checkNaN(data: TNumberArray,
        throwOnNaN = false, replaceNanBy = 0, warnOnNaN = true,
        info: INumberArrayInfo = new NumberArrayInfo()
    ) {
        let nans = 0
        for (let i = 0, l = data.length; i < l; i++) {
            let v = data[i]
            if (isNaN(v)) {
                nans++
                if (throwOnNaN) throw 'NaN at ' + i
                if (warnOnNaN) console.warn(v, i)
                data[i] = replaceNanBy
            }
        }
        info.nans = nans
        return info
    }


    static getDistinctValues(data: TNumberArray, sorted = false) {
        let r = Array.from(new Set(data).values())
        if (sorted) r = r.sort((a, b) => a - b)
        return r
    }

    static countDistinctValues(data: TNumberArray) {
        return NumberArrays.getDistinctValues(data).length
    }

    static makeEven<T extends TNumberArray>(data: T, dataWidth: number, divisor = 2,
        getNewArray: (newWidth: number, newHeight: number) => T
    ) {
        let w = dataWidth, w0 = w
        let h = data.length / w

        if (w % divisor == 0 && h % divisor == 0) return data

        //could really do better ^^
        while (w && w % divisor) w--
        while (h && h % divisor) h--

        const result = getNewArray(w, h)
        for (let j = 0; j < h; j++) {
            const jwr = j * w, jwd = j * w0
            for (let i = 0; i < w; i++)
                result[jwr + i] = data[jwd + i]
        }

        return result
    }

}
