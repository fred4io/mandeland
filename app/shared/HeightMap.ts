import { Region2D } from "./Region2D";
import { NumberArrays, TNumberArray, ICheckNaN, NumberArrayInfo, INumberArrayInfo } from "./NumberArrays";

export interface IHeightMapProducer {
    getHeightMap(flatten?: boolean, info?: { min: number }): HeightMap
    getHeightMapAsync(flatten?: boolean, info?: { min: number }): Promise<HeightMap>
}

declare type TData = TNumberArray

export class HeightMap {

    protected static say(log: boolean, ...args: any[]) {
        if(log) { console.log(HeightMap.name, ...args) }
    }

    static fromData(
        regionOrWidthOrSize: Region2D | number | { width: number, height: number },
        inputData: TData,
        min?: number,
        max?: number,
        dataWidth = typeof regionOrWidthOrSize == 'number' ? regionOrWidthOrSize : regionOrWidthOrSize.width,
        dataHolder?: Float32Array,
        log = false,
        checkNaN?: ICheckNaN,
        info: INumberArrayInfo = new NumberArrayInfo()
    ) {
        const region = regionOrWidthOrSize instanceof Region2D ? regionOrWidthOrSize : Region2D.fromScreen(regionOrWidthOrSize)
        const hmap = new HeightMap(
            region,
            inputData ?
                NumberArrays.normalize(inputData, dataHolder, min, max, checkNaN, info)
                : new Float32Array(dataWidth * region.height),
            dataWidth)

        HeightMap.say(log, 'fromData', {
            length: inputData.length, width: dataWidth, height: (inputData.length / dataWidth),
            min: info.min, max: info.max, nans: info.nans
        })

        return hmap
    }

    static fromRegion(regionOrWidthOrSize: Region2D | number | { width: number, height: number }, log = false) {
        const region = regionOrWidthOrSize instanceof Region2D ? regionOrWidthOrSize : Region2D.fromScreen(regionOrWidthOrSize)
        const dataWidth = Math.round(region.width),
              dataHeight = Math.round(region.height)
        const inputData = new Float32Array(dataWidth * dataHeight)
        HeightMap.say(log, 'fromRegion', {
            length: inputData.length, width: dataWidth, height: (inputData.length / dataWidth)
        })
        return new HeightMap(region, inputData, dataWidth)
    }

    private constructor(
        public region: Region2D,
        public data: Float32Array,
        public dataWidth: number
    ) {
    }

    makeEven(divisor: number, log = false) {
        HeightMap.say(log, 'makeEven', divisor, this.dataWidth, this.data.length, this.data.length / this.dataWidth)

        let w = this.dataWidth,
            h = this.data.length / this.dataWidth

        this.data = NumberArrays.makeEven(this.data, this.dataWidth, divisor,
            (nw: number, nh: number) => {
                w = nw
                h = nh
                return new Float32Array(nw * nh)
            })

        if (this.region.width == this.dataWidth) {
            this.region.width = w
            this.region.height = h
        }
        this.dataWidth = w

        HeightMap.say(log, 'makeEven', this.dataWidth, this.data.length, this.data.length / this.dataWidth)
        return this
    }

    clone() {
        return new HeightMap(
            this.region.clone(),
            this.data.slice(),
            this.dataWidth)
    }
    copy(hmap: HeightMap, byRef: boolean) {
        this.region = byRef ? hmap.region : hmap.region.clone()
        this.data = byRef ? hmap.data : hmap.data.slice()
        this.dataWidth = hmap.dataWidth
        return this
    }

    getMin() { return NumberArrays.getMinMax(this.data).min }

    flatten(info?: { min: number, nans?: number }, ckeckNaN?: ICheckNaN) {
        NumberArrays.normalize(this.data, this.data,
            undefined, undefined, ckeckNaN, info)
        return this
    }

    combine(newParts: HeightMap[], commonPart?: Region2D, log = false) {
        HeightMap.say(log, 'combine', newParts.length, !!commonPart)
        const isDezoom = commonPart && newParts && newParts.length == 4
        if (commonPart)
            if (isDezoom) this.dezoom(commonPart, log)
            else this.move(commonPart, log)
        newParts.forEach(part => this.copyPart(part.region, part.data, log))
        return this
    }

    private copyPart(sr: Region2D, sd: Float32Array, log = false) {
        const dd = this.data, dr = this.region, dw = dr.width
        let y0 = sr.ymin, x0 = sr.xmin, h = sr.height, w = sr.width
        //const xodd = sr.xmin % 2 !== 0
        HeightMap.say(log, 'copy part', x0, y0, w, h)
        for (let y = 0, d, s; y < h; y++) {
            d = dw * (y0 + y) + x0
            s = w * y
            for (let x = 0; x < w; x++) {
                dd[d + x] = sd[s + x]
            }
        }
    }

    private move(dr: Region2D, log = false) {
        const d = this.data, sr = this.region,
            txp = dr.xmin - sr.xmin, txn = dr.xmax - sr.xmax,
            typ = dr.ymin - sr.ymin, tyn = dr.ymax - sr.ymax
        HeightMap.say(log, 'move part', txp || txn, typ || tyn)
        const s = sr.width, h = dr.height, w = dr.width, t = 1,
            dx = txp ? -1 : 1, dy = typ ? -1 : 1,
            x0 = txp ? (sr.xmax - txp - t) : sr.xmin,
            y0 = typ ? (sr.ymax - typ - t) : sr.ymin
        for (let j = 0, y = y0, syw, dyw; j < h; j++ , y += dy) {
            syw = s * (y - tyn)
            dyw = s * (y + typ)
            for (let i = 0, x = x0; i < w; i++ , x += dx) {
                d[dyw + x + txp] = d[syw + x - txn]
            }
        }
    }

    private dezoom(dr: Region2D, log = false) {
        const dd = this.data, sd = dd.slice(),
            sr = this.region, sw = sr.width, sh = sr.height,
            dw = dr.width, dh = dr.height, dx0 = dr.xmin, dy0 = dr.ymin,
            ax = sw / dw,
            ay = sh / dh

        if(log) HeightMap.say(log, 'dezoom',
            (100 / ax).toFixed(1) + '%',
            (100 / ay).toFixed(1) + '%')

        for (let y = 0, sy: number, dywx0, syw; y < dh; y++) {
            sy = Math.round(y * ay)
            syw = sy * sw
            dywx0 = (dy0 + y) * sw + dx0
            for (let x = 0, sx: number; x < dw; x++)
                dd[dywx0 + x] = sd[syw + Math.round(x * ax)]
        }
    }

}
