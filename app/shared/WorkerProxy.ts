declare type TInput = any
declare type TResult = { progress: number, data?: any, error?: any }

export class WorkerProxy {
    public busy: boolean
    public input: TInput
    public inputId: string | number
    public result: TResult
    private worker: Worker
    private started: number
    public canceled: boolean
    private cancelRequired: boolean
    public extData: any

    constructor(
        blobUrl: string,
        private onCompleted: (worker: WorkerProxy) => void,
        private onProgressCancel: (worker: WorkerProxy) => boolean,
        public id: number | string = 'worker',
        public log = false
    ) {
        this.worker = new Worker(blobUrl)
    }
    protected say(...args: any[]) {
        if(this.log) { console.log(this.constructor.name, '(' + this.id + ')', ...args) }
    }
    protected sayMs(start: number, ...args: any[]) {
        if(this.log) { this.say(...args, (performance.now() - start).toFixed(3) + 'ms') }
    }

    terminate() {
        if (!this.busy) { return }
        this.worker.terminate()
        this.say('terminated')
    }

    cancel() {
        if (!this.busy || this.cancelRequired) { return }
        this.say('canceling')
        this.cancelRequired = true
    }

    start(input: TInput, inputId: number | string, extData: any) {
        if (this.busy) throw 'busy!'
        this.busy = true
        this.input = input
        this.inputId = inputId
        this.canceled = false
        this.cancelRequired = false
        this.extData = extData
        this.worker.onmessage = e => this.progress(e.data)
        // if (inputId) { this.say('starting', inputId) }
        // else { this.say('starting') }
        this.started = performance.now()
        this.worker.postMessage(input)
    }

    private progress(wr: TResult) {
        this.result = wr
        let canceled = this.cancelRequired
        if (!canceled && this.onProgressCancel && wr && wr.progress) {
            canceled = this.onProgressCancel(this)
        }
        if (canceled) {
            this.worker.postMessage('cancel')
            this.completed(true)
        } else if (wr && wr.data) {
            this.completed(false)
        } else if (!wr || wr.error) {
            this.completed(false, wr && wr.error)
        }
    }

    private completed(canceled: boolean, error?: any) {
        this.canceled = canceled
        if (this.log) {
            if (canceled) {
                this.say('canceled')
            } else if (error) {
                this.say('error', error)
            } else {
                this.sayMs(this.started, 'computed', this.inputId)
            }
        }
        this.onCompleted(this)
        this.busy = false
    }
}
