///<reference path="../../node_modules/reflect-metadata/reflect-metadata.d.ts"/>"
import "reflect-metadata"

declare type TPar<T> = Param<T> | ParamSet
declare type TGetNextVal<T> = (v: T, reverse?: boolean, alt?: boolean) => T
declare type TOnChanged<T> = (v?: T, payload?: any) => void

export class ParamSet {

    //TODO: add onchange parameter
    static skip() {
        return Reflect.metadata(ParamSet.skip, true)
    }

    static object() {
        return Reflect.metadata(ParamSet.wrapObjectMetaKey, true)
    }
    static number(params: INumParamArgs) {
        return NumParam.meta(params)
    }
    static enum<E extends object>(enumObj: E) {
        return ENumParam.meta(enumObj)
    }
    static boolean() {
        return BoolParam.meta()
    }
    static custom<T>(getNextValue: TGetNextVal<T>) {
        return CustomParam.meta(getNextValue)
    }


    static generate<TObj extends object>(obj: TObj,
        name?: string, greedy = false,
        logOnChanged = false, logWarnings = true
    ) {

        function getPropDescr<T>(obj: any, k: string, par?: TPar<T>) {
            return par ?
                par instanceof ParamSet ?
                    { get: () => par, set: () => { console.warn('readonly property') } }
                    : { get: () => par.get(), set: (v: T) => par.set(v) }
                : { get: () => obj[k] as T, set: (v: T) => obj[k] = v }
        }

        const proxy = new ParamSet(name, logWarnings)

        Object.keys(obj).forEach(k => {
            let par: TPar<any> = undefined
            let pd: PropertyDescriptor
            const noWrap = ParamSet._isSkip(obj, k)
            switch (typeof (obj as any)[k]) {
                case 'boolean':
                    if (!noWrap) {
                        if (CustomParam._isCustomParam<boolean>(obj, k))
                            par = CustomParam._generate<boolean>(obj, k, logOnChanged, proxy)
                        else if (greedy || BoolParam._isBoolParam(obj, k))
                            par = BoolParam._generate(obj, k, logOnChanged, proxy)
                    }
                    pd = getPropDescr<boolean>(obj, k, par as Param<boolean>)
                    break
                case 'number':
                    if (!noWrap) {
                        if (CustomParam._isCustomParam<number>(obj, k))
                            par = CustomParam._generate<number>(obj, k, logOnChanged, proxy)
                        else if (ENumParam._isENumParam(obj, k))
                            par = ENumParam._generate(obj, k, logOnChanged, proxy)
                        else if (greedy || NumParam._isNumParam(obj, k))
                            par = NumParam._generate(obj, k, logOnChanged, proxy)
                    }
                    pd = getPropDescr<number>(obj, k, par as Param<number>)
                    break
                case 'string': pd = getPropDescr<string>(obj, k)
                    break
                case 'object':
                    if (!noWrap) {
                        const ps = ParamSet.generate((obj as any)[k], k, greedy, logOnChanged, logWarnings)
                        par = ps
                        ps._parent = proxy
                        pd = getPropDescr<object>(obj, k, ps)
                    }
                    else pd = getPropDescr<object>(obj, k)
                    break
            }
            if (par) ParamSet.addParam(proxy, par)
            if (pd) Object.defineProperty(proxy, k, pd)
        })

        return proxy as ParamSet & TObj
    }


    private constructor(
        public _name: string,
        private _warn = false
    ) { }

    _getName(key?: string) {
        const path: string = this._parent ? this._parent._getName(this._name) : (this._name || '')
        return key ? (path ? (path + '.' + key) : key) : path
    }

    private _params = new Map<string, TPar<any>>()
    private _parent?: ParamSet

    get<T>(key: string) {
        const p = this.getParam<T>(key)
        return p ? p.get() : (this as any)[key] as T
    }
    set<T>(key: string, value: T, payload?: any, noAction = false) {
        const p = this.getParam<T>(key)
        if (p) return p.set(value, payload, noAction)
        const self = this as any
        self[key] = value
        return self[key] as T
    }
    reset<T>(key: string, payload?: any, noAction = false) {
        const p = this.getParam<T>(key)
        if (p) return p.reset(payload, noAction)
        if (this._warn) console.warn("not a parameter: " + key)
        return (this as any)[key] as T
    }
    toggle(key: string, force: boolean, payload?: any, noAction = false) {
        const p = this.getParam<boolean>(key)
        if (p) return p.set(force != undefined ? force : !p.get(), payload, noAction)
        const self = this as any
        self[key] = (force != undefined ? force : !self[key])
        return self[key] as boolean
    }
    next<T>(key: string, reverse?: boolean, alt?: boolean, payload?: any, noAction = false) {
        const p = this.getParam<T>(key)
        return p && p.next(reverse, alt, payload, noAction)
    }
    attach(key: string, action: (v?: any, payload?: any) => void) {
        const p = this.getParam(key)
        if (p) p.attach(action)
    }
    detach(key: string) {
        const p = this.getParam(key)
        if (p) p.detach()
    }

    private getParamSet(key: string) {
        const p = this._params.get(key)
        if (p && p instanceof ParamSet) return p
        if (this._warn) console.warn("not a ParamSet: " + key + ' ' + typeof p)
        return undefined
    }

    private getParam<T>(key: string) {
        let p: Param<T>
        if (key.indexOf('.') != -1) {
            let ps: ParamSet = this, keys = key.split('.'), l = keys.length - 1
            for (let i = 0; i <= l; i++) {
                if (i == 0 && keys[i] == this._name) continue
                if (!ps) break
                if (i == l) p = ps.getParam<T>(keys[i])
                else ps = ps.getParamSet(keys[i])
            }
        }
        else p = this._params.get(key) as Param<T>
        if (!p && this._warn) console.warn("parameter not found: " + key)
        return p
    }


    private static addParam<T>(ps: ParamSet, par: TPar<T>) {
        ps._params.set(par instanceof ParamSet ? par._name : par.name, par)
    }

    private static wrapObjectMetaKey = "ParamSet.isObject"
    private static _isObject(obj: any, key: string) {
        return Reflect.hasMetadata(ParamSet.wrapObjectMetaKey, obj, key)
    }

    private static noWrapMetaKey = "ParamSet.skip"
    private static _isSkip(obj: any, key: string) {
        return Reflect.hasMetadata(ParamSet.skip, obj, key)
    }
}

abstract class Param<T> {

    private initialValue: T
    private onChanged: TOnChanged<T>
    private _value: T

    private get value() {
        return this.target ? this.target[this.name] : this._value
    }
    private set value(v: T) {
        if (this.target) this.target[this.name] = v
        else this._value = v
    }

    constructor(
        public target: any,
        public name: string,
        value: T,
        public logOnChange = false,
        protected parent: ParamSet
    ) {
        if (target) {
            if (value == undefined) value = target[name]
            else this.value = value
            this.initialValue = value
        }
        else this.initialValue = this.value = value
    }

    get() {
        return this.value
    }
    set(value: T, payload?: any, noAction = false) {
        const oldValue = this.value
        const newValue = this.value = this._set(value)
        if (newValue != oldValue) {
            if (this.logOnChange)
                console.log(
                    (this.parent ? this.parent._getName(this.name) : this.name)
                    + ': ' + oldValue + '->' + newValue)
            if (!noAction && this.onChanged)
                try { this.onChanged(newValue, payload) }
                catch (e) { console.warn('attached action threw an error: ' + e) }
        }
        return newValue
    }
    reset(payload?: any, noAction = false) {
        return this.set(this.initialValue, payload, noAction)
    }
    next(reverse?: boolean, alt?: boolean, payload?: any, noAction = false) {
        return this.set(this._next(this.value, reverse, alt), payload, noAction)
    }

    attach(onChanged: TOnChanged<T>) {
        this.onChanged = onChanged
    }
    detach() {
        this.onChanged = null
    }

    toString(prefix = '', sep = ':') {
        return prefix + this.name + sep + this._valueAsString(this.value)
    }


    protected abstract _next(v: T, reverse?: boolean, alt?: boolean): T

    protected _set(v: T) {
        return v
    }
    protected _valueAsString(v: T) {
        return v == undefined ? '' : v.toString()
    }
}

export interface INumParamArgs {
    incStep?: number
    incFactor?: number
    altIncFactor?: number
    min?: number
    max?: number
    isCyclic?: boolean
    isInteger?: boolean
    excludeZero?: boolean
    timesStep?: boolean
    altTimesStep?: boolean
    divStep?: boolean
    altDivStep?: boolean
}

class NumParam extends Param<number> {

    static _generate(obj: any, k: string, logOnChange: boolean, parent: ParamSet) {
        return new NumParam(obj, k, undefined,
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.incStep),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.incFactor),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.altIncFactor),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.min),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.max),
            NumParam.getMeta<boolean>(obj, k, NumParam.metaArgs.isCyclic),
            NumParam.getMeta<boolean>(obj, k, NumParam.metaArgs.isInteger),
            NumParam.getMeta<boolean>(obj, k, NumParam.metaArgs.excludeZero),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.timesStep),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.altTimesStep),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.divStep),
            NumParam.getMeta<number>(obj, k, NumParam.metaArgs.altDivStep),
            logOnChange, parent
        )
    }

    static meta(args: INumParamArgs) {
        return args && Reflect.metadata(NumParam.metaKey, args)
    }

    static _isNumParam(target: any, propertyKey: string) {
        return Reflect.hasMetadata(NumParam.metaKey, target, propertyKey)
    }

    private static metaKey = 'NumParam.MetaArgs'

    private static metaArgs = {
        min: 0,
        max: 1,
        incStep: 2,
        incFactor: 3,
        altIncFactor: 4,
        isCyclic: 5,
        isInteger: 6,
        excludeZero: 7,
        timesStep: 8,
        altTimesStep: 9,
        divStep: 10,
        altDivStep: 11,
    }

    private static getMeta<T>(target: any, propertyKey: string, metaKeyId: number) {
        const metaObj = Reflect.getMetadata(NumParam.metaKey, target, propertyKey) as any
        const metaKey = metaObj && Object.keys(NumParam.metaArgs)[metaKeyId]
        return metaObj && metaObj[metaKey] as T
    }

    constructor(
        target: any,
        name: string,
        value?: number,
        public incStep = 1,
        public incFactor = 1,
        public altIncFactor = incFactor,
        public min: number = -Infinity,
        public max: number = Infinity,
        public isCyclic = false,
        public isInteger = false,
        public excludeZero = false,
        public timesStep = 1,
        public altTimesStep = timesStep,
        public divStep = timesStep,
        public altDivStep = divStep,
        logOnChange = false,
        parent?: ParamSet
    ) {
        super(target, name, value, logOnChange, parent)
        if (this.isCyclic) {
            if (this.max > 0 && this.min == -Infinity) this.min = 0
        }
    }
    private minMax(v: number) {
        if (this.min != undefined && v < this.min) v = this.isCyclic ? this.max : this.min
        if (this.max != undefined && v > this.max) v = this.isCyclic ? this.min : this.max
        if (this.isInteger && this.excludeZero) v = Math.abs(v) < 1 ? v < 0 ? -1 : 1 : v
        return v
    }
    protected _next(v: number, reverse: boolean, alt: boolean) {
        if (reverse) {
            v = v / (alt ? this.altDivStep : this.divStep)
                - this.incStep * (alt ? this.altIncFactor : this.incFactor)
            v = this.minMax(v)
            if (this.isInteger) v = Math.floor(v)
        }
        else {
            v = v * (alt ? this.altTimesStep : this.timesStep)
                + this.incStep * (alt ? this.altIncFactor : this.incFactor)
            v = this.minMax(v)
            if (this.isInteger) v = Math.ceil(v)
        }
        return v
    }
    protected _set(v: number) {
        return this.isInteger ? Math.round(v) : v
    }
}

class ENumParam<E extends object, T extends keyof E | number>
    extends NumParam {

    static _generate(obj: any, k: string,
        logOnChange = false,
        parent?: ParamSet
    ) {
        const enumObj = Reflect.getMetadata(ENumParam.enumParamMetaKey, obj, k)
        return new ENumParam(obj, k, enumObj, undefined, logOnChange, parent)
    }

    private static enumParamMetaKey = Symbol("ENumParam")

    static meta<E extends object>(
        enumObj: E
    ) {
        return Reflect.metadata(ENumParam.enumParamMetaKey, enumObj);
    }

    static _isENumParam(target: any, propertyKey: string) {
        return Reflect.hasMetadata(ENumParam.enumParamMetaKey, target, propertyKey)
    }

    constructor(
        target: any,
        name: string,
        private enumObj: E,
        value?: T,
        logOnChange = false,
        parent?: ParamSet
    ) {
        super(target, name, <number>value || 0)
        super.min = 0
        super.max = ENumParam.getLast(enumObj)
        super.isCyclic = super.isInteger = true
        super.logOnChange = logOnChange
        super.parent = parent
    }

    private static getLast(enumObj: object) {
        const values = ENumParam.getValues(enumObj)
        return values[values.length - 1]
    }
    private static getNames(E: object) {
        return Object.keys(E).filter(k => typeof (E as any)[k] === "number")
    }
    private static getValues(E: object) {
        return ENumParam.getNames(E).map(k => (E as any)[k]) as number[]
    }

    protected _valueAsString(v: keyof E | number) {
        return '' + this.enumObj[v as keyof E]
    }
}


class BoolParam extends Param<boolean> {

    static _generate(obj: any, k: string, logOnChange: boolean, parent: ParamSet) {
        return new BoolParam(obj, k, undefined, logOnChange, parent)
    }

    private static metaKey = 'BoolParam'

    static meta() {
        return Reflect.metadata(BoolParam.metaKey, true)
    }

    static _isBoolParam(target: any, propertyKey: string) {
        return Reflect.hasMetadata(BoolParam.metaKey, target, propertyKey)
    }

    private constructor(
        target: any,
        name: string,
        value?: boolean,
        logOnChange = false,
        parent?: ParamSet
    ) {
        super(target, name, value, logOnChange, parent)
    }

    protected _next(v: boolean) { return !v }
    protected _set(v: boolean) { return !!v }
}

class CustomParam<T> extends Param<T>{

    private static metaKey = "CustomParam"

    static meta<T>(getNextValue: TGetNextVal<T>) {
        return Reflect.metadata(CustomParam.metaKey, getNextValue)
    }

    static _isCustomParam<T>(obj: any, k: string) {
        return Reflect.hasMetadata(CustomParam.metaKey, obj, k)
    }

    static _generate<T>(obj: any, k: string, logOnChange: boolean, parent: ParamSet) {
        const getNextVal: TGetNextVal<T> = Reflect.getMetadata(CustomParam.metaKey, obj, k)
        return new CustomParam<T>(obj, k, undefined, getNextVal, logOnChange, parent)
    }

    private constructor(
        public target: any,
        public name: string,
        value: T,
        private nextValue: TGetNextVal<T>,
        logOnChange = false,
        parent?: ParamSet
    ) {
        super(target, name, value, logOnChange, parent)
    }

    protected _next(v: T, reverse?: boolean, alt?: boolean): T {
        return this.nextValue(v, alt)
    }
}

