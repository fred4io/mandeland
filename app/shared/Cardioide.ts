declare type Point = { x: number, y: number }
declare type TCircle = { x: number, y: number, r: number }
declare type Size = { width: number, height: number }
declare type StrokeStyle = string | { r: number, g: number, b: number }
function rgb(c: StrokeStyle) { return typeof c == 'string' ? c : `rgb(${c.r},${c.g},${c.b})` }

export class Cardioide {

    public static getXYs(cx: number, cy: number, r: number, thetaStart: number, thetaEnd: number, steps: number) {
        const l = steps * 2, xys = new Array<number>(l || 0);
        if (!steps) { return xys; }
        const thetaStep = (thetaEnd - thetaStart) / steps;
        for (let i = 0, theta = thetaStart; i <= l; theta += thetaStep, i += 2) {
            const t = Math.tan(theta / 2), t2 = t * t, d = 1 + t2, d2 = d * d;
            const x = 2 * r * ((1 - t2) / d2), y = (4 * r * t) / d2;
            xys[i] = cx + x;
            xys[i + 1] = cy + y;
        }
        return xys;
    }
    public static getBox(x: number, y: number, r: number) {
        const ar = Math.abs(r), hh = r * Cardioide._hp6;
        return r > 0 ? { xmin: x - ar / 4, ymin: y - hh, xmax: x + ar * 2, ymax: y + hh }
            : { xmin: x - ar * 2, ymin: y + hh, xmax: x + ar / 4, ymax: y - hh }
    }
    public static getPointAtAngle(x: number, y: number, r: number, angle: number) {
        const t = Math.tan(angle / 2), t2 = t * t, d = 1 + t2, d2 = d * d;
        const dx = 2 * r * ((1 - t2) / d2), dy = (4 * r * t) / d2;
        return { x: x + dx, y: y + dy }
    }
    public static getAngleRayTo(x: number, y: number, r: number, px: number, py: number) {
        const vx = px - x, vy = py - y
        let a = Math.acos(vx / Math.sqrt(vx * vx + vy * vy))
        return (py < y ? -a : a) + (r < 0 ? Math.PI : 0)
    }
    public static getPointIntersectRayTo(x: number, y: number, r: number, px: number, py: number) {
        return this.getPointAtAngle(x, y, r, this.getAngleRayTo(x, y, r, px, py));
    }
    public static draw(ctx: CanvasRenderingContext2D, x: number, y: number, r: number,
        segments: number = 100, strokeStyle?: StrokeStyle
    ) {
        const twoPI = 2 * Math.PI;
        const xys = this.getXYs(x, y, r, 0, twoPI, segments);
        const l = xys.length;
        ctx.beginPath();
        if (strokeStyle) { ctx.strokeStyle = rgb(strokeStyle); }
        ctx.moveTo(xys[0] - .5, xys[1] - .5);
        for (let i = 2; i < l; i += 2) { ctx.lineTo(xys[i] - .5, xys[i + 1] - .5); }
        ctx.stroke();
    }
    public static drawNth(ctx: CanvasRenderingContext2D, cx: number, cy: number, r: number, nth: number, n: number,
        strokeStyle?: StrokeStyle
    ) {
        const t = Math.tan(Math.PI / nth / 2), t2 = t * t, d = 1 + t2, d2 = d * d;
        const x = 2 * r * ((1 - t2) / d2), y = (4 * r * t) / d2;
        ctx.beginPath();
        if (strokeStyle) { ctx.strokeStyle = rgb(strokeStyle); }
        ctx.moveTo(cx - .5, cy - .5);
        ctx.lineTo(cx + x - .5, cy + y - .5);
        ctx.stroke();
    }
    public static isIn(px: number, py: number, cx: number, cy: number, r: number) {
        const vx = px - cx, vy = py - cy, lcp = Math.sqrt(vx * vx + vy * vy);
        let a = Math.acos(vx / lcp); if (r < 0) { a += Math.PI; } if (py < cy) { a = - a }
        const t = Math.tan(a / 2), t2 = t * t, d = 1 + t2, d2 = d * d, dr = 2 * r;
        const ux = dr * ((1 - t2) / d2), uy = (2 * dr * t) / d2;
        return lcp < Math.sqrt(ux * ux + uy * uy);
    }

    public static random(xmax: number, ymax: number, rmax?: number) {
        const x = Math.random() * xmax;
        const y = Math.random() * ymax;
        const ar = Math.random() * (rmax == undefined ? (ymax / 2) : rmax);
        const r = Math.random() > .5 ? ar : -ar;
        return new Cardioide(r, x, y);
    }
    public static randomIn(size: Size) {
        const cx = size.width / 2, cy = size.height / 2;
        const x = cx - (Math.random() * cx / 2);
        const y = cy - (Math.random() * cy / 2);
        const ar = Math.random() * Math.min(size.width, size.height) / 4;
        const r = Math.random() > .5 ? ar : -ar;
        return new Cardioide(r, x, y);
    }

    private static _hp6 = (() => { const t = Math.tan(Math.PI / 6), d = 1 + t * t; return 4 * t / (d * d) })();

    constructor(
        public r: number = 1,
        public x: number = 0,
        public y: number = 0
    ) { }

    public draw(ctx: CanvasRenderingContext2D, segments: number = 100, strokeStyle?: StrokeStyle) {
        Cardioide.draw(ctx, this.x, this.y, this.r, segments, strokeStyle);
    }
    public getCircleIn() {
        return { x: this.x + this.r, y: this.y, r: Math.abs(this.r) }
    }
    public getBox() {
        return Cardioide.getBox(this.x, this.y, this.r);
    }
    public getHeight() {
        return 2 * Math.abs(this.r) * Cardioide._hp6;
    }
    public getWidth() {
        return 2.25 * Math.abs(this.r);
    }

    public getPointAtAngle(angle: number) {
        return Cardioide.getPointAtAngle(this.x, this.y, this.r, angle);
    }
    public getAngleRayTo(px: number, py: number) {
        return Cardioide.getAngleRayTo(this.x, this.y, this.r, px, py);
    }
    public getIntersectRayTo(p: Point) {
        return Cardioide.getPointIntersectRayTo(this.x, this.y, this.r, p.x, p.y);
    }

    public containsPoint(p: Point): boolean {
        return this.contains(p.x, p.y);
    }

    public contains(px: number, py: number): boolean {

        const r = this.r, ar = r < 0 ? -r : r;

        const x = this.x;
        if (px < (r > 0 ? (x - ar / 4) : (x - ar * 2)) || (r > 0 ? (x + ar * 2) : (x + ar / 4)) < px) {
            return false;
        }

        const y = this.y, hh = ar * Cardioide._hp6;
        if (py < (y - hh) || (y + hh) < py) {
            return false;
        }

        const vx = px - x, vy = py - y, vy2 = vy * vy;

        const dx = vx - r;
        if (dx * dx + vy2 <= r * r) {
            return true;
        }

        const lcp = Math.sqrt(vx * vx + vy2);
        let a = Math.acos(vx / lcp); if (r < 0) { a += Math.PI; } if (py < y) { a = - a }
        const t = Math.tan(a / 2), t2 = t * t, d = 1 + t2, d2 = d * d,
            ux = 2 * r * ((1 - t2) / d2), uy = (4 * r * t) / d2;

        return lcp < Math.sqrt(ux * ux + uy * uy);
    }

}