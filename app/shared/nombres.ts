//nombres

export function fractionEntiere(n: number) {
    const an = Math.abs(n);
    if (an == Math.trunc(an)) { return '' + n; }
    const sign = n / an, eps = Math.pow(10, -9);
    for (let i = 1, x: number, y: number, ex: number, ey: number; i < 10000000; i++) {
        x = i * an, ex = Math.trunc(x);
        if (x - ex < eps) { return (sign * ex) + '/' + i; }
        y = i / an, ey = Math.trunc(y);
        if (y - ey < eps) { return (sign * i) + '/' + ey; }
    }
    return '' + n;
}

export function pgcd(a: number, b: number) {
    a = Math.trunc(a), b = Math.trunc(b);
    let t; while (b != 0) { t = a; a = b; b = t % b; }
    if (a == 0) { throw 'pgcd zero'; }
    return a;
}

export function simplifieFraction(x: number, y: number) {
    const p = pgcd(x, y);
    return (x / p) + '/' + (y / p)
}
export function simplifieFractionObj(o: { x: number, y: number }, toString = false) {
    if (toString) { return simplifieFraction(o.x, o.y) }
    const p = pgcd(o.x, o.y);
    return { x: o.x / p, y: o.y / p }
}
