import lxml.html
import re
import sys
import textwrap

def concat_html_file(root, src_file_name, dst_file_name, remove_source_mapping):

    def read_text(path):
        with open(path, 'rt') as f:
            return f.read()

    def write_html(html_bytes, path):
        with open(path, 'wb') as f:
            f.write(html_bytes)

    def replace(parent, child, el_or_string):
        if isinstance(el_or_string, str): el_or_string = lxml.html.fragment_fromstring(el_or_string)
        parent.insert(parent.index(child)+1, el_or_string)
        parent.remove(child)
        return el_or_string

    def remove_lines_beginning_with(text, start):
        lines = []
        for line in text.splitlines():
            if not line.startswith(start):
                lines.append(line)
        return '\n'.join(lines)

    re_relative_file = re.compile(r'^((?!((http|https|file)\:\/\/)).)*$')

    doc = lxml.html.parse(root + src_file_name)
    for part in [doc.find('head'), doc.find('body')]:
        for el in part:
            if el.tag == 'link':
                href = el.get('href')
                if el.get('type') == 'text/css' and href is not None:
                    css_text = read_text(root + href)
                    #css_text = textwrap.indent(css_text, 2 * '\t')
                    css_text = css_text.replace('     ', '').replace('\n', ' ')
                    replace(part, el, '<style type="text/css">\n' + css_text + '\n</style>')
            elif el.tag == 'script':
                src = el.get('src')
                if src is not None and re_relative_file.match(src):
                    script = lxml.html.fragment_fromstring('<script>')
                    type = el.get('type')
                    if type is not None: script.set('type', type)
                    script_text = read_text(root + src)
                    if remove_source_mapping: script_text = remove_lines_beginning_with(script_text, '//# sourceMappingURL=')
                    script.text = script_text 
                    replace(part, el, script)
            
    write_html(lxml.html.tostring(doc, True), root + dst_file_name)


#concat_html_file('./app/client/html/', 'index-local.html', 'fract.html', True)
root = sys.argv[1]
src_file_name = sys.argv[2]
dst_file_name = sys.argv[3]
concat_html_file(root, src_file_name, dst_file_name, True)

        