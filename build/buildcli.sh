#!/bin/bash
# tsc \
# -t es6 \
# -m system \
# --watch \
# --noImplicitAny \
# --experimentalDecorators \
# --emitDecoratorMetadata \
# --inlineSourceMap \
# --listEmittedFiles \
# --outfile ../app/client/html/js/bundle.js \
# ../app/client/Fract.ts
# #--inlineSources \
# #--allowJs \
npm run build