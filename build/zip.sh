#!/bin/bash
zip -r fract.zip ../ -x *node_modules* *.DS_Store* *.git*
mv fract.zip ~/Downloads/fract/fract-`date +%Y%m%d%H%M%S`.zip
ls ~/Downloads/fract/fract-*.zip