#!/bin/bash

echo build...
#tsc -t es5 -m system --experimentalDecorators --emitDecoratorMetadata --listEmittedFiles --outfile ../app/client/html/js/bundle-es5.js ../app/client/Fract.ts
tsc -t es6 -m system --experimentalDecorators --emitDecoratorMetadata --listEmittedFiles --outfile ../app/client/html/js/bundle-es6.js ../app/client/Fract.ts

#echo minify...
#uglifyjs ../app/client/html/js/bundle-es5.js -o ../app/client/html/js/bundle.min.js
mv ../app/client/html/js/bundle-es6.js ../app/client/html/js/bundle.min.js

echo concat...
/usr/local/Cellar/python3/3.6.0/bin/python3.6 /Users/Fred/Code/my/fract/build/concat_html.py /Users/Fred/Code/my/fract/app/client/html/ index-local-min.html mandeland.html
