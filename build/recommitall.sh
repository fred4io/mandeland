#!/bin/bash
echo nope
exit 0

yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit $2; }
try() { "$@" || die "cannot $*"; }

cd ~/Code/my/repos/git/fract-arch
r=~/Downloads/fract
if [ ! -d "$r" ] 
then
	echo directory not found: $r
	exit 1
fi
for f in $r/fract-*.zip; do
	rm -R -- */
	rm -R .vscode/
	rm *
	unzip -o -q $f 
	git add .
	id=$(basename $f | cut -c7-20)
	git commit -m"$id"
	mv $f $r/done
done