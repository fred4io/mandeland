20180204
    2D: progressbar automaticaly shown when computing > 2/3s
    2D: computeByParts fixed, worker and optimmize now enabled by default
    2D: properties [optimize, parts, progress, worker] moved from FractRenderPar to Fract2DState
    3D: default mode = Mesh, default lod = 4; L key now increases LOD
    global: help/sections
    global: #dev in url activates log and keys/functions [log, optimize, parts, progress, worker]
    3D: added key/functions colors, invert, easing
20180205
    3D: fixed: scene helpers state is now kept between setups
20180212
    2D: edges and zones: added single zone detection/colorization
20180213
    2D: new presets 20-22, optimized zone detection
20180214
    global: fixed: initial processing is not canceled anymore
20180218
    completed contour detection
20180219
    optimized contour detection
20180220
    HeighMapZones: fixed lod
    added zone triangulation (buggy)
    2D: new zone-test
20180223
    optimized contour detection
    HeighMapZones: fixed lod (contour)
    fixed (quite) zone triangulation
20180224
    optimized zone triangulation: added steiner points
20180225
    triangulation: removed Delaunay lib
    optimized triangulation: interior steiner points clipping
    added zones seams computation
    zone-test: added AllLevels mode, small zones info, seams drawing
20180226
    added noise zones detection (ie groups of small zones)
    fixed duplicate points in zone detection
    zone-test: added checkduplicate
20180302
    3D: added special keys for not drawing optimized mesh parts: (ctrl-) Z, N, S
20180303
    fixed 2D pan and 3D lod
20180305
    removed seam between max level zone and noise zone
20180308
    3D: added points, vertices and faces counts
20180309
    3D: fixed duplicate faces in seams, normal vectors direction
    3D: added face tracker
20180312
    3D: replaced basic material (no light interaction) with standard material
20180317
    3D: added viewer params persistance
20180326
    Easing: now with Tweenjs
    zone-test: added neighborzones
20181111
    index-local: refactor module registering
20190615
    parallel processing; refactored log
20201205
    adapt to typescript 4
    parallel processing using gpu
TODO
    2D: fix partial dezoom
    MouseEvents: don't use layer
    2D: fix screenfull
    random tour
    add info panels click
    3D: upgrade three from Minuit
    3D: seams: remove colinear faces
    3D: fix normals: align seams normals with zones
    3D: round noise zones corners
    3D: fix color at max level
    2D: gradient composer
    2D: optimize zooming (compute inter-points)
    2D: selection: divs instead of pixels
    2D: isMaskVisible(): compute(this.viewport.region, this.state.a, this.state.b, this.state.z)